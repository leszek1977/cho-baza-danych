﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSQL
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSQL))
        Me.dgvLista = New System.Windows.Forms.DataGridView
        Me.txtSQL = New System.Windows.Forms.TextBox
        Me.btnWykonaj = New System.Windows.Forms.Button
        Me.dgvWyniki = New System.Windows.Forms.DataGridView
        Me.btnXls = New System.Windows.Forms.Button
        Me.btnNew = New System.Windows.Forms.Button
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.gbBtn = New System.Windows.Forms.GroupBox
        CType(Me.dgvLista, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvWyniki, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.gbBtn.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgvLista
        '
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.dgvLista.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvLista.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvLista.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvLista.BackgroundColor = System.Drawing.SystemColors.Control
        Me.dgvLista.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvLista.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLista.ColumnHeadersVisible = False
        Me.dgvLista.Location = New System.Drawing.Point(3, 3)
        Me.dgvLista.Name = "dgvLista"
        Me.dgvLista.RowHeadersVisible = False
        Me.dgvLista.RowTemplate.Height = 24
        Me.dgvLista.Size = New System.Drawing.Size(397, 156)
        Me.dgvLista.TabIndex = 0
        '
        'txtSQL
        '
        Me.txtSQL.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSQL.Location = New System.Drawing.Point(406, 3)
        Me.txtSQL.Multiline = True
        Me.txtSQL.Name = "txtSQL"
        Me.txtSQL.Size = New System.Drawing.Size(344, 156)
        Me.txtSQL.TabIndex = 1
        '
        'btnWykonaj
        '
        Me.btnWykonaj.Location = New System.Drawing.Point(119, 18)
        Me.btnWykonaj.Name = "btnWykonaj"
        Me.btnWykonaj.Size = New System.Drawing.Size(94, 36)
        Me.btnWykonaj.TabIndex = 2
        Me.btnWykonaj.Text = "&Wykonaj"
        Me.btnWykonaj.UseVisualStyleBackColor = True
        '
        'dgvWyniki
        '
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.dgvWyniki.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvWyniki.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvWyniki.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvWyniki.BackgroundColor = System.Drawing.SystemColors.Control
        Me.dgvWyniki.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvWyniki.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.TableLayoutPanel1.SetColumnSpan(Me.dgvWyniki, 2)
        Me.dgvWyniki.Location = New System.Drawing.Point(3, 235)
        Me.dgvWyniki.Name = "dgvWyniki"
        Me.dgvWyniki.RowHeadersVisible = False
        Me.dgvWyniki.RowTemplate.Height = 24
        Me.dgvWyniki.Size = New System.Drawing.Size(747, 264)
        Me.dgvWyniki.TabIndex = 3
        '
        'btnXls
        '
        Me.btnXls.Location = New System.Drawing.Point(228, 18)
        Me.btnXls.Name = "btnXls"
        Me.btnXls.Size = New System.Drawing.Size(94, 36)
        Me.btnXls.TabIndex = 4
        Me.btnXls.Text = "Do &Excela"
        Me.btnXls.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Location = New System.Drawing.Point(6, 18)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.Size = New System.Drawing.Size(94, 36)
        Me.btnNew.TabIndex = 5
        Me.btnNew.Text = "&Nowe"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 350.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.gbBtn, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.dgvWyniki, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.txtSQL, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.dgvLista, 0, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(12, 12)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 3
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 37.5!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 70.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 62.5!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(753, 502)
        Me.TableLayoutPanel1.TabIndex = 6
        '
        'gbBtn
        '
        Me.gbBtn.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbBtn.Controls.Add(Me.btnNew)
        Me.gbBtn.Controls.Add(Me.btnXls)
        Me.gbBtn.Controls.Add(Me.btnWykonaj)
        Me.gbBtn.Location = New System.Drawing.Point(419, 165)
        Me.gbBtn.Name = "gbBtn"
        Me.gbBtn.Size = New System.Drawing.Size(331, 64)
        Me.gbBtn.TabIndex = 0
        Me.gbBtn.TabStop = False
        '
        'frmSQL
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 18.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(777, 526)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimumSize = New System.Drawing.Size(785, 559)
        Me.Name = "frmSQL"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Zapytania dotyczące "
        CType(Me.dgvLista, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvWyniki, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.gbBtn.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dgvLista As System.Windows.Forms.DataGridView
    Friend WithEvents txtSQL As System.Windows.Forms.TextBox
    Friend WithEvents btnWykonaj As System.Windows.Forms.Button
    Friend WithEvents dgvWyniki As System.Windows.Forms.DataGridView
    Friend WithEvents btnXls As System.Windows.Forms.Button
    Friend WithEvents btnNew As System.Windows.Forms.Button
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents gbBtn As System.Windows.Forms.GroupBox
End Class
