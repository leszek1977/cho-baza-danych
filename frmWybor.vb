﻿
Imports System.Data
Imports System.Data.OleDb

Public Class frmWybor

    Private m_Ok As Boolean = False
    Private m_SQL As String = ""
    Private m_Wybor(5) As String

    Public ReadOnly Property Akceptowane() As Boolean
        Get
            Return m_Ok
        End Get
    End Property

    Public ReadOnly Property Zakres() As Array
        Get
            Return m_Wybor
        End Get
    End Property

    Public ReadOnly Property Zapytanie() As String
        Get
            Return m_SQL
        End Get
    End Property

    Private Sub Me_Load() Handles Me.Load
        Dim da As New OleDbDataAdapter
        Dim dt As New DataTable
        With Me
            .dtpOd.Value = DateSerial(frmMain.dtpOkres.Value.Year, 1, 1)
            .dtpDo.Value = frmMain.dtpOkres.Value
            ' generowanie list:
            da = New OleDbDataAdapter("SELECT ID, ID + ' - ' + GrMaterEN AS [Opis] FROM tblGrupyMat WHERE Wydzial IN ('PS', 'MS', 'MT') ORDER BY ID", frmMain.cns)
            da.Fill(dt)
            With .cmbGrMat
                .DataSource = dt.DefaultView
                .DisplayMember = "Opis"
                .ValueMember = "ID"
                .Text = ""
            End With
            da = New OleDbDataAdapter("SELECT ID, ID + ' - ' + GrKalkEN AS [Opis] FROM tblGrupyKalk WHERE Wydzial IN ('PS', 'MS', 'MT') ORDER BY ID", frmMain.cns)
            dt = New DataTable
            da.Fill(dt)
            With .cmbGrKalk
                .DataSource = dt.DefaultView
                .DisplayMember = "Opis"
                .ValueMember = "ID"
                .Text = ""
            End With
            da = New OleDbDataAdapter("SELECT ID, ID + ' - ' + MaterialEN AS [Opis] FROM tblMaterialy WHERE LEFT(ID,2) = 'JF' ORDER BY ID", frmMain.cns)
            dt = New DataTable
            da.Fill(dt)
            With .cmbMaterial
                .DataSource = dt.DefaultView
                .DisplayMember = "Opis"
                .ValueMember = "ID"
                .Text = ""
            End With
            da = New OleDbDataAdapter("SELECT ID, Nazwa FROM tblKlienci ORDER BY Nazwa", frmMain.cns)
            dt = New DataTable
            da.Fill(dt)
            With .cmbKlient
                .DataSource = dt.DefaultView
                .DisplayMember = "Nazwa"
                .ValueMember = "ID"
                .Text = ""
            End With
            .chkDate.Checked = True
        End With
        With My.Settings
            Me.Size = .scrWybor
            Me.Location = .StartOfWybor
        End With
    End Sub

    Private Sub btnOk_Click() Handles btnOk.Click

        Dim v As DataRowView
        m_Ok = True
        With Me
            If .chkMiesieczne.Checked Then
                m_SQL = "SELECT GrMater, RK, SUM(IloscRzecz), SUM(WartoscRzecz), SUM(TnProdukcjiReal), SUM(IloscPlan), SUM(WartoscPlan), SUM(TnProdukcjiPlan), SUM(WsadReal), SUM(WsadPlan), Mc " _
                                              + "FROM tblZWKZlecenia "
            Else
                m_SQL = "SELECT MPK, RK, SUM(IloscRzecz), SUM(WartoscRzecz), SUM(TnProdukcjiReal), SUM(IloscPlan), SUM(WartoscPlan), SUM(TnProdukcjiPlan), SUM(WsadReal), SUM(WsadPlan) " _
                                              + "FROM tblZWKZlecenia "
            End If
            If .chkDate.Checked Then
                If .dtpDo.Value.Month < .dtpOd.Value.Month Then
                    MessageBox.Show("Błędnie wybrany okres", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Exit Sub
                End If
                m_SQL += "WHERE Mc >= " & .dtpOd.Value.Month & " AND Mc <= " & .dtpDo.Value.Month
                m_Wybor(0) = .dtpOd.Value.Month & " ÷ " & .dtpDo.Value.Month & " " & .dtpDo.Value.Year
            End If
            If .chkGM.Checked Then
                If Not m_SQL.Contains("WHERE") Then
                    m_SQL += "WHERE"
                Else
                    m_SQL += " AND"
                End If
                v = CType(.cmbGrMat.SelectedItem, DataRowView)
                m_SQL += " GrMater = '" + v("ID").ToString + "'"
                m_Wybor(1) = .Text
            End If
            If .chkGK.Checked Then
                If Not m_SQL.Contains("WHERE") Then
                    m_SQL += "WHERE"
                Else
                    m_SQL += " AND"
                End If
                v = CType(.cmbGrKalk.SelectedItem, DataRowView)
                m_SQL += " GrKalk = '" + v("ID").ToString + "'"
                m_Wybor(2) = +v("Opis").ToString
            End If
            If .chkKl.Checked Then
                With .cmbKlient
                    If .SelectedIndex.Equals(-1) Then
                        Try
                            .SelectedValue = CLng(.Text)
                        Catch ex As Exception
                            .SelectedIndex = -1
                        End Try
                    End If
                    If Not .SelectedIndex.Equals(-1) Then
                        v = CType(.SelectedItem, DataRowView)
                        If Not m_SQL.Contains("WHERE") Then
                            m_SQL += "WHERE"
                        Else
                            m_SQL += " AND"
                        End If
                        m_SQL += " IDKlienta = " & v("ID").ToString
                        m_Wybor(4) = v("ID").ToString & " - " & v("Nazwa").ToString
                    End If
                End With
            End If
            If .chkMat.Checked Then
                If Not m_SQL.Contains("WHERE") Then
                    m_SQL += "WHERE"
                Else
                    m_SQL += " AND"
                End If
                v = CType(.cmbMaterial.SelectedItem, DataRowView)
                m_SQL += " Material = '" + v("ID").ToString + "'"
                m_Wybor(3) = v("Opis").ToString
            End If
            If .chkZl.Checked Then
                If Not m_SQL.Contains("WHERE") Then
                    m_SQL += "WHERE"
                Else
                    m_SQL += " AND"
                End If
                m_SQL += " NrZlecenia = '" + .txtZlecenie.Text + "'"
                m_Wybor(5) = .txtZlecenie.Text
            End If
            If .chkMiesieczne.Checked Then
                m_SQL += " GROUP BY GrMater, RK, Mc ORDER BY GrMater, Mc"
            Else
                m_SQL += " GROUP BY MPK, RK ORDER BY RK"
            End If
        End With
        Me.Dispose()
    End Sub

    Private Sub Me_FormClosed() Handles Me.FormClosed

        With My.Settings
            .scrWybor = Me.Size
            .StartOfWybor = Me.Location
        End With
    End Sub

End Class