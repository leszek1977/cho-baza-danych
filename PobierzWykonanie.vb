﻿
Imports System.IO
Imports System.Data.OleDb
Imports CM = ClHelpFul.CenterMsg

Public Class PobierzWykonanie

#Region "Deklaracje"

    Friend MF As frmMain

#End Region ' Deklaracje

    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()
        MF = frmMain
    End Sub

    Private Sub Me_Load() Handles Me.Load

        MF = frmMain
    End Sub

    Private Sub RZKoszty() Handles btnKoszty.Click

        Dim dt As New DataTable
        Dim tr As OleDbTransaction = Nothing
        Dim cmd As New OleDbCommand
        Dim i As Integer
        Dim strBrak As String = ""
        Dim m_Ext As String = ".xls*"
        Dim m_File As String = "CHO_Result_" + MF.Okres.Substring(0, 7)
        Dim _Path As String = My.Settings.ToResult
        Dim aVal As Array
        Const CO_ACTION As String = "Pobieranie danych kosztowych"
        Try
            If BlokadaAktualizacji("tblZWPC", True) Then Return
            _Path = CP.GetSourcePath(_Path + m_File, m_Ext, CO_ACTION)
            If String.IsNullOrEmpty(_Path) Then Return
            With My.Settings
                .ToResult = _Path
                .Save()
            End With
            m_File += m_Ext
            StartOfProcess(Me.btnKoszty)
            FillStatusBar(0, "Odczyt danych źródłowych")
            With New Cl4Excel.GetFromXls(_Path + m_File, "ZWPC003")
                .Data2Array()
                aVal = .Dane
                .Zwolnij()
            End With
            If aVal Is Nothing Then Return
            ' 1 - Instalacja
            ' 2 - INEF
            ' 3 - OpisINEF
            ' 4 - CALCOD
            ' 5 - OpisCAL
            ' 6 - MPK
            ' 7 - OpisMPK
            ' 8 - RK
            ' 9 - OpisRK
            '10  - Material
            '11 - OpisMater
            '12 - GrK
            '13 - OpisGrK
            '14 - Dostawca
            '15 - NazwaDostawcy
            '16 - JM
            '17 - Ilość
            '18 - Wartość
            '19 - Typ
            Using cn As New OleDbConnection(MF.cns.ConnectionString)
                Try
                    cn.Open()
                    tr = cn.BeginTransaction
                    i = 0
                    With cmd
                        .Connection = cn
                        .Transaction = tr
                        .CommandText = "SELECT * FROM tblZWPC wHERE Mc = 14"
                        dt.Load(.ExecuteReader)
                    End With
                    With dt.Columns
                        .Add("OpisMater")
                        .Add("NazwaDostawcy")
                    End With
                    FillStatusBar(aVal.GetUpperBound(0), CO_ACTION + "...")
                    For i = 2 To aVal.GetUpperBound(0)
                        If aVal(i, 1) Is Nothing Then Exit For
                        If aVal(i, 1).Equals("JML04") Then aVal(i, 1) = "JML03"
                        dt.Rows.Add(New Object() {0, aVal(i, 1), aVal(i, 6), aVal(i, 4), aVal(i, 8), _
                                                  aVal(i, 10), aVal(i, 12), aVal(i, 14), aVal(i, 16), aVal(i, 17), _
                                                  aVal(i, 18), aVal(i, 19), "", aVal(i, 11), aVal(i, 15)})
                        With MF.tspbMain
                            If (i Mod 500).Equals(0) Then
                                .Value = i
                                MF.tsslLicznik.Text = i.ToString + "/" + aVal.GetUpperBound(0).ToString
                                Application.DoEvents()
                            End If
                        End With
                    Next i
                    ClearStatus()
                    i = 0
                    Using New CM
                        If MessageBox.Show("Aktualizujesz materiały i dostawców?", CO_ACTION, _
                                           MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2).Equals(Windows.Forms.DialogResult.No) Then i = 2
                    End Using
                    FillStatusBar(, "Zapis do bazy...")
                    For i = i To 3
                        Select Case i
                            Case 0
                                Dim query = (From p In dt.AsEnumerable _
                                             Where Not (IsDBNull(p("Material")) OrElse String.IsNullOrEmpty(p("Material"))) _
                                             Select ID = p("Material"), Opis = p("OpisMater")).Distinct().ToArray()
                                With cmd
                                    For Each q In query
                                        Try
                                            .CommandText = "INSERT INTO tblMaterialy (ID, MaterialPL) VALUES ('" + q.ID + "', '" + q.Opis.ToString.Replace("'", "") + "')"
                                            .ExecuteNonQuery()
                                            If Not strBrak.Contains("materiałach") Then strBrak += vbCrLf + "Dane o materiałach: "
                                            strBrak += q.ID + ", "
                                        Catch ex As Exception
                                        End Try
                                    Next q
                                End With
                            Case 1
                                Dim query = (From p In dt.AsEnumerable _
                                             Where Not (IsDBNull(p("Dostawca")) OrElse String.IsNullOrEmpty(p("Dostawca"))) _
                                             Select ID = p("Dostawca"), Opis = p("NazwaDostawcy")).Distinct().ToArray()
                                With cmd
                                    For Each q In query
                                        Try
                                            .CommandText = "INSERT INTO tblDostawcy (ID, Nazwa) " _
                                                 + "VALUES (" + q.ID.ToString + ", '" + q.Opis.ToString.Replace("'", "") + "')"
                                            .ExecuteNonQuery()
                                            If Not strBrak.Contains("dostawcach") Then strBrak += vbCrLf + "Dane o dostawcach: "
                                            strBrak += q.ID.ToString + ", "
                                        Catch ex As Exception
                                        End Try
                                    Next q
                                End With
                            Case 2
                                Dim query = (From p In dt.AsEnumerable _
                                             Where (IsDBNull(p!CalCo) OrElse String.IsNullOrEmpty(p!CalCo)) AndAlso p!Typ.Equals("L") _
                                             Select Instalacja = p("Instalacja"), MPK = p("MPK")).Distinct().ToArray()
                                For Each q In query
                                    Using New CM
                                        MessageBox.Show("Instalacja [" + q.Instalacja + "] MPK [" + q.MPK + "]: dla typu L brak CalCo", CO_ACTION, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                    End Using
                                Next q
                            Case 3
                                Dim query = (From p In dt.AsEnumerable _
                                             Group By Instalacja = p!Instalacja, MPK = p!MPK, CalCo = p!CalCo, RK = p!RK, _
                                             Material = p!Material, GrKoszt = p!GrKoszt, Dostawca = p!Dostawca, JM = p!JM, Typ = p!Typ _
                                             Into Ilosc = Sum(p.Field(Of Double)("Ilosc")), Wartosc = Sum(p.Field(Of Double)("Wartosc"))).ToArray
                                With cmd
                                    ' usuń poprzednie zapisy
                                    .CommandText = "DELETE FROM tblZWPC WHERE Mc = " + MF.dtpOkres.Value.Month.ToString
                                    .ExecuteNonQuery()
                                    For Each q In query
                                        Try
                                            .CommandText = "INSERT INTO tblZWPC (Mc, Instalacja, MPK, CalCo, RK, Material, GrKoszt, Dostawca, JM, Typ, Ilosc, Wartosc) " _
                                                 + "VALUES (" + MF.dtpOkres.Value.Month.ToString + ", '" + q.Instalacja + "', '" + q.MPK + "', '"
                                            If Not IsDBNull(q.CalCo) Then .CommandText += q.CalCo
                                            .CommandText += "', '"
                                            If Not IsDBNull(q.RK) Then .CommandText += q.RK
                                            .CommandText += "', '"
                                            If Not IsDBNull(q.Material) Then .CommandText += q.Material
                                            .CommandText += "', '"
                                            If Not IsDBNull(q.GrKoszt) Then .CommandText += q.GrKoszt
                                            .CommandText += "', "
                                            If Not IsDBNull(q.Dostawca) Then
                                                .CommandText += q.Dostawca.ToString
                                            Else
                                                .CommandText += "0"
                                            End If
                                            .CommandText += ", '"
                                            If Not IsDBNull(q.JM) Then .CommandText += q.JM
                                            .CommandText += "', '"
                                            If Not IsDBNull(q.Typ) Then .CommandText += q.Typ
                                            .CommandText += "', " + q.Ilosc.ToString.Replace(",", ".") + ", " + q.Wartosc.ToString.Replace(",", ".") + ")"
                                            .ExecuteNonQuery()
                                        Catch ex As Exception
                                            Throw New ArgumentException(ex.Message + .CommandText)
                                        End Try
                                    Next q
                                End With
                        End Select
                    Next i
                    tr.Commit()
                    If Not String.IsNullOrEmpty(strBrak) Then
                        MF.txtKomunikat.Text = "Uzupełnij:" + strBrak
                        KomunikatShow()
                    End If
                Catch ex As Exception
                    tr.Rollback()
                    Throw New ArgumentException(ex.Message)
                End Try
            End Using
        BlokadaAktualizacji("tblZWPC", , True)
        Using New CM
            MessageBox.Show(CO_ACTION + " zakończone pomyślnie", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Using
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message, CO_ACTION, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
        Finally
            EndOfProcess(Me.btnKoszty)
            dt = Nothing
            cmd = Nothing
            aVal = Nothing
        End Try
    End Sub

    Private Sub RZMedia() Handles btnMedia.Click

        Dim _Path As String = My.Settings.ToMedia
        Dim m_Ext As String = ".xlsx"
        Dim da As OleDbDataAdapter
        Dim dt As New DataTable
        Dim dtG As New DataTable
        Dim r As DataRow
        Dim tr As OleDbTransaction = Nothing
        Dim cmd As New OleDbCommand
        Dim i, iR, k As Integer
        Dim dbl As Double
        Dim aVal As Array
        Const CO_ACTION As String = "Pobieranie danych o mediach"
        Try
            If BlokadaAktualizacji("tblMedia") Then Return
            _Path = CP.GetSourcePath(_Path + "Media_" + MF.Okres.Substring(0, 7), m_Ext, CO_ACTION)
            If String.IsNullOrEmpty(_Path) Then Return
            With My.Settings
                .ToMedia = _Path
                .Save()
            End With
            StartOfProcess(Me.btnMedia)
            Using cn As New OleDbConnection(MF.cns.ConnectionString)
                Try
                    cn.Open()
                    tr = cn.BeginTransaction
                    With cmd
                        .Connection = cn
                        .Transaction = tr
                        .CommandText = "SELECT * FROM tblGEn ORDER BY ID"
                        dtG.Load(.ExecuteReader)
                        .CommandText = "DELETE FROM tblMedia WHERE Mc = " + MF.Okres.Substring(5, 2)
                        .ExecuteNonQuery()
                    End With
                    FillStatusBar(dtG.Rows.Count - 1, CO_ACTION)
                    With New Cl4Excel.GetFromXls(_Path + "Media_" + MF.Okres.Substring(0, 7) + m_Ext, , "B6:F400")
                        For Each r In dtG.Rows
                            ' odczyt wszystkich arkuszy za wyjątkiem Menu
                            If r("Ark").Equals("Menu") Then GoTo NastepnyArk
                            .Arkusz = r("Ark")
                            .Data2Array()
                            aVal = .Dane
                            k = CInt(dtG(i)("Kol").ToString) + 1
                            For iR = 1 To aVal.GetUpperBound(0)
                                If aVal(iR, 1) Is Nothing OrElse String.IsNullOrEmpty(aVal(iR, 1).ToString) Then GoTo Nastepny
                                If Not aVal(iR, 1).ToString.StartsWith("J") Then GoTo Nastepny
                                If aVal(iR, k) Is Nothing Then GoTo Nastepny
                                dbl = CP.Str2Dbl(aVal(iR, k).ToString)
                                If dbl.Equals(0.0) Then GoTo Nastepny
                                With cmd
                                    .CommandText = "INSERT INTO tblMedia (Mc, MPK, Media, JM, Ilosc) " _
                                                    + "VALUES (" + MF.Okres.Substring(5, 2) _
                                                    + ", '" + aVal(iR, 1) + "', '" + dtG(i)("ID").ToString _
                                                    + "', '" + dtG(i)("JM").ToString + "', " + dbl.ToString.Replace(",", ".") + ")"
                                    .ExecuteNonQuery()
                                End With
Nastepny:
                            Next iR
NastepnyArk:
                            i += 1
                            If i.Equals(dtG.Rows.Count) Then Exit For
                            With MF
                                .tspbMain.Value = i
                                .Refresh()
                            End With
                        Next r
                        .Zwolnij()
                    End With
                    tr.Commit()
                Catch ex As Exception
                    tr.Rollback()
                    Throw New ArgumentException(ex.Message)
                End Try
            End Using
            Using New CM
                MessageBox.Show(CO_ACTION + " zakończone pomyślnie", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End Using
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message, CO_ACTION, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
        Finally
            EndOfProcess(Me.btnMedia)
            da = Nothing
            dt = Nothing
            dtG = Nothing
            tr = Nothing
            cmd = Nothing
        End Try
    End Sub

    Private Sub RZNaleznosci() Handles btnNaleznosci.Click

        Dim _Path As String = My.Settings.ToNaleznosci
        Dim _File As String = "Receivables_" + MF.Okres.Substring(0, 7)
        Dim _Ext As String = ".xls"
        Dim da As OleDbDataAdapter
        Dim dt As New DataTable
        Dim dv As New DataView
        Dim cmd As New OleDbCommand
        Dim tr As OleDbTransaction = Nothing
        Dim i, j As Integer
        Dim K() As Integer = Nothing
        Dim aVal As Array
        Const CO_ACTION As String = "Pobieranie danych o należnościach"
        Try
            If BlokadaAktualizacji("tblNaleznosci") Then Return
            _Path = CP.GetSourcePath(_Path + _File, _Ext, CO_ACTION)
            If String.IsNullOrEmpty(_Path) Then Return
            With My.Settings
                .ToNaleznosci = _Path
                .Save()
            End With
            _File += _Ext
            StartOfProcess(Me.btnNaleznosci)
            With New Cl4Excel.GetFromXls(_Path + _File, "CustDetails", "A1:E1")
                .Data2Array()
                aVal = .Dane
                If aVal Is Nothing Then .Zwolnij() : Throw New ArgumentException("Plik [" + _File + "] nie spełnia oczekiwań")
                If Not CP.SourceLayoutControl(aVal, "Receivables", MF.cns, K) Then .Zwolnij() : Return
                .Obszar = "A3:E"
                .Data2Array()
                aVal = .Dane
                .Zwolnij()
            End With
            If aVal Is Nothing Then Throw New ArgumentException("Plik [" + _File + "] nie spełnia oczekiwań")
            FillStatusBar(aVal.GetUpperBound(0), "Należności - " + _File)
            Using cn As New OleDbConnection(MF.cns.ConnectionString)
                Try
                    cn.Open()
                    tr = cn.BeginTransaction
                    With cmd
                        .Connection = cn
                        .Transaction = tr
                        ' usuwanie okresowych zapisów
                        .CommandText = "DELETE FROM tblNaleznosci WHERE Miesiac = " + MF.dtpOkres.Value.Month.ToString
                        .ExecuteNonQuery()
                        .CommandText = "SELECT * FROM tblNaleznosci WHERE Miesiac = 14"
                        dt.Load(.ExecuteReader)
                    End With
                    dv = dt.DefaultView
                    dv.Sort = "IDKlienta, Zaklad"
                    For i = 1 To aVal.GetUpperBound(0)
                        If aVal(i, K(1)).Equals("OV36") Then
                            aVal(i, K(1)) = "ZWK"
                        Else
                            aVal(i, K(1)) = "ZWW"
                        End If
                        j = dv.Find(New Object() {aVal(i, K(0)), aVal(i, K(1))})
                        If j.Equals(-1) Then
                            dt.Rows.Add(New Object() {MF.dtpOkres.Value.Month, aVal(i, K(1)), aVal(i, K(0)), aVal(i, K(2))})
                        Else
                            dv(j)("Kwota") = dv(j)("Kwota") + aVal(i, K(2))
                        End If
                        If (i Mod 5).Equals(0) Then MF.tspbMain.Value = i
                    Next i
                    With cmd
                        For Each r In dt.Rows
                            .CommandText = "INSERT INTO tblNaleznosci (Miesiac, IDKlienta, Zaklad, Kwota) " _
                                        + "VALUES(" + r("Miesiac").ToString + ", " + r("IDKlienta").ToString + ", '" + r("Zaklad") + "', " + r("Kwota").ToString.Replace(",", ".") + ")"
                            .ExecuteNonQuery()
                        Next r
                    End With
                    tr.Commit()
                Catch ex As Exception
                    tr.Rollback()
                    Throw New ArgumentException(ex.Message)
                End Try
            End Using
            Using New CM
                MessageBox.Show(CO_ACTION + " zakończone pomyślnie", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End Using
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message, CO_ACTION, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
        Finally
            EndOfProcess(Me.btnNaleznosci)
            da = Nothing
            dt = Nothing
            dv = Nothing
            cmd = Nothing
            tr = Nothing
        End Try
    End Sub

    Private Sub RZ_Sprzedaz() Handles btnSprzedaz.Click

        RZSprzedaz(0)
    End Sub

    Sub RZSprzedaz(ByVal m_Ster As Integer)

        Dim _Path As String = My.Settings.ToResult
        Dim _File As String = "CHO_Result_" + MF.Okres.Substring(0, 7)
        Dim _Ext As String = ".xls"
        Dim iRok, iMc As Integer
        Dim strGrMater As String = ""
        Dim strGrKalk As String = ""
        Dim strMaterial As String = ""
        Dim strKlient As String = ""
        Dim _Wydz, strOkres As String
        Dim m_Ado As New ClADO.ADOGet
        Dim da As OleDbDataAdapter
        Dim dt As New DataTable
        Dim dtA As New DataTable
        Dim dtS As New DataTable
        Dim dtMM As New DataTable
        Dim dtW As New DataTable
        Dim dvK As New DataView
        Dim dvW As New DataView
        Dim dv As New DataView
        Dim dvS As New DataView
        Dim v As DataRowView
        Dim cmd As New OleDbCommand
        Dim tr As OleDbTransaction = Nothing
        Dim i, j, k, iR, iW As Integer
        Dim m_Waga As Double
        Dim m_Kwota, m_Val As Double
        Dim m_Pip As Double
        Dim m_PipSAP As Double
        Dim r As DataRow = Nothing
        Dim blnTylkoSprzedaz As Boolean = False
        Dim oDane As Object = Nothing, oTr As Object = Nothing, oMS As Object = Nothing
        Dim oPS As Object = Nothing, oMT As Object = Nothing, oWS As Object = Nothing
        Dim oR1 As Object = Nothing, oR2 As Object = Nothing, oOR As Object = Nothing
        Const CO_ACTION As String = "Pobieranie danych sprzedaży"
        If m_Ster.Equals(0) AndAlso BlokadaAktualizacji("tblSprzedaz") Then Return
        strOkres = Format(MF.dtpOkres.Value, "MM.yyyy")
        With MF.dtpOkres.Value
            iRok = .Year
            iMc = .Month
        End With
        ' kursy walut
        Try
            da = MF.DA_TI("tblKursyWalut", "MM_dd", "MM_dd = '" + strOkres.Substring(0, 2) + "'")
            da.Fill(dt)
            If dt.Rows.Count.Equals(0) Then
                Dim aKursy As Array
                With New Cl4Excel.GetFromXls("W:\Costs\CELSA\Kursy walut\Kursy_" + iRok.ToString + "_Średnioważone.xls", "średnie M ", "A2:O39")
                    .Data2Array() : aKursy = .Dane : .Zwolnij()
                End With
                If aKursy Is Nothing Then Exit Try
                r = dt.NewRow
                r("MM_dd") = strOkres.Substring(0, 2)
                For iR = 1 To 4
                    For i = 2 To aKursy.GetUpperBound(0)
                        If aKursy.GetValue(i, 3).ToString.ToLower.Contains(dt.Columns(iR).ColumnName.ToLower) Then
                            r(iR) = aKursy.GetValue(i, 3 + iMc)
                            Exit For
                        End If
                    Next i
                Next iR
                dt.Rows.Add(r)
                da.Update(dt.GetChanges)
            End If
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message, "Pobieranie kursów walut", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Using
        End Try
        Try
            _Path = CP.GetSourcePath(_Path + _File, _Ext, CO_ACTION)
            If String.IsNullOrEmpty(_Path) Then Return
            With My.Settings
                .ToResult = _Path
                .Save()
            End With
            _File += _Ext
            StartOfProcess(Me.btnSprzedaz)
            FillStatusBar(, "Odczyt danych źródłowych...")
            da = New OleDbDataAdapter("SELECT ID, Wydzial FROM tblGrupyMat WHERE NOT (Wydzial IS NULL OR wydzial = '') ORDER BY ID", MF.cns)
            da.Fill(dtW)
            dvW = dtW.DefaultView
            ' amortyzacja
            dvW.Sort = "Wydzial"
            With New Cl4Excel.GetFromXls(_Path + _File, "Depreciation", My.Settings.RangeOfDepreciationByDiv)
                .Data2Array() : oDane = .Dane : .Zwolnij()
            End With
            da = MF.DA_TI("tblAmortyzacja", "Wydzial, Mc", "Mc = " + iMc.ToString)
            da.Fill(dtA)
            dv = dtA.DefaultView
            dv.Sort = "Wydzial"
            For i = 1 To oDane.GetUpperBound(0)
                If Not (oDane(i, 3) Is Nothing OrElse String.IsNullOrEmpty(oDane(i, 3).ToString) _
                        OrElse dvW.Find(oDane(i, 3).ToString).Equals(-1)) Then
                    Try
                        m_Kwota = oDane(i, 1)
                        If Not m_Kwota.Equals(0.0) Then
                            j = dv.Find(oDane(i, 3))
                            If j.Equals(-1) Then
                                dtA.Rows.Add(New Object() {oDane(i, 3).ToString, iMc, oDane(i, 2) / m_Kwota})
                            Else
                                dv(j)("Stawka") = oDane(i, 2) / m_Kwota
                            End If
                            dv(j).EndEdit()
                        End If
                    Catch ex As Exception
                    End Try
                End If
            Next i
            If dtA.GetChanges IsNot Nothing Then _
                da.Update(dtA.GetChanges)
            If m_Ster.Equals(1) Then GoTo Finish
            dt = New DataTable
            With dt.Columns
                .Add("Material") : .Add("IDKlienta", GetType(Integer))
                .Add("GrMater") : .Add("Dostawa", GetType(Integer))
                .Add("Spedytor", GetType(Integer)) : .Add("Dostawca")
                .Add("Waga", GetType(Double))
                .Add("TrPLN", GetType(Double)) : .Add("TrWaga", GetType(Double))
                .Add("FrachtPLN", GetType(Double)) : .Add("FrachtWaga", GetType(Double))
            End With
            dvW.Sort = "ID"
            With New Cl4Excel.GetFromXls(_Path + _File, "Sales")
                Try
                    .Data2Array()
                    oDane = .Dane
                    If oDane Is Nothing Then Throw New ArgumentException("Plik nie spełnia oczekiwań - brak danych w arkuszu [Sales]")
                    .Arkusz = "StK" : .Zakres = "A6:BG"
                    .Data2Array() : oMS = .Dane
                    If oMS Is Nothing Then Throw New ArgumentException("Plik nie spełnia oczekiwań - brak danych w arkuszu [StK]")
                    .Arkusz = "PRA" : .Zakres = "A6:BG"
                    .Data2Array() : oPS = .Dane
                    If oPS Is Nothing Then Throw New ArgumentException("Plik nie spełnia oczekiwań - brak danych w arkuszu [PRA]")
                    .Arkusz = "WOM" : .Zakres = "A6:BG"
                    .Data2Array() : oMT = .Dane
                    If oMT Is Nothing Then Throw New ArgumentException("Plik nie spełnia oczekiwań - brak danych w arkuszu [WOM]")
                    .Arkusz = "StW" : .Zakres = "A6:BG"
                    .Data2Array() : oWS = .Dane
                    If oWS Is Nothing Then Throw New ArgumentException("Plik nie spełnia oczekiwań - brak danych w arkuszu [StW]")
                    .Arkusz = "RM1" : .Zakres = "A6:BG"
                    .Data2Array() : oR1 = .Dane
                    If oR1 Is Nothing Then Throw New ArgumentException("Plik nie spełnia oczekiwań - brak danych w arkuszu [RM1]")
                    .Arkusz = "RM2" : .Zakres = "A6:BG"
                    .Data2Array() : oR2 = .Dane
                    If oR2 Is Nothing Then Throw New ArgumentException("Plik nie spełnia oczekiwań - brak danych w arkuszu [StK]")
                    .Arkusz = "OTHER" : .Zakres = "A6:BG"
                    .Data2Array() : oOR = .Dane
                    If oOR Is Nothing Then Throw New ArgumentException("Plik nie spełnia oczekiwań - brak danych w arkuszu [OTHER]")
                    .Arkusz = "Transport" : .Zakres = "A1:AC" : .Data2Array() : oTr = .Dane
                    If oTr Is Nothing Then
                        Using New CM
                            If MessageBox.Show("Odświeżanie tylko sprzedaży?", CO_ACTION, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2).Equals(Windows.Forms.DialogResult.Yes) Then
                                blnTylkoSprzedaz = True
                            Else
                                Throw New ArgumentException("Plik nie spełnia oczekiwań - brak danych w arkuszu [Transport]")
                            End If
                        End Using
                    End If
                Catch ex As Exception
                    Throw New ArgumentException(ex.Message)
                Finally
                    .Zwolnij()
                End Try
            End With
            FillStatusBar(oDane.GetUpperBound(0), "Aktualizacja sprzedaży...")
            da = MF.DA_TI("tblSprzedaz", "Miesiac, Material, IDKlienta", "Miesiac = 14")
            da.Fill(dtS)
            With dtS
                With .Columns
                    .Add("GM_Opis") : .Add("M_Opis")
                    .Add("KK") : .Add("Kraj")
                    .Add("Klient_Opis")
                End With
                dvS = .DefaultView
            End With
            dvS.Sort = "Material, IDKlienta"
            For i = 2 To oDane.GetUpperBound(0)
                If String.IsNullOrEmpty(oDane(i, 1).ToString) Then Exit For
                If Not oDane(i, 1).ToString.Equals(strOkres) Then GoTo Nastepny
                If String.IsNullOrEmpty(oDane(i, 13).ToString) Then
                    Try
                        oDane(i, 13) = CInt(InputBox("Podaj nr klienta:", oDane(i, 13).ToString, , CP.PositionX, CP.PositionY))
                    Catch ex As Exception
                        Throw New ArgumentException("Błędny nr klienta")
                    End Try
                End If
                j = dvS.Find(New Object() {oDane(i, 9), oDane(i, 13)})
                If j.Equals(-1) Then
                    _Wydz = "OR"
                    If Not oDane(i, 7).ToString.ToUpper.Contains("JSWZ") Then
                        k = dvW.Find(oDane(i, 2))
                        If Not k.Equals(-1) Then _Wydz = dvW(k)(1)
                    End If
                    dtS.Rows.Add(New Object() {iMc, oDane(i, 9), oDane(i, 13), oDane(i, 2), oDane(i, 6).ToString.Replace("#", ""), _Wydz, oDane(i, 4), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, _
                                               oDane(i, 3).ToString.Replace("'", "").Replace("'", ""), oDane(i, 10).ToString.Replace("'", ""), oDane(i, 11), oDane(i, 12), oDane(i, 14).ToString.Replace("'", "")})
                    j = dvS.Find(New Object() {oDane(i, 9), oDane(i, 13)})
                End If
                v = dvS(j)
                oDane(i, 7) = v("Wydzial")
                If Not "WS_R1_R2".Contains(v("Wydzial")) Then
                    oDane(i, 20) = 0
                    Try
                        v("Waga") = v("Waga") + oDane(i, 19) / 1000
                    Catch ex As Exception
                        oDane(i, 19) = 0
                    End Try
                Else
                    oDane(i, 19) = 0
                    Try
                        v("Waga") = v("Waga") + oDane(i, 20) / 1000
                    Catch ex As Exception
                        oDane(i, 20) = 0
                    End Try
                End If
                v("Kwota") = v("Kwota") + oDane(i, 23)
                v("Rabat") = v("Rabat") + oDane(i, 24)
                v("PIP_SAP") = v("PIP_SAP") + oDane(i, 27)
                v("PIP") = v("PIP") + oDane(i, 27)
                v.EndEdit()
Nastepny:
                With MF.tspbMain
                    If i <= .Maximum AndAlso (i Mod 5).Equals(0) Then _
                        .Value = i : Application.DoEvents()
                End With
            Next i
            If dvS.Count.Equals(0) Then Throw New ArgumentException("Dane sprzedaży nie istnieją")
            ' aktualizacja transportu
            If blnTylkoSprzedaz Then GoTo Kontrola
            PobierzTransportSprzedazy(oTr, dtW, oDane)
            For i = 2 To oTr.GetUpperBound(0)
                If Not oTr(i, 1).Equals(strOkres) Then GoTo Nastepny1
                iR = dvS.Find(New Object() {oTr(i, 10), oTr(i, 18)})
                If iR.Equals(-1) Then
                    dtS.Rows.Add(New Object() {iMc, oTr(i, 10), oTr(i, 18), oTr(i, 6), oTr(i, 8).ToString.Replace("#", ""), oTr(i, 2), oTr(i, 4), 0, 0, 0, 0, oTr(i, 29), _
                                               0, 0, 0, 0, 0, oTr(i, 7), oTr(i, 11), oTr(i, 14), oTr(i, 15), oTr(i, 19)})
                Else
                    dvS(iR)("Transport") = dvS(iR)("Transport") + oTr(i, 29)
                End If
Nastepny1:
            Next i
Kontrola:
            Using cn As New OleDbConnection(MF.cns.ConnectionString)
                Try
                    cn.Open()
                    tr = cn.BeginTransaction
                    With cmd
                        .Connection = cn
                        .Transaction = tr
                    End With
                    FillStatusBar(4, "Aktualizacja danych stałych...")
                    For i = 0 To 3
                        Select Case i
                            Case 0
                                Dim query = (From p In dtS.AsEnumerable _
                                             Select ID = p("GrMater"), Opis = p("GM_Opis"), Wydzial = p("Wydzial")).Distinct()
                                With cmd
                                    For Each q In query
                                        .CommandText = "INSERT INTO tblGrupyMat (ID, Wydzial, GrMaterPL) VALUES ('" + q.ID + "', '" _
                                                                + q.Wydzial + "', '" + q.Opis.ToString + "')"
                                        Try
                                            .ExecuteNonQuery()
                                        Catch ex As Exception
                                        End Try
                                    Next q
                                End With
                            Case 1
                                Dim query = (From p In dtS.AsEnumerable _
                                             Select ID = p("GrKalk"), GM = p("GrMater"), Wydzial = p("Wydzial")).Distinct()
                                With cmd
                                    For Each q In query
                                        .CommandText = "INSERT INTO tblGrupyKalk (ID, GrMater, Wydzial) VALUES ('" + q.ID + "', '" _
                                                                + q.GM + "', '" + q.Wydzial.ToString + "')"
                                        Try
                                            .ExecuteNonQuery()
                                        Catch ex As Exception
                                        End Try
                                    Next q
                                End With
                            Case 2
                                Dim query = (From p In dtS.AsEnumerable _
                                             Select ID = p("Material"), Opis = p("M_Opis"), GK = p("GrKalk"), GM = p("GrMater")).Distinct()
                                With cmd
                                    For Each q In query
                                        .CommandText = "INSERT INTO tblMaterialy (ID, MaterialPL, GrMaterID, GrKalkID) VALUES ('" + q.ID + "', '" _
                                                                + q.Opis + "', '" + q.GM + "', '" + q.GK.ToString + "')"
                                        Try
                                            .ExecuteNonQuery()
                                        Catch ex As Exception
                                        End Try
                                    Next q
                                    .CommandText = "SELECT * FROM tblMaterialy"
                                    dtMM.Load(.ExecuteReader)
                                End With
                            Case 3
                                Dim query = (From p In dtS.AsEnumerable _
                                             Select ID = p("IDKlienta"), Opis = p("Klient_Opis"), KK = p("KK"), Kraj = p("Kraj"), GD = p("GrDekretacji")).Distinct()
                                With cmd
                                    For Each q In query
                                        .CommandText = "INSERT INTO tblKlienci (ID, Nazwa, GrDekretacji, Kraj, KK) VALUES (" + q.ID.ToString + ", '" _
                                                                + q.Opis.ToString.Replace("'", "") + "', " + q.GD + ", '" + q.Kraj + "', '" + q.KK + "')"
                                        Try
                                            .ExecuteNonQuery()
                                        Catch ex As Exception
                                        End Try
                                    Next q
                                End With
                        End Select
                        MF.tspbMain.Value = i : Application.DoEvents()
                    Next i
                    With dtS.Columns
                        .Remove("GM_Opis")
                        .Remove("M_Opis")
                        .Remove("KK")
                        .Remove("Kraj")
                        .Remove("Klient_Opis")
                    End With
                    ' Other and overheads
                    FillStatusBar(7, "Pobieranie danych: pozostałe, other, overhead i PIP...")
                    dvK = dtMM.DefaultView
                    dvK.Sort = "ID"
                    dvS.Sort = "Wydzial, GrKalk, GrDekretacji"
                    Dim m_Sum(4, 7) As Double
                    Dim wsk As Integer
                    _Wydz = ""
                    For iW = 0 To 6
                        Dim aVal As Object = Nothing
                        Select Case iW
                            Case 0  'Stalownia
                                aVal = oMS
                                _Wydz = "MS"
                            Case 1  'Prasownia
                                aVal = oPS
                                _Wydz = "PS"
                            Case 2  'WOM
                                aVal = oMT
                                _Wydz = "MT"
                            Case 3  'Stalownia ZWW
                                aVal = oWS
                                _Wydz = "WS"
                            Case 4  'RM1
                                aVal = oR1
                                _Wydz = "R1"
                            Case 5  'RM2
                                aVal = oR2
                                _Wydz = "R2"
                            Case 6  'Pozostałe
                                aVal = oOR
                                _Wydz = "OR"
                        End Select
                        For i = 1 To aVal.GetUpperBound(0)
                            If aVal(i, 1) Is Nothing _
                                OrElse String.IsNullOrEmpty(aVal(i, 1).ToString) Then GoTo Nastepny4
                            For j = 0 To m_Sum.GetUpperBound(0)
                                For k = 0 To m_Sum.GetUpperBound(1)
                                    m_Sum(j, k) = 0
                                Next k
                            Next j
                            For k = 1 To 4
                                wsk = 1 + (k * 14)
                                If aVal(i, wsk) IsNot Nothing Then _
                                    m_Sum(k, 0) = aVal(i, wsk) 'Koszty zarządu
                                If aVal(i, wsk + 2) IsNot Nothing Then _
                                    m_Sum(k, 1) = aVal(i, wsk + 2) 'Pozostały wynik
                                If aVal(i, wsk - 5) IsNot Nothing Then _
                                    m_Sum(k, 4) = aVal(i, wsk - 5) 'COGS - korekta
                                If aVal(i, wsk - 2) IsNot Nothing Then _
                                    m_Sum(k, 6) = aVal(i, wsk - 2) 'Koszty sprzedaży - inne
                                If Math.Abs(m_Sum(k, 0)) < 0.01 AndAlso Math.Abs(m_Sum(k, 1)) < 0.01 _
                                        AndAlso Math.Abs(m_Sum(k, 4)) < 0.01 AndAlso Math.Abs(m_Sum(k, 6)) < 0.01 Then GoTo Nastepny3
                                If _Wydz.Equals("OR") Then
                                    dvS.RowFilter = "Wydzial = '" + _Wydz + "' AND Material = '" + aVal(i, 1) _
                                            + "' AND GrDekretacji = '0" + k.ToString + "'"
                                Else
                                    dvS.RowFilter = "Wydzial = '" + _Wydz + "' AND GrKalk = '" + aVal(i, 1) _
                                            + "' AND GrDekretacji = '0" + k.ToString + "'"
                                End If
                                m_Waga = 0
                                m_Pip = 0
                                m_PipSAP = 0
                                'sumowanie wagi i PIP_SAP dla grupy
                                For j = 0 To dvS.Count - 1
                                    m_Waga += CP.Str2Dbl(dvS(j)("Waga").ToString)
                                    m_Pip += CP.Str2Dbl(dvS(j)("Kwota").ToString)
                                    m_PipSAP += Math.Abs(CP.Str2Dbl(dvS(j)("PIP_SAP").ToString))
                                Next j
                                If m_Waga.Equals(0.0) _
                                    AndAlso m_PipSAP.Equals(0.0) _
                                    AndAlso m_Pip.Equals(0.0) Then GoTo Nastepny3
                                ' podział na materiały
                                For j = 0 To dvS.Count - 1
                                    If Not m_Waga.Equals(0.0) Then
                                        m_Sum(0, 0) = CP.Str2Dbl(dvS(j)("Waga").ToString)
                                        m_Kwota = m_Sum(0, 0) * m_Sum(k, 0) / m_Waga
                                        m_Sum(k, 2) += m_Kwota
                                        dvS(j)("Overhead") = m_Kwota
                                        m_Kwota = m_Sum(0, 0) * m_Sum(k, 1) / m_Waga
                                        m_Sum(k, 3) += m_Kwota
                                        dvS(j)("Other") = m_Kwota
                                        m_Kwota = m_Sum(0, 0) * m_Sum(k, 6) / m_Waga
                                        m_Sum(k, 7) += m_Kwota
                                        dvS(j)("Pozostale") = m_Kwota
                                    End If
                                    ' przeliczenie PIP w stosunku do PIP_SAP
                                    m_Kwota = 0
                                    m_Val = CP.Str2Dbl(dvS(j)("PIP").ToString)
                                    If _Wydz.Equals("OR") Then
                                        If Not m_Pip.Equals(0.0) Then
                                            m_Kwota = (m_Sum(k, 4) * CP.Str2Dbl(dvS(j)("Kwota").ToString)) / m_Pip
                                        Else
                                            GoTo PoPIP
                                        End If
                                    ElseIf Not m_PipSAP.Equals(0.0) Then
                                        m_Kwota = (m_Sum(k, 4) * Math.Abs(m_Val)) / m_PipSAP
                                    Else
                                        GoTo PoPIP
                                    End If
                                    m_Sum(k, 5) += m_Kwota
                                    dvS(j)("PIP") = m_Val + m_Kwota
PoPIP:
                                Next j
                                ' podział różnic
                                If Math.Abs(m_Sum(k, 0) - m_Sum(k, 2)) > 0.01 Then
                                    For j = 0 To dvS.Count - 1
                                        m_Kwota = CP.Str2Dbl(dvS(j)("Overhead").ToString)
                                        If Not m_Kwota.Equals(0.0) Then
                                            dvS(j)("Overhead") = m_Kwota + m_Sum(k, 0) - m_Sum(k, 2)
                                            Exit For
                                        End If
                                    Next j
                                End If
                                If Math.Abs(m_Sum(k, 1) - m_Sum(k, 3)) > 0.01 Then
                                    For j = 0 To dvS.Count - 1
                                        m_Kwota = CP.Str2Dbl(dvS(j)("Other").ToString)
                                        If Not m_Kwota.Equals(0.0) Then
                                            dvS(j)("Other") = m_Kwota + m_Sum(k, 1) - m_Sum(k, 3)
                                            Exit For
                                        End If
                                    Next j
                                End If
                                If Math.Abs(m_Sum(k, 5) - m_Sum(k, 4)) > 0.01 Then
                                    For j = 0 To dvS.Count - 1
                                        m_Kwota = CP.Str2Dbl(dvS(j)("PIP").ToString)
                                        If Not m_Kwota.Equals(0.0) Then
                                            dvS(j)("PIP") = m_Kwota + (m_Sum(k, 5) - m_Sum(k, 4))
                                            Exit For
                                        End If
                                    Next j
                                End If
                                If Math.Abs(m_Sum(k, 6) - m_Sum(k, 7)) > 0.01 Then
                                    For j = 0 To dvS.Count - 1
                                        m_Kwota = CP.Str2Dbl(dvS(j)("Pozostale").ToString)
                                        If Not m_Kwota.Equals(0.0) Then
                                            dvS(j)("Pozostale") = m_Kwota + m_Sum(k, 6) - m_Sum(k, 7)
                                            Exit For
                                        End If
                                    Next j
                                End If
                                dvS.RowFilter = Nothing
Nastepny3:
                            Next k
Nastepny4:
                        Next i
                        MF.tspbMain.Value = iW : Me.Refresh()
                    Next iW
                    ' usuń poprzednie zapisy
                    With cmd
                        .CommandText = "DELETE FROM tblSprzedaz WHERE Miesiac = " + iMc.ToString
                        .ExecuteNonQuery()
                    End With
                    tr.Commit()
                    If dtS.GetChanges IsNot Nothing Then
                        da = MF.DA_TI("tblSprzedaz", "Miesiac DESC, Material, IDKlienta")
                        da.Update(dtS.GetChanges)
                    End If
                    RZ_EBiTDA()
                    Dim m_Kontr As Integer = -1
                    MF.RZSprzedazVsZS(iRok.ToString, iMc, m_Kontr)
                    With MF.txtKomunikat
                        If .Text.Length > 0 Then
                            .Text = CO_ACTION + .Text
                            KomunikatShow()
                        Else
                            Using New CM
                                MessageBox.Show(CO_ACTION + " zakończone pomyślnie", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
                            End Using
                        End If
                    End With
                Catch ex As Exception
                    tr.Rollback()
                    Throw New ArgumentException(ex.Message + vbCrLf + ex.StackTrace)
                End Try
            End Using
Finish:
            If Not blnTylkoSprzedaz Then BlokadaAktualizacji("tblTransport", , True)
            BlokadaAktualizacji("tblSprzedaz", , True)
            BlokadaAktualizacji("tblAmortyzacja", , True)
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message + dtS.Rows.Count.ToString, CO_ACTION, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Using
        Finally
            If m_Ster.Equals(0) Then
                EndOfProcess(Me.btnSprzedaz)
            Else
                MF.dtpOkres.Enabled = True
                ClearStatus()
            End If
            da = Nothing
            dt = Nothing
            dv = Nothing
            cmd = Nothing
            tr = Nothing
            m_Ado = Nothing
            dtA = Nothing
            dtW = Nothing
            dv = Nothing
            dvS = Nothing
            dvW = Nothing
            oDane = Nothing
            oTr = Nothing
            oMS = Nothing
            oPS = Nothing
            oMT = Nothing
            oWS = Nothing
            oR1 = Nothing
            oR2 = Nothing
            oOR = Nothing
            GC.Collect()
        End Try
    End Sub

    Sub RZ_EBiTDA()

        Dim dt As New DataTable
        Dim dtS As New DataTable
        Dim m_Kwota As Double
        Dim da As OleDbDataAdapter = New OleDbDataAdapter("SELECT * FROM tblSprzedaz S LEFT JOIN tblAmortyzacja A ON S.Wydzial = A.Wydzial AND S.Miesiac = A.Mc WHERE S.Miesiac = " + MF.dtpOkres.Value.Month.ToString, MF.cns)
        FillStatusBar(0, "Przeliczanie EBiTDA...")
        Try
            da.Fill(dtS)
            If dtS.Rows.Count.Equals(0) Then Throw New ArgumentException("Brak amortyzacji miesięcznej")
            For Each r As DataRow In dtS.Rows
                Try
                    m_Kwota = CDbl(r("Waga").ToString)
                Catch ex As Exception
                    r("Waga") = 0
                End Try
                Try
                    m_Kwota = CDbl(r("Kwota").ToString)
                Catch ex As Exception
                    r("Kwota") = 0
                End Try
                Try
                    m_Kwota = CDbl(r("Rabat").ToString)
                Catch ex As Exception
                    r("Rabat") = 0
                End Try
                Try
                    m_Kwota = CDbl(r("PIP").ToString)
                Catch ex As Exception
                    r("PIP") = 0
                End Try
                Try
                    m_Kwota = CDbl(r("Transport").ToString)
                Catch ex As Exception
                    r("Transport") = 0
                End Try
                Try
                    m_Kwota = CDbl(r("Other").ToString)
                Catch ex As Exception
                    r("Other") = 0
                End Try
                Try
                    m_Kwota = CDbl(r("Overhead").ToString)
                Catch ex As Exception
                    r("Overhead") = 0
                End Try
                Try
                    m_Kwota = CDbl(r("Pozostale").ToString)
                Catch ex As Exception
                    r("Pozostale") = 0
                End Try
                Try
                    Try
                        m_Kwota = r("Stawka") * r("Waga")
                    Catch ex As Exception
                        m_Kwota = 0
                    End Try
                    r("EBiTDA") = r("Kwota") - r("Rabat") - r("PIP") - r("Transport") - r("Other") - r("Overhead") - r("Pozostale") + m_Kwota
                Catch ex As Exception
                    r("EBiTDA") = 0
                End Try
            Next r
            da = MF.DA_TI("tblSprzedaz", "Miesiac, Material, IDKlienta")
            With dtS
                .Columns.Remove("A.Wydzial")
                .Columns.Remove("Mc")
                .Columns.Remove("Stawka")
                .Columns("S.Wydzial").ColumnName = "Wydzial"
            End With
            da.Update(dtS.GetChanges)
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message, "Przeliczanie EBiTDA", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Using
        Finally
            ClearStatus()
        End Try
    End Sub

    Private Sub RZZWKWIP() Handles btnZWKWIP.Click

        Dim _Path As String = My.Settings.ToWIP
        Dim _File As String = "BWD_WIP_*"
        Dim _Ext As String = ".xl*"
        Dim strGrMater As String = ""
        Dim strGrKalk As String = ""
        Dim strMaterial As String = ""
        Dim strKlient As String = ""
        Dim strWydzial As String = ""
        Dim da As OleDbDataAdapter
        Dim dt As New DataTable
        Dim dv As DataView
        Dim cmd As New OleDbCommand
        Dim tr As OleDbTransaction = Nothing
        Dim i, j, iPl As Integer
        Dim m_Check, m_Waga, m_Waga3, m_Waga4, m_Waga5, m_Kwota As Double
        Dim m_Key As String
        Dim m_FI() As FileInfo
        Dim aDane As Object = Nothing
        Dim Kol() As Integer = Nothing
        Const CO_ACTION As String = "Pobieranie danych WIP "
        Try
            If BlokadaAktualizacji("tblWIP") Then Return
            ' Sprawdź, czy są pliki
            _Path = CP.GetSourcePath(_Path + _File, _Ext, CO_ACTION)
            If String.IsNullOrEmpty(_Path) Then Exit Sub
            With My.Settings
                .ToWIP = _Path
                .Save()
            End With
            _File += _Ext
            m_FI = New DirectoryInfo(_Path).GetFiles(_File)
            StartOfProcess(Me.btnZWKWIP)
            Using cn As New OleDbConnection(MF.cns.ConnectionString)
                cn.Open()
                tr = cn.BeginTransaction
                With cmd
                    .Connection = cn
                    .Transaction = tr
                    .CommandText = "SELECT * FROM tblWIP WHERE Okres = '" + MF.Okres.Substring(0, 7) + "'"
                    dt.Load(.ExecuteReader)
                    .CommandText = "DELETE FROM tblWIP WHERE Okres = '" + MF.Okres.Substring(0, 7) + "'"
                    .ExecuteNonQuery()
                End With
                With dt.Columns
                    .Add("GrMaterPL")
                    .Add("GrKalkPL")
                    .Add("MaterialPL")
                    .Add("Nazwa")
                    .Add("Wydzial")
                End With
                dv = dt.DefaultView
                dv.Sort = "Instalacja, NrZlecenia, Partia"
                ' 0  1	Instalacja			
                ' 1  3	Grupamateriałowa			
                '    4	GrupamateriałowaOpis			
                ' 2  5	Grupakalkulacyjna			
                '    6	GrupakalkulacyjnaOpis
                ' 3  7	Materiał			
                '    8	MateriałOpis			
                ' 4  9	Klient			
                '   10	KlientNazwa
                ' 5 11	WIPZlecenieprod			
                ' 6 12	Statuszlecprod			
                ' 7 13	Partia			
                ' 8 14	Licznik_nowy			
                ' 9 15	Wagaodkuwki			
                '10 16	Waga	WagaDoKlienta		
                '11 17	WagaWIP			
                '12 18	WIP(Całkowicie)			
                '13 19	WIPAktualnyZapas	
                Try
                    For iPl = 0 To m_FI.GetUpperBound(0)
                        With New Cl4Excel.GetFromXls(m_FI(iPl).FullName, "BWD", "J5:J6")
                            Try
                                .Data2Array() : aDane = .Dane
                                If aDane Is Nothing Then Throw New ArgumentException("Brak danych")
                                With aDane(2, 1)
                                    If Not .ToString.Substring(0, 2).Equals(MF.Okres.Substring(5, 2)) OrElse Not .ToString.Substring(3, 4).Equals(MF.Okres.Substring(0, 4)) Then _
                                        Throw New ArgumentException("Wybrany okres nie odpowiada okresowi danych BWD")
                                End With
                                .Obszar = "F15:X" : .Data2Array() : aDane = .Dane
                            Catch ex As Exception
                                Throw New ArgumentException(ex.Message)
                            Finally
                                .Zwolnij()
                            End Try
                        End With
                        If aDane Is Nothing Then Throw New ArgumentException("Plik [" + m_FI(iPl).Name + "] nie spełnia oczekiwań - brak danych")
                        ' sprawdzenie układu kolumn 
                        If Not CP.SourceLayoutControl(aDane, "BWD_WIP", MF.cns, Kol) Then Exit Try
                        With MF
                            .tsslMain.Text = "Przetwarzanie danych BWD..."
                            With .tspbMain
                                .Minimum = 0
                                .Maximum = aDane.GetLength(0)
                            End With
                        End With
                        i = 2
                        Do Until i > aDane.GetLength(0)
                            If aDane(i, Kol(2)) Is Nothing OrElse String.IsNullOrEmpty(aDane(i, Kol(2)).ToString) Then
                                i += 1 : GoTo Nastepny
                            End If
                            strWydzial = "  "
                            With aDane(i, Kol(2)).ToString
                                If .Trim.Equals("#") Then
                                    aDane(i, Kol(2)) = ""
                                ElseIf .StartsWith("J7") Then
                                    strWydzial = "MT"
                                ElseIf .StartsWith("J43") Then
                                    strWydzial = "MS"
                                ElseIf .StartsWith("J48") Then
                                    strWydzial = "PS"
                                End If
                            End With
                            m_Key = aDane(i, Kol(5)).ToString
                            m_Kwota = 0
                            m_Waga = 0
                            j = i
                            ' sprawdzenie zleceń
                            While i <= aDane.GetLength(0) AndAlso aDane(i, Kol(5)) IsNot Nothing AndAlso aDane(i, Kol(5)).ToString.Equals(m_Key)
                                With aDane
                                    Try
                                        m_Check = CType(.GetValue(i, Kol(11)), Double)
                                    Catch ex As Exception
                                        m_Check = 0
                                    End Try
                                    If m_Check > 0 Then
                                        m_Waga += m_Check
                                    End If
                                    m_Kwota += CType(.GetValue(i, Kol(12)), Double)
                                End With
                                If aDane(i, Kol(6)) Is Nothing OrElse aDane(i, Kol(6)).ToString.Length > 4 Then aDane(i, Kol(6)) = "BŁĄD"
                                If aDane(i, Kol(0)) Is Nothing OrElse aDane(i, Kol(0)).ToString.Equals("#") Then aDane(i, Kol(0)) = "JSU01"
                                If aDane(i, Kol(4)) Is Nothing OrElse aDane(i, Kol(4)).ToString.Equals("#") Then aDane(i, Kol(4)) = 999
                                i += 1
                            End While
                            If Math.Round(m_Waga, 1).Equals(0.0) Then
                                If Not Math.Round(m_Kwota, 1).Equals(0.0) Then
                                    ' zapis zlecenia bez partii
                                    Try
                                        dt.Rows.Add(New Object() {MF.Okres.Substring(0, 7), aDane(j, Kol(0)), _
                                                                  m_Key, "", aDane(j, Kol(6)), _
                                                                  aDane(j, Kol(3)), aDane(j, Kol(4)), _
                                                                  aDane(j, Kol(1)), aDane(j, Kol(2)), 0, _
                                                                  m_Kwota, 0, 0, 0, aDane(j, 4), aDane(j, 6), _
                                                                  aDane(j, 8), aDane(j, 10), strWydzial})
                                    Catch ex As Exception
                                        Stop
                                    End Try
                                End If
                            Else
                                i = j
                                While (i <= aDane.GetLength(0) AndAlso aDane.GetValue(i, Kol(5)) IsNot Nothing) AndAlso aDane(i, Kol(5)).ToString.Equals(m_Key)
                                    With aDane
                                        m_Check = CType(aDane(i, Kol(11)), Double) ' waga końcowa
                                        If m_Check > 0 Then
                                            m_Waga3 = CType(aDane(i, Kol(9)), Double)
                                            m_Waga4 = CType(aDane(i, Kol(10)), Double) ' waga
                                            m_Waga5 = CType(aDane(i, Kol(13)), Double) ' waga
                                            dt.Rows.Add(New Object() {MF.Okres.Substring(0, 7), aDane(i, Kol(0)), _
                                                                      m_Key, aDane(i, Kol(7)), aDane(i, Kol(6)), _
                                                                      aDane(i, Kol(3)), aDane(i, Kol(4)), _
                                                                      aDane(i, Kol(1)), aDane(i, Kol(2)), m_Waga4 / 1000, _
                                                                      Math.Round(m_Kwota * m_Check / m_Waga, 2), m_Waga3 / 1000, m_Check / 1000, m_Waga5 / 1000, aDane(i, 4), aDane(i, 6), _
                                                                      aDane(i, 8), aDane(i, 10), strWydzial})
                                        End If
                                    End With
                                    i += 1
                                End While
                            End If
Nastepny:
                            With MF.tspbMain
                                If i < aDane.GetUpperBound(0) AndAlso (i Mod 5).Equals(0) Then _
                                    .Value = i : Application.DoEvents()
                            End With
                        Loop
                    Next iPl
                    For i = 0 To 3
                        Select Case i
                            Case 0
                                ' kontrola i zapis grup materiałowych
                                Dim query = (From p In dt.AsEnumerable _
                                             Select GM = p!GrMater, Opis = p!GrMaterPL, Wydz = p!Wydzial).Distinct().ToArray
                                For Each q In query
                                    Try
                                        With cmd
                                            .CommandText = "INSERT INTO tblGrupyMat (ID, Wydzial, GrMaterPL) " _
                                                    + " VALUES ('" + q.GM + "', '" + q.Wydz + "', '" + q.Opis.ToString.Replace("'", "") + "')"
                                            .ExecuteNonQuery()
                                        End With
                                    Catch ex As Exception
                                    End Try
                                Next q
                            Case 1
                                ' kontrola i zapis grup kalkulacyjnych
                                Dim query = (From p In dt.AsEnumerable _
                                             Select GK = p!GrKalk, GM = p!GrMater, Opis = p!GrKalkPL, Wydz = p!Wydzial).Distinct().ToArray
                                For Each q In query
                                    Try
                                        With cmd
                                            .CommandText = "INSERT INTO tblGrupyKalk (ID, GrMater, Wydzial, GrKalkPL) " _
                                                    + " VALUES ('" + q.GK + "', '" + q.GM + "', '" + q.Wydz + "', '" + q.Opis.ToString.Replace("'", "") + "')"
                                            .ExecuteNonQuery()
                                        End With
                                    Catch ex As Exception
                                    End Try
                                Next q
                            Case 2
                                ' kontrola i zapis materiałów
                                Dim query = (From p In dt.AsEnumerable _
                                             Select ID = p!Material, GK = p!GrKalk, GM = p!GrMater, Opis = p!MaterialPL).Distinct().ToArray
                                For Each q In query
                                    Try
                                        With cmd
                                            .CommandText = "INSERT INTO tblMaterialy (ID, GrMaterID, GrKalkID, MaterialPL) " _
                                                    + " VALUES ('" + q.ID + "', '" + q.GK + "', '" + q.GM + "', '" + q.Opis.ToString.Replace("'", "") + "')"
                                            .ExecuteNonQuery()
                                        End With
                                    Catch ex As Exception
                                    End Try
                                Next q
                            Case 3
                                ' kontrola i zapis klientów
                                Dim query = (From p In dt.AsEnumerable _
                                             Select ID = p!IDKlienta, Nazwa = p!Nazwa).Distinct().ToArray
                                For Each q In query
                                    Try
                                        With cmd
                                            .CommandText = "INSERT INTO tblKlienci (ID, Nazwa) " _
                                                    + " VALUES (" + q.ID.ToString + ", '" + q.Nazwa.ToString.Replace("'", "") + "')"
                                            .ExecuteNonQuery()
                                        End With
                                    Catch ex As Exception
                                    End Try
                                Next q
                        End Select
                    Next i
                    tr.Commit()
                Catch ex As Exception
                    Try
                        tr.Rollback()
                    Catch
                    End Try
                    Throw New ArgumentException(ex.Message)
                End Try
            End Using
            With dt.Columns
                .Remove("GrMaterPL")
                .Remove("GrKalkPL")
                .Remove("MaterialPL")
                .Remove("Nazwa")
                .Remove("Wydzial")
            End With
            da = MF.DA_TI("tblWIP", "Okres, Instalacja, NrZlecenia, Partia")
            da.Update(dt.GetChanges)
            Using New CM
                MessageBox.Show(CO_ACTION + " zakończone pomyślnie", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End Using
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message, CO_ACTION, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
        Finally
            EndOfProcess(Me.btnZWKWIP)
            m_FI = Nothing
            da = Nothing
            dt = Nothing
            cmd = Nothing
            tr = Nothing
        End Try
    End Sub

    Private Sub RZZapasy() Handles btnZapasy.Click

        Dim _Path As String = My.Settings.ToStock
        Dim _File As String = "BWD_Magazyn*"
        Dim _Ext As String = ".xl*"
        Dim m_FI() As FileInfo
        Dim da As OleDbDataAdapter
        Dim dt As New DataTable
        Dim dv As New DataView
        Dim cmd As New OleDbCommand
        Dim tr As OleDbTransaction = Nothing
        Dim i, iR, iPl As Integer
        Dim dblW, dblI As Double
        Dim strSklad, strMater, strKlient, strPartia As String
        Dim strBrak As String = ""
        Dim aDane As Object = Nothing
        Dim Kol() As Integer = Nothing
        Const CO_ACTION As String = "Pobieranie danych o zapasach"
        Try
            If BlokadaAktualizacji("tblZapasy") Then Return
            _Path = CP.GetSourcePath(_Path + _File, _Ext, CO_ACTION)
            If String.IsNullOrEmpty(_Path) Then Return
            With My.Settings
                .ToStock = _Path
                .Save()
            End With
            _File += _Ext
            m_FI = New DirectoryInfo(_Path).GetFiles(_File)
            StartOfProcess(Me.btnZapasy)
            Using cn As New OleDbConnection(MF.cns.ConnectionString)
                cn.Open()
                tr = cn.BeginTransaction
                With cmd
                    .Connection = cn
                    .Transaction = tr
                    .CommandText = "SELECT * FROM tblZapasy WHERE Miesiac = 14"
                    dt.Load(.ExecuteReader)
                    .CommandText = "DELETE FROM tblZapasy WHERE Miesiac = " + MF.dtpOkres.Value.Month.ToString
                    .ExecuteNonQuery()
                End With
                ' sprawdzenie układu kolumn 
                '     1 Skład
                '     3	Rodzaj materiału
                '     5	Grupa materiałowa
                '     7 Grupa kalkulacyjna		
                '  0  9 Materiał		
                '  1 11 Partia	
                '  2 12 Klient		
                '    14 Grupa kosztowa	
                '  3 15 Ilość zapasu całk.	
                '  4 16 Waga magazyn (dane dodatkowe)	
                '    17 Waga netto	
                '    18 Waga brutto	
                '    19 Masa wsadu	
                '    20 Waga odkuwki	'
                '    21 Waga do klienta	
                '  5 22 Wart. wyc. zap.
                With dt.Columns
                    .Add("MaterialPL")
                    .Add("Nazwa")
                End With
                dv = dt.DefaultView
                dv.Sort = "Sklad, Material, KlientID, Partia"
                Try
                    For iPl = 0 To m_FI.GetUpperBound(0)
                        With New Cl4Excel.GetFromXls(m_FI(iPl).FullName, "Table", "I6:J6")
                            Try
                                .Data2Array() : aDane = .Dane
                                If aDane Is Nothing Then Throw New ArgumentException("Brak danych")
                                With aDane(1, 2)
                                    If Not .ToString.Substring(0, 2).Equals(MF.Okres.Substring(5, 2)) OrElse Not .ToString.Substring(3, 4).Equals(MF.Okres.Substring(0, 4)) Then _
                                        Throw New ArgumentException("Wybrany okres nie odpowiada okresowi danych BWD")
                                End With
                                aDane = Nothing
                                .Obszar = "F15:AA" : .Data2Array() : aDane = .Dane
                            Catch ex As Exception
                                Throw New ArgumentException(ex.Message)
                            Finally
                                .Zwolnij()
                            End Try
                        End With
                        If aDane Is Nothing Then Throw New ArgumentException("Plik [" + m_FI(iPl).Name + "] nie spełnia oczekiwań - brak danych")
                        If Not CP.SourceLayoutControl(aDane, "Zapasy", MF.cns, Kol) Then Exit Try
                        FillStatusBar(aDane.GetUpperBound(0), "Zapasy - " + MF.dtpOkres.Text)
                        For i = 2 To aDane.GetUpperBound(0)
                            strSklad = ""
                            If aDane(i, 1) IsNot Nothing Then strSklad = aDane(i, 1).ToString.Replace("#", "")
                            strMater = ""
                            If aDane(i, Kol(0)) IsNot Nothing Then strMater = aDane(i, Kol(0)).ToString
                            strKlient = ""
                            If aDane(i, Kol(2)) IsNot Nothing Then strKlient = aDane(i, Kol(2)).ToString.Replace("#", "999")
                            strPartia = ""
                            If aDane(i, Kol(1)) IsNot Nothing Then strPartia = aDane(i, Kol(1)).ToString.Replace("#", "")
                            dblI = 0 : dblW = 0
                            If aDane(i, Kol(3)) IsNot Nothing Then dblI = aDane(i, Kol(3))
                            If aDane(i, Kol(4)) IsNot Nothing Then dblW = aDane(i, Kol(4))
                            If Not strMater.StartsWith("JF") AndAlso dblW.Equals(0.0) Then dblW = dblI
                            iR = dv.Find(New Object() {strSklad, strMater, CInt(strKlient), strPartia})
                            If iR.Equals(-1) Then
                                dt.Rows.Add(New Object() {MF.dtpOkres.Value.Month, strSklad, strMater, strPartia, CInt(strKlient), dblI, dblW, _
                                                          aDane(i, Kol(5)), aDane(i, Kol(0) + 1), aDane(i, Kol(2) + 1)})
                            Else
                                dv(iR)("Ilosc") = dv(iR)("Ilosc") + dblI
                                dv(iR)("Waga") = dv(iR)("Waga") + dblW
                                dv(iR)("Kwota") = dv(iR)("Kwota") + aDane(i, Kol(5))
                            End If
                            If (i Mod 100).Equals(0) Then _
                                    MF.tspbMain.Value = i : Application.DoEvents()
                        Next i
                    Next iPl
                    FillStatusBar(0, "Kontrola danych stałych...")
                    For i = 0 To 1
                        Select Case i
                            Case 0
                                ' kontrola i zapis materiałów
                                Dim query = (From p In dt.AsEnumerable _
                                             Select ID = p!Material, Opis = p!MaterialPL).Distinct().ToArray
                                For Each q In query
                                    Try
                                        With cmd
                                            .CommandText = "INSERT INTO tblMaterialy (ID, MaterialPL) " _
                                                    + " VALUES ('" + q.ID + "', '" + q.Opis.ToString.Replace("'", "") + "')"
                                            .ExecuteNonQuery()
                                        End With
                                        strBrak += "Materiał " + q.ID.ToString + vbCrLf
                                    Catch ex As Exception
                                    End Try
                                Next q
                            Case 1
                                ' kontrola i zapis klientów
                                Dim query = (From p In dt.AsEnumerable _
                                             Select ID = p!KlientID, Nazwa = p!Nazwa).Distinct().ToArray
                                For Each q In query
                                    Try
                                        With cmd
                                            .CommandText = "INSERT INTO tblKlienci (ID, Nazwa) " _
                                                    + " VALUES (" + q.ID.ToString + ", '" + q.Nazwa.ToString.Replace("'", "") + "')"
                                            .ExecuteNonQuery()
                                        End With
                                        strBrak += "Klienta " + q.ID.ToString + vbCrLf
                                    Catch ex As Exception
                                    End Try
                                Next q
                        End Select
                    Next i
                    tr.Commit()
                Catch ex As Exception
                    Try
                        tr.Rollback()
                    Catch
                    End Try
                    Throw New ArgumentException(ex.Message)
                End Try
            End Using
        With dt.Columns
            .Remove("MaterialPL")
            .Remove("Nazwa")
        End With
        da = MF.DA_TI("tblZapasy", "Miesiac, Sklad, Material, Partia, KlientID")
        da.Update(dt.GetChanges)
        Using New CM
            MessageBox.Show(CO_ACTION + " zakończone pomyślnie", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Using
        If Not String.IsNullOrEmpty(strBrak) Then
            MF.txtKomunikat.Text = "Uzupełnij:" + strBrak
            KomunikatShow()
        End If
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message, CO_ACTION, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
        Finally
            EndOfProcess(Me.btnZapasy)
            m_FI = Nothing
            da = Nothing
            dt = Nothing
            dv = Nothing
            cmd = Nothing
            tr = Nothing
        End Try
    End Sub

    Private Sub RZZatrudnienie() Handles btnZatrudnienie.Click

        Dim _Path As String = My.Settings.ToZatrudnienie
        Dim _File As String = "Zatrudnienie_" + MF.Okres.Substring(0, 7)
        Dim _Ext As String = ".xlsx"
        Dim dt As New DataTable
        Dim dv As New DataView
        Dim da As OleDbDataAdapter = MF.DA_TI("tblZatrudnienie", "Mc, Mpk, Grupa", "Mc = " + MF.dtpOkres.Value.Month.ToString)
        Dim i, j As Integer
        Dim aDane As Object
        Const CO_ACTION As String = "Pobieranie danych o zatrudnieniu"
        Try
            If BlokadaAktualizacji("tblZatrudnienie") Then Return
            _Path = CP.GetSourcePath(_Path + _File, _Ext, CO_ACTION)
            If String.IsNullOrEmpty(_Path) Then Throw New ArgumentException("Brak pliku [Zatrudnienie_" + Format(MF.dtpOkres.Value, "yyyy-MM") + "]")
            With My.Settings
                .ToZatrudnienie = _Path
                .Save()
            End With
            StartOfProcess(Me.btnZatrudnienie)
            With New Cl4Excel.GetFromXls(_Path + _File + _Ext, "mpk")
                .Data2Array()
                aDane = .Dane
                .Zwolnij()
            End With
            If aDane Is Nothing Then Throw New ArgumentException("Brak danych")
            da.Fill(dt)
            For Each r As DataRow In dt.Rows
                r.Delete()
            Next r
            dv = dt.DefaultView
            dv.Sort = "Mpk, Grupa"
            For i = 2 To aDane.GetUpperBound(0)
                j = dv.Find(New Object() {aDane(i, 6), aDane(i, 9)})
                If j.Equals(-1) Then
                    dt.Rows.Add(New Object() {MF.dtpOkres.Value.Month, aDane(i, 6), aDane(i, 9), 1})
                Else
                    dv(j)("Ilosc") = CInt(dv(j)("Ilosc")) + 1
                End If
            Next i
            da.Update(dt.GetChanges)
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message, CO_ACTION, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
        Finally
            ClearStatus()
            EndOfProcess(Me.btnZatrudnienie)
            da = Nothing
            dt = Nothing
            dv = Nothing
            aDane = Nothing
            GC.Collect()
        End Try
    End Sub

    Private Sub RZZWKZlecenia() Handles btnZWKZlecenia.Click

        Dim _Path As String = My.Settings.ToZWKZlecenia
        Dim _Ext As String = ".xl*"
        Dim strBrak As String = ""
        Dim da As OleDbDataAdapter
        Dim dt As New DataTable
        Dim dv As DataView
        Dim v As DataRowView
        Dim dtM As New DataTable
        Dim dvM As New DataView
        Dim cmd As New OleDbCommand
        Dim tr As OleDbTransaction = Nothing
        Dim i, j, iPl As Integer
        Dim m_Klient As Integer
        Dim m_RK, m_MatWs, m_MPK, m_Sklad As String
        Dim m_FI() As FileInfo
        Dim m_Okres() As Object
        Dim aDane As Object
        Dim Kol() As Integer = Nothing
        Dim m_Day1, m_Day2 As Date
        Dim m_Ilo1, m_Ilo2, m_War1, m_War2, m_Tn1, m_Tn2, m_Wsad1, m_Wsad2 As Double
        Const CO_ACTION As String = "Pobieranie danych o zleceniach ZWK "
        Try
            If BlokadaAktualizacji("tblZWKZlecenia") Then Return
            ' Sprawdź, czy są pliki
            _Path = CP.GetSourcePath(_Path + "BWD_ZleceniaHistZWK*", _Ext, CO_ACTION)
            If String.IsNullOrEmpty(_Path) Then Return
            With My.Settings
                .ToZWKZlecenia = _Path
                .Save()
            End With
            StartOfProcess(Me.btnZWKZlecenia)
            m_FI = New DirectoryInfo(_Path).GetFiles("BWD_ZleceniaHistZWK*" + _Ext)
            Using cn As New OleDbConnection(MF.cns.ConnectionString)
                cn.Open()
                tr = cn.BeginTransaction
                With cmd
                    .Connection = cn
                    .Transaction = tr
                    .CommandText = "DELETE FROM tblZWKZlecenia WHERE Mc = " + m_Day1.Month.ToString
                    .ExecuteNonQuery()
                    .CommandText = "SELECT * FROM tblZWKZlecenia WHERE Mc = 14"
                    dt.Load(.ExecuteReader)
                    .CommandText = "SELECT ID, Waga FROM tblMaterialy WHERE LEFT(ID,3) = 'JFI' OR LEFT(ID,3) = 'JFN'"
                    dtM.Load(.ExecuteReader)
                    dvM = dtM.DefaultView
                    dvM.Sort = "ID"
                End With
                With dt.Columns
                    .Add("GrMaterOpis")
                    .Add("MaterialOpis")
                    .Add("RKOpis")
                    .Add("KlientOpis")
                End With
                dv = dt.DefaultView
                dv.Sort = "NrZlecenia, RK, MPK, Wsad, Sklad"
                Try
                    With MF.dtpOkres.Value
                        m_Day1 = DateSerial(.Year, .Month, 1)
                        With .AddMonths(1)
                            m_Day2 = DateSerial(.Year, .Month, 0)
                        End With
                    End With
                    For iPl = 0 To m_FI.GetUpperBound(0)
                        With New Cl4Excel.GetFromXls(m_FI(iPl).FullName, "BWD", "J9:J10")
                            .Data2Array()
                            aDane = .Dane
                            m_Okres = Split(aDane(2, 1).ToString, "..")
                            If IsDate(m_Okres(0)) AndAlso IsDate(m_Okres(1)) Then
                                If Not (CDate(m_Okres(0)).CompareTo(m_Day1).Equals(0) OrElse CDate(m_Okres(1)).CompareTo(m_Day2).Equals(0)) _
                                    Then .Zwolnij() : Throw New ArgumentException("Wybrany okres nie odpowiada okresowi danych BWD")
                            Else
                                If Not (CInt(m_Okres(0).Substring(0, 2)).Equals(m_Day1.Month) AndAlso CInt(m_Okres(0).Substring(3, 4)).Equals(m_Day1.Year)) _
                                    Then .Zwolnij() : Throw New ArgumentException("Wybrany okres nie odpowiada okresowi danych BWD")
                            End If
                            .Zakres = "F15:AC"
                            .Data2Array()
                            aDane = .Dane
                            .Zwolnij()
                            If aDane Is Nothing Then _
                                Throw New ArgumentException("Plik +[" + m_FI(iPl).Name + "] nie spełnia oczekiwań")
                        End With
                        ' sprawdzenie układu kolumn 
                        If Not CP.SourceLayoutControl(aDane, "BWD_ZWK_Zlec", MF.cns, Kol) Then Exit Try
                        FillStatusBar(aDane.GetUpperBound(0), "Przetwarzanie danych BWD...")
                        ' 0  1	GrupaMateriałowa			
                        ' 1  3	GrupaKalkulacyjna			
                        ' 2  4	Materiał			
                        ' 3  6	Rodzajkosztów			
                        ' 4  8	Miejscepowstawaniakosztów			
                        ' 5 10	ZlecenieProdukcyjne			
                        ' 6 12	Klient			
                        ' 7 14	MateriałZużyty			
                        ' 8 16	Skład			
                        ' 9 18	Waganetto			
                        '10 19	Ilośćrzeczywista			
                        '11 20	Wartośćrzeczywista			
                        '12 21	TnProdukcjiREAL			
                        '13 22	Ilośćplanowana			
                        '14 23	Wartośćplanowana			
                        '15 24	TnProdukcjiPLAN		
                        For i = 2 To aDane.GetUpperBound(0)
                            If aDane(i, Kol(0)) Is Nothing OrElse String.IsNullOrEmpty(aDane(i, Kol(0)).ToString) Then Exit For
                            aDane(i, Kol(3)) = aDane(i, Kol(3)).ToString.Replace("S35/", "").Replace("#", "")
                            If aDane(i, Kol(4)) Is Nothing Then
                                m_MPK = ""
                            Else
                                m_MPK = aDane(i, Kol(4)).ToString.Replace("S35/", "").Replace("#", "")
                            End If
                            If aDane(i, Kol(6)) Is Nothing OrElse aDane(i, Kol(6)).ToString.Contains("#") Then
                                m_Klient = 999
                            Else
                                m_Klient = CInt(aDane(i, Kol(6)).ToString)
                            End If
                            If aDane(i, Kol(7)) Is Nothing Then
                                m_MatWs = ""
                            Else
                                m_MatWs = aDane(i, Kol(7)).ToString.Replace("#", "")
                            End If
                            If aDane(i, Kol(8)) Is Nothing Then
                                m_Sklad = ""
                            Else
                                m_Sklad = aDane(i, Kol(8)).ToString.Replace("C35/", "").Replace("#", "")
                            End If
                            m_Wsad1 = 0
                            m_Wsad2 = 0
                            If m_MatWs.StartsWith("JFI") OrElse m_MatWs.StartsWith("JFN") Then
                                j = dvM.Find(m_MatWs)
                                If j.Equals(-1) Then
                                    Using New CM
                                        MessageBox.Show("Brak materiału w słowniku", m_MatWs, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                    End Using
                                ElseIf Not IsDBNull(dvM(j)("Waga")) Then
                                    m_Wsad1 = dvM(j)("Waga")
                                    m_Wsad2 = dvM(j)("Waga")
                                End If
                            End If
                            m_RK = aDane(i, Kol(3))
                            Try
                                m_Ilo1 = aDane(i, Kol(10))
                            Catch ex As Exception
                                m_Ilo1 = 0
                            End Try
                            Try
                                m_Ilo2 = aDane(i, Kol(13))
                            Catch ex As Exception
                                m_Ilo2 = 0
                            End Try
                            Try
                                m_War1 = aDane(i, Kol(11))
                            Catch ex As Exception
                                m_War1 = 0
                            End Try
                            Try
                                m_War2 = aDane(i, Kol(14))
                            Catch ex As Exception
                                m_War2 = 0
                            End Try
                            Try
                                m_Tn1 = aDane(i, Kol(12))
                            Catch ex As Exception
                                m_Tn1 = 0
                            End Try
                            Try
                                m_Tn2 = aDane(i, Kol(15))
                            Catch ex As Exception
                                m_Tn2 = 0
                            End Try
                            If m_RK.StartsWith("61") Then
                                If m_MatWs.Length > 2 Then
                                    If m_MatWs.ToString.StartsWith("JFR") Then
                                        m_Wsad1 = m_Ilo1 / 1000
                                        m_Wsad2 = m_Ilo2 / 1000
                                        m_Ilo1 /= 1000
                                        m_Ilo2 /= 1000
                                    Else
                                        m_Wsad1 *= m_Ilo1
                                        m_Ilo1 = m_Ilo1 * aDane(i, Kol(9)) / 1000
                                        m_Wsad2 *= m_Ilo2
                                        m_Ilo2 = m_Ilo2 * aDane(i, Kol(9)) / 1000
                                    End If
                                End If
                            ElseIf m_RK.StartsWith("711015") Then
                                m_Ilo1 /= 1000
                                m_Ilo2 /= 1000
                            ElseIf m_RK.StartsWith("711050") Then
                                m_Ilo1 /= 1000
                                m_Ilo2 /= 1000
                            End If
                            j = dv.Find(New Object() {aDane(i, Kol(5)), m_RK, m_MPK, m_MatWs, m_Sklad})
                            If j.Equals(-1) Then
                                dt.Rows.Add(New Object() {MF.dtpOkres.Value.Month, aDane(i, Kol(5)), m_RK, m_MPK, _
                                                          m_MatWs, m_Klient, aDane(i, Kol(2)), aDane(i, Kol(0)), _
                                                          aDane(i, Kol(1)), m_Sklad, aDane(i, Kol(9)), m_Ilo1, _
                                                          m_War1, m_Tn1, m_Ilo2, m_War2, m_Tn2, m_Wsad1, m_Wsad2, _
                                                          aDane(i, Kol(0) + 1), aDane(i, Kol(2) + 1), _
                                                          aDane(i, Kol(3) + 1), aDane(i, Kol(6) + 1)})
                            Else
                                dv(j)("Waga") = dv(j)("Waga") + aDane(i, Kol(9))
                                dv(j)("IloscRzecz") = dv(j)("IloscRzecz") + m_Ilo1
                                dv(j)("WartoscRzecz") = dv(j)("WartoscRzecz") + m_War1
                                dv(j)("TnProdukcjiReal") = dv(j)("TnProdukcjiReal") + m_Tn1
                                dv(j)("IloscPlan") = dv(j)("IloscPlan") + m_Ilo2
                                dv(j)("WartoscPlan") = dv(j)("WartoscPlan") + m_War2
                                dv(j)("TnProdukcjiReal") = dv(j)("TnProdukcjiReal") + m_Tn2
                                dv(j)("WsadReal") = dv(j)("WsadReal") + m_Wsad1
                                dv(j)("WsadPlan") = dv(j)("WsadPlan") + m_Wsad2
                            End If
                            If (i Mod 100).Equals(0) Then _
                                   MF.tspbMain.Value = i : Application.DoEvents()
                        Next i
                    Next iPl
                    For i = 0 To 3
                        Select Case i
                            Case 0
                                ' materiały
                                Dim query = (From p In dt.AsEnumerable _
                                             Where Not (IsDBNull(p("Material")) OrElse String.IsNullOrEmpty(p("Material"))) _
                                             Select ID = p("Material"), Opis = p("MaterialOpis"), GrMater = p("GrMater"), _
                                             GrKalk = p("GrKalk")).Distinct().ToArray()
                                With cmd
                                    For Each q In query
                                        Try
                                            .CommandText = "INSERT INTO tblMaterialy (ID, MaterialPL, GrMaterID, GrKalkID) " _
                                                + "VALUES ('" + q.ID + "', '" + q.Opis.ToString.Replace("'", "") + "', '" + q.GrMater _
                                                + "', '" + q.GrKalk + "')"
                                            .ExecuteNonQuery()
                                            If Not strBrak.Contains("materiałach") Then strBrak += vbCrLf + "Dane o materiałach: "
                                            strBrak += q.ID + ", "
                                        Catch ex As Exception
                                        End Try
                                    Next q
                                End With
                            Case 1
                                ' grupa materiałowa
                                Dim query = (From p In dt.AsEnumerable _
                                             Where Not (IsDBNull(p("GrMater")) OrElse String.IsNullOrEmpty(p("GrMater"))) _
                                             Select ID = p("GrMater"), Opis = p("GrMaterOpis")).Distinct().ToArray()
                                With cmd
                                    For Each q In query
                                        Try
                                            .CommandText = "INSERT INTO tblGrupyMat (ID, GrMaterPL) " _
                                                + "VALUES ('" + q.ID + "', '" + q.Opis.ToString.Replace("'", "") + "')"
                                            .ExecuteNonQuery()
                                            If Not strBrak.Contains("grupach materiałowych") Then strBrak += vbCrLf + "Dane o grupach materiałowych: "
                                            strBrak += q.ID.ToString + ", "
                                        Catch ex As Exception
                                        End Try
                                    Next q
                                End With
                            Case 2
                                ' grupa kalkulacyjna
                                Dim query = (From p In dt.AsEnumerable _
                                             Where Not (IsDBNull(p("GrKalk")) OrElse String.IsNullOrEmpty(p("GrKalk"))) _
                                             Select ID = p("GrKalk"), Opis = "", GrMater = p("GrMater")).Distinct().ToArray()
                                With cmd
                                    For Each q In query
                                        Try
                                            .CommandText = "INSERT INTO tblGrupyKalk (ID, GrMater, GrKalkPL) " _
                                                + "VALUES ('" + q.ID + "', '" + q.GrMater + "', '" + q.Opis + "')"
                                            .ExecuteNonQuery()
                                            If Not strBrak.Contains("grupach kalkulacyjnych") Then strBrak += vbCrLf + "Dane o grupach kalkulacyjnych: "
                                            strBrak += q.ID.ToString + ", "
                                        Catch ex As Exception
                                        End Try
                                    Next q
                                End With
                            Case 3
                                ' klient
                                Dim query = (From p In dt.AsEnumerable _
                                             Where Not (IsDBNull(p("IDKlienta")) OrElse String.IsNullOrEmpty(p("IDKlienta").ToString)) _
                                             Select ID = p("IDKlienta"), Opis = p("KlientOpis")).Distinct().ToArray()
                                With cmd
                                    For Each q In query
                                        Try
                                            .CommandText = "INSERT INTO tblKlienci (ID, Nazwa) " _
                                                + "VALUES (" + q.ID.ToString + ", '" + q.Opis + "')"
                                            .ExecuteNonQuery()
                                            If Not strBrak.Contains("klientach") Then strBrak += vbCrLf + "Dane o klientach: "
                                            strBrak += q.ID.ToString + ", "
                                        Catch ex As Exception
                                        End Try
                                    Next q
                                End With
                        End Select
                    Next i
                    tr.Commit()
                Catch ex As Exception
                    Try
                        tr.Rollback()
                    Catch
                    End Try
                    Throw New ArgumentException(ex.Message)
                End Try
            End Using
            With dt.Columns
                .Remove("GrMaterOpis")
                .Remove("MaterialOpis")
                .Remove("RKOpis")
                .Remove("KlientOpis")
            End With
            da = MF.DA_TI("tblZWKZlecenia", "Mc, NrZlecenia, RK, MPK, Wsad, Sklad")
            da.Update(dt.GetChanges)
            Using New CM
                MessageBox.Show(CO_ACTION + " zakończone pomyślnie", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End Using
            If Not String.IsNullOrEmpty(strBrak) Then
                MF.txtKomunikat.Text = "Uzupełnij:" + strBrak
                KomunikatShow()
            End If
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message, CO_ACTION, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
        Finally
            EndOfProcess(Me.btnZWKZlecenia)
            m_FI = Nothing
            da = Nothing
            dt = Nothing
            cmd = Nothing
            tr = Nothing
            dv = Nothing
            v = Nothing
            dtM = Nothing
            dvM = Nothing
        End Try
    End Sub

End Class