﻿
Imports System.Windows.Forms

Public Class ZaOkres

    Private OdDnia As DateTime
    Private DoDnia As DateTime

    Public Property Poczatek() As DateTime
        Get
            Return OdDnia
        End Get
        Set(ByVal value As DateTime)
            OdDnia = value
        End Set
    End Property

    Public Property Koniec() As DateTime
        Get
            Return DoDnia
        End Get
        Set(ByVal value As DateTime)
            DoDnia = value
        End Set
    End Property

    Private Sub OK_Button_Click() Handles btnOk.Click

        With Me
            If .dtpOd.Value > .dtpDo.Value Then MessageBox.Show("Ustaw dobrze daty", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation) : Return
            .Poczatek = .dtpOd.Value
            .Koniec = .dtpDo.Value
            .DialogResult = System.Windows.Forms.DialogResult.OK
            .Close()
        End With
    End Sub

    Private Sub Cancel_Button_Click() Handles btnExit.Click

        With Me
            .DialogResult = System.Windows.Forms.DialogResult.Cancel
            .Close()
        End With
    End Sub

    Private Sub Me_Load() Handles Me.Load

        With Me
            If OdDnia < CDate("1900-01-01") Then
                OdDnia = frmMain.dtpOkres.Value
                DoDnia = frmMain.dtpOkres.Value
            End If
            If OdDnia.Equals(DoDnia) Then
                OdDnia = DateSerial(Year(OdDnia), Month(OdDnia), 1)
                DoDnia = DateSerial(Year(OdDnia), Month(OdDnia) + 1, 0)
            End If
            With .dtpOd
                .Value = OdDnia
                AddHandler .KeyPress, AddressOf CP.EnterKeyPressed
            End With
            With .dtpDo
                .Value = DoDnia
                AddHandler .KeyPress, AddressOf CP.EnterKeyPressed
            End With
        End With
    End Sub

End Class
