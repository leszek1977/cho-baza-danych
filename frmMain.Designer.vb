﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.MainMenu = New System.Windows.Forms.MenuStrip
        Me.tsmDane = New System.Windows.Forms.ToolStripMenuItem
        Me.tsm_DP = New System.Windows.Forms.ToolStripMenuItem
        Me.tsm_DPw = New System.Windows.Forms.ToolStripMenuItem
        Me.tsm_DPp = New System.Windows.Forms.ToolStripMenuItem
        Me.tsm_DPpC = New System.Windows.Forms.ToolStripMenuItem
        Me.tsm_DPps = New System.Windows.Forms.ToolStripMenuItem
        Me.tsm_DPb = New System.Windows.Forms.ToolStripMenuItem
        Me.tsm_DPbC = New System.Windows.Forms.ToolStripMenuItem
        Me.tsm_DPbs = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator23 = New System.Windows.Forms.ToolStripSeparator
        Me.tsm_DPk = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
        Me.tsm_Dk = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmWykonania = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmtblSprzedaz = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmtblAmortyzacja = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmtblTransport = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator10 = New System.Windows.Forms.ToolStripSeparator
        Me.tsmtblZWPC = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmtblMedia = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator
        Me.tsmtblZapasy = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmtblNaleznosci = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmtblWIP = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmtblZWKZlecenia = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator16 = New System.Windows.Forms.ToolStripSeparator
        Me.tsmtblZatrudnienie = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmPlanu = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmtblPLSprzedaz = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator14 = New System.Windows.Forms.ToolStripSeparator
        Me.tsmtblPLZWPC = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmtblPLMedia = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator
        Me.tsmtblPLZatrudnienie = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmBudzetu = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmtblBUSprzedaz = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator15 = New System.Windows.Forms.ToolStripSeparator
        Me.tsmtblBUZWPC = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmtblBUMedia = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator8 = New System.Windows.Forms.ToolStripSeparator
        Me.tsmtblBUZatrudnienie = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmtblKursyWalut = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmtblBlokady = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmBlokowanieProcesow = New System.Windows.Forms.ToolStripSeparator
        Me.tsmtblGrupyKalk = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmtblGrKoszt = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmtblGrupyMat = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmtblRodzajeMat = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmtblMaterialy = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator7 = New System.Windows.Forms.ToolStripSeparator
        Me.tsmtblMagazyny = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator
        Me.tsmtblKlienci = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmtblDostawcy = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator9 = New System.Windows.Forms.ToolStripSeparator
        Me.tsmtblInstalacje = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmtblINEF = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmtblCALCO = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmtblMPK = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmtblRK = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmtblGEn = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator6 = New System.Windows.Forms.ToolStripSeparator
        Me.tsmtblFormuly = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmtblSources = New System.Windows.Forms.ToolStripMenuItem
        Me.tsm_Df = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.tsm_DB = New System.Windows.Forms.ToolStripMenuItem
        Me.tsm_Dm = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmExport = New System.Windows.Forms.ToolStripMenuItem
        Me.tsm_Et = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmAktualizacjaZesztowBezWOT = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmZeszytySprzedazy = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator21 = New System.Windows.Forms.ToolStripSeparator
        Me.tsm_Em = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator11 = New System.Windows.Forms.ToolStripSeparator
        Me.tsmExKoszty = New System.Windows.Forms.ToolStripMenuItem
        Me.tsm_Edp = New System.Windows.Forms.ToolStripMenuItem
        Me.tsm_Edo = New System.Windows.Forms.ToolStripMenuItem
        Me.tsm_EdI = New System.Windows.Forms.ToolStripMenuItem
        Me.tsm_Eds = New System.Windows.Forms.ToolStripMenuItem
        Me.tsm_EdC = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator24 = New System.Windows.Forms.ToolStripSeparator
        Me.tsm_Edb = New System.Windows.Forms.ToolStripMenuItem
        Me.tsm_Edu = New System.Windows.Forms.ToolStripMenuItem
        Me.tsm_En = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator18 = New System.Windows.Forms.ToolStripSeparator
        Me.tsm_Eh = New System.Windows.Forms.ToolStripMenuItem
        Me.tsm_Ehs = New System.Windows.Forms.ToolStripMenuItem
        Me.tsm_Est = New System.Windows.Forms.ToolStripMenuItem
        Me.tsm_Es = New System.Windows.Forms.ToolStripMenuItem
        Me.tsm_Ei = New System.Windows.Forms.ToolStripMenuItem
        Me.tsm_Ep = New System.Windows.Forms.ToolStripMenuItem
        Me.tsm_ER = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator17 = New System.Windows.Forms.ToolStripSeparator
        Me.tsm_Eg = New System.Windows.Forms.ToolStripMenuItem
        Me.tsm_Eu = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator22 = New System.Windows.Forms.ToolStripSeparator
        Me.tsm_Ez = New System.Windows.Forms.ToolStripMenuItem
        Me.tsm_Ea = New System.Windows.Forms.ToolStripMenuItem
        Me.tsm_Eo = New System.Windows.Forms.ToolStripMenuItem
        Me.tsm_Ew = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmPomocne = New System.Windows.Forms.ToolStripMenuItem
        Me.tsm_Pd = New System.Windows.Forms.ToolStripMenuItem
        Me.tsm_Pf = New System.Windows.Forms.ToolStripMenuItem
        Me.tsm_Pn = New System.Windows.Forms.ToolStripMenuItem
        Me.tsm_PS = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator19 = New System.Windows.Forms.ToolStripSeparator
        Me.tsm_PK = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmAdministrator = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmTworzenieDB = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmZmianaUstawien = New System.Windows.Forms.ToolStripMenuItem
        Me.tscmbDostepy = New System.Windows.Forms.ToolStripComboBox
        Me.ToolStripSeparator12 = New System.Windows.Forms.ToolStripSeparator
        Me.tsmWeryfSprzWydz = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmAdPobierzSprzedaz = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmOdswiezAmortyzacje = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmAdEBiTDA = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmTransportBezSprzedazy = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator13 = New System.Windows.Forms.ToolStripSeparator
        Me.tsmWeryfDostawcow = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmWeryfikacjaMaterialow = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmWeryfikacjaMagazynow = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmWeryfikacjaINEFCalCo = New System.Windows.Forms.ToolStripMenuItem
        Me.tsm_Test = New System.Windows.Forms.ToolStripMenuItem
        Me.tsm_H = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmAnalizaZrodel = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmAdHoc = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmPobierzDaneSprzZBW = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmPobierzKosztyArchiwalne = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmPobierzKosztyArchiwalne2 = New System.Windows.Forms.ToolStripMenuItem
        Me.tsm_PobierzTransport20062012 = New System.Windows.Forms.ToolStripMenuItem
        Me.tsm_mKT = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmXlsToTblTransport = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmWstawKonta = New System.Windows.Forms.ToolStripMenuItem
        Me.ssMain = New System.Windows.Forms.StatusStrip
        Me.tspbMain = New System.Windows.Forms.ToolStripProgressBar
        Me.tsslMain = New System.Windows.Forms.ToolStripStatusLabel
        Me.tsslLicznik = New System.Windows.Forms.ToolStripStatusLabel
        Me.ttMain = New System.Windows.Forms.ToolTip(Me.components)
        Me.MainHelp = New System.Windows.Forms.HelpProvider
        Me.dtpOkres = New MyControls.DateTimePickerReadOnly
        Me.txtKomunikat = New System.Windows.Forms.TextBox
        Me.gbParametry = New System.Windows.Forms.GroupBox
        Me.txtPaWydzialy = New System.Windows.Forms.TextBox
        Me.lstPaWybor = New System.Windows.Forms.CheckedListBox
        Me.lblPaParent = New System.Windows.Forms.Label
        Me.btnPaOk = New System.Windows.Forms.Button
        Me.txtPaRok2 = New System.Windows.Forms.TextBox
        Me.cmbPaMiesiace2 = New System.Windows.Forms.ComboBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtPaRok1 = New System.Windows.Forms.TextBox
        Me.cmbPaMiesiace1 = New System.Windows.Forms.ComboBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.gbZmianaNazwyPlikow = New System.Windows.Forms.GroupBox
        Me.lblZFNP = New System.Windows.Forms.Label
        Me.btnZFNP = New System.Windows.Forms.Button
        Me.txtZFNP2 = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtZFNP1 = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.MainMenu.SuspendLayout()
        Me.ssMain.SuspendLayout()
        Me.gbParametry.SuspendLayout()
        Me.gbZmianaNazwyPlikow.SuspendLayout()
        Me.SuspendLayout()
        '
        'MainMenu
        '
        Me.MainMenu.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.MainMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmDane, Me.tsmExport, Me.tsmPomocne, Me.tsmAdministrator, Me.tsm_H})
        Me.MainMenu.Location = New System.Drawing.Point(0, 0)
        Me.MainMenu.Name = "MainMenu"
        Me.MainMenu.Size = New System.Drawing.Size(801, 26)
        Me.MainMenu.TabIndex = 0
        Me.MainMenu.Text = "MenuStrip1"
        '
        'tsmDane
        '
        Me.tsmDane.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsm_DP, Me.ToolStripSeparator2, Me.tsm_Dk, Me.tsm_Df, Me.ToolStripSeparator1, Me.tsm_DB, Me.tsm_Dm})
        Me.tsmDane.Name = "tsmDane"
        Me.tsmDane.Size = New System.Drawing.Size(54, 22)
        Me.tsmDane.Text = "&Dane"
        '
        'tsm_DP
        '
        Me.tsm_DP.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsm_DPw, Me.tsm_DPp, Me.tsm_DPb, Me.ToolStripSeparator23, Me.tsm_DPk})
        Me.tsm_DP.Name = "tsm_DP"
        Me.tsm_DP.Size = New System.Drawing.Size(225, 22)
        Me.tsm_DP.Text = "&Pobierz"
        '
        'tsm_DPw
        '
        Me.tsm_DPw.Name = "tsm_DPw"
        Me.tsm_DPw.Size = New System.Drawing.Size(149, 22)
        Me.tsm_DPw.Text = "&wykonanie"
        '
        'tsm_DPp
        '
        Me.tsm_DPp.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsm_DPpC, Me.tsm_DPps})
        Me.tsm_DPp.Name = "tsm_DPp"
        Me.tsm_DPp.Size = New System.Drawing.Size(149, 22)
        Me.tsm_DPp.Text = "&plan"
        '
        'tsm_DPpC
        '
        Me.tsm_DPpC.Name = "tsm_DPpC"
        Me.tsm_DPpC.Size = New System.Drawing.Size(178, 22)
        Me.tsm_DPpC.Text = "&CHO"
        '
        'tsm_DPps
        '
        Me.tsm_DPps.Name = "tsm_DPps"
        Me.tsm_DPps.Size = New System.Drawing.Size(178, 22)
        Me.tsm_DPps.Text = "&sprzedaży ZWK"
        '
        'tsm_DPb
        '
        Me.tsm_DPb.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsm_DPbC, Me.tsm_DPbs})
        Me.tsm_DPb.Name = "tsm_DPb"
        Me.tsm_DPb.Size = New System.Drawing.Size(149, 22)
        Me.tsm_DPb.Text = "&budżet"
        '
        'tsm_DPbC
        '
        Me.tsm_DPbC.Name = "tsm_DPbC"
        Me.tsm_DPbC.Size = New System.Drawing.Size(178, 22)
        Me.tsm_DPbC.Text = "&CHO"
        '
        'tsm_DPbs
        '
        Me.tsm_DPbs.Name = "tsm_DPbs"
        Me.tsm_DPbs.Size = New System.Drawing.Size(178, 22)
        Me.tsm_DPbs.Text = "&sprzedaży ZWK"
        '
        'ToolStripSeparator23
        '
        Me.ToolStripSeparator23.Name = "ToolStripSeparator23"
        Me.ToolStripSeparator23.Size = New System.Drawing.Size(146, 6)
        '
        'tsm_DPk
        '
        Me.tsm_DPk.Name = "tsm_DPk"
        Me.tsm_DPk.Size = New System.Drawing.Size(149, 22)
        Me.tsm_DPk.Text = "&kursy walut"
        Me.tsm_DPk.ToolTipText = "Przenosi kursy podstawowych walut z W:\Costs\CELSA\Wykonyyyy\Rzeczyyyy\Poz_yyyyrz" & _
            ".xls"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(222, 6)
        '
        'tsm_Dk
        '
        Me.tsm_Dk.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmWykonania, Me.tsmPlanu, Me.tsmBudzetu, Me.tsmtblKursyWalut, Me.tsmtblBlokady, Me.tsmBlokowanieProcesow, Me.tsmtblGrupyKalk, Me.tsmtblGrKoszt, Me.tsmtblGrupyMat, Me.tsmtblRodzajeMat, Me.tsmtblMaterialy, Me.ToolStripSeparator7, Me.tsmtblMagazyny, Me.ToolStripSeparator3, Me.tsmtblKlienci, Me.tsmtblDostawcy, Me.ToolStripSeparator9, Me.tsmtblInstalacje, Me.tsmtblINEF, Me.tsmtblCALCO, Me.tsmtblMPK, Me.tsmtblRK, Me.tsmtblGEn, Me.ToolStripSeparator6, Me.tsmtblFormuly, Me.tsmtblSources})
        Me.tsm_Dk.Name = "tsm_Dk"
        Me.tsm_Dk.Size = New System.Drawing.Size(225, 22)
        Me.tsm_Dk.Text = "Po&każ tabelę"
        '
        'tsmWykonania
        '
        Me.tsmWykonania.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmtblSprzedaz, Me.tsmtblAmortyzacja, Me.tsmtblTransport, Me.ToolStripSeparator10, Me.tsmtblZWPC, Me.tsmtblMedia, Me.ToolStripSeparator4, Me.tsmtblZapasy, Me.tsmtblNaleznosci, Me.tsmtblWIP, Me.tsmtblZWKZlecenia, Me.ToolStripSeparator16, Me.tsmtblZatrudnienie})
        Me.tsmWykonania.Name = "tsmWykonania"
        Me.tsmWykonania.Size = New System.Drawing.Size(214, 22)
        Me.tsmWykonania.Text = "&Wykonania"
        '
        'tsmtblSprzedaz
        '
        Me.tsmtblSprzedaz.Name = "tsmtblSprzedaz"
        Me.tsmtblSprzedaz.Size = New System.Drawing.Size(162, 22)
        Me.tsmtblSprzedaz.Text = "&sprzedaży"
        '
        'tsmtblAmortyzacja
        '
        Me.tsmtblAmortyzacja.Name = "tsmtblAmortyzacja"
        Me.tsmtblAmortyzacja.Size = New System.Drawing.Size(162, 22)
        Me.tsmtblAmortyzacja.Text = "&amortyzacji"
        '
        'tsmtblTransport
        '
        Me.tsmtblTransport.Name = "tsmtblTransport"
        Me.tsmtblTransport.Size = New System.Drawing.Size(162, 22)
        Me.tsmtblTransport.Text = "&transportu"
        '
        'ToolStripSeparator10
        '
        Me.ToolStripSeparator10.Name = "ToolStripSeparator10"
        Me.ToolStripSeparator10.Size = New System.Drawing.Size(159, 6)
        '
        'tsmtblZWPC
        '
        Me.tsmtblZWPC.Name = "tsmtblZWPC"
        Me.tsmtblZWPC.Size = New System.Drawing.Size(162, 22)
        Me.tsmtblZWPC.Text = "&kosztów"
        '
        'tsmtblMedia
        '
        Me.tsmtblMedia.Name = "tsmtblMedia"
        Me.tsmtblMedia.Size = New System.Drawing.Size(162, 22)
        Me.tsmtblMedia.Text = "&mediów"
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(159, 6)
        '
        'tsmtblZapasy
        '
        Me.tsmtblZapasy.Name = "tsmtblZapasy"
        Me.tsmtblZapasy.Size = New System.Drawing.Size(162, 22)
        Me.tsmtblZapasy.Text = "&zapasów"
        '
        'tsmtblNaleznosci
        '
        Me.tsmtblNaleznosci.Name = "tsmtblNaleznosci"
        Me.tsmtblNaleznosci.Size = New System.Drawing.Size(162, 22)
        Me.tsmtblNaleznosci.Text = "należn&ości"
        '
        'tsmtblWIP
        '
        Me.tsmtblWIP.Name = "tsmtblWIP"
        Me.tsmtblWIP.Size = New System.Drawing.Size(162, 22)
        Me.tsmtblWIP.Text = "ZWK WI&P"
        '
        'tsmtblZWKZlecenia
        '
        Me.tsmtblZWKZlecenia.Name = "tsmtblZWKZlecenia"
        Me.tsmtblZWKZlecenia.Size = New System.Drawing.Size(162, 22)
        Me.tsmtblZWKZlecenia.Text = "ZWK zlece&nia"
        '
        'ToolStripSeparator16
        '
        Me.ToolStripSeparator16.Name = "ToolStripSeparator16"
        Me.ToolStripSeparator16.Size = New System.Drawing.Size(159, 6)
        '
        'tsmtblZatrudnienie
        '
        Me.tsmtblZatrudnienie.Name = "tsmtblZatrudnienie"
        Me.tsmtblZatrudnienie.Size = New System.Drawing.Size(162, 22)
        Me.tsmtblZatrudnienie.Text = "zatr&udnienia"
        '
        'tsmPlanu
        '
        Me.tsmPlanu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmtblPLSprzedaz, Me.ToolStripSeparator14, Me.tsmtblPLZWPC, Me.tsmtblPLMedia, Me.ToolStripSeparator5, Me.tsmtblPLZatrudnienie})
        Me.tsmPlanu.Name = "tsmPlanu"
        Me.tsmPlanu.Size = New System.Drawing.Size(214, 22)
        Me.tsmPlanu.Text = "&Planu"
        '
        'tsmtblPLSprzedaz
        '
        Me.tsmtblPLSprzedaz.Name = "tsmtblPLSprzedaz"
        Me.tsmtblPLSprzedaz.Size = New System.Drawing.Size(153, 22)
        Me.tsmtblPLSprzedaz.Text = "&sprzedaży"
        '
        'ToolStripSeparator14
        '
        Me.ToolStripSeparator14.Name = "ToolStripSeparator14"
        Me.ToolStripSeparator14.Size = New System.Drawing.Size(150, 6)
        '
        'tsmtblPLZWPC
        '
        Me.tsmtblPLZWPC.Name = "tsmtblPLZWPC"
        Me.tsmtblPLZWPC.Size = New System.Drawing.Size(153, 22)
        Me.tsmtblPLZWPC.Text = "&kosztów"
        '
        'tsmtblPLMedia
        '
        Me.tsmtblPLMedia.Name = "tsmtblPLMedia"
        Me.tsmtblPLMedia.Size = New System.Drawing.Size(153, 22)
        Me.tsmtblPLMedia.Text = "&mediów"
        '
        'ToolStripSeparator5
        '
        Me.ToolStripSeparator5.Name = "ToolStripSeparator5"
        Me.ToolStripSeparator5.Size = New System.Drawing.Size(150, 6)
        '
        'tsmtblPLZatrudnienie
        '
        Me.tsmtblPLZatrudnienie.Name = "tsmtblPLZatrudnienie"
        Me.tsmtblPLZatrudnienie.Size = New System.Drawing.Size(153, 22)
        Me.tsmtblPLZatrudnienie.Text = "zatr&udnienia"
        '
        'tsmBudzetu
        '
        Me.tsmBudzetu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmtblBUSprzedaz, Me.ToolStripSeparator15, Me.tsmtblBUZWPC, Me.tsmtblBUMedia, Me.ToolStripSeparator8, Me.tsmtblBUZatrudnienie})
        Me.tsmBudzetu.Name = "tsmBudzetu"
        Me.tsmBudzetu.Size = New System.Drawing.Size(214, 22)
        Me.tsmBudzetu.Text = "&Budżetu"
        '
        'tsmtblBUSprzedaz
        '
        Me.tsmtblBUSprzedaz.Name = "tsmtblBUSprzedaz"
        Me.tsmtblBUSprzedaz.Size = New System.Drawing.Size(153, 22)
        Me.tsmtblBUSprzedaz.Text = "&sprzedaży"
        '
        'ToolStripSeparator15
        '
        Me.ToolStripSeparator15.Name = "ToolStripSeparator15"
        Me.ToolStripSeparator15.Size = New System.Drawing.Size(150, 6)
        '
        'tsmtblBUZWPC
        '
        Me.tsmtblBUZWPC.Name = "tsmtblBUZWPC"
        Me.tsmtblBUZWPC.Size = New System.Drawing.Size(153, 22)
        Me.tsmtblBUZWPC.Text = "&kosztów"
        '
        'tsmtblBUMedia
        '
        Me.tsmtblBUMedia.Name = "tsmtblBUMedia"
        Me.tsmtblBUMedia.Size = New System.Drawing.Size(153, 22)
        Me.tsmtblBUMedia.Text = "&mediów"
        '
        'ToolStripSeparator8
        '
        Me.ToolStripSeparator8.Name = "ToolStripSeparator8"
        Me.ToolStripSeparator8.Size = New System.Drawing.Size(150, 6)
        '
        'tsmtblBUZatrudnienie
        '
        Me.tsmtblBUZatrudnienie.Name = "tsmtblBUZatrudnienie"
        Me.tsmtblBUZatrudnienie.Size = New System.Drawing.Size(153, 22)
        Me.tsmtblBUZatrudnienie.Text = "zatr&udnienia"
        '
        'tsmtblKursyWalut
        '
        Me.tsmtblKursyWalut.Name = "tsmtblKursyWalut"
        Me.tsmtblKursyWalut.Size = New System.Drawing.Size(214, 22)
        Me.tsmtblKursyWalut.Text = "Kursów wa&lut"
        '
        'tsmtblBlokady
        '
        Me.tsmtblBlokady.Name = "tsmtblBlokady"
        Me.tsmtblBlokady.Size = New System.Drawing.Size(214, 22)
        Me.tsmtblBlokady.Text = "Blokowania procesów"
        '
        'tsmBlokowanieProcesow
        '
        Me.tsmBlokowanieProcesow.Name = "tsmBlokowanieProcesow"
        Me.tsmBlokowanieProcesow.Size = New System.Drawing.Size(211, 6)
        '
        'tsmtblGrupyKalk
        '
        Me.tsmtblGrupyKalk.Name = "tsmtblGrupyKalk"
        Me.tsmtblGrupyKalk.Size = New System.Drawing.Size(214, 22)
        Me.tsmtblGrupyKalk.Text = "&Grup kalkulacyjnych"
        '
        'tsmtblGrKoszt
        '
        Me.tsmtblGrKoszt.Name = "tsmtblGrKoszt"
        Me.tsmtblGrKoszt.Size = New System.Drawing.Size(214, 22)
        Me.tsmtblGrKoszt.Text = "Grup &kosztów"
        '
        'tsmtblGrupyMat
        '
        Me.tsmtblGrupyMat.Name = "tsmtblGrupyMat"
        Me.tsmtblGrupyMat.Size = New System.Drawing.Size(214, 22)
        Me.tsmtblGrupyMat.Text = "Grup materiałowyc&h"
        '
        'tsmtblRodzajeMat
        '
        Me.tsmtblRodzajeMat.Name = "tsmtblRodzajeMat"
        Me.tsmtblRodzajeMat.Size = New System.Drawing.Size(214, 22)
        Me.tsmtblRodzajeMat.Text = "&Rodzajów materiałów"
        '
        'tsmtblMaterialy
        '
        Me.tsmtblMaterialy.Name = "tsmtblMaterialy"
        Me.tsmtblMaterialy.Size = New System.Drawing.Size(214, 22)
        Me.tsmtblMaterialy.Text = "M&ateriałów"
        '
        'ToolStripSeparator7
        '
        Me.ToolStripSeparator7.Name = "ToolStripSeparator7"
        Me.ToolStripSeparator7.Size = New System.Drawing.Size(211, 6)
        '
        'tsmtblMagazyny
        '
        Me.tsmtblMagazyny.Name = "tsmtblMagazyny"
        Me.tsmtblMagazyny.Size = New System.Drawing.Size(214, 22)
        Me.tsmtblMagazyny.Text = "Maga&zynów"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(211, 6)
        '
        'tsmtblKlienci
        '
        Me.tsmtblKlienci.Name = "tsmtblKlienci"
        Me.tsmtblKlienci.Size = New System.Drawing.Size(214, 22)
        Me.tsmtblKlienci.Text = "Klien&tów"
        '
        'tsmtblDostawcy
        '
        Me.tsmtblDostawcy.Name = "tsmtblDostawcy"
        Me.tsmtblDostawcy.Size = New System.Drawing.Size(214, 22)
        Me.tsmtblDostawcy.Text = "&Dostawców"
        '
        'ToolStripSeparator9
        '
        Me.ToolStripSeparator9.Name = "ToolStripSeparator9"
        Me.ToolStripSeparator9.Size = New System.Drawing.Size(211, 6)
        '
        'tsmtblInstalacje
        '
        Me.tsmtblInstalacje.Name = "tsmtblInstalacje"
        Me.tsmtblInstalacje.Size = New System.Drawing.Size(214, 22)
        Me.tsmtblInstalacje.Text = "Inst&alacji"
        '
        'tsmtblINEF
        '
        Me.tsmtblINEF.Name = "tsmtblINEF"
        Me.tsmtblINEF.Size = New System.Drawing.Size(214, 22)
        Me.tsmtblINEF.Text = "INE&F"
        '
        'tsmtblCALCO
        '
        Me.tsmtblCALCO.Name = "tsmtblCALCO"
        Me.tsmtblCALCO.Size = New System.Drawing.Size(214, 22)
        Me.tsmtblCALCO.Text = "&CALCO"
        '
        'tsmtblMPK
        '
        Me.tsmtblMPK.Name = "tsmtblMPK"
        Me.tsmtblMPK.Size = New System.Drawing.Size(214, 22)
        Me.tsmtblMPK.Text = "&MPK"
        '
        'tsmtblRK
        '
        Me.tsmtblRK.Name = "tsmtblRK"
        Me.tsmtblRK.Size = New System.Drawing.Size(214, 22)
        Me.tsmtblRK.Text = "Rodza&jów kosztów"
        '
        'tsmtblGEn
        '
        Me.tsmtblGEn.Name = "tsmtblGEn"
        Me.tsmtblGEn.Size = New System.Drawing.Size(214, 22)
        Me.tsmtblGEn.Text = "Rozdziału m&ediów"
        '
        'ToolStripSeparator6
        '
        Me.ToolStripSeparator6.Name = "ToolStripSeparator6"
        Me.ToolStripSeparator6.Size = New System.Drawing.Size(211, 6)
        '
        'tsmtblFormuly
        '
        Me.tsmtblFormuly.Name = "tsmtblFormuly"
        Me.tsmtblFormuly.Size = New System.Drawing.Size(214, 22)
        Me.tsmtblFormuly.Text = "Zamiany &formuł"
        '
        'tsmtblSources
        '
        Me.tsmtblSources.Name = "tsmtblSources"
        Me.tsmtblSources.Size = New System.Drawing.Size(214, 22)
        Me.tsmtblSources.Text = "K&ontroli pobrań"
        '
        'tsm_Df
        '
        Me.tsm_Df.Name = "tsm_Df"
        Me.tsm_Df.Size = New System.Drawing.Size(225, 22)
        Me.tsm_Df.Text = "Ustaw/usuń &filtr okresu"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(222, 6)
        '
        'tsm_DB
        '
        Me.tsm_DB.Name = "tsm_DB"
        Me.tsm_DB.Size = New System.Drawing.Size(225, 22)
        Me.tsm_DB.Text = "&Blokuj okres"
        '
        'tsm_Dm
        '
        Me.tsm_Dm.Name = "tsm_Dm"
        Me.tsm_Dm.Size = New System.Drawing.Size(225, 22)
        Me.tsm_Dm.Text = "Ko&mpaktuj"
        '
        'tsmExport
        '
        Me.tsmExport.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsm_Et, Me.tsmAktualizacjaZesztowBezWOT, Me.tsmZeszytySprzedazy, Me.ToolStripSeparator21, Me.tsm_Em, Me.ToolStripSeparator11, Me.tsmExKoszty, Me.tsm_En, Me.ToolStripSeparator18, Me.tsm_Eh, Me.tsm_Es, Me.tsm_Ei, Me.tsm_Ep, Me.tsm_ER, Me.ToolStripSeparator17, Me.tsm_Eg, Me.tsm_Eu, Me.ToolStripSeparator22, Me.tsm_Ez, Me.tsm_Ea, Me.tsm_Eo, Me.tsm_Ew})
        Me.tsmExport.Name = "tsmExport"
        Me.tsmExport.Size = New System.Drawing.Size(62, 22)
        Me.tsmExport.Text = "&Export"
        '
        'tsm_Et
        '
        Me.tsm_Et.Name = "tsm_Et"
        Me.tsm_Et.Size = New System.Drawing.Size(366, 22)
        Me.tsm_Et.Text = "Ak&tualizacja zeszytów kosztów wydziałowych"
        '
        'tsmAktualizacjaZesztowBezWOT
        '
        Me.tsmAktualizacjaZesztowBezWOT.Name = "tsmAktualizacjaZesztowBezWOT"
        Me.tsmAktualizacjaZesztowBezWOT.Size = New System.Drawing.Size(366, 22)
        Me.tsmAktualizacjaZesztowBezWOT.Text = "Zeszyty WOM i Prasowni bez WOT"
        '
        'tsmZeszytySprzedazy
        '
        Me.tsmZeszytySprzedazy.Enabled = False
        Me.tsmZeszytySprzedazy.Name = "tsmZeszytySprzedazy"
        Me.tsmZeszytySprzedazy.Size = New System.Drawing.Size(366, 22)
        Me.tsmZeszytySprzedazy.Text = "Aktualizacja zeszytów &sprzedaży"
        Me.tsmZeszytySprzedazy.ToolTipText = "Realizowane z poziomu CHO_Result_rrrr-mm.xls"
        Me.tsmZeszytySprzedazy.Visible = False
        '
        'ToolStripSeparator21
        '
        Me.ToolStripSeparator21.Name = "ToolStripSeparator21"
        Me.ToolStripSeparator21.Size = New System.Drawing.Size(363, 6)
        '
        'tsm_Em
        '
        Me.tsm_Em.Name = "tsm_Em"
        Me.tsm_Em.Size = New System.Drawing.Size(366, 22)
        Me.tsm_Em.Text = "Danych &monitoringu"
        '
        'ToolStripSeparator11
        '
        Me.ToolStripSeparator11.Name = "ToolStripSeparator11"
        Me.ToolStripSeparator11.Size = New System.Drawing.Size(363, 6)
        '
        'tsmExKoszty
        '
        Me.tsmExKoszty.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsm_Edp, Me.tsm_Edo, Me.tsm_EdI, Me.tsm_Eds, Me.tsm_EdC, Me.ToolStripSeparator24, Me.tsm_Edb, Me.tsm_Edu})
        Me.tsmExKoszty.Name = "tsmExKoszty"
        Me.tsmExKoszty.Size = New System.Drawing.Size(366, 22)
        Me.tsmExKoszty.Text = "Koszty w podziale na &dostawców"
        '
        'tsm_Edp
        '
        Me.tsm_Edp.Name = "tsm_Edp"
        Me.tsm_Edp.Size = New System.Drawing.Size(234, 22)
        Me.tsm_Edp.Text = "&podstawowe"
        '
        'tsm_Edo
        '
        Me.tsm_Edo.Name = "tsm_Edo"
        Me.tsm_Edo.Size = New System.Drawing.Size(234, 22)
        Me.tsm_Edo.Text = "&overheads"
        '
        'tsm_EdI
        '
        Me.tsm_EdI.Name = "tsm_EdI"
        Me.tsm_EdI.Size = New System.Drawing.Size(234, 22)
        Me.tsm_EdI.Text = "wybrane &INEF"
        '
        'tsm_Eds
        '
        Me.tsm_Eds.Name = "tsm_Eds"
        Me.tsm_Eds.Size = New System.Drawing.Size(234, 22)
        Me.tsm_Eds.Text = "&suma dla dostawcy"
        '
        'tsm_EdC
        '
        Me.tsm_EdC.Name = "tsm_EdC"
        Me.tsm_EdC.Size = New System.Drawing.Size(234, 22)
        Me.tsm_EdC.Text = "wybrane &CalCo (UR)"
        '
        'ToolStripSeparator24
        '
        Me.ToolStripSeparator24.Name = "ToolStripSeparator24"
        Me.ToolStripSeparator24.Size = New System.Drawing.Size(231, 6)
        '
        'tsm_Edb
        '
        Me.tsm_Edb.Name = "tsm_Edb"
        Me.tsm_Edb.Size = New System.Drawing.Size(234, 22)
        Me.tsm_Edb.Text = "&budżet następnego roku"
        '
        'tsm_Edu
        '
        Me.tsm_Edu.Name = "tsm_Edu"
        Me.tsm_Edu.Size = New System.Drawing.Size(234, 22)
        Me.tsm_Edu.Text = "&uzupełnij dane bieżące"
        '
        'tsm_En
        '
        Me.tsm_En.Name = "tsm_En"
        Me.tsm_En.Size = New System.Drawing.Size(366, 22)
        Me.tsm_En.Text = "A&naliza dostawców"
        Me.tsm_En.ToolTipText = "Dla wybranych dostawców pobierz dane:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "1. ZWMM080 wariant /S35KB1_NEW zapisz w C:" & _
            "\Tmp\ZWMM080.XLS" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "2. FBL1N wariant /S35KB_SUPPL zapisz w C:\Tmp\FBL1N.XLS"
        '
        'ToolStripSeparator18
        '
        Me.ToolStripSeparator18.Name = "ToolStripSeparator18"
        Me.ToolStripSeparator18.Size = New System.Drawing.Size(363, 6)
        '
        'tsm_Eh
        '
        Me.tsm_Eh.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsm_Ehs, Me.tsm_Est})
        Me.tsm_Eh.Name = "tsm_Eh"
        Me.tsm_Eh.Size = New System.Drawing.Size(366, 22)
        Me.tsm_Eh.Text = "Analiza &handlowa ZWW"
        '
        'tsm_Ehs
        '
        Me.tsm_Ehs.Name = "tsm_Ehs"
        Me.tsm_Ehs.Size = New System.Drawing.Size(143, 22)
        Me.tsm_Ehs.Text = "&sprzedaży"
        '
        'tsm_Est
        '
        Me.tsm_Est.Name = "tsm_Est"
        Me.tsm_Est.Size = New System.Drawing.Size(143, 22)
        Me.tsm_Est.Text = "&transportu"
        '
        'tsm_Es
        '
        Me.tsm_Es.Name = "tsm_Es"
        Me.tsm_Es.Size = New System.Drawing.Size(366, 22)
        Me.tsm_Es.Text = "Analiza &sprzedaży wg materiałów"
        '
        'tsm_Ei
        '
        Me.tsm_Ei.Name = "tsm_Ei"
        Me.tsm_Ei.Size = New System.Drawing.Size(366, 22)
        Me.tsm_Ei.Text = "Wieloletnia anal&iza transportu ZWW"
        '
        'tsm_Ep
        '
        Me.tsm_Ep.Name = "tsm_Ep"
        Me.tsm_Ep.Size = New System.Drawing.Size(366, 22)
        Me.tsm_Ep.Text = "Lista s&przedaży dla materiałów i klientów"
        '
        'tsm_ER
        '
        Me.tsm_ER.Name = "tsm_ER"
        Me.tsm_ER.Size = New System.Drawing.Size(366, 22)
        Me.tsm_ER.Text = "&Ranking klientów"
        '
        'ToolStripSeparator17
        '
        Me.ToolStripSeparator17.Name = "ToolStripSeparator17"
        Me.ToolStripSeparator17.Size = New System.Drawing.Size(363, 6)
        '
        'tsm_Eg
        '
        Me.tsm_Eg.Name = "tsm_Eg"
        Me.tsm_Eg.Size = New System.Drawing.Size(366, 22)
        Me.tsm_Eg.Text = "Koszty lo&gistyki"
        '
        'tsm_Eu
        '
        Me.tsm_Eu.Name = "tsm_Eu"
        Me.tsm_Eu.Size = New System.Drawing.Size(366, 22)
        Me.tsm_Eu.Text = "Analiza &uzysku"
        '
        'ToolStripSeparator22
        '
        Me.ToolStripSeparator22.Name = "ToolStripSeparator22"
        Me.ToolStripSeparator22.Size = New System.Drawing.Size(363, 6)
        '
        'tsm_Ez
        '
        Me.tsm_Ez.Name = "tsm_Ez"
        Me.tsm_Ez.Size = New System.Drawing.Size(366, 22)
        Me.tsm_Ez.Text = "Zlecenia &zamknięte ZWK"
        Me.tsm_Ez.Visible = False
        '
        'tsm_Ea
        '
        Me.tsm_Ea.Name = "tsm_Ea"
        Me.tsm_Ea.Size = New System.Drawing.Size(366, 22)
        Me.tsm_Ea.Text = "Przestarzałe z&apasy"
        '
        'tsm_Eo
        '
        Me.tsm_Eo.Name = "tsm_Eo"
        Me.tsm_Eo.Size = New System.Drawing.Size(366, 22)
        Me.tsm_Eo.Text = "P&odział zapasów"
        Me.tsm_Eo.ToolTipText = "Podział"
        '
        'tsm_Ew
        '
        Me.tsm_Ew.Name = "tsm_Ew"
        Me.tsm_Ew.Size = New System.Drawing.Size(366, 22)
        Me.tsm_Ew.Text = "No&wi klienci ZWK"
        '
        'tsmPomocne
        '
        Me.tsmPomocne.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsm_Pd, Me.tsm_Pf, Me.tsm_Pn, Me.tsm_PS, Me.ToolStripSeparator19, Me.tsm_PK})
        Me.tsmPomocne.Name = "tsmPomocne"
        Me.tsmPomocne.Size = New System.Drawing.Size(80, 22)
        Me.tsmPomocne.Text = "&Pomocne"
        '
        'tsm_Pd
        '
        Me.tsm_Pd.Name = "tsm_Pd"
        Me.tsm_Pd.Size = New System.Drawing.Size(286, 22)
        Me.tsm_Pd.Text = "Generowanie wieloletnich &danych"
        Me.tsm_Pd.ToolTipText = "Generowanie danych z kilku lat na podstawie zapytania"
        '
        'tsm_Pf
        '
        Me.tsm_Pf.Name = "tsm_Pf"
        Me.tsm_Pf.Size = New System.Drawing.Size(286, 22)
        Me.tsm_Pf.Text = "Zmiana &formuł"
        '
        'tsm_Pn
        '
        Me.tsm_Pn.Name = "tsm_Pn"
        Me.tsm_Pn.Size = New System.Drawing.Size(286, 22)
        Me.tsm_Pn.Text = "Zmiana fragmentu &nazwy plików"
        '
        'tsm_PS
        '
        Me.tsm_PS.Name = "tsm_PS"
        Me.tsm_PS.Size = New System.Drawing.Size(286, 22)
        Me.tsm_PS.Text = "&Szerokość wyświetlania okresu"
        '
        'ToolStripSeparator19
        '
        Me.ToolStripSeparator19.Name = "ToolStripSeparator19"
        Me.ToolStripSeparator19.Size = New System.Drawing.Size(283, 6)
        '
        'tsm_PK
        '
        Me.tsm_PK.Name = "tsm_PK"
        Me.tsm_PK.Size = New System.Drawing.Size(286, 22)
        Me.tsm_PK.Text = "&Koniec"
        '
        'tsmAdministrator
        '
        Me.tsmAdministrator.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmTworzenieDB, Me.tsmZmianaUstawien, Me.ToolStripSeparator12, Me.tsmWeryfSprzWydz, Me.tsmAdPobierzSprzedaz, Me.tsmOdswiezAmortyzacje, Me.tsmAdEBiTDA, Me.tsmTransportBezSprzedazy, Me.ToolStripSeparator13, Me.tsmWeryfDostawcow, Me.tsmWeryfikacjaMaterialow, Me.tsmWeryfikacjaMagazynow, Me.tsmWeryfikacjaINEFCalCo, Me.tsm_Test})
        Me.tsmAdministrator.Name = "tsmAdministrator"
        Me.tsmAdministrator.Size = New System.Drawing.Size(105, 22)
        Me.tsmAdministrator.Text = "Ad&ministrator"
        '
        'tsmTworzenieDB
        '
        Me.tsmTworzenieDB.Name = "tsmTworzenieDB"
        Me.tsmTworzenieDB.Size = New System.Drawing.Size(305, 22)
        Me.tsmTworzenieDB.Text = "Plik bazy na następny rok"
        Me.tsmTworzenieDB.ToolTipText = "Opcja tworzy bazę danych na kolejny rok"
        '
        'tsmZmianaUstawien
        '
        Me.tsmZmianaUstawien.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tscmbDostepy})
        Me.tsmZmianaUstawien.Name = "tsmZmianaUstawien"
        Me.tsmZmianaUstawien.Size = New System.Drawing.Size(305, 22)
        Me.tsmZmianaUstawien.Text = "Zmiana ustawień dostępu do:"
        '
        'tscmbDostepy
        '
        Me.tscmbDostepy.Items.AddRange(New Object() {"budżetu", "mediów", "należności", "planu", "sprzedaży", "wykonania", "zapasów", "zatrudnienia", "WIP", "zleceń ZWK", "wyniku CHO", "Bazy Danych"})
        Me.tscmbDostepy.MaxDropDownItems = 11
        Me.tscmbDostepy.Name = "tscmbDostepy"
        Me.tscmbDostepy.Size = New System.Drawing.Size(121, 26)
        '
        'ToolStripSeparator12
        '
        Me.ToolStripSeparator12.Name = "ToolStripSeparator12"
        Me.ToolStripSeparator12.Size = New System.Drawing.Size(302, 6)
        '
        'tsmWeryfSprzWydz
        '
        Me.tsmWeryfSprzWydz.Name = "tsmWeryfSprzWydz"
        Me.tsmWeryfSprzWydz.Size = New System.Drawing.Size(305, 22)
        Me.tsmWeryfSprzWydz.Text = "Weryfikacja sprzedaży wydziałowej"
        Me.tsmWeryfSprzWydz.ToolTipText = resources.GetString("tsmWeryfSprzWydz.ToolTipText")
        Me.tsmWeryfSprzWydz.Visible = False
        '
        'tsmAdPobierzSprzedaz
        '
        Me.tsmAdPobierzSprzedaz.Name = "tsmAdPobierzSprzedaz"
        Me.tsmAdPobierzSprzedaz.Size = New System.Drawing.Size(305, 22)
        Me.tsmAdPobierzSprzedaz.Text = "Awaryjne odświeżanie sprzedaży"
        Me.tsmAdPobierzSprzedaz.Visible = False
        '
        'tsmOdswiezAmortyzacje
        '
        Me.tsmOdswiezAmortyzacje.Name = "tsmOdswiezAmortyzacje"
        Me.tsmOdswiezAmortyzacje.Size = New System.Drawing.Size(305, 22)
        Me.tsmOdswiezAmortyzacje.Text = "Odśwież amortyzację"
        '
        'tsmAdEBiTDA
        '
        Me.tsmAdEBiTDA.Name = "tsmAdEBiTDA"
        Me.tsmAdEBiTDA.Size = New System.Drawing.Size(305, 22)
        Me.tsmAdEBiTDA.Text = "Przelicz EBiTDA"
        '
        'tsmTransportBezSprzedazy
        '
        Me.tsmTransportBezSprzedazy.Name = "tsmTransportBezSprzedazy"
        Me.tsmTransportBezSprzedazy.Size = New System.Drawing.Size(305, 22)
        Me.tsmTransportBezSprzedazy.Text = "Pobierz transport bez sprzedaży"
        '
        'ToolStripSeparator13
        '
        Me.ToolStripSeparator13.Name = "ToolStripSeparator13"
        Me.ToolStripSeparator13.Size = New System.Drawing.Size(302, 6)
        '
        'tsmWeryfDostawcow
        '
        Me.tsmWeryfDostawcow.Name = "tsmWeryfDostawcow"
        Me.tsmWeryfDostawcow.Size = New System.Drawing.Size(305, 22)
        Me.tsmWeryfDostawcow.Text = "Weryfikacja dostawców"
        Me.tsmWeryfDostawcow.ToolTipText = "Na podstawie listy dostawców należy pobrać dane z SAP->ZWMM090;" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Wynik zapisz w C" & _
            ":\Tmp\ZWMM090.xls"
        '
        'tsmWeryfikacjaMaterialow
        '
        Me.tsmWeryfikacjaMaterialow.Name = "tsmWeryfikacjaMaterialow"
        Me.tsmWeryfikacjaMaterialow.Size = New System.Drawing.Size(305, 22)
        Me.tsmWeryfikacjaMaterialow.Text = "Weryfikacja materiałów"
        Me.tsmWeryfikacjaMaterialow.ToolTipText = "Uzupełnia dane materiałów na podstawie plików" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "W:\Costs\CHO_SAP_SYSTEM\BWD_Materi" & _
            "ały*.XLS"
        '
        'tsmWeryfikacjaMagazynow
        '
        Me.tsmWeryfikacjaMagazynow.Name = "tsmWeryfikacjaMagazynow"
        Me.tsmWeryfikacjaMagazynow.Size = New System.Drawing.Size(305, 22)
        Me.tsmWeryfikacjaMagazynow.Text = "Weryfikacja magazynów"
        Me.tsmWeryfikacjaMagazynow.ToolTipText = "Aktualizuje tabelę magazynów na podstawie pliku" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "W:\Controlling\Controlling Dpt\O" & _
            "bowiązki\PlanInwentaryzacji_2012_1.xls" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'tsmWeryfikacjaINEFCalCo
        '
        Me.tsmWeryfikacjaINEFCalCo.Name = "tsmWeryfikacjaINEFCalCo"
        Me.tsmWeryfikacjaINEFCalCo.Size = New System.Drawing.Size(305, 22)
        Me.tsmWeryfikacjaINEFCalCo.Text = "Weryfikacja INEF/CalCo"
        '
        'tsm_Test
        '
        Me.tsm_Test.Name = "tsm_Test"
        Me.tsm_Test.Size = New System.Drawing.Size(305, 22)
        Me.tsm_Test.Text = "Test"
        '
        'tsm_H
        '
        Me.tsm_H.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmAnalizaZrodel, Me.tsmAdHoc, Me.tsmPobierzDaneSprzZBW, Me.tsmPobierzKosztyArchiwalne, Me.tsmPobierzKosztyArchiwalne2, Me.tsm_PobierzTransport20062012, Me.tsm_mKT, Me.tsmXlsToTblTransport, Me.tsmWstawKonta})
        Me.tsm_H.Name = "tsm_H"
        Me.tsm_H.Size = New System.Drawing.Size(67, 22)
        Me.tsm_H.Text = "Ad &Hoc"
        Me.tsm_H.Visible = False
        '
        'tsmAnalizaZrodel
        '
        Me.tsmAnalizaZrodel.Name = "tsmAnalizaZrodel"
        Me.tsmAnalizaZrodel.Size = New System.Drawing.Size(311, 22)
        Me.tsmAnalizaZrodel.Text = "Analiza zewnętrznych źródeł danych"
        '
        'tsmAdHoc
        '
        Me.tsmAdHoc.Name = "tsmAdHoc"
        Me.tsmAdHoc.Size = New System.Drawing.Size(311, 22)
        Me.tsmAdHoc.Text = "Ad hoc"
        '
        'tsmPobierzDaneSprzZBW
        '
        Me.tsmPobierzDaneSprzZBW.Name = "tsmPobierzDaneSprzZBW"
        Me.tsmPobierzDaneSprzZBW.Size = New System.Drawing.Size(311, 22)
        Me.tsmPobierzDaneSprzZBW.Text = "Pobierz dane sprzedaży z BW"
        '
        'tsmPobierzKosztyArchiwalne
        '
        Me.tsmPobierzKosztyArchiwalne.Name = "tsmPobierzKosztyArchiwalne"
        Me.tsmPobierzKosztyArchiwalne.Size = New System.Drawing.Size(311, 22)
        Me.tsmPobierzKosztyArchiwalne.Text = "Pobierz koszty 2003-2005"
        Me.tsmPobierzKosztyArchiwalne.ToolTipText = "Pobiera roczne koszty z archiwalnych zeszytów kosztowych"
        Me.tsmPobierzKosztyArchiwalne.Visible = False
        '
        'tsmPobierzKosztyArchiwalne2
        '
        Me.tsmPobierzKosztyArchiwalne2.Name = "tsmPobierzKosztyArchiwalne2"
        Me.tsmPobierzKosztyArchiwalne2.Size = New System.Drawing.Size(311, 22)
        Me.tsmPobierzKosztyArchiwalne2.Text = "Pobierz koszty 2006-2010"
        Me.tsmPobierzKosztyArchiwalne2.Visible = False
        '
        'tsm_PobierzTransport20062012
        '
        Me.tsm_PobierzTransport20062012.Name = "tsm_PobierzTransport20062012"
        Me.tsm_PobierzTransport20062012.Size = New System.Drawing.Size(311, 22)
        Me.tsm_PobierzTransport20062012.Text = "Pobierz transport 2006-2012"
        Me.tsm_PobierzTransport20062012.Visible = False
        '
        'tsm_mKT
        '
        Me.tsm_mKT.Name = "tsm_mKT"
        Me.tsm_mKT.Size = New System.Drawing.Size(311, 22)
        Me.tsm_mKT.Text = "Klient-Transport"
        Me.tsm_mKT.Visible = False
        '
        'tsmXlsToTblTransport
        '
        Me.tsmXlsToTblTransport.Name = "tsmXlsToTblTransport"
        Me.tsmXlsToTblTransport.Size = New System.Drawing.Size(311, 22)
        Me.tsmXlsToTblTransport.Text = "Xls to tblTransport "
        '
        'tsmWstawKonta
        '
        Me.tsmWstawKonta.Name = "tsmWstawKonta"
        Me.tsmWstawKonta.Size = New System.Drawing.Size(311, 22)
        Me.tsmWstawKonta.Text = "Wstaw konta"
        '
        'ssMain
        '
        Me.ssMain.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tspbMain, Me.tsslMain, Me.tsslLicznik})
        Me.ssMain.Location = New System.Drawing.Point(0, 475)
        Me.ssMain.Name = "ssMain"
        Me.ssMain.Size = New System.Drawing.Size(801, 23)
        Me.ssMain.TabIndex = 1
        Me.ssMain.Text = "StatusStrip1"
        '
        'tspbMain
        '
        Me.tspbMain.AutoSize = False
        Me.tspbMain.Name = "tspbMain"
        Me.tspbMain.Size = New System.Drawing.Size(100, 17)
        '
        'tsslMain
        '
        Me.tsslMain.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenInner
        Me.tsslMain.Name = "tsslMain"
        Me.tsslMain.Size = New System.Drawing.Size(57, 18)
        Me.tsslMain.Text = "Gotowe"
        '
        'tsslLicznik
        '
        Me.tsslLicznik.BorderStyle = System.Windows.Forms.Border3DStyle.Sunken
        Me.tsslLicznik.Name = "tsslLicznik"
        Me.tsslLicznik.Size = New System.Drawing.Size(0, 18)
        '
        'MainHelp
        '
        Me.MainHelp.HelpNamespace = "C:\CHO_DataBase\CHO BazaDanych.chm"
        '
        'dtpOkres
        '
        Me.dtpOkres.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dtpOkres.CustomFormat = "MMMM yyyy"
        Me.dtpOkres.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpOkres.Location = New System.Drawing.Point(620, 0)
        Me.dtpOkres.Name = "dtpOkres"
        Me.dtpOkres.ReadOnly = False
        Me.dtpOkres.ShowUpDown = True
        Me.dtpOkres.Size = New System.Drawing.Size(169, 26)
        Me.dtpOkres.TabIndex = 21
        '
        'txtKomunikat
        '
        Me.txtKomunikat.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.txtKomunikat.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtKomunikat.Location = New System.Drawing.Point(383, 46)
        Me.txtKomunikat.Margin = New System.Windows.Forms.Padding(2)
        Me.txtKomunikat.MinimumSize = New System.Drawing.Size(200, 100)
        Me.txtKomunikat.Multiline = True
        Me.txtKomunikat.Name = "txtKomunikat"
        Me.txtKomunikat.ReadOnly = True
        Me.txtKomunikat.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtKomunikat.Size = New System.Drawing.Size(200, 100)
        Me.txtKomunikat.TabIndex = 23
        Me.txtKomunikat.Visible = False
        '
        'gbParametry
        '
        Me.gbParametry.Controls.Add(Me.txtPaWydzialy)
        Me.gbParametry.Controls.Add(Me.lstPaWybor)
        Me.gbParametry.Controls.Add(Me.lblPaParent)
        Me.gbParametry.Controls.Add(Me.btnPaOk)
        Me.gbParametry.Controls.Add(Me.txtPaRok2)
        Me.gbParametry.Controls.Add(Me.cmbPaMiesiace2)
        Me.gbParametry.Controls.Add(Me.Label4)
        Me.gbParametry.Controls.Add(Me.txtPaRok1)
        Me.gbParametry.Controls.Add(Me.cmbPaMiesiace1)
        Me.gbParametry.Controls.Add(Me.Label3)
        Me.gbParametry.Location = New System.Drawing.Point(27, 80)
        Me.gbParametry.Margin = New System.Windows.Forms.Padding(2)
        Me.gbParametry.Name = "gbParametry"
        Me.gbParametry.Padding = New System.Windows.Forms.Padding(2)
        Me.gbParametry.Size = New System.Drawing.Size(352, 342)
        Me.gbParametry.TabIndex = 24
        Me.gbParametry.TabStop = False
        Me.gbParametry.Text = "Parametry"
        Me.gbParametry.Visible = False
        '
        'txtPaWydzialy
        '
        Me.txtPaWydzialy.Location = New System.Drawing.Point(74, 292)
        Me.txtPaWydzialy.Margin = New System.Windows.Forms.Padding(2)
        Me.txtPaWydzialy.Name = "txtPaWydzialy"
        Me.txtPaWydzialy.Size = New System.Drawing.Size(100, 26)
        Me.txtPaWydzialy.TabIndex = 10
        Me.txtPaWydzialy.Visible = False
        '
        'lstPaWybor
        '
        Me.lstPaWybor.CheckOnClick = True
        Me.lstPaWybor.FormattingEnabled = True
        Me.lstPaWybor.Location = New System.Drawing.Point(16, 125)
        Me.lstPaWybor.Margin = New System.Windows.Forms.Padding(2)
        Me.lstPaWybor.Name = "lstPaWybor"
        Me.lstPaWybor.Size = New System.Drawing.Size(318, 130)
        Me.lstPaWybor.TabIndex = 9
        Me.lstPaWybor.ThreeDCheckBoxes = True
        '
        'lblPaParent
        '
        Me.lblPaParent.AutoSize = True
        Me.lblPaParent.Location = New System.Drawing.Point(29, 284)
        Me.lblPaParent.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblPaParent.Name = "lblPaParent"
        Me.lblPaParent.Size = New System.Drawing.Size(0, 18)
        Me.lblPaParent.TabIndex = 8
        Me.lblPaParent.Visible = False
        '
        'btnPaOk
        '
        Me.btnPaOk.Location = New System.Drawing.Point(249, 292)
        Me.btnPaOk.Margin = New System.Windows.Forms.Padding(2)
        Me.btnPaOk.Name = "btnPaOk"
        Me.btnPaOk.Size = New System.Drawing.Size(75, 34)
        Me.btnPaOk.TabIndex = 7
        Me.btnPaOk.Text = "Ok"
        Me.btnPaOk.UseVisualStyleBackColor = True
        '
        'txtPaRok2
        '
        Me.txtPaRok2.Location = New System.Drawing.Point(268, 78)
        Me.txtPaRok2.Margin = New System.Windows.Forms.Padding(2)
        Me.txtPaRok2.MaxLength = 4
        Me.txtPaRok2.Name = "txtPaRok2"
        Me.txtPaRok2.Size = New System.Drawing.Size(66, 26)
        Me.txtPaRok2.TabIndex = 5
        Me.txtPaRok2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'cmbPaMiesiace2
        '
        Me.cmbPaMiesiace2.FormattingEnabled = True
        Me.cmbPaMiesiace2.Items.AddRange(New Object() {"Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień"})
        Me.cmbPaMiesiace2.Location = New System.Drawing.Point(125, 78)
        Me.cmbPaMiesiace2.Margin = New System.Windows.Forms.Padding(2)
        Me.cmbPaMiesiace2.Name = "cmbPaMiesiace2"
        Me.cmbPaMiesiace2.Size = New System.Drawing.Size(122, 26)
        Me.cmbPaMiesiace2.TabIndex = 4
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(84, 81)
        Me.Label4.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(27, 18)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "do"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtPaRok1
        '
        Me.txtPaRok1.Location = New System.Drawing.Point(268, 38)
        Me.txtPaRok1.Margin = New System.Windows.Forms.Padding(2)
        Me.txtPaRok1.MaxLength = 4
        Me.txtPaRok1.Name = "txtPaRok1"
        Me.txtPaRok1.Size = New System.Drawing.Size(66, 26)
        Me.txtPaRok1.TabIndex = 2
        Me.txtPaRok1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'cmbPaMiesiace1
        '
        Me.cmbPaMiesiace1.FormattingEnabled = True
        Me.cmbPaMiesiace1.Items.AddRange(New Object() {"Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień"})
        Me.cmbPaMiesiace1.Location = New System.Drawing.Point(125, 38)
        Me.cmbPaMiesiace1.Margin = New System.Windows.Forms.Padding(2)
        Me.cmbPaMiesiace1.Name = "cmbPaMiesiace1"
        Me.cmbPaMiesiace1.Size = New System.Drawing.Size(122, 26)
        Me.cmbPaMiesiace1.TabIndex = 1
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(22, 41)
        Me.Label3.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(98, 18)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Za okres od"
        '
        'gbZmianaNazwyPlikow
        '
        Me.gbZmianaNazwyPlikow.Controls.Add(Me.lblZFNP)
        Me.gbZmianaNazwyPlikow.Controls.Add(Me.btnZFNP)
        Me.gbZmianaNazwyPlikow.Controls.Add(Me.txtZFNP2)
        Me.gbZmianaNazwyPlikow.Controls.Add(Me.Label2)
        Me.gbZmianaNazwyPlikow.Controls.Add(Me.txtZFNP1)
        Me.gbZmianaNazwyPlikow.Controls.Add(Me.Label1)
        Me.gbZmianaNazwyPlikow.Location = New System.Drawing.Point(595, 158)
        Me.gbZmianaNazwyPlikow.Margin = New System.Windows.Forms.Padding(2)
        Me.gbZmianaNazwyPlikow.Name = "gbZmianaNazwyPlikow"
        Me.gbZmianaNazwyPlikow.Padding = New System.Windows.Forms.Padding(2)
        Me.gbZmianaNazwyPlikow.Size = New System.Drawing.Size(118, 169)
        Me.gbZmianaNazwyPlikow.TabIndex = 26
        Me.gbZmianaNazwyPlikow.TabStop = False
        Me.gbZmianaNazwyPlikow.Text = "Zmiana nazwy plików"
        Me.gbZmianaNazwyPlikow.Visible = False
        '
        'lblZFNP
        '
        Me.lblZFNP.AutoSize = True
        Me.lblZFNP.Location = New System.Drawing.Point(29, 134)
        Me.lblZFNP.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblZFNP.Name = "lblZFNP"
        Me.lblZFNP.Size = New System.Drawing.Size(0, 18)
        Me.lblZFNP.TabIndex = 5
        Me.lblZFNP.Visible = False
        '
        'btnZFNP
        '
        Me.btnZFNP.Location = New System.Drawing.Point(208, 112)
        Me.btnZFNP.Margin = New System.Windows.Forms.Padding(2)
        Me.btnZFNP.Name = "btnZFNP"
        Me.btnZFNP.Size = New System.Drawing.Size(75, 35)
        Me.btnZFNP.TabIndex = 2
        Me.btnZFNP.Text = "Ok"
        Me.btnZFNP.UseVisualStyleBackColor = True
        '
        'txtZFNP2
        '
        Me.txtZFNP2.Location = New System.Drawing.Point(71, 69)
        Me.txtZFNP2.Margin = New System.Windows.Forms.Padding(2)
        Me.txtZFNP2.Name = "txtZFNP2"
        Me.txtZFNP2.Size = New System.Drawing.Size(212, 26)
        Me.txtZFNP2.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(38, 72)
        Me.Label2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(26, 18)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "na"
        '
        'txtZFNP1
        '
        Me.txtZFNP1.Location = New System.Drawing.Point(71, 38)
        Me.txtZFNP1.Margin = New System.Windows.Forms.Padding(2)
        Me.txtZFNP1.Name = "txtZFNP1"
        Me.txtZFNP1.Size = New System.Drawing.Size(212, 26)
        Me.txtZFNP1.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(38, 40)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(18, 18)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Z"
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(120.0!, 120.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.ClientSize = New System.Drawing.Size(801, 498)
        Me.Controls.Add(Me.gbZmianaNazwyPlikow)
        Me.Controls.Add(Me.gbParametry)
        Me.Controls.Add(Me.txtKomunikat)
        Me.Controls.Add(Me.dtpOkres)
        Me.Controls.Add(Me.ssMain)
        Me.Controls.Add(Me.MainMenu)
        Me.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.IsMdiContainer = True
        Me.MainMenuStrip = Me.MainMenu
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.MinimumSize = New System.Drawing.Size(809, 522)
        Me.Name = "frmMain"
        Me.MainHelp.SetShowHelp(Me, True)
        Me.Text = "CHO Baza danych"
        Me.MainMenu.ResumeLayout(False)
        Me.MainMenu.PerformLayout()
        Me.ssMain.ResumeLayout(False)
        Me.ssMain.PerformLayout()
        Me.gbParametry.ResumeLayout(False)
        Me.gbParametry.PerformLayout()
        Me.gbZmianaNazwyPlikow.ResumeLayout(False)
        Me.gbZmianaNazwyPlikow.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MainMenu As System.Windows.Forms.MenuStrip
    Friend WithEvents tsmDane As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ssMain As System.Windows.Forms.StatusStrip
    Friend WithEvents tspbMain As System.Windows.Forms.ToolStripProgressBar
    Friend WithEvents tsslMain As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents cmbMiesiace As System.Windows.Forms.ComboBox
    Friend WithEvents tsm_Dm As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsm_DP As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsm_Dk As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmtblGrupyKalk As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmtblGrupyMat As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmtblMaterialy As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmtblKlienci As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmtblSources As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator7 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator6 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ttMain As System.Windows.Forms.ToolTip
    Friend WithEvents tsmtblMagazyny As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsmtblRodzajeMat As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmPomocne As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmAdministrator As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmWeryfikacjaMaterialow As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmWeryfikacjaMagazynow As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmtblDostawcy As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator9 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsmtblCALCO As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmtblGrKoszt As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmtblInstalacje As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmtblINEF As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmtblMPK As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmtblRK As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsslLicznik As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents tsmtblGEn As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmExport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmExKoszty As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsm_Et As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsm_Pf As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmTworzenieDB As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator12 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsmBlokowanieProcesow As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsm_Pn As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsm_En As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmWykonania As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmtblSprzedaz As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator10 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsmtblZWPC As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmPlanu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmtblPLSprzedaz As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator14 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsmtblPLZWPC As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmBudzetu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmtblBUSprzedaz As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator15 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsmtblBUZWPC As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmtblMedia As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmtblZapasy As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmtblZWKZlecenia As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmtblWIP As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmtblZatrudnienie As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmtblPLMedia As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmtblPLZatrudnienie As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmtblBUMedia As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmtblBUZatrudnienie As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator8 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator16 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsmWeryfikacjaINEFCalCo As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsm_PK As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmZeszytySprzedazy As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsm_DPw As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsm_DPp As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsm_DPb As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmZmianaUstawien As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tscmbDostepy As System.Windows.Forms.ToolStripComboBox
    Friend WithEvents tsm_Es As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator11 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator18 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsm_Ep As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MainHelp As System.Windows.Forms.HelpProvider
    Friend WithEvents tsmtblKursyWalut As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmAktualizacjaZesztowBezWOT As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmWeryfDostawcow As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator19 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator17 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsm_Eg As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmtblFormuly As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmAdPobierzSprzedaz As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmWeryfSprzWydz As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsm_Eu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsm_Edp As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsm_Edo As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsm_EdI As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsm_DB As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmtblBlokady As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmtblAmortyzacja As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmAdEBiTDA As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator13 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsmOdswiezAmortyzacje As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator21 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsm_Em As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsm_Df As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator22 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsm_Ez As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmtblNaleznosci As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsm_PS As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsm_ER As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsm_Eds As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsm_Ea As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsm_Ew As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsm_DPps As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsm_DPpC As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsm_Eo As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsm_Eh As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator23 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsm_DPk As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsm_Pd As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents dtpOkres As MyControls.DateTimePickerReadOnly
    Friend WithEvents txtKomunikat As System.Windows.Forms.TextBox
    Friend WithEvents gbParametry As System.Windows.Forms.GroupBox
    Friend WithEvents txtPaWydzialy As System.Windows.Forms.TextBox
    Friend WithEvents lstPaWybor As System.Windows.Forms.CheckedListBox
    Friend WithEvents lblPaParent As System.Windows.Forms.Label
    Friend WithEvents btnPaOk As System.Windows.Forms.Button
    Friend WithEvents txtPaRok2 As System.Windows.Forms.TextBox
    Friend WithEvents cmbPaMiesiace2 As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtPaRok1 As System.Windows.Forms.TextBox
    Friend WithEvents cmbPaMiesiace1 As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents gbZmianaNazwyPlikow As System.Windows.Forms.GroupBox
    Friend WithEvents lblZFNP As System.Windows.Forms.Label
    Friend WithEvents btnZFNP As System.Windows.Forms.Button
    Friend WithEvents txtZFNP2 As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtZFNP1 As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents tsmTransportBezSprzedazy As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmtblTransport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsm_Ehs As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsm_Est As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsm_EdC As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator24 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsm_Edb As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsm_Ei As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsm_H As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmAnalizaZrodel As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmAdHoc As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmPobierzDaneSprzZBW As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmPobierzKosztyArchiwalne As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmPobierzKosztyArchiwalne2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsm_PobierzTransport20062012 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsm_mKT As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmXlsToTblTransport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmWstawKonta As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsm_Edu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsm_DPbs As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsm_DPbC As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsm_Test As System.Windows.Forms.ToolStripMenuItem

End Class
