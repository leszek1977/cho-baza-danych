﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PobierzWykonanie
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gbPobierz = New System.Windows.Forms.GroupBox
        Me.tlpPobierz = New System.Windows.Forms.TableLayoutPanel
        Me.btnNaleznosci = New System.Windows.Forms.Button
        Me.btnZapasy = New System.Windows.Forms.Button
        Me.btnKoszty = New System.Windows.Forms.Button
        Me.btnMedia = New System.Windows.Forms.Button
        Me.btnSprzedaz = New System.Windows.Forms.Button
        Me.btnZatrudnienie = New System.Windows.Forms.Button
        Me.btnZWKZlecenia = New System.Windows.Forms.Button
        Me.btnZWKWIP = New System.Windows.Forms.Button
        Me.gbPobierz.SuspendLayout()
        Me.tlpPobierz.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbPobierz
        '
        Me.gbPobierz.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.gbPobierz.BackColor = System.Drawing.SystemColors.Control
        Me.gbPobierz.Controls.Add(Me.tlpPobierz)
        Me.gbPobierz.Location = New System.Drawing.Point(11, 15)
        Me.gbPobierz.Margin = New System.Windows.Forms.Padding(2)
        Me.gbPobierz.Name = "gbPobierz"
        Me.gbPobierz.Padding = New System.Windows.Forms.Padding(2)
        Me.gbPobierz.Size = New System.Drawing.Size(215, 462)
        Me.gbPobierz.TabIndex = 4
        Me.gbPobierz.TabStop = False
        Me.gbPobierz.Text = "Pobierz dane o wykonaniu:"
        '
        'tlpPobierz
        '
        Me.tlpPobierz.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tlpPobierz.ColumnCount = 1
        Me.tlpPobierz.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tlpPobierz.Controls.Add(Me.btnNaleznosci, 0, 4)
        Me.tlpPobierz.Controls.Add(Me.btnZapasy, 0, 3)
        Me.tlpPobierz.Controls.Add(Me.btnKoszty, 0, 1)
        Me.tlpPobierz.Controls.Add(Me.btnMedia, 0, 2)
        Me.tlpPobierz.Controls.Add(Me.btnSprzedaz, 0, 0)
        Me.tlpPobierz.Controls.Add(Me.btnZatrudnienie, 0, 7)
        Me.tlpPobierz.Controls.Add(Me.btnZWKZlecenia, 0, 6)
        Me.tlpPobierz.Controls.Add(Me.btnZWKWIP, 0, 5)
        Me.tlpPobierz.Location = New System.Drawing.Point(6, 41)
        Me.tlpPobierz.Margin = New System.Windows.Forms.Padding(2)
        Me.tlpPobierz.Name = "tlpPobierz"
        Me.tlpPobierz.RowCount = 8
        Me.tlpPobierz.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.tlpPobierz.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.tlpPobierz.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.tlpPobierz.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.tlpPobierz.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.tlpPobierz.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.tlpPobierz.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.tlpPobierz.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.tlpPobierz.Size = New System.Drawing.Size(202, 412)
        Me.tlpPobierz.TabIndex = 0
        '
        'btnNaleznosci
        '
        Me.btnNaleznosci.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNaleznosci.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.btnNaleznosci.Location = New System.Drawing.Point(2, 206)
        Me.btnNaleznosci.Margin = New System.Windows.Forms.Padding(2)
        Me.btnNaleznosci.Name = "btnNaleznosci"
        Me.btnNaleznosci.Size = New System.Drawing.Size(198, 47)
        Me.btnNaleznosci.TabIndex = 18
        Me.btnNaleznosci.Text = "należności"
        Me.btnNaleznosci.UseVisualStyleBackColor = True
        '
        'btnZapasy
        '
        Me.btnZapasy.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnZapasy.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.btnZapasy.Location = New System.Drawing.Point(2, 155)
        Me.btnZapasy.Margin = New System.Windows.Forms.Padding(2)
        Me.btnZapasy.Name = "btnZapasy"
        Me.btnZapasy.Size = New System.Drawing.Size(198, 47)
        Me.btnZapasy.TabIndex = 2
        Me.btnZapasy.Text = "zapasów"
        Me.btnZapasy.UseVisualStyleBackColor = True
        '
        'btnKoszty
        '
        Me.btnKoszty.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnKoszty.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.btnKoszty.Location = New System.Drawing.Point(2, 53)
        Me.btnKoszty.Margin = New System.Windows.Forms.Padding(2)
        Me.btnKoszty.Name = "btnKoszty"
        Me.btnKoszty.Size = New System.Drawing.Size(198, 47)
        Me.btnKoszty.TabIndex = 4
        Me.btnKoszty.Text = "kosztów"
        Me.btnKoszty.UseVisualStyleBackColor = True
        '
        'btnMedia
        '
        Me.btnMedia.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnMedia.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.btnMedia.Location = New System.Drawing.Point(2, 104)
        Me.btnMedia.Margin = New System.Windows.Forms.Padding(2)
        Me.btnMedia.Name = "btnMedia"
        Me.btnMedia.Size = New System.Drawing.Size(198, 47)
        Me.btnMedia.TabIndex = 5
        Me.btnMedia.Text = "mediów"
        Me.btnMedia.UseVisualStyleBackColor = True
        '
        'btnSprzedaz
        '
        Me.btnSprzedaz.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSprzedaz.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.btnSprzedaz.Location = New System.Drawing.Point(2, 2)
        Me.btnSprzedaz.Margin = New System.Windows.Forms.Padding(2)
        Me.btnSprzedaz.Name = "btnSprzedaz"
        Me.btnSprzedaz.Size = New System.Drawing.Size(198, 47)
        Me.btnSprzedaz.TabIndex = 0
        Me.btnSprzedaz.Text = "sprzedaży"
        Me.btnSprzedaz.UseVisualStyleBackColor = True
        '
        'btnZatrudnienie
        '
        Me.btnZatrudnienie.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnZatrudnienie.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.btnZatrudnienie.Location = New System.Drawing.Point(2, 359)
        Me.btnZatrudnienie.Margin = New System.Windows.Forms.Padding(2)
        Me.btnZatrudnienie.Name = "btnZatrudnienie"
        Me.btnZatrudnienie.Size = New System.Drawing.Size(198, 51)
        Me.btnZatrudnienie.TabIndex = 6
        Me.btnZatrudnienie.Text = "zatrudnienia"
        Me.btnZatrudnienie.UseVisualStyleBackColor = True
        '
        'btnZWKZlecenia
        '
        Me.btnZWKZlecenia.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnZWKZlecenia.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.btnZWKZlecenia.Location = New System.Drawing.Point(2, 308)
        Me.btnZWKZlecenia.Margin = New System.Windows.Forms.Padding(2)
        Me.btnZWKZlecenia.Name = "btnZWKZlecenia"
        Me.btnZWKZlecenia.Size = New System.Drawing.Size(198, 47)
        Me.btnZWKZlecenia.TabIndex = 4
        Me.btnZWKZlecenia.Text = "ZWK zlecenia"
        Me.btnZWKZlecenia.UseVisualStyleBackColor = True
        '
        'btnZWKWIP
        '
        Me.btnZWKWIP.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnZWKWIP.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.btnZWKWIP.Location = New System.Drawing.Point(2, 257)
        Me.btnZWKWIP.Margin = New System.Windows.Forms.Padding(2)
        Me.btnZWKWIP.Name = "btnZWKWIP"
        Me.btnZWKWIP.Size = New System.Drawing.Size(198, 47)
        Me.btnZWKWIP.TabIndex = 5
        Me.btnZWKWIP.Text = "ZWK WIP"
        Me.btnZWKWIP.UseVisualStyleBackColor = True
        '
        'PobierzWykonanie
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(786, 488)
        Me.Controls.Add(Me.gbPobierz)
        Me.Font = New System.Drawing.Font("Verdana", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "PobierzWykonanie"
        Me.Text = "Podstawowe"
        Me.gbPobierz.ResumeLayout(False)
        Me.tlpPobierz.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbPobierz As System.Windows.Forms.GroupBox
    Friend WithEvents tlpPobierz As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents btnNaleznosci As System.Windows.Forms.Button
    Friend WithEvents btnZapasy As System.Windows.Forms.Button
    Friend WithEvents btnKoszty As System.Windows.Forms.Button
    Friend WithEvents btnMedia As System.Windows.Forms.Button
    Friend WithEvents btnSprzedaz As System.Windows.Forms.Button
    Friend WithEvents btnZatrudnienie As System.Windows.Forms.Button
    Friend WithEvents btnZWKZlecenia As System.Windows.Forms.Button
    Friend WithEvents btnZWKWIP As System.Windows.Forms.Button
End Class
