﻿
#Region "Opcje i referencje"

Option Explicit On
Option Strict On
Option Infer Off

Imports System.ComponentModel
Imports System.Data
Imports System.Data.OleDb
Imports System.Text
Imports XL = Microsoft.Office.Interop.Excel

#End Region ' Opcje i referencje

Public Class frmDane

#Region "Deklaracje"

    Private MF As frmMain
    Private m_Zapisane As Boolean
    Private m_Filtr As String = ""
    Private dt As New DataTable

    Private oApp As XL.Application
    Private oBook As XL._Workbook
    Private oSheet As XL._Worksheet

    Public ReadOnly Property Zapisane() As Boolean
        Get
            Return m_Zapisane
        End Get
    End Property

    Public Property Filtr() As String
        Get
            Return m_Filtr
        End Get
        Set(ByVal value As String)
            m_Filtr = value
        End Set
    End Property

#End Region ' Deklaracje

    Private Sub frmDane_Load() Handles Me.Load

        Dim da As New OleDbDataAdapter
        Dim bs As New BindingSource
        Dim m_SQL As String = ""
        Dim i As Integer

        Const CO_OKRESOWE As String = "tblNaleznosci_tblPLNaleznosci_tblBUNaleznosci_" _
                                    + "tblSprzedaz_tblPLSprzedaz_tblBUSprzedaz_" _
                                    + "tblWIP_tblPLWIP_tblBUWIP_" _
                                    + "tblZapasy_tblPLZapasy_tblBUZapasy_" _
                                    + "tblZWPC_tblPLZWPC_tblBUZWPC_" _
                                    + "tblMedia_tblPLMedia_tblBUMedia_" _
                                    + "tblZatrudnienie_tblPLZatrudnienie_tblBUZatrudnienie_" _
                                    + "tblBlokady_tblAmortyzacja"

        Try

            MF = frmMain

            With MF

                Select Case .WybranaTabela

                    Case "tblNaleznosci", "tblPLNaleznosci", "tblBUNaleznosci"
                        da = .DA_TI(.WybranaTabela, "Miesiac DESC, IDKlienta")

                    Case "tblSprzedaz", "tblPLSprzedaz", "tblBUSprzedaz"
                        da = .DA_TI(.WybranaTabela, "Miesiac DESC, Material, IDKlienta")

                    Case "tblSources"
                        da = .DA_TI("tblSources", "Source, NrKol")

                    Case "tblWIP", "tblPLWIP", "tblBUWIP"
                        da = .DA_TI(.WybranaTabela, "Okres DESC, NrZlecenia, Partia")

                    Case "tblZapasy", "tblPLZapasy", "tblBUZapasy"
                        da = .DA_TI(.WybranaTabela, "Miesiac DESC, Material")

                    Case "tblZWPC", "tblPLZWPC", "tblBUZWPC"
                        da = .DA_TI(.WybranaTabela, "Mc DESC, Instalacja, MPK, CalCo, RK, Material, GrKoszt, Dostawca, JM")

                    Case "tblMedia", "tblPLMedia", "tblBUMedia"
                        da = .DA_TI(.WybranaTabela, "Mc DESC, MPK, Media, JM")

                    Case "tblZatrudnienie", "tblPLZatrudnienie", "tblBUZatrudnienie"
                        da = .DA_TI(.WybranaTabela, "Mc DESC, MPK")

                    Case "tblKursyWalut"
                        da = .DA_TI(.WybranaTabela, "MM_dd DESC")

                    Case "tblBlokady"
                        da = .DA_TI(.WybranaTabela, "Miesiac, Obiekt")

                    Case "tblAmortyzacja"
                        da = .DA_TI(.WybranaTabela, "Wydzial, Mc")

                    Case Else
                        da = .DA_TI(.WybranaTabela, "ID")

                End Select

            End With

            If Not m_Filtr = "" Then
                With da.SelectCommand
                    i = .CommandText.IndexOf("ORDER BY")
                    If i > 0 Then
                        .CommandText = .CommandText.Substring(0, i) + " WHERE " + m_Filtr + " " + .CommandText.Substring(i - 1)
                    Else
                        da.SelectCommand.CommandText += " WHERE " + m_Filtr
                    End If
                End With
            End If

            dt.Rows.Clear()
            da.Fill(dt)
            dt.TableName = MF.WybranaTabela

            With Me
                .KeyPreview = True
                .Text = MF.WybranaTabela.Replace("tbl", "")

                .dgvDane.DataSource = Nothing
                bs.DataSource = dt
                .dgvDane.DataSource = bs

                .bnDane.BindingSource = bs
                .tsbCopy.Enabled = False
                .tsmFiltrujOkres.Enabled = CO_OKRESOWE.IndexOf(MF.WybranaTabela) > -1

            End With

            With My.Settings

                Select Case MF.WybranaTabela

                    Case "tblGrupyKalk"
                        Me.Size = .scrGrKalk
                        Me.tsbCopy.Enabled = True
                        Me.dgvDane.Columns(0).Frozen = True

                    Case "tblGrupyMat"
                        Me.Size = .scrGrMat
                        Me.tsbCopy.Enabled = True
                        Me.dgvDane.Columns(0).Frozen = True

                    Case "tblKlienci"
                        Me.Size = .scrKlienci
                        Me.dgvDane.Columns(0).Frozen = True

                    Case "tblMagazyny"
                        Me.Size = .scrMagazyny
                        Me.tsbCopy.Enabled = True
                        Me.dgvDane.Columns(0).Frozen = True

                    Case "tblMaterialy"
                        Me.Size = .scrMaterialy
                        Me.tsbCopy.Enabled = True
                        Me.dgvDane.Columns(0).Frozen = True

                    Case "tblNaleznosci", "tblPLNaleznosci", "tblBUNaleznosci"
                        Me.Size = .scrNaleznosci
                        With Me.dgvDane.Columns("Kwota").DefaultCellStyle
                            .Format = "# ### ##0.00"
                            .Alignment = DataGridViewContentAlignment.TopRight
                        End With
                        Me.dgvDane.Columns(0).Frozen = True

                    Case "tblSprzedaz", "tblPLSprzedaz", "tblBUSprzedaz"
                        Me.Size = .scrSprzedaz
                        With Me.dgvDane
                            With .Columns("Waga").DefaultCellStyle
                                .Format = "# ##0.000"
                                .Alignment = DataGridViewContentAlignment.TopRight
                            End With
                            With .Columns("Kwota").DefaultCellStyle
                                .Format = "# ### ##0.00"
                                .Alignment = DataGridViewContentAlignment.TopRight
                            End With
                            With .Columns("Rabat").DefaultCellStyle
                                .Format = "# ### ##0.00"
                                .Alignment = DataGridViewContentAlignment.TopRight
                            End With
                            With .Columns("PIP").DefaultCellStyle
                                .Format = "# ### ##0.00"
                                .Alignment = DataGridViewContentAlignment.TopRight
                            End With
                            With .Columns("Transport").DefaultCellStyle
                                .Format = "# ### ##0.00"
                                .Alignment = DataGridViewContentAlignment.TopRight
                            End With
                            With .Columns("Other").DefaultCellStyle
                                .Format = "# ### ##0.00"
                                .Alignment = DataGridViewContentAlignment.TopRight
                            End With
                            With .Columns("Overhead").DefaultCellStyle
                                .Format = "# ### ##0.00"
                                .Alignment = DataGridViewContentAlignment.TopRight
                            End With
                            With .Columns("Pozostale").DefaultCellStyle
                                .Format = "# ### ##0.00"
                                .Alignment = DataGridViewContentAlignment.TopRight
                            End With
                            With .Columns("PIP_SAP").DefaultCellStyle
                                .Format = "# ### ##0.00"
                                .Alignment = DataGridViewContentAlignment.TopRight
                            End With
                            With .Columns("EBiTDA").DefaultCellStyle
                                .Format = "# ### ##0.00"
                                .Alignment = DataGridViewContentAlignment.TopRight
                            End With
                            .Columns(1).Frozen = True
                        End With

                    Case "tblRodzajeMat"
                        Me.Size = .scrRodzajeMat
                        Me.dgvDane.Columns(0).Frozen = True

                    Case "tblSources"
                        Me.Size = .scrSources
                        Me.dgvDane.Columns(0).Frozen = True

                    Case "tblWIP", "tblPLWIP", "tblBUWIP"
                        Me.Size = .scrWIP
                        With Me.dgvDane
                            With .Columns("Waga").DefaultCellStyle
                                .Format = "# ##0.000"
                                .Alignment = DataGridViewContentAlignment.TopRight
                            End With
                            With .Columns("Kwota").DefaultCellStyle
                                .Format = "# ### ##0.00"
                                .Alignment = DataGridViewContentAlignment.TopRight
                            End With
                            With .Columns("WagaOdkuwki").DefaultCellStyle
                                .Format = "# ##0.000"
                                .Alignment = DataGridViewContentAlignment.TopRight
                            End With
                            With .Columns("WagaKoncowa").DefaultCellStyle
                                .Format = "# ##0.000"
                                .Alignment = DataGridViewContentAlignment.TopRight
                            End With
                            .Columns(1).Frozen = True
                        End With

                    Case "tblZapasy", "tblPLZapasy", "tblBUZapasy"
                        Me.Size = .scrZapasy
                        With Me.dgvDane
                            With .Columns("Ilosc").DefaultCellStyle
                                .Format = "# ##0"
                                .Alignment = DataGridViewContentAlignment.TopRight
                            End With
                            With .Columns("Waga").DefaultCellStyle
                                .Format = "# ##0.000"
                                .Alignment = DataGridViewContentAlignment.TopRight
                            End With
                            With .Columns("Kwota").DefaultCellStyle
                                .Format = "# ### ##0.00"
                                .Alignment = DataGridViewContentAlignment.TopRight
                            End With
                            .Columns(1).Frozen = True
                        End With

                        ' 2012-08-03
                    Case "tblCALCO"
                        Me.Size = .scrCALCO
                        Me.dgvDane.Columns(0).Frozen = True

                    Case "tblDostawcy"
                        Me.Size = .scrDostawcy
                        Me.dgvDane.Columns(0).Frozen = True

                    Case "tblGrKoszt"
                        Me.Size = .scrGrKoszt
                        Me.dgvDane.Columns(0).Frozen = True

                    Case "tblINEF"
                        Me.Size = .scrINEF
                        Me.dgvDane.Columns(0).Frozen = True

                    Case "tblMPK"
                        Me.Size = .scrMPK
                        Me.dgvDane.Columns(0).Frozen = True

                    Case "tblRK"
                        Me.Size = .scrRK
                        Me.dgvDane.Columns(0).Frozen = True

                    Case "tblZWPC", "tblPLZWPC", "tblBUZWPC"
                        Me.Size = .scrZWPC
                        With Me.dgvDane
                            With .Columns("Ilosc").DefaultCellStyle
                                .Format = "# ##0.000"
                                .Alignment = DataGridViewContentAlignment.TopRight
                            End With
                            With .Columns("Wartosc").DefaultCellStyle
                                .Format = "# ### ##0.00"
                                .Alignment = DataGridViewContentAlignment.TopRight
                            End With
                        End With

                    Case "tblGEn"
                        Me.Size = .scrGEn

                    Case "tblMedia", "tblPLMedia", "tblBUMedia"
                        Me.Size = .scrMedia
                        With Me.dgvDane
                            With .Columns("Ilosc").DefaultCellStyle
                                .Format = "# ##0.000"
                                .Alignment = DataGridViewContentAlignment.TopRight
                            End With
                            With .Columns("Wartosc").DefaultCellStyle
                                .Format = "# ### ##0.00"
                                .Alignment = DataGridViewContentAlignment.TopRight
                            End With
                        End With

                    Case "tblZatrudnienie", "tblZPLatrudnienie", "tblBUZatrudnienie"
                        Me.Size = .scrZatrudnienie
                        With Me.dgvDane
                            With .Columns("Ilosc").DefaultCellStyle
                                .Format = "# ##0"
                                .Alignment = DataGridViewContentAlignment.TopRight
                            End With
                        End With

                    Case "tblKursyWalut"
                        Me.Size = .scrKursyWalut
                        With Me.dgvDane
                            With .Columns("EUR").DefaultCellStyle
                                .Format = "##0.0000"
                                .Alignment = DataGridViewContentAlignment.TopRight
                            End With
                            With .Columns("USD").DefaultCellStyle
                                .Format = "##0.0000"
                                .Alignment = DataGridViewContentAlignment.TopRight
                            End With
                            With .Columns("GBP").DefaultCellStyle
                                .Format = "##0.0000"
                                .Alignment = DataGridViewContentAlignment.TopRight
                            End With
                            With .Columns("DKK").DefaultCellStyle
                                .Format = "##0.0000"
                                .Alignment = DataGridViewContentAlignment.TopRight
                            End With
                        End With

                    Case "tblBlokady"
                        Me.Size = .scrBlokady
                        Me.dgvDane.Columns(1).Frozen = True

                    Case "tblFormuly"
                        Me.Size = .scrFormuly

                    Case "tblAmortyzacja"
                        Me.Size = .scrAmortyzacja
                        With Me.dgvDane
                            With .Columns("Stawka").DefaultCellStyle
                                .Format = "##0.0000"
                                .Alignment = DataGridViewContentAlignment.TopRight
                            End With
                        End With
                End Select

                Me.Location = .StartOfDane

            End With

        Catch ex As Exception

            MessageBox.Show(ex.Message, MF.WybranaTabela, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Me.Dispose()

        Finally

            bs = Nothing
            da = Nothing
            With MF
                .tsslMain.Text = ""
                .Cursor = Cursors.Default
                .WindowState = FormWindowState.Minimized
            End With
            Application.DoEvents()

        End Try

    End Sub

    ''' <summary>
    ''' Kopiuje opis polski do angielskiego, gdy angielski pusty
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CopyDescription() Handles tsbCopy.Click
        Dim i As Integer

        Select Case MF.WybranaTabela
            Case "tblGrupyKalk"
                For i = 0 To dt.Rows.Count - 1
                    With dt.Rows(i)
                        If .Item("GrKalkEN") Is Nothing OrElse Trim(.Item("GrKalkEN").ToString) = "" Then
                            .Item("GrKalkEN") = .Item("GrKalkPL").ToString
                        End If
                    End With
                Next i

            Case "tblGrupyMat"
                For i = 0 To dt.Rows.Count - 1
                    With dt.Rows(i)
                        If .Item("GrMaterEN") Is Nothing OrElse Trim(.Item("GrMaterEN").ToString) = "" Then
                            .Item("GrMaterEN") = .Item("GrMaterPL").ToString
                        End If
                    End With
                Next i

            Case "tblMaterialy"
                For i = 0 To dt.Rows.Count - 1
                    With dt.Rows(i)
                        If .Item("MaterialEN") Is Nothing OrElse Trim(.Item("MaterialEN").ToString) = "" Then
                            .Item("MaterialEN") = .Item("MaterialPL").ToString
                        End If
                    End With
                Next i

        End Select

    End Sub

    Private Sub Zapisz() Handles tsbZapisz.Click
        Dim da As OleDbDataAdapter = New OleDbDataAdapter
        Dim dtChange As DataTable = dt.GetChanges
        Dim blnStale As Boolean = False

        If dtChange Is Nothing Then Exit Sub

        ' 2013-04-11 sprawdzenie blokady

        If MF.BlokadaAktualizacji(MF.WybranaTabela) Then Exit Sub

        Select Case MF.WybranaTabela
            Case "tblNaleznosci", "tblPLNaleznosci", "tblBUNaleznosci"
                da = MF.DA_TI(MF.WybranaTabela, "Miesiac DESC, IDKlienta")

            Case "tblSources"
                da = MF.DA_TI("tblSources", "Source, NrKol")

            Case "tblSprzedaz", "tblPLSprzedaz", "tblBUSprzedaz"
                da = MF.DA_TI(MF.WybranaTabela, "Miesiac DESC, Material, IDKlienta")

            Case "tblWIP", "tblPLWIP", "tblBUWIP"
                da = MF.DA_TI(MF.WybranaTabela, "Okres DESC, NrZlecenia, Partia")

            Case "tblZapasy", "tblPLZapasy", "tblBUZapasy"
                da = MF.DA_TI(MF.WybranaTabela, "Miesiac DESC, Material")

            Case "tblZWPC", "tblPLZWPC", "tblBUZWPC"
                da = MF.DA_TI(MF.WybranaTabela, "Mc DESC, Instalacja, MPK, CalCo, RK, Material, GrKoszt, Dostawca, JM")

            Case "tblMedia", "tblPLMedia", "tblBUMedia"
                da = MF.DA_TI(MF.WybranaTabela, "Mc DESC, MPK, Media, JM")

            Case "tblZatrudnienie", "tblPLZatrudnienie", "tblBUZatrudnienie"
                da = MF.DA_TI(MF.WybranaTabela, "Mc DESC, MPK")

            Case "tblKursyWalut"
                da = MF.DA_TI(MF.WybranaTabela, "MM_dd DESC")

            Case "tblBlokady"
                da = MF.DA_TI(MF.WybranaTabela, "Miesiac, Obiekt")

            Case "tblAmortyzacja"
                da = MF.DA_TI(MF.WybranaTabela, "Wydzial, Mc")

            Case Else
                da = MF.DA_TI(MF.WybranaTabela, "ID")
                blnStale = True

        End Select

        Try

            If blnStale Then UzupelnienieRocznych(dtChange, da.SelectCommand.CommandText)
Zapisz:

            da.Update(dtChange)
            dt.Rows.Clear()
            da.Fill(dt)

        Catch ex As Exception

            MessageBox.Show(ex.Message, MF.WybranaTabela, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

        Finally

            da = Nothing
            dtChange = Nothing

        End Try

    End Sub

    Private Sub UzupelnienieRocznych(ByVal dtChange As DataTable, ByVal m_Select As String)
        ' Uzupełnienie danych stałych rok wcześniej i do ostatniego istniejącego
        Dim m_Year As String = (CInt(MF.Okres.Substring(0, 4)) - 1).ToString
        Dim m_File As String
        Dim i As Integer
        Dim obj(0 To dtChange.Columns.Count - 1) As Object
        Dim m_cns As New OleDbConnection
        Dim dtG As New DataTable
        Dim dtC As New DataTable
        Dim bs As New BindingSource
        Dim daC As OleDbDataAdapter = New OleDbDataAdapter
        Dim m_Ado As New CADO.CADO
        Dim cb As OleDbCommandBuilder

        If MessageBox.Show("Uzupełnić dane z poprzedniego roku i lat następnych?", "Aktualizacja danych stałych", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then Exit Sub

        Try

            While True
                If m_Year = MF.tsdtpOkres.Value.Year.ToString Then GoTo Nastepny
                m_File = My.Settings.Baza.Replace(MF.tsdtpOkres.Value.Year.ToString, m_Year)

                Try
                    With m_Ado
                        .Baza = m_File
                        .Folder = My.Settings.Sciezka
                        .Haslo = My.Settings.Myk
                        If Not .OpenData(True) Then Throw New ArgumentException
                        m_cns.ConnectionString = .CN.ConnectionString
                    End With
                    daC = New OleDbDataAdapter

                    daC.SelectCommand = New OleDbCommand(m_Select, m_cns)
                    cb = New OleDbCommandBuilder(daC)
                    daC.InsertCommand = cb.GetInsertCommand
                    daC.UpdateCommand = cb.GetUpdateCommand
                    daC.DeleteCommand = cb.GetDeleteCommand

                    dtG = New DataTable
                    daC.Fill(dtG)
                    bs.DataSource = dtG

                    For Each dr As DataRow In dtChange.Rows
                        If dr.RowState = DataRowState.Added OrElse dr.RowState = DataRowState.Modified Then
                            With dtG.Columns(0)
                                i = bs.Find(.ColumnName, dr(.ColumnName))
                            End With
                            obj = dr.ItemArray
                            If i < 0 Then
                                dtG.Rows.Add(obj)
                            Else
                                dtG.Rows(i).ItemArray = obj
                            End If

                        ElseIf dr.RowState = DataRowState.Deleted Then
                            With dtG.Columns(0)
                                i = bs.Find(.ColumnName, dr(.ColumnName, DataRowVersion.Original))
                            End With
                            If i > -1 Then
                                dtG.Rows(i).Delete()
                            End If
                        End If
                    Next dr

                    dtC = dtG.GetChanges
                    If Not dtC Is Nothing Then _
                        daC.Update(dtC)

                Catch ex As Exception
                    MessageBox.Show(ex.Message, m_File, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End Try
Nastepny:
                m_Year = (CInt(m_Year) + 1).ToString
                If CInt(m_Year) > Now.Year Then Exit While

            End While

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Przenoszenie danych stałych", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally

            bs = Nothing
            dtC = Nothing
            dtG = Nothing
            daC = Nothing
            m_Ado = Nothing
            cb = Nothing

        End Try


    End Sub

    Private Sub frmDaneFinish() Handles Me.FormClosing

        With My.Settings

            Select Case MF.WybranaTabela

                Case "tblGrupyKalk"
                    .scrGrKalk = Me.Size

                Case "tblGrupyMat"
                    .scrGrMat = Me.Size

                Case "tblKlienci"
                    .scrKlienci = Me.Size

                Case "tblMagazyny"
                    .scrMagazyny = Me.Size

                Case "tblMaterialy"
                    .scrMaterialy = Me.Size

                Case "tblNaleznosci", "tblPLNaleznosci", "tblBUNaleznosci"
                    .scrNaleznosci = Me.Size

                Case "tblRodzajeMat"
                    .scrRodzajeMat = Me.Size

                Case "tblSources"
                    .scrSources = Me.Size

                Case "tblSprzedaz", "tblPLSprzedaz", "tblBUSprzedaz"
                    .scrSprzedaz = Me.Size

                Case "tblWIP", "tblPLWIP", "tblBUWIP"
                    .scrWIP = Me.Size

                Case "tblZapasy", "tblPLZapasy", "tblBUZapasy"
                    .scrZapasy = Me.Size

                    ' 2012-08-03
                Case "tblCALCO"
                    .scrCALCO = Me.Size

                Case "tblDostawcy"
                    .scrDostawcy = Me.Size

                Case "tblGrKoszt"
                    .scrGrKoszt = Me.Size

                Case "tblINEF"
                    .scrINEF = Me.Size

                Case "tblMPK"
                    .scrMPK = Me.Size

                Case "tblRK"
                    .scrRK = Me.Size

                Case "tblZWPC", "tblPLZWPC", "tblBUZWPC"
                    .scrZWPC = Me.Size

                Case "tblGEn"
                    .scrGEn = Me.Size

                Case "tblMedia", "tblPLMedia", "tblBUMedia"
                    .scrMedia = Me.Size

                Case "tblZatrudnienie", "tblPLZatrudnienie", "tblBUZatrudnienie"
                    .scrZatrudnienie = Me.Size

                Case "tblKursyWalut"
                    .scrKursyWalut = Me.Size

                Case "tblFormuly"
                    .scrFormuly = Me.Size

                Case "tblBlokady"
                    .scrBlokady = Me.Size

                Case "tblAmortyzacja"
                    .scrAmortyzacja = Me.Size

            End Select

            .StartOfDane = Me.Location
            .Save()

        End With

    End Sub

#Region "Menu kontekstowe"

    Private Sub KopiujDoSchowka() Handles tsmCopy.Click
        Me.dgvDane.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
        Clipboard.SetDataObject(dgvDane.GetClipboardContent)
    End Sub

    Private Sub WklejDoExcela() Handles tsmPasteToExcel.Click
        Dim i, j As Integer
        Dim dc As DataGridViewColumn
        Dim dr As DataRow
        Dim bs As New BindingSource
        Dim aVal(dgvDane.Rows.Count, dgvDane.Columns.Count - 1) As Object
        Dim m_File As String = MF.WybranaTabela.Substring(3) + ".XLS"

        Me.Cursor = Cursors.WaitCursor

        Try

            oApp = New XL.Application
            bs.DataSource = Me.bnDane.BindingSource.DataSource
            oBook = oApp.Workbooks.Add()
            oSheet = CType(oBook.Sheets(1), XL._Worksheet)

            For Each dc In dgvDane.Columns
                aVal(0, j) = dc.HeaderText
                j += 1
            Next dc
            For i = 1 To bs.Count
                dr = CType(bs.Item(i - 1), DataRowView).Row
                For j = 0 To dgvDane.Columns.Count - 1
                    aVal(i, j) = dr.Item(j)
                Next
            Next

            oSheet.Range("A1").Resize(dgvDane.Rows.Count + 1, dgvDane.Columns.Count).Value = aVal

            Try
                Me.WindowState = FormWindowState.Minimized
Poczatek:
                With oApp
                    .Visible = True
                    .ScreenUpdating = True
                    While .Visible
                    End While
                    .Quit()
                End With
            Catch ex As Exception
                GoTo Poczatek
            Finally

                pReleaseObject(oSheet)

                Try
                    oBook.Close()
                Catch ex As Exception
                End Try
                pReleaseObject(oBook)

                Try
                    oApp.Quit()
                Catch ex As Exception
                End Try
                pReleaseObject(oApp)

            End Try

        Catch ex As Exception

            MessageBox.Show(ex.Message, "Wklejanie do Excel-a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub

        Finally
            WindowState = FormWindowState.Normal
            Cursor = Cursors.Default
        End Try

        If "tblCALCO_tblDostawcy_tblGEn_tblGrKoszt_tblGrupyKalk_tblGrupyMat_tblINEF_tblKlienci_tblMagazyny_tblMaterialy_tblMedia_tblMPK_tblRK_tblRodzajeMat_tblFormuly_tblKursyWalut".IndexOf(MF.WybranaTabela) < 0 Then Exit Sub
        If Not My.Computer.FileSystem.FileExists("C:\Tmp\" + m_File) Then Exit Sub

        If MessageBox.Show("Aktualizować bazę danych na podstawie arkusza 1?", "Hurtowa zmiana danych", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) _
                        = Windows.Forms.DialogResult.No Then Exit Sub

        Dim m_Ado As New CADO.CADO
        Dim da As OleDbDataAdapter
        Dim dtB As New DataTable

        Try

            With m_Ado
                .Baza = m_File
                .Folder = "C:\Tmp\"
                .HDR = True
                If Not .OpenData(True) Then
                    Throw New ArgumentException("Brak dostępu do pliku <" + m_File + ">")
                End If
                dt = New DataTable
                da = New OleDbDataAdapter("SELECT * FROM [Arkusz1$]  ", .CN)
                da.Fill(dt)

                My.Computer.FileSystem.DeleteFile(.Folder + .Baza)

            End With

            If dt.Rows.Count = 0 Then Exit Try

            da = MF.DA_TI(MF.WybranaTabela)
            da.Fill(dtB)
            bs.DataSource = dtB

            For Each dr In dt.Rows
                With dt.Columns(0)
                    i = bs.Find(.ColumnName, dr(.ColumnName))
                End With
                If i > -1 Then
                    With dtB.Rows(i)
                        For j = 1 To dtB.Columns.Count - 1
                            .Item(j) = dr.Item(j)
                        Next j
                    End With
                End If
            Next

            dt = New DataTable
            dt = dtB.GetChanges

            UzupelnienieRocznych(dt, da.SelectCommand.CommandText)

            da.Update(dt)

            Me.Close()

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Aktualizacja na podstawie <" + m_File + ">", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally

            m_Zapisane = False

            bs = Nothing
            dr = Nothing
            dt = Nothing
            dtB = Nothing

        End Try

    End Sub

    Private Sub SzukajFiltruj(ByVal sender As System.Object, ByVal e As System.EventArgs) _
                                Handles tsmFind.Click, _
                                        tsmFiltrujKolumne.Click
        Dim tsm As ToolStripMenuItem = CType(sender, ToolStripMenuItem)

        If tsm.Name.IndexOf("Find") > -1 Then
            pDGSearch(Me.dgvDane)
        Else
            pDGSetFilter(Me.dgvDane)
        End If
    End Sub

    Private Sub FiltrujOkres(ByVal sender As System.Object, ByVal e As System.EventArgs) _
                                    Handles tsmFiltrujOkres.Click, tsmUsunFiltr.Click
        Dim tsm As ToolStripMenuItem = DirectCast(sender, ToolStripMenuItem)
        Dim bs As New BindingSource
        Try
            With bs

                .DataSource = dgvDane.DataSource
                .RemoveFilter()

                If tsm.Name <> "tsmUsunFiltr" Then

                    If "tblBUWIP_tblPLWIP_tblWIP".IndexOf(MF.WybranaTabela) > -1 Then
                        .Filter = "Okres = '" + MF.Okres.Substring(0, 7) + "'"

                    ElseIf "tblBUZWPC_tblPLZWPC_tblZWPC".IndexOf(MF.WybranaTabela) > -1 Then
                        .Filter = "Mc = " + MF.Okres.Substring(5, 2)

                    ElseIf "tblBUMedia_tblPLMedia_tblMedia".IndexOf(MF.WybranaTabela) > -1 Then
                        .Filter = "Mc = '" + MF.Okres.Substring(5, 2) + "'"

                    ElseIf "tblBUZatrudnienie_tblPLZatrudnienie_tblZatrudnienie".IndexOf(MF.WybranaTabela) > -1 Then
                        .Filter = "Mc = " + MF.Okres.Substring(5, 2)

                    Else
                        .Filter = "Miesiac = " + mf.Okres.Substring(5, 2)
                    End If

                End If
            End With

            Me.dgvDane.DataSource = bs
            Me.bnDane.BindingSource = bs

        Catch ex As Exception
        Finally
            bs = Nothing
            tsm = Nothing
        End Try
    End Sub

    Private Sub ZapytaniaSQL() Handles tsmZapytania.Click
        Dim ZS As frmSQL = New frmSQL
        Dim txt As String = "SELECT"
        Dim i As Integer

        With Me.dgvDane
            For i = 0 To .ColumnCount - 1
                txt += " " + .Columns(i).HeaderText + ","
            Next i
        End With

        If txt.Substring(txt.Length - 1) = "," Then
            txt = txt.Substring(0, txt.Length - 1)
        Else
            txt += " *"
        End If
        txt += " FROM " + MF.WybranaTabela + " WHERE ? GROUP BY ? ORDER BY ?"

        With ZS
            .Zapytanie = txt
            .ShowDialog()
        End With

    End Sub

#End Region ' Menu kontekstowe

End Class