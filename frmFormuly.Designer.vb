﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFormuly
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.btnStart = New System.Windows.Forms.Button
        Me.btnExit = New System.Windows.Forms.Button
        Me.txtZmianaZ = New System.Windows.Forms.TextBox
        Me.txtZmianaNa = New System.Windows.Forms.TextBox
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel
        Me.chkLacza = New System.Windows.Forms.CheckBox
        Me.chkArkusz = New System.Windows.Forms.CheckBox
        Me.btnLista = New System.Windows.Forms.Button
        Me.txtArkusz = New System.Windows.Forms.TextBox
        Me.txtFile = New System.Windows.Forms.TextBox
        Me.lstArkusze = New System.Windows.Forms.ListBox
        Me.ssFormuly = New System.Windows.Forms.StatusStrip
        Me.prgAkcja = New System.Windows.Forms.ToolStripProgressBar
        Me.tsslFormuly = New System.Windows.Forms.ToolStripStatusLabel
        Me.txtWymiana = New System.Windows.Forms.TextBox
        Me.ttFormuly = New System.Windows.Forms.ToolTip(Me.components)
        Me.TableLayoutPanel1.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.ssFormuly.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.btnStart, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.btnExit, 1, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(364, 388)
        Me.TableLayoutPanel1.Margin = New System.Windows.Forms.Padding(4)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(195, 40)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'btnStart
        '
        Me.btnStart.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnStart.Location = New System.Drawing.Point(4, 4)
        Me.btnStart.Margin = New System.Windows.Forms.Padding(4)
        Me.btnStart.Name = "btnStart"
        Me.btnStart.Size = New System.Drawing.Size(89, 32)
        Me.btnStart.TabIndex = 0
        Me.btnStart.Text = "&Start"
        '
        'btnExit
        '
        Me.btnExit.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnExit.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnExit.Location = New System.Drawing.Point(101, 4)
        Me.btnExit.Margin = New System.Windows.Forms.Padding(4)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(89, 32)
        Me.btnExit.TabIndex = 1
        Me.btnExit.Text = "E&xit"
        '
        'txtZmianaZ
        '
        Me.txtZmianaZ.Location = New System.Drawing.Point(20, 12)
        Me.txtZmianaZ.Name = "txtZmianaZ"
        Me.txtZmianaZ.Size = New System.Drawing.Size(539, 26)
        Me.txtZmianaZ.TabIndex = 1
        '
        'txtZmianaNa
        '
        Me.txtZmianaNa.Location = New System.Drawing.Point(20, 44)
        Me.txtZmianaNa.Name = "txtZmianaNa"
        Me.txtZmianaNa.Size = New System.Drawing.Size(539, 26)
        Me.txtZmianaNa.TabIndex = 2
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 2
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.82931!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 72.17069!))
        Me.TableLayoutPanel2.Controls.Add(Me.chkLacza, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.chkArkusz, 0, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.btnLista, 0, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.txtArkusz, 0, 3)
        Me.TableLayoutPanel2.Controls.Add(Me.txtFile, 0, 4)
        Me.TableLayoutPanel2.Controls.Add(Me.lstArkusze, 1, 0)
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(20, 87)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 5
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(539, 285)
        Me.TableLayoutPanel2.TabIndex = 3
        '
        'chkLacza
        '
        Me.chkLacza.AutoSize = True
        Me.chkLacza.Location = New System.Drawing.Point(3, 3)
        Me.chkLacza.Name = "chkLacza"
        Me.chkLacza.Size = New System.Drawing.Size(119, 22)
        Me.chkLacza.TabIndex = 0
        Me.chkLacza.Text = "Wymień łą&cza"
        Me.ttFormuly.SetToolTip(Me.chkLacza, "Blokuje wybór arkuszy")
        Me.chkLacza.UseVisualStyleBackColor = True
        '
        'chkArkusz
        '
        Me.chkArkusz.AutoSize = True
        Me.chkArkusz.Location = New System.Drawing.Point(3, 37)
        Me.chkArkusz.Name = "chkArkusz"
        Me.chkArkusz.Size = New System.Drawing.Size(133, 22)
        Me.chkArkusz.TabIndex = 1
        Me.chkArkusz.Text = "Wybrany a&rkusz"
        Me.ttFormuly.SetToolTip(Me.chkArkusz, "Nie działa przy wymianie łączy")
        Me.chkArkusz.UseVisualStyleBackColor = True
        '
        'btnLista
        '
        Me.btnLista.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.btnLista.Location = New System.Drawing.Point(37, 71)
        Me.btnLista.Name = "btnLista"
        Me.btnLista.Size = New System.Drawing.Size(75, 28)
        Me.btnLista.TabIndex = 2
        Me.btnLista.Text = "O&k"
        Me.btnLista.UseVisualStyleBackColor = True
        '
        'txtArkusz
        '
        Me.txtArkusz.Location = New System.Drawing.Point(3, 105)
        Me.txtArkusz.Name = "txtArkusz"
        Me.txtArkusz.Size = New System.Drawing.Size(133, 26)
        Me.txtArkusz.TabIndex = 3
        '
        'txtFile
        '
        Me.TableLayoutPanel2.SetColumnSpan(Me.txtFile, 2)
        Me.txtFile.Location = New System.Drawing.Point(3, 254)
        Me.txtFile.Name = "txtFile"
        Me.txtFile.Size = New System.Drawing.Size(533, 26)
        Me.txtFile.TabIndex = 4
        '
        'lstArkusze
        '
        Me.lstArkusze.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lstArkusze.FormattingEnabled = True
        Me.lstArkusze.ItemHeight = 18
        Me.lstArkusze.Location = New System.Drawing.Point(152, 3)
        Me.lstArkusze.Name = "lstArkusze"
        Me.TableLayoutPanel2.SetRowSpan(Me.lstArkusze, 4)
        Me.lstArkusze.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple
        Me.lstArkusze.Size = New System.Drawing.Size(384, 238)
        Me.lstArkusze.TabIndex = 5
        '
        'ssFormuly
        '
        Me.ssFormuly.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.prgAkcja, Me.tsslFormuly})
        Me.ssFormuly.Location = New System.Drawing.Point(0, 458)
        Me.ssFormuly.Name = "ssFormuly"
        Me.ssFormuly.Size = New System.Drawing.Size(580, 22)
        Me.ssFormuly.TabIndex = 4
        Me.ssFormuly.Text = "StatusStrip1"
        '
        'prgAkcja
        '
        Me.prgAkcja.Name = "prgAkcja"
        Me.prgAkcja.Size = New System.Drawing.Size(100, 16)
        '
        'tsslFormuly
        '
        Me.tsslFormuly.Name = "tsslFormuly"
        Me.tsslFormuly.Size = New System.Drawing.Size(0, 17)
        '
        'txtWymiana
        '
        Me.txtWymiana.Location = New System.Drawing.Point(20, 392)
        Me.txtWymiana.Name = "txtWymiana"
        Me.txtWymiana.Size = New System.Drawing.Size(100, 26)
        Me.txtWymiana.TabIndex = 5
        Me.txtWymiana.Visible = False
        '
        'frmFormuly
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 18.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnExit
        Me.ClientSize = New System.Drawing.Size(580, 480)
        Me.ControlBox = False
        Me.Controls.Add(Me.txtWymiana)
        Me.Controls.Add(Me.ssFormuly)
        Me.Controls.Add(Me.TableLayoutPanel2)
        Me.Controls.Add(Me.txtZmianaNa)
        Me.Controls.Add(Me.txtZmianaZ)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(586, 466)
        Me.Name = "frmFormuly"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Zmiana formuł"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel2.PerformLayout()
        Me.ssFormuly.ResumeLayout(False)
        Me.ssFormuly.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents btnStart As System.Windows.Forms.Button
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents txtZmianaZ As System.Windows.Forms.TextBox
    Friend WithEvents txtZmianaNa As System.Windows.Forms.TextBox
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents chkLacza As System.Windows.Forms.CheckBox
    Friend WithEvents chkArkusz As System.Windows.Forms.CheckBox
    Friend WithEvents btnLista As System.Windows.Forms.Button
    Friend WithEvents txtArkusz As System.Windows.Forms.TextBox
    Friend WithEvents txtFile As System.Windows.Forms.TextBox
    Friend WithEvents lstArkusze As System.Windows.Forms.ListBox
    Friend WithEvents ssFormuly As System.Windows.Forms.StatusStrip
    Friend WithEvents prgAkcja As System.Windows.Forms.ToolStripProgressBar
    Friend WithEvents txtWymiana As System.Windows.Forms.TextBox
    Friend WithEvents ttFormuly As System.Windows.Forms.ToolTip
    Friend WithEvents tsslFormuly As System.Windows.Forms.ToolStripStatusLabel

End Class
