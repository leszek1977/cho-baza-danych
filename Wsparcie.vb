﻿
Imports System.IO
Imports System.Data.OleDb
Imports CM = ClHelpFul.CenterMsg
Imports XL = Microsoft.Office.Interop.Excel

Module Wsparcie

    Sub PobierzTransportSprzedazy(ByVal oTra As Object, _
                                  ByVal dtW As DataTable, _
                                  ByVal oSpr As Object)

        Dim iMc, i As Integer
        Dim iR As Integer
        Dim _Wydz, _Okres As String
        Dim dtS As New DataTable, dt As New DataTable
        Dim dvS, dvW, dv As DataView
        Dim v As DataRowView
        Dim cmd As New OleDbCommand
        Dim tr As OleDbTransaction = Nothing
        Const CO_ACTION As String = "Pobieranie danych transportu sprzedaży"
        Try
            FillStatusBar(oTra.GetUpperBound(0), CO_ACTION + "...")
            With dtS.Columns
                .Add("GrMater") : .Add("Dostawa", GetType(Double))
                .Add("Waga", GetType(Double))
            End With
            dvS = dtS.DefaultView
            dvS.Sort = "GrMater, Dostawa"
            _Okres = Format(frmMain.dtpOkres.Value, "MM.yyyy")
            iMc = frmMain.dtpOkres.Value.Month
            dvW = dtW.DefaultView
            dvW.Sort = "ID"
            For i = 2 To oSpr.GetUpperBound(0)
                iR = dvS.Find(New Object() {oSpr(i, 2), oSpr(i, 17)})
                If iR.Equals(-1) Then
                    dtS.Rows.Add(New Object() {oSpr(i, 2), oSpr(i, 17), (oSpr(i, 19) + oSpr(i, 20)) / 1000})
                Else
                    dvS(iR)("Waga") = dvS(iR)("Waga") + (oSpr(i, 19) + oSpr(i, 20)) / 1000
                End If
            Next i
            dtS.AcceptChanges()
            Using cn As New OleDbConnection(frmMain.cns.ConnectionString)
                Try
                    cn.Open()
                    tr = cn.BeginTransaction
                    With cmd
                        .Connection = cn
                        .Transaction = tr
                        ' usuń poprzednie zapisy
                        .CommandText = "SELECT * FROM tblTransport WHERE Miesiac = 14"
                        dt.Load(.ExecuteReader)
                        .CommandText = "DELETE FROM tblTransport WHERE Miesiac = " + iMc.ToString
                        .ExecuteNonQuery()
                    End With
                    With dt.Columns
                        .Add("Spedytor")
                        .Add("Dostawa", GetType(Integer))
                    End With
                    dv = dt.DefaultView
                    dv.Sort = "Dostawa, GrMater"
                    For i = 2 To oTra.GetUpperBound(0)
                        If Not oTra(i, 1).Equals(_Okres) Then GoTo Nastepny
                        _Wydz = "OR"
                        If Not oTra(i, 12).ToString.ToUpper.Contains("JSWZ") Then
                            iR = dvW.Find(oTra(i, 6).ToString)
                            If Not iR.Equals(-1) Then _Wydz = dvW(iR)(1)
                        End If
                        oTra(i, 2) = _Wydz
                        iR = dv.Find(New Object() {oTra(i, 27), oTra(i, 6)})
                        If iR.Equals(-1) Then
                            dt.Rows.Add(New Object() {iMc, oTra(i, 6), oTra(i, 18), oTra(i, 20), _Wydz, oTra(i, 4), oTra(i, 24), oTra(i, 28), oTra(i, 29), oTra(i, 21), oTra(i, 27)})
                        Else
                            dv(iR)("Waga") = dv(iR)("Waga") + oTra(i, 28)
                            dv(iR)("Kwota") = dv(iR)("Kwota") + oTra(i, 29)
                        End If
                        With frmMain.tspbMain
                            If i <= .Maximum AndAlso (i Mod 5).Equals(0) Then _
                                .Value = i : Application.DoEvents()
                        End With
Nastepny:
                    Next i
                    ' weryfikacja wagi
                    For i = 0 To dv.Count - 1
                        v = dv(i)
                        iR = dvS.Find(New String() {v("GrMater"), v("Dostawa")})
                        If Not iR.Equals(-1) Then v("Waga") = dvS(iR)("Waga")
                    Next i
                    For i = 0 To 1
                        If i.Equals(0) Then
                            Dim query = (From p In dt.AsEnumerable _
                                         Select ID = p("IDDostawcy"), Nazwa = p("Spedytor")).Distinct()
                            For Each q In query
                                With cmd
                                    Try
                                        .CommandText = "INSERT INTO tblDostawcy (ID, Nazwa) VALUES (" + q.ID.ToString + ", '" _
                                                        + q.Nazwa.ToString.Replace("'", "") + "')"
                                        .ExecuteNonQuery()
                                    Catch ex As Exception
                                    End Try
                                End With
                            Next q
                        Else
                            Dim query = (From p In dt.AsEnumerable Group By GM = p("GrMater"), IDKl = p("IDKlienta"), IDDo = p("IDDostawcy"), Wydz = p("Wydzial"), GD = p("GrDekretacji"), WW = p("WarunekWysylki") _
                                       Into Waga = Sum(p.Field(Of Double)("Waga")), Kwota = Sum(p.Field(Of Double)("Kwota")))
                            For Each q In query
                                With cmd
                                    Try
                                        .CommandText = "INSERT INTO tblTransport (Miesiac, GrMater, IDKlienta, IDDostawcy, Wydzial, GrDekretacji, Warunekwysylki, Waga, Kwota) VALUES (" + iMc.ToString + ", '" _
                                                        + q.GM + "', " + q.IDKl.ToString + ", " + q.IDDo.ToString + ", '" + q.Wydz + "', '" + q.GD + "', '" + q.WW + "', " + q.Waga.ToString.Replace(",", ".") + ", " + q.Kwota.ToString.Replace(",", ".") + ")"
                                        .ExecuteNonQuery()
                                    Catch ex As Exception
                                    End Try
                                End With
                            Next q
                        End If
                    Next i
                    tr.Commit()
                Catch ex As Exception
                    tr.Rollback()
                    Throw New ArgumentException(ex.Message)
                End Try
            End Using
        Catch ex As Exception
            Throw New ArgumentException(CO_ACTION + ":" + vbCrLf + ex.Message + i)
        Finally
            ClearStatus()
            cmd = Nothing
            tr = Nothing
            dt = Nothing
            dtS = Nothing
            dv = Nothing
            dvW = Nothing
        End Try
    End Sub

    Sub PobierzTransportSprzedazy(ByVal dt As DataTable, _
                                  ByVal dtW As DataTable, _
                                  ByVal dtS As DataTable)

        Dim iMc, i, iR As Integer
        Dim strDostawcy As String = ""
        Dim strWydzial, strOkres, m_Key As String
        Dim dvW As New DataView
        Dim dv, dvS As New DataView
        Dim v As DataRowView
        Dim m_Waga, m_Kwota As Double
        Dim r As DataRow = Nothing
        Dim cmd As New OleDbCommand
        Dim tr As OleDbTransaction = Nothing
        Const CO_ACTION As String = "Pobieranie danych transportu sprzedaży"
        Try
            strOkres = Format(frmMain.dtpOkres.Value, "MM.yyyy")
            'iRok = Me.dtpOkres.Value.Year
            iMc = frmMain.dtpOkres.Value.Month
            ' porządkowanie sprzedaży
            dvW = dtW.DefaultView
            dvW.Sort = "ID"
            dtS.Columns(1).ColumnName = "GrMater"
            dvS = dtS.DefaultView
            dvS.Sort = "Dostawa, GrMater"
            For i = dvS.Count - 1 To 1 Step -1
                strWydzial = "OR"
                If Not dvS(i)(6).ToString.ToUpper.Contains("JSWZ") Then
                    iR = dvW.Find(dvS(i)(1).ToString)
                    If Not iR.Equals(-1) Then _
                        strWydzial = dvW(iR)(1)
                End If
                If Not "WS_R1_R2".Contains(strWydzial) Then
                    dvS(i)(19) = 0
                    If (dvS(i)("Dostawa").ToString + dvS(i)("GrMater").ToString).Equals(dvS(i - 1)("Dostawa").ToString + dvS(i - 1)("GrMater").ToString) Then
                        dvS(i - 1)(18) = CP.Str2Dbl(dvS(i - 1)(18).ToString) + CP.Str2Dbl(dvS(i)(18).ToString)
                        dvS(i).Delete()
                    End If
                Else
                    dvS(i)(18) = 0
                    If (dvS(i)("Dostawa").ToString + dvS(i)("GrMater").ToString).Equals(dvS(i - 1)("Dostawa").ToString + dvS(i - 1)("GrMater").ToString) Then
                        dvS(i - 1)(19) = CP.Str2Dbl(dvS(i - 1)(19).ToString) + CP.Str2Dbl(dvS(i)(19).ToString)
                        dvS(i).Delete()
                    End If
                End If
            Next i
            dtS.AcceptChanges()
            dt.Columns(0).ColumnName = "Okres"
            dt.Columns(5).ColumnName = "GrMater"
            dt.Columns(19).ColumnName = "Dostawca"
            dv = dt.DefaultView
            dv.Sort = "Dostawa, GrMater"
            For i = dv.Count - 1 To 1 Step -1
                If (dv(i)("Dostawa").ToString + dv(i)("GrMater").ToString).Equals(dv(i - 1)("Dostawa").ToString + dv(i - 1)("GrMater").ToString) Then
                    dv(i - 1)(27) = CP.Str2Dbl(dv(i - 1)(27).ToString) + CP.Str2Dbl(dv(i)(27).ToString)
                    dv(i - 1)(28) = CP.Str2Dbl(dv(i - 1)(28).ToString) + CP.Str2Dbl(dv(i)(28).ToString)
                    dv(i).Delete()
                End If
            Next i
            dt.AcceptChanges()
            ' weryfikacja wagi
            For i = 0 To dv.Count - 1
                v = dv(i)
                iR = dvS.Find(New String() {v("Dostawa"), v("GrMater")})
                If Not iR.Equals(-1) Then v(27) = (dvS(iR)(18) + dvS(iR)(19)) / 1000
            Next i
            With dv
                .Sort = Nothing
                .RowFilter = "Okres = '" + strOkres + "'"
                .Sort = "Okres, GrMater, Zleceniodawca, Dostawca"
                FillStatusBar(.Count, CO_ACTION + "...")
            End With
            i = 0
            Using cn As New OleDbConnection(frmMain.cns.ConnectionString)
                Try
                    cn.Open()
                    tr = cn.BeginTransaction
                    With cmd
                        .Connection = cn
                        .Transaction = tr
                        ' usuń poprzednie zapisy
                        .CommandText = "DELETE FROM tblTransport WHERE Miesiac = " + iMc.ToString
                        .ExecuteNonQuery()
                    End With
                    Do While i < dv.Count
                        v = dv(i)
                        strWydzial = "OR"
                        If Not v(11).ToString.ToUpper.Contains("JSWZ") Then
                            iR = dvW.Find(v(5).ToString)
                            If Not iR.Equals(-1) Then strWydzial = dvW(iR)(1)
                        End If
                        ' kontrola 
                        If Not strDostawcy.Contains(v(19).ToString) Then
                            With cmd
                                Try
                                    .CommandText = "INSERT INTO tblDostawcy (ID, Nazwa) VALUES (" + v(19).ToString + ", '" _
                                                    + v(20).ToString.Replace("'", "") + "')"
                                    .ExecuteNonQuery()
                                Catch ex As Exception
                                End Try
                            End With
                            strDostawcy += "#" + v(19).ToString
                        End If
                        m_Waga = 0
                        m_Kwota = 0
                        m_Key = v(5).ToString + v(17).ToString + v(19).ToString
                        cmd.CommandText = "INSERT INTO tblTransport (Miesiac, GrMater, IDKlienta, IDDostawcy, Wydzial, GrDekretacji, Warunekwysylki, Waga, Kwota) " _
                                            + "VALUES (" + iMc.ToString + ", '" + v(5).ToString + "', " + v(17).ToString + ", " _
                                            + v(19).ToString + ", '" + strWydzial + "', '" + v(3).ToString + "', '" + v(23).ToString + "', "
                        Do While i < dv.Count AndAlso (dv(i)(5).ToString + dv(i)(17).ToString + dv(i)(19).ToString).Equals(m_Key)
                            m_Waga += CP.Str2Dbl(dv(i)(27).ToString)
                            m_Kwota += CP.Str2Dbl(dv(i)(28).ToString)
                            i += 1
                        Loop
                        With cmd
                            .CommandText += m_Waga.ToString.Replace(",", ".") + ", " + m_Kwota.ToString.Replace(",", ".") + ")"
                            .ExecuteNonQuery()
                        End With
Nastepny:
                        With frmMain.tspbMain
                            If i <= .Maximum AndAlso (i Mod 5).Equals(0) Then _
                                .Value = i : Application.DoEvents()
                        End With
                    Loop
                    tr.Commit()
                Catch ex As Exception
                    tr.Rollback()
                    Throw New ArgumentException(ex.Message)
                End Try
            End Using
        Catch ex As Exception
            Throw New ArgumentException(CO_ACTION + ":" + vbCrLf + ex.Message)
        Finally
            ClearStatus()
            dv = Nothing
            cmd = Nothing
            tr = Nothing
            dv = Nothing
            dvW = Nothing
        End Try
    End Sub

#Region "Aktualizacja danych stałych"

    'Sub UzupelnijDostawcow(ByVal cmd As OleDbCommand, ByVal oDane As Object, Optional ByRef Info As String = "")

    '    FillStatusBar(0, "Uzupełnianie listy dostawców...")
    '    For Each o In oDane
    '        With cmd
    '            Try
    '                .CommandText = "INSERT INTO tblDostawcy (ID, Nazwa) " _
    '                     + "VALUES (" + o.ID.ToString + ", '" + o.Opis.ToString.Replace("'", "") + "')"
    '                .ExecuteNonQuery()
    '                If Not Info.Contains("dostawcach") Then Info += vbCrLf + "Dane o dostawcach: "
    '                Info += o.ID.ToString + ", "
    '            Catch ex As Exception
    '                .CommandText = "UPDATE tblDostawcy SET Nazwa = '" + o.Opis.ToString.Replace("'", "") + "' WHERE ID = " + o.ID.ToString
    '                .ExecuteNonQuery()
    '            End Try
    '        End With
    '    Next o
    '    ClearStatus()
    'End Sub

    '    Sub UzupelnijKlientow(ByVal cmd As OleDbCommand, ByVal oDane As Object, Optional ByRef Info As String = "")

    '        FillStatusBar(0, "Uzupełnianie listy klientów...")
    '        For Each o In oDane
    '            With cmd
    '                Try
    '                    .CommandText = "INSERT INTO tblKlienci (ID, Nazwa) " _
    '                         + "VALUES (" + o.ID.ToString + ", '" + o.Opis.ToString.Replace("'", "") + "')"
    '                    .ExecuteNonQuery()
    '                    If Not Info.Contains("klientach") Then Info += vbCrLf + "Dane o klientach: "
    '                    Info += o.ID.ToString + ", "
    '                Catch ex As Exception
    '                    .CommandText = "UPDATE tblKlienci SET Nazwa = '" + o.Opis.ToString.Replace("'", "") + "' WHERE ID = " + o.ID.ToString
    '                    .ExecuteNonQuery()
    '                End Try
    '            End With
    '        Next o
    '        ClearStatus()
    '    End Sub

    '    Sub UzupelnijKlientow(ByVal cmd As OleDbCommand, ByVal aDane As Array, _
    '                                 ByVal ID As Integer, ByVal Opis As Integer, Optional ByRef Info As String = "")

    '        Dim _Txt As String = "#"
    '        Dim i As Integer
    '        FillStatusBar(0, "Uzupełnianie listy klientów...")
    '        For i = 2 To aDane.GetUpperBound(0)
    '            If aDane(i, ID) Is Nothing OrElse String.IsNullOrEmpty(aDane(i, ID).ToString) _
    '                OrElse aDane(i, ID).ToString.Trim.Equals("#") Then GoTo Nastepny
    '            If _Txt.Contains("#" + aDane(i, ID).ToString + "#") Then GoTo Nastepny
    '            _Txt += aDane(i, ID).ToString + "#"
    '            With cmd
    '                Try
    '                    .CommandText = "INSERT INTO tblKlienci (ID, Nazwa) " _
    '                             + "VALUES (" + aDane(i, ID).ToString + ", '" + aDane(i, Opis) + "')"
    '                    .ExecuteNonQuery()
    '                    If Not Info.Contains("klientach") Then Info += vbCrLf + "Dane o klientach: "
    '                    Info += aDane(i, ID) + ", "
    '                Catch ex As Exception
    '                    .CommandText = "UPDATE tblKlienci SET Nazwa = '" + aDane(i, Opis) + "' WHERE ID = " + aDane(i, ID).ToString
    '                    Try
    '                        .ExecuteNonQuery()
    '                    Catch
    '                    End Try
    '                End Try
    '            End With
    'Nastepny:
    '        Next i
    '        ClearStatus()
    '    End Sub

    '    Sub UzupelnijGrKalk(ByVal cmd As OleDbCommand, ByVal oDane As Object, Optional ByRef Info As String = "")

    '        FillStatusBar(0, "Uzupełnianie grup materiałowych...")
    '        For Each o In oDane
    '            With cmd
    '                Try
    '                    .CommandText = "INSERT INTO tblGrupyKalk (ID, GrMater, GrKalkPL) " _
    '                         + "VALUES ('" + o.ID + "', '" + o.GrMater.ToString.Replace("#", "") + "', '" + o.Opis + "')"
    '                    .ExecuteNonQuery()
    '                    If Not Info.Contains("grupach materiałowych") Then Info += vbCrLf + "Dane o grupach materiałowych: "
    '                    Info += o.ID.ToString + ", "
    '                Catch ex As Exception
    '                    .CommandText = "UPDATE tblGrupyKalk SET GrMater = '" + o.GrMater.ToString.Replace("#", "") + "', GrKalkPL = '" + o.Opis + "' WHERE ID = '" + o.ID + "'"
    '                    .ExecuteNonQuery()
    '                End Try
    '            End With
    '        Next o
    '        ClearStatus()
    '    End Sub

    '    Sub UzupelnijGrKalk(ByVal cmd As OleDbCommand, ByVal aDane As Array, _
    '                                ByVal ID As Integer, ByVal Opis As Integer, ByVal GrMater As Integer, _
    '                                Optional ByRef Info As String = "")

    '        Dim _Txt As String = "#"
    '        Dim i As Integer
    '        FillStatusBar(0, "Uzupełnianie grup kalkulacyjnych...")
    '        For i = 2 To aDane.GetUpperBound(0)
    '            If aDane(i, ID) Is Nothing OrElse String.IsNullOrEmpty(aDane(i, ID).ToString) _
    '                OrElse aDane(i, ID).ToString.Trim.Equals("#") Then GoTo Nastepny
    '            If _Txt.Contains("#" + aDane(i, ID) + "#") Then GoTo Nastepny
    '            _Txt += aDane(i, ID) + "#"
    '            With cmd
    '                Try
    '                    .CommandText = "INSERT INTO tblGrupyKalk (ID "
    '                    If Not Opis.Equals(0) Then .CommandText += ", GrKalkPL"
    '                    If Not GrMater.Equals(0) Then .CommandText += ", GrMater"
    '                    .CommandText += ") VALUES ('" + aDane(i, ID)
    '                    If Not Opis.Equals(0) Then .CommandText += "', '" + aDane(i, Opis)
    '                    If Not GrMater.Equals(0) Then .CommandText += "', '" + aDane(i, GrMater)
    '                    .CommandText += "')"
    '                    .ExecuteNonQuery()
    '                    If Not Info.Contains("grupach kalkulacyjnych") Then Info += vbCrLf + "Dane o grupach kalkulacyjnych: "
    '                    Info += aDane(i, ID) + ", "
    '                Catch ex As Exception
    '                    .CommandText = "UPDATE tblGrupyKalk SET "
    '                    If Not Opis.Equals(0) Then .CommandText += "GrKalkPL = '" + aDane(i, Opis) + "', "
    '                    If Not GrMater.Equals(0) Then
    '                        .CommandText += "GrMater = '" + aDane(i, GrMater)
    '                    ElseIf .CommandText.EndsWith(", ") Then
    '                        .CommandText = .CommandText.Substring(0, .CommandText.Length - 2)
    '                    End If
    '                    .CommandText += "' WHERE ID = '" + aDane(i, ID) + "'"
    '                    .ExecuteNonQuery()
    '                End Try
    '            End With
    'Nastepny:
    '        Next i
    '        ClearStatus()
    '    End Sub

    '    Sub UzupelnijGrMater(ByVal cmd As OleDbCommand, ByVal oDane As Object, Optional ByRef Info As String = "")

    '        FillStatusBar(0, "Uzupełnianie grup materiałowych...")
    '        For Each o In oDane
    '            With cmd
    '                Try
    '                    .CommandText = "INSERT INTO tblGrupyMat (ID, GrMaterPL) " _
    '                         + "VALUES ('" + o.ID + "', '" + o.Opis + "')"
    '                    .ExecuteNonQuery()
    '                    If Not Info.Contains("grupach materiałowych") Then Info += vbCrLf + "Dane o grupach materiałowych: "
    '                    Info += o.ID + ", "
    '                Catch ex As Exception
    '                    .CommandText = "UPDATE tblGrupyMat SET GrMaterPL = '" + o.Opis + "' WHERE ID = '" + o.ID + "'"
    '                    .ExecuteNonQuery()
    '                End Try
    '            End With
    '        Next o
    '        ClearStatus()
    '    End Sub

    '    Sub UzupelnijGrMater(ByVal cmd As OleDbCommand, ByVal aDane As Array, _
    '                                 ByVal ID As Integer, ByVal Opis As Integer, Optional ByRef Info As String = "")

    '        Dim _Txt As String = "#"
    '        Dim i As Integer
    '        FillStatusBar(0, "Uzupełnianie grup materiałowych...")
    '        For i = 2 To aDane.GetUpperBound(0)
    '            If aDane(i, ID) Is Nothing OrElse String.IsNullOrEmpty(aDane(i, ID)) _
    '                OrElse aDane(i, ID).ToString.Trim.Equals("#") Then GoTo Nastepny
    '            If _Txt.Contains("#" + aDane(i, ID) + "#") Then GoTo Nastepny
    '            _Txt += aDane(i, ID) + "#"
    '            With cmd
    '                Try
    '                    .CommandText = "INSERT INTO tblGrupyMat (ID, GrMaterPL) " _
    '                             + "VALUES ('" + aDane(i, ID) + "', '" + aDane(i, Opis) + "')"
    '                    .ExecuteNonQuery()
    '                    If Not Info.Contains("grupach materiałowych") Then Info += vbCrLf + "Dane o grupach materiałowych: "
    '                    Info += aDane(i, ID) + ", "
    '                Catch ex As Exception
    '                    .CommandText = "UPDATE tblGrupyMat SET GrMaterPL = '" + aDane(i, Opis) + "' WHERE ID = '" + aDane(i, ID) + "'"
    '                    .ExecuteNonQuery()
    '                End Try
    '            End With
    'Nastepny:
    '        Next i
    '        ClearStatus()
    '    End Sub

    '    Sub UzupelnijRodzMat(ByVal cmd As OleDbCommand, ByVal oDane As Object, Optional ByRef Info As String = "")

    '        FillStatusBar(0, "Uzupełnianie rodzajów materiału...")
    '        For Each o In oDane
    '            With cmd
    '                Try
    '                    .CommandText = "INSERT INTO tblRodzajeMat (ID, RodzMaterPL) " _
    '                         + "VALUES ('" + o.ID + "', '" + o.Opis + "')"
    '                    .ExecuteNonQuery()
    '                    If Not Info.Contains("rodzajach materiału") Then Info += vbCrLf + "Dane o rodzajach materiału: "
    '                    Info += o.ID + ", "
    '                Catch ex As Exception
    '                    .CommandText = "UPDATE tblRodzajeMat SET RodzMaterPL = '" + o.Opis + "' WHERE ID = '" + o.ID + "'"
    '                    .ExecuteNonQuery()
    '                End Try
    '            End With
    '        Next o
    '        ClearStatus()
    '    End Sub

    '    Sub UzupelnijRodzMat(ByVal cmd As OleDbCommand, ByVal aDane As Array, _
    '                                 ByVal ID As Integer, ByVal Opis As Integer, Optional ByRef Info As String = "")

    '        Dim _Txt As String = "#"
    '        Dim i As Integer
    '        FillStatusBar(0, "Uzupełnianie rodzajów materiału...")
    '        For i = 2 To aDane.GetUpperBound(0)
    '            If aDane(i, ID) Is Nothing OrElse String.IsNullOrEmpty(aDane(i, ID)) _
    '                OrElse aDane(i, ID).ToString.Trim.Equals("#") Then GoTo Nastepny
    '            If _Txt.Contains("#" + aDane(i, ID) + "#") Then GoTo Nastepny
    '            _Txt += aDane(i, ID) + "#"
    '            Try
    '                cmd.CommandText = "INSERT INTO tblRodzajeMat (ID, RodzMaterPL) " _
    '                         + "VALUES ('" + aDane(i, ID) + "', '" + aDane(i, Opis) + "')"
    '                cmd.ExecuteNonQuery()
    '                If Not Info.Contains("rodzajach materiału") Then Info += vbCrLf + "Dane o rodzajach materiału: "
    '                Info += aDane(i, ID) + ", "
    '            Catch ex As Exception
    '                cmd.CommandText = "UPDATE tblRodzajeMat SET RodzMaterPL = '" + aDane(i, Opis) + "' WHERE ID = '" + aDane(i, ID) + "'"
    '                cmd.ExecuteNonQuery()
    '            End Try
    'Nastepny:
    '        Next i
    '        ClearStatus()
    '    End Sub

    '    Sub UzupelnijMaterialy(ByVal cmd As OleDbCommand, ByVal oDane As Object, Optional ByRef Info As String = "")

    '        Dim _Rodz, _Mat, _Kalk, _Koszt, _Txt, _Txt2 As String
    '        FillStatusBar(0, "Uzupełnianie materiałów...")
    '        Try
    '            _Rodz = oDane(0).RodzId
    '        Catch ex As Exception
    '            _Rodz = ""
    '        End Try
    '        Try
    '            _Mat = oDane(0).GrMaterID
    '        Catch ex As Exception
    '            _Mat = ""
    '        End Try
    '        Try
    '            _Kalk = oDane(0).GrKalkID
    '        Catch ex As Exception
    '            _Kalk = ""
    '        End Try
    '        Try
    '            _Koszt = oDane(0).GrKosztID
    '        Catch ex As Exception
    '            _Koszt = ""
    '        End Try
    '        _Txt2 = ""
    '        For Each o In oDane
    '            With cmd
    '                Try
    '                    .CommandText = "INSERT INTO tblMaterialy (ID, MaterialPL"
    '                    _Txt = "VALUES ('" + o.ID + "', '" + o.Opis.ToString.Replace("'", "") + "'"
    '                    _Txt2 = ""
    '                    If Not String.IsNullOrEmpty(_Rodz) Then
    '                        .CommandText += ", RodzMaterID"
    '                        _Txt += ", '" + o.RodzId + "'"
    '                        _Txt2 += ", RodzMaterID = '" + o.RodzId + "'"
    '                    End If
    '                    If Not String.IsNullOrEmpty(_Mat) Then
    '                        .CommandText += ", GrMaterID"
    '                        _Txt += ", '" + o.GrMaterID.ToString.Replace("#", "") + "'"
    '                        _Txt2 += ", GrMaterID = '" + o.GrMaterID.ToString.Replace("#", "") + "'"
    '                    End If
    '                    If Not String.IsNullOrEmpty(_Kalk) Then
    '                        .CommandText += ", GrKalkID"
    '                        _Txt += ", '" + o.GrKalkID.ToString.Replace("#", "") + "'"
    '                        _Txt2 += ", GrKalkID = '" + o.GrKalkID.ToString.Replace("#", "") + "'"
    '                    End If
    '                    If Not String.IsNullOrEmpty(_Koszt) Then
    '                        .CommandText += ", GrKosztID"
    '                        _Txt += ", '" + o.GrKosztID.ToString.Replace("#", "") + "'"
    '                        _Txt2 += ", GrKosztID = '" + o.GrKosztID.ToString.Replace("#", "") + "'"
    '                    End If
    '                    .CommandText += ") " + _Txt + ")"
    '                    .ExecuteNonQuery()
    '                    If Not Info.Contains("materiałach") Then Info += vbCrLf + "Dane o materiałach: "
    '                    Info += o.ID + ", "
    '                Catch ex As Exception
    '                    .CommandText = "UPDATE tblMaterialy SET MaterialPL = '" + o.Opis + "'" + _Txt2 + " WHERE ID = '" + o.ID + "'"
    '                    Try
    '                        .ExecuteNonQuery()
    '                    Catch
    '                    End Try
    '                End Try
    '            End With
    '        Next o
    '        ClearStatus()
    '    End Sub

    '    Sub UzupelnijMaterialy(ByVal cmd As OleDbCommand, ByVal aDane As Array, ByVal ID As Integer, _
    '                                   ByVal Opis As Integer, ByVal RodzId As Integer, ByVal GrMaterID As Integer, _
    '                                   ByVal GrKalkID As Integer, ByVal GrKosztID As Integer, Optional ByRef Info As String = "")

    '        Dim _Txt As String = "#"
    '        Dim i As Integer
    '        FillStatusBar(0, "Uzupełnianie materiałów...")
    '        For i = 2 To aDane.GetUpperBound(0)
    '            If aDane(i, ID) Is Nothing OrElse String.IsNullOrEmpty(aDane(i, ID)) _
    '                OrElse aDane(i, ID).ToString.Trim.Equals("#") Then GoTo Nastepny
    '            If _Txt.Contains("#" + aDane(i, ID) + "#") Then GoTo Nastepny
    '            _Txt += aDane(i, ID) + "#"
    '            Try
    '                With cmd
    '                    .CommandText = "INSERT INTO tblMaterialy (ID, MaterialPL"
    '                    If Not RodzId.Equals(0) OrElse aDane(i, RodzId) Is Nothing Then .CommandText += ", RodzMaterID"
    '                    If Not GrMaterID.Equals(0) OrElse aDane(i, GrMaterID) Is Nothing Then .CommandText += ", GrMaterID"
    '                    If Not GrKalkID.Equals(0) OrElse aDane(i, GrKalkID) Is Nothing Then .CommandText += ", GrKalkID"
    '                    .CommandText += ") VALUES ('" + aDane(i, ID) + "', '" + aDane(i, Opis)
    '                    If Not RodzId.Equals(0) OrElse aDane(i, RodzId) Is Nothing Then .CommandText += "', '" + aDane(i, RodzId)
    '                    If Not GrMaterID.Equals(0) OrElse aDane(i, GrMaterID) Is Nothing Then .CommandText += "', '" + aDane(i, GrMaterID).ToString.Replace("#", "")
    '                    If Not GrKalkID.Equals(0) OrElse aDane(i, GrKalkID) Is Nothing Then .CommandText += "', '" + aDane(i, GrKalkID).ToString.Replace("#", "")
    '                    If Not GrKosztID.Equals(0) OrElse aDane(i, GrKosztID) Is Nothing Then .CommandText += "', '" + aDane(i, GrKosztID).ToString.Replace("#", "")
    '                    .CommandText += "')"
    '                    .ExecuteNonQuery()
    '                End With
    '                If Not Info.Contains("materiałach") Then Info += vbCrLf + "Dane o materiałach: "
    '                Info += aDane(i, ID) + ", "
    '            Catch ex As Exception
    '                With cmd
    '                    .CommandText = "UPDATE tblMaterialy SET MaterialPL = '" + aDane(i, Opis)
    '                    If Not RodzId.Equals(0) OrElse aDane(i, RodzId) Is Nothing Then .CommandText += "', RodzMaterID = '" + aDane(i, RodzId)
    '                    If Not GrMaterID.Equals(0) OrElse aDane(i, GrMaterID) Is Nothing Then .CommandText += "', GrMaterID = '" + aDane(i, GrMaterID).ToString.Replace("#", "")
    '                    If Not GrKalkID.Equals(0) OrElse aDane(i, GrKalkID) Is Nothing Then .CommandText += "', GrKalkID = '" + aDane(i, GrKalkID).ToString.Replace("#", "")
    '                    If Not GrKosztID.Equals(0) OrElse aDane(i, GrKosztID) Is Nothing Then .CommandText += "', GrKosztID = '" + aDane(i, GrKosztID).ToString.Replace("#", "")
    '                    .CommandText += "' WHERE ID = '" + aDane(i, ID) + "'"
    '                    Try
    '                        .ExecuteNonQuery()
    '                    Catch
    '                    End Try
    '                End With
    '            End Try
    'Nastepny:
    '        Next i
    '        ClearStatus()
    '    End Sub

#End Region 'Aktualizacja danych stałych

    Function BlokadaAktualizacji(ByVal Procedura As String, _
                                 Optional ByVal Komunikat As Boolean = True, _
                                 Optional ByVal Pobrane As Boolean = False) As Boolean

        Dim dt As New DataTable
        Dim tr As OleDbTransaction = Nothing
        Dim cmd As New OleDbCommand
        Using cn As New OleDbConnection(frmMain.cns.ConnectionString)
            Try
                cn.Open()
                tr = cn.BeginTransaction
                With cmd
                    .Connection = cn
                    .Transaction = tr
                    .CommandText = "SELECT TOP 1 * FROM tblBlokady WHERE Miesiac = " _
                                                          + frmMain.dtpOkres.Value.Month.ToString + " AND Obiekt = '" + Procedura + "'"
                    dt.Load(.ExecuteReader)
                End With
                If Pobrane Then
                    With cmd
                        If dt.Rows.Count.Equals(0) Then
                            .CommandText = "INSERT INTO tblBlokady (Miesiac, Obiekt, Pobrane) VALUES (" & frmMain.dtpOkres.Value.Month & ", '" & Procedura & "', #" & Now & "#)"
                            .ExecuteNonQuery()
                        Else
                            .CommandText = "UPDATE tblBlokady SET Pobrane = #" & Now & "# WHERE Miesiac = " & frmMain.dtpOkres.Value.Month & " AND Obiekt = '" & Procedura & "'"
                            .ExecuteNonQuery()
                        End If
                    End With
                    tr.Commit()
                    Return True
                End If
                Try
                    If dt.Rows.Count.Equals(0) Then Throw New ArgumentException
                    If Not dt(0)("Zablokowane") Then Throw New ArgumentException
                    If Komunikat Then
                        Using New CM
                            MessageBox.Show("Blokada aktualizacji", Procedura, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        End Using
                    End If
                    Return True
                Catch ex As Exception
                    Return False
                End Try
            Catch ex As Exception
                tr.Rollback()
                Using New CM
                    MessageBox.Show(ex.Message, "Blokada procesu", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End Using
            End Try
        End Using
        tr = Nothing
        cmd = Nothing
    End Function

    Sub ClearStatus()

        With frmMain
            .tsslLicznik.Text = ""
            .tsslMain.Text = "Gotowe"
            .tspbMain.Value = 0
            .Cursor = Cursors.Default
        End With
        Application.DoEvents()
    End Sub

    Function KomunikatShow() As Boolean

        Dim _Wd, _Hg As Integer
        With frmMain
            _Wd = .Size.Width \ 2
            _Hg = .Size.Height \ 2
            With .txtKomunikat
                If String.IsNullOrEmpty(.Text) Then .Visible = False : Return False
                With .Text
                    If .EndsWith(", ") Then frmMain.txtKomunikat.Text = .Substring(.Length - 2)
                End With
                .Location = New Point(_Wd - _Wd \ 2, _Hg - _Hg \ 2)
                If _Wd < .MinimumSize.Width OrElse _Hg < .MinimumSize.Height Then
                    .Size = .MinimumSize
                Else
                    .Size = New Size(_Wd, _Hg)
                End If
                .Anchor = AnchorStyles.Top And AnchorStyles.Right
                .Visible = True
            End With
        End With
        KomunikatShow = True
    End Function

    Sub EndOfProcess(ByVal sender As Button)

        With frmMain
            ClearStatus()
            .dtpOkres.ReadOnly = False
            With sender
                .BackColor = Button.DefaultBackColor
                .ForeColor = Button.DefaultForeColor
            End With
        End With
        Application.DoEvents()
    End Sub

    Sub FillStatusBar(Optional ByVal Max As Integer = 0, Optional ByVal Tekst As String = "")

        With frmMain
            With .tspbMain
                .Minimum = 0
                .Maximum = Max
                .Value = 0
            End With
            .tsslMain.Text = Tekst
            .Cursor = Cursors.WaitCursor
        End With
        Application.DoEvents()
    End Sub

    Sub StartOfProcess(ByVal sender As Button)

        With frmMain
            With .txtKomunikat
                .Clear()
                .Visible = False
            End With
            .Cursor = Cursors.WaitCursor
            .dtpOkres.ReadOnly = True
            .tsslMain.Text = "Odczyt danych źródłowych..."
            With sender
                .BackColor = Color.AntiqueWhite
                .ForeColor = Color.Red
            End With
        End With
        Application.DoEvents()
    End Sub

    Sub ZWW_AnalizaTransportu()

        Dim da As OleDbDataAdapter = New OleDbDataAdapter("SELECT T.Miesiac, T.Wydzial, T.GrDekretacji, T.WarunekWysylki, D.Nazwa AS [Spedytor], D.Kraj AS [KK], K.Kraj AS [KlKraj], M.AHOfZWW, SUM(Waga) AS [Mg], SUM(Kwota) AS [Kwota] " _
                                                        + "FROM ((tblTransport T LEFT JOIN tblDostawcy D ON T.IDDostawcy = D.ID) LEFT JOIN tblKlienci K ON T.IdKlienta = K.ID) LEFT JOIN tblGrupyMat M ON T.GrMater = M.ID " _
                                                        + "WHERE T.Wydzial IN ('WS', 'R1', 'R2') AND T.Miesiac <= " + frmMain.dtpOkres.Value.Month.ToString _
                                                        + " GROUP BY T.Miesiac, T.Wydzial, T.GrDekretacji, T.WarunekWysylki, M.AHOfZWW, D.Nazwa, D.Kraj, K.Kraj ORDER BY M.AHOfZWW, Miesiac", frmMain.cns)
        Dim dt As New DataTable, dtW As New DataTable
        Dim dvW As DataView
        Dim aDane(0 To 52, 0 To 12), aVal(0 To 52, 0 To 12, 1) As Object
        Dim i, j, k, m, n As Integer
        Dim _WB As String = "W:\Costs\Celsa\Raporty kwartalne ZWW\Transport Report_" + frmMain.dtpOkres.Value.Year.ToString + ".xls"
        Dim _Mc, _LongMc, M_c As String
        Dim Rng As String = ""
        Dim _EUR As Boolean
        Try
            _Mc = "0" + frmMain.dtpOkres.Value.Month.ToString
            _Mc = _Mc.Substring(_Mc.Length - 2, 2)
            _LongMc = CP.FullEnglishDate(frmMain.dtpOkres.Value, True, True)
            FillStatusBar(0, frmMain.tsm_Eh.Text.Replace("&", "") + " - transport")
            da.Fill(dt)
            If dt.Rows.Count.Equals(0) Then Throw New ArgumentException("Brak danych")
            dvW = Nothing
            Using New CM
                _EUR = MessageBox.Show("EUR version?", frmMain.tsm_Eh.Text.Replace("&", "") + " - transport", MessageBoxButtons.YesNo, MessageBoxIcon.Question).Equals(Windows.Forms.DialogResult.Yes)
            End Using
            If _EUR Then
                da = New OleDbDataAdapter("SELECT TOP " + frmMain.dtpOkres.Value.Month.ToString + " MM_dd, EUR FROM tblKursyWalut WHERE INT(MM_dd) <= " & _Mc & " ORDER BY MM_dd", frmMain.cns)
                da.Fill(dtW)
                If Not dtW.Rows.Count.Equals(frmMain.dtpOkres.Value.Month) Then Throw New ArgumentException("Sprawdź kursy EUR")
                dvW = dtW.DefaultView
                dvW.Sort = "MM_dd"
                _WB = _WB.Replace("ZWW\Transport Report_", "ZWW\Transport Report_EUR_")
            End If
            Dim XE As New Cl4Excel.Excel(_WB, "Menu", , "m")
            dt.Columns.Add("EUR", GetType(Double))
            For Each r In dt.Rows
                M_c = "0" + r("Miesiac").ToString
                M_c = M_c.Substring(M_c.Length - 2)
                If _EUR Then
                    i = dvW.Find(M_c)
                    If i.Equals(-1) Then _
                        Throw New ArgumentException("Brak przelicznika EUR dla [" + M_c + "]")
                    r("EUR") = dvW(i)("EUR")
                    r("Kwota") = CDbl(r("Kwota").ToString) / CDbl(r("EUR").ToString)
                End If
                r("Spedytor") = Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(r("Spedytor").ToString.ToLower)
                If Not String.IsNullOrEmpty(r("KK").ToString) Then _
                    r("Spedytor") = r("Spedytor").ToString + " (" + r("KK").ToString + ")"
                If r("WarunekWysylki").ToString.Equals("W9") Then r("WarunekWysylki") = "W1"
            Next r
            With XE
                .oSheet.Range("A1").Value = CP.Conv2Str(_Mc)
                ' czyszczenie
                For j = 1 To .oBook.Worksheets.Count
                    .oSheet = CType(.oBook.Sheets(j), XL._Worksheet)
                    Try
                        If Not .oSheet.Tab.ColorIndex.ToString.Equals("41") Then Exit Try
                        If .oSheet.Name.Contains("Data") Then
                            For i = 1 To 8
                                Select Case i
                                    Case 1
                                        Rng = "B4:D54"
                                    Case 2
                                        Rng = "H4:J54"
                                    Case 3
                                        Rng = "N4:P54"
                                    Case 4
                                        Rng = "T4:V54"
                                    Case 5
                                        Rng = "B60:D110"
                                    Case 6
                                        Rng = "H60:J110"
                                    Case 7
                                        Rng = "N60:P110"
                                    Case 8
                                        Rng = "T60:V110"
                                End Select
                                Try
                                    .oSheet.Range(Rng).SpecialCells(XL.XlCellType.xlCellTypeConstants).ClearContents()
                                Catch ex As Exception
                                End Try
                                With .oSheet.Range(Rng)
                                    If i > 4 Then
                                        .Offset(-2, 1).Resize(1, 1).Value = _LongMc + " YTD"
                                    Else
                                        .Offset(-2, 1).Resize(1, 1).Value = _LongMc
                                    End If
                                End With
                            Next i
                        Else
                            For i = 1 To 8
                                Select Case i
                                    Case 1
                                        Rng = "B3:M54"
                                    Case 2
                                        Rng = "B59:M110"
                                    Case 3
                                        Rng = "B115:M166"
                                    Case 4
                                        Rng = "B171:M222"
                                    Case 5
                                        Rng = "B227:M278"
                                    Case 6
                                        Rng = "B283:M334"
                                    Case 7
                                        Rng = "B339:M390"
                                    Case 8
                                        Rng = "B395:M446"
                                End Select
                                With .oSheet.Range(Rng)
                                    Try
                                        .SpecialCells(XL.XlCellType.xlCellTypeConstants).ClearContents()
                                    Catch ex As Exception
                                    End Try
                                    If i > 4 Then
                                        .Offset(-1, -1).Resize(1, 1).Value = _LongMc + " YTD"
                                    Else
                                        .Offset(-1, -1).Resize(1, 1).Value = _LongMc
                                    End If
                                End With
                            Next i
                        End If
                    Catch ex As Exception
                    End Try
                Next j
            End With
            RozdzielTnPln(dt, "Tons", "Mg", XE)
            If _EUR Then
                RozdzielTnPln(dt, "EUR", "Kwota", XE)
            Else
                RozdzielTnPln(dt, "PLN", "Kwota", XE)
            End If
            For m = 0 To 2
                Select Case m
                    Case 0
                        With XE
                            .oSheet = CType(.oBook.Sheets("Domestic_perTon"), XL._Worksheet)
                        End With
                        For k = 1 To 8
                            Dim query As IEnumerable = Nothing
                            Dim query2 As IEnumerable = Nothing
                            Dim query3 As IEnumerable = Nothing
                            Select Case k
                                Case 1
                                    Rng = "B3:N55"
                                    query = (From p In dt _
                                             Where p!GrDekretacji.ToString.Equals("01") AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                             Group By ID = p!Spedytor _
                                             Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                             Order By RVal / RIl Descending)
                                Case 2
                                    Rng = "B59:N111"
                                    query = (From p In dt _
                                             Where p!GrDekretacji.ToString.Equals("01") AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                    AndAlso p!WarunekWysylki.ToString.Equals("W1") _
                                             Group By ID = p!Spedytor _
                                             Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                             Order By RVal / RIl Descending)
                                Case 3
                                    Rng = "B115:N167"
                                    query = (From p In dt _
                                             Where p!GrDekretacji.ToString.Equals("01") AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                    AndAlso p!WarunekWysylki.ToString.Equals("W2") _
                                             Group By ID = p!Spedytor _
                                             Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                             Order By RVal / RIl Descending)
                                Case 4
                                    Rng = "B171:N223"
                                    query = (From p In dt _
                                             Where p!GrDekretacji.ToString.Equals("01") AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                    AndAlso p!WarunekWysylki.ToString.Equals("W4") _
                                             Group By ID = p!Spedytor _
                                             Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                             Order By RVal / RIl Descending)
                                Case 5
                                    Rng = "B227:N279"
                                    query = (From p In dt _
                                             Where p!GrDekretacji.ToString.Equals("01") _
                                             Group By ID = p!Spedytor _
                                             Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                             Order By RVal / RIl Descending)
                                Case 6
                                    Rng = "B283:N335"
                                    query = (From p In dt _
                                             Where p!GrDekretacji.ToString.Equals("01") _
                                                    AndAlso p!WarunekWysylki.ToString.Equals("W1") _
                                             Group By ID = p!Spedytor _
                                             Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                             Order By RVal / RIl Descending)
                                Case 7
                                    Rng = "B339:N391"
                                    query = (From p In dt _
                                             Where p!GrDekretacji.ToString.Equals("01") _
                                                    AndAlso p!WarunekWysylki.ToString.Equals("W2") _
                                             Group By ID = p!Spedytor _
                                             Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                             Order By RVal / RIl Descending)
                                Case 8
                                    Rng = "B395:N447"
                                    query = (From p In dt _
                                             Where p!GrDekretacji.ToString.Equals("01") _
                                                    AndAlso p!WarunekWysylki.ToString.Equals("W4") _
                                             Group By ID = p!Spedytor _
                                             Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                             Order By RVal / RIl Descending)
                            End Select
                            i = 0
                            ReDim aDane(0 To 52, 0 To 12)
                            ReDim aVal(0 To 52, 0 To 12, 1)
                            For Each q In query
                                i += 1
                                aDane(i, 0) = q.ID
                                If i.Equals(50) Then Exit For
                            Next q
                            aDane(51, 0) = "Other Transport Companies"
                            aDane(52, 0) = "Total"
                            aDane(0, 11) = "Other Countries"
                            aDane(0, 12) = "Total"
                            Select Case k
                                Case 1
                                    query2 = (From p In dt _
                                              Where p!GrDekretacji.ToString.Equals("01") AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                              Group By KK = p!KlKraj _
                                              Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                              Order By RVal / RIl Descending)
                                Case 2
                                    query2 = (From p In dt _
                                              Where p!GrDekretacji.ToString.Equals("01") AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                    AndAlso p!WarunekWysylki.ToString.Equals("W1") _
                                              Group By KK = p!KlKraj _
                                              Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                              Order By RVal / RIl Descending)
                                Case 3
                                    query2 = (From p In dt _
                                              Where p!GrDekretacji.ToString.Equals("01") AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                    AndAlso p!WarunekWysylki.ToString.Equals("W2") _
                                              Group By KK = p!KlKraj _
                                              Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                              Order By RVal / RIl Descending)
                                Case 4
                                    query2 = (From p In dt _
                                              Where p!GrDekretacji.ToString.Equals("01") AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                    AndAlso p!WarunekWysylki.ToString.Equals("W4") _
                                              Group By KK = p!KlKraj _
                                              Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                              Order By RVal / RIl Descending)
                                Case 5
                                    query2 = (From p In dt _
                                              Where p!GrDekretacji.ToString.Equals("01") _
                                              Group By KK = p!KlKraj _
                                              Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                              Order By RVal / RIl Descending)
                                Case 6
                                    query2 = (From p In dt _
                                              Where p!GrDekretacji.ToString.Equals("01") _
                                                    AndAlso p!WarunekWysylki.ToString.Equals("W1") _
                                              Group By KK = p!KlKraj _
                                              Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                              Order By RVal / RIl Descending)
                                Case 7
                                    query2 = (From p In dt _
                                              Where p!GrDekretacji.ToString.Equals("01") _
                                                    AndAlso p!WarunekWysylki.ToString.Equals("W2") _
                                              Group By KK = p!KlKraj _
                                              Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                              Order By RVal / RIl Descending)
                                Case 8
                                    query2 = (From p In dt _
                                              Where p!GrDekretacji.ToString.Equals("01") _
                                                    AndAlso p!WarunekWysylki.ToString.Equals("W4") _
                                              Group By KK = p!KlKraj _
                                              Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                              Order By RVal / RIl Descending)
                            End Select
                            i = 0
                            For Each q In query2
                                i += 1
                                aDane(0, i) = q.KK
                                If i.Equals(10) Then Exit For
                            Next q
                            Select Case k
                                Case 1
                                    query3 = (From p In dt _
                                              Where p!GrDekretacji.ToString.Equals("01") AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                              Group By ID = p!Spedytor, KK = p!KlKraj _
                                              Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                              Order By RVal / RIl Descending)
                                Case 2
                                    query3 = (From p In dt _
                                              Where p!GrDekretacji.ToString.Equals("01") AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                    AndAlso p!WarunekWysylki.ToString.Equals("W1") _
                                              Group By ID = p!Spedytor, KK = p!KlKraj _
                                              Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                              Order By RVal / RIl Descending)
                                Case 3
                                    query3 = (From p In dt _
                                              Where p!GrDekretacji.ToString.Equals("01") AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                    AndAlso p!WarunekWysylki.ToString.Equals("W2") _
                                              Group By ID = p!Spedytor, KK = p!KlKraj _
                                              Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                              Order By RVal / RIl Descending)
                                Case 4
                                    query3 = (From p In dt _
                                              Where p!GrDekretacji.ToString.Equals("01") AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                    AndAlso p!WarunekWysylki.ToString.Equals("W4") _
                                              Group By ID = p!Spedytor, KK = p!KlKraj _
                                              Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                              Order By RVal / RIl Descending)
                                Case 5
                                    query3 = (From p In dt _
                                              Where p!GrDekretacji.ToString.Equals("01") _
                                              Group By ID = p!Spedytor, KK = p!KlKraj _
                                              Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                              Order By RVal / RIl Descending)
                                Case 6
                                    query3 = (From p In dt _
                                              Where p!GrDekretacji.ToString.Equals("01") _
                                                    AndAlso p!WarunekWysylki.ToString.Equals("W1") _
                                              Group By ID = p!Spedytor, KK = p!KlKraj _
                                              Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                              Order By RVal / RIl Descending)
                                Case 7
                                    query3 = (From p In dt _
                                              Where p!GrDekretacji.ToString.Equals("01") _
                                                    AndAlso p!WarunekWysylki.ToString.Equals("W2") _
                                              Group By ID = p!Spedytor, KK = p!KlKraj _
                                              Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                              Order By RVal / RIl Descending)
                                Case 8
                                    query3 = (From p In dt _
                                              Where p!GrDekretacji.ToString.Equals("01") _
                                                    AndAlso p!WarunekWysylki.ToString.Equals("W4") _
                                              Group By ID = p!Spedytor, KK = p!KlKraj _
                                              Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                              Order By RVal / RIl Descending)
                            End Select
                            For Each q In query3
                                For i = 1 To 50
                                    If aDane(i, 0).Equals(q.ID) Then Exit For
                                Next i
                                For j = 1 To 10
                                    If aDane(0, j).Equals(q.KK) Then Exit For
                                Next j
                                aVal(i, j, 0) += q.Rval
                                aVal(i, j, 1) += q.RIl
                                aVal(i, 12, 0) += q.Rval
                                aVal(i, 12, 1) += q.RIl
                                aVal(52, j, 0) += q.Rval
                                aVal(52, j, 1) += q.RIl
                                aVal(52, 12, 0) += q.Rval
                                aVal(52, 12, 1) += q.RIl
                            Next q
                            For i = 1 To 52
                                For j = 1 To 12
                                    If Not (aVal(i, j, 0) Is Nothing OrElse aVal(i, j, 1) Is Nothing) Then
                                        aDane(i, j) = aVal(i, j, 0) / aVal(i, j, 1)
                                    End If
                                Next j
                            Next i
                            With XE
                                With .oSheet
                                    .Activate()
                                    .Range(Rng).Value = aDane
                                End With
                                .oApp.Run("PodzielStrone")
                            End With
                        Next k
                    Case 1
                        With XE
                            .oSheet = CType(.oBook.Sheets("Export_perTon"), XL._Worksheet)
                        End With
                        For k = 1 To 8
                            Dim query As IEnumerable = Nothing
                            Dim query2 As IEnumerable = Nothing
                            Dim query3 As IEnumerable = Nothing
                            Select Case k
                                Case 1
                                    Rng = "B3:N55"
                                    query = (From p In dt _
                                             Where Not p!GrDekretacji.ToString.Equals("01") AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                             Group By ID = p!Spedytor _
                                             Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                             Order By RVal / RIl Descending)
                                Case 2
                                    Rng = "B59:N111"
                                    query = (From p In dt _
                                             Where Not p!GrDekretacji.ToString.Equals("01") AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                    AndAlso p!WarunekWysylki.ToString.Equals("W1") _
                                             Group By ID = p!Spedytor _
                                             Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                             Order By RVal / RIl Descending)
                                Case 3
                                    Rng = "B115:N167"
                                    query = (From p In dt _
                                             Where Not p!GrDekretacji.ToString.Equals("01") AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                    AndAlso p!WarunekWysylki.ToString.Equals("W2") _
                                             Group By ID = p!Spedytor _
                                             Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                             Order By RVal / RIl Descending)
                                Case 4
                                    Rng = "B171:N223"
                                    query = (From p In dt _
                                             Where Not p!GrDekretacji.ToString.Equals("01") AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                    AndAlso p!WarunekWysylki.ToString.Equals("W4") _
                                             Group By ID = p!Spedytor _
                                             Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                             Order By RVal / RIl Descending)
                                Case 5
                                    Rng = "B227:N279"
                                    query = (From p In dt _
                                             Where Not p!GrDekretacji.ToString.Equals("01") _
                                             Group By ID = p!Spedytor _
                                             Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                             Order By RVal / RIl Descending)
                                Case 6
                                    Rng = "B283:N335"
                                    query = (From p In dt _
                                             Where Not p!GrDekretacji.ToString.Equals("01") _
                                                    AndAlso p!WarunekWysylki.ToString.Equals("W1") _
                                             Group By ID = p!Spedytor _
                                             Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                             Order By RVal / RIl Descending)
                                Case 7
                                    Rng = "B339:N391"
                                    query = (From p In dt _
                                             Where Not p!GrDekretacji.ToString.Equals("01") _
                                                    AndAlso p!WarunekWysylki.ToString.Equals("W2") _
                                             Group By ID = p!Spedytor _
                                             Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                             Order By RVal / RIl Descending)
                                Case 8
                                    Rng = "B395:N447"
                                    query = (From p In dt _
                                             Where Not p!GrDekretacji.ToString.Equals("01") _
                                                    AndAlso p!WarunekWysylki.ToString.Equals("W4") _
                                             Group By ID = p!Spedytor _
                                             Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                             Order By RVal / RIl Descending)
                            End Select
                            i = 0
                            ReDim aDane(0 To 52, 0 To 12)
                            ReDim aVal(0 To 52, 0 To 12, 1)
                            For Each q In query
                                i += 1
                                aDane(i, 0) = q.ID
                                If i.Equals(50) Then Exit For
                            Next q
                            aDane(51, 0) = "Other Transport Companies"
                            aDane(52, 0) = "Total"
                            aDane(0, 11) = "Other Countries"
                            aDane(0, 12) = "Total"
                            Select Case k
                                Case 1
                                    query2 = (From p In dt _
                                              Where Not p!GrDekretacji.ToString.Equals("01") AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                              Group By KK = p!KlKraj _
                                              Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                              Order By RVal / RIl Descending)
                                Case 2
                                    query2 = (From p In dt _
                                              Where Not p!GrDekretacji.ToString.Equals("01") AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                    AndAlso p!WarunekWysylki.ToString.Equals("W1") _
                                              Group By KK = p!KlKraj _
                                              Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                              Order By RVal / RIl Descending)
                                Case 3
                                    query2 = (From p In dt _
                                              Where Not p!GrDekretacji.ToString.Equals("01") AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                    AndAlso p!WarunekWysylki.ToString.Equals("W2") _
                                              Group By KK = p!KlKraj _
                                              Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                              Order By RVal / RIl Descending)
                                Case 4
                                    query2 = (From p In dt _
                                              Where Not p!GrDekretacji.ToString.Equals("01") AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                    AndAlso p!WarunekWysylki.ToString.Equals("W4") _
                                              Group By KK = p!KlKraj _
                                              Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                              Order By RVal / RIl Descending)
                                Case 5
                                    query2 = (From p In dt _
                                              Where Not p!GrDekretacji.ToString.Equals("01") _
                                              Group By KK = p!KlKraj _
                                              Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                              Order By RVal / RIl Descending)
                                Case 6
                                    query2 = (From p In dt _
                                              Where Not p!GrDekretacji.ToString.Equals("01") _
                                                    AndAlso p!WarunekWysylki.ToString.Equals("W1") _
                                              Group By KK = p!KlKraj _
                                              Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                              Order By RVal / RIl Descending)
                                Case 7
                                    query2 = (From p In dt _
                                              Where Not p!GrDekretacji.ToString.Equals("01") _
                                                    AndAlso p!WarunekWysylki.ToString.Equals("W2") _
                                              Group By KK = p!KlKraj _
                                              Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                              Order By RVal / RIl Descending)
                                Case 8
                                    query2 = (From p In dt _
                                              Where Not p!GrDekretacji.ToString.Equals("01") _
                                                    AndAlso p!WarunekWysylki.ToString.Equals("W4") _
                                              Group By KK = p!KlKraj _
                                              Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                              Order By RVal / RIl Descending)
                            End Select
                            i = 0
                            For Each q In query2
                                i += 1
                                aDane(0, i) = q.KK
                                If i.Equals(10) Then Exit For
                            Next q
                            Select Case k
                                Case 1
                                    query3 = (From p In dt _
                                              Where Not p!GrDekretacji.ToString.Equals("01") AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                              Group By ID = p!Spedytor, KK = p!KlKraj _
                                              Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                              Order By RVal / RIl Descending)
                                Case 2
                                    query3 = (From p In dt _
                                              Where Not p!GrDekretacji.ToString.Equals("01") AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                    AndAlso p!WarunekWysylki.ToString.Equals("W1") _
                                              Group By ID = p!Spedytor, KK = p!KlKraj _
                                              Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                              Order By RVal / RIl Descending)
                                Case 3
                                    query3 = (From p In dt _
                                              Where Not p!GrDekretacji.ToString.Equals("01") AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                    AndAlso p!WarunekWysylki.ToString.Equals("W2") _
                                              Group By ID = p!Spedytor, KK = p!KlKraj _
                                              Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                              Order By RVal / RIl Descending)
                                Case 4
                                    query3 = (From p In dt _
                                              Where Not p!GrDekretacji.ToString.Equals("01") AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                    AndAlso p!WarunekWysylki.ToString.Equals("W4") _
                                              Group By ID = p!Spedytor, KK = p!KlKraj _
                                              Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                              Order By RVal / RIl Descending)
                                Case 5
                                    query3 = (From p In dt _
                                              Where Not p!GrDekretacji.ToString.Equals("01") _
                                              Group By ID = p!Spedytor, KK = p!KlKraj _
                                              Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                              Order By RVal / RIl Descending)
                                Case 6
                                    query3 = (From p In dt _
                                              Where Not p!GrDekretacji.ToString.Equals("01") _
                                                    AndAlso p!WarunekWysylki.ToString.Equals("W1") _
                                              Group By ID = p!Spedytor, KK = p!KlKraj _
                                              Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                              Order By RVal / RIl Descending)
                                Case 7
                                    query3 = (From p In dt _
                                              Where Not p!GrDekretacji.ToString.Equals("01") _
                                                    AndAlso p!WarunekWysylki.ToString.Equals("W2") _
                                              Group By ID = p!Spedytor, KK = p!KlKraj _
                                              Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                              Order By RVal / RIl Descending)
                                Case 8
                                    query3 = (From p In dt _
                                              Where Not p!GrDekretacji.ToString.Equals("01") _
                                                    AndAlso p!WarunekWysylki.ToString.Equals("W4") _
                                              Group By ID = p!Spedytor, KK = p!KlKraj _
                                              Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                              Order By RVal / RIl Descending)
                            End Select
                            For Each q In query3
                                For i = 1 To 50
                                    If aDane(i, 0).Equals(q.ID) Then Exit For
                                Next i
                                For j = 1 To 10
                                    If aDane(0, j).Equals(q.KK) Then Exit For
                                Next j
                                aVal(i, j, 0) += q.Rval
                                aVal(i, j, 1) += q.RIl
                                aVal(i, 12, 0) += q.Rval
                                aVal(i, 12, 1) += q.RIl
                                aVal(52, j, 0) += q.Rval
                                aVal(52, j, 1) += q.RIl
                                aVal(52, 12, 0) += q.Rval
                                aVal(52, 12, 1) += q.RIl
                            Next q
                            For i = 1 To 52
                                For j = 1 To 12
                                    If Not (aVal(i, j, 0) Is Nothing OrElse aVal(i, j, 1) Is Nothing) Then
                                        aDane(i, j) = aVal(i, j, 0) / aVal(i, j, 1)
                                    End If
                                Next j
                            Next i
                            With XE
                                With .oSheet
                                    .Activate()
                                    .Range(Rng).Value = aDane
                                End With
                                .oApp.Run("PodzielStrone")
                            End With
                        Next k
                    Case 2
                        With XE
                            .oSheet = CType(.oBook.Sheets("Total_perTon"), XL._Worksheet)
                        End With
                        For k = 1 To 8
                            Dim query As IEnumerable = Nothing
                            Dim query2 As IEnumerable = Nothing
                            Dim query3 As IEnumerable = Nothing
                            Select Case k
                                Case 1
                                    Rng = "B3:N55"
                                    query = (From p In dt _
                                             Where p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                             Group By ID = p!Spedytor _
                                             Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                             Order By RVal / RIl Descending)
                                Case 2
                                    Rng = "B59:N111"
                                    query = (From p In dt _
                                             Where p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                    AndAlso p!WarunekWysylki.ToString.Equals("W1") _
                                             Group By ID = p!Spedytor _
                                             Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                             Order By RVal / RIl Descending)
                                Case 3
                                    Rng = "B115:N167"
                                    query = (From p In dt _
                                             Where p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                    AndAlso p!WarunekWysylki.ToString.Equals("W2") _
                                             Group By ID = p!Spedytor _
                                             Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                             Order By RVal / RIl Descending)
                                Case 4
                                    Rng = "B171:N223"
                                    query = (From p In dt _
                                             Where p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                    AndAlso p!WarunekWysylki.ToString.Equals("W4") _
                                             Group By ID = p!Spedytor _
                                             Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                             Order By RVal / RIl Descending)
                                Case 5
                                    Rng = "B227:N279"
                                    query = (From p In dt _
                                             Where Not p!Mg.Equals(0.0) _
                                             Group By ID = p!Spedytor _
                                             Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                             Order By RVal / RIl Descending)
                                Case 6
                                    Rng = "B283:N335"
                                    query = (From p In dt _
                                             Where p!WarunekWysylki.ToString.Equals("W1") _
                                             Group By ID = p!Spedytor _
                                             Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                             Order By RVal / RIl Descending)
                                Case 7
                                    Rng = "B339:N391"
                                    query = (From p In dt _
                                             Where p!WarunekWysylki.ToString.Equals("W2") _
                                             Group By ID = p!Spedytor _
                                             Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                             Order By RVal / RIl Descending)
                                Case 8
                                    Rng = "B395:N447"
                                    query = (From p In dt _
                                             Where p!WarunekWysylki.ToString.Equals("W4") _
                                             Group By ID = p!Spedytor _
                                             Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                             Order By RVal / RIl Descending)
                            End Select
                            i = 0
                            ReDim aDane(0 To 52, 0 To 12)
                            ReDim aVal(0 To 52, 0 To 12, 1)
                            For Each q In query
                                i += 1
                                aDane(i, 0) = q.ID
                                If i.Equals(50) Then Exit For
                            Next q
                            aDane(51, 0) = "Other Transport Companies"
                            aDane(52, 0) = "Total"
                            aDane(0, 11) = "Other Countries"
                            aDane(0, 12) = "Total"
                            Select Case k
                                Case 1
                                    query2 = (From p In dt _
                                              Where p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                              Group By KK = p!KlKraj _
                                              Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                              Order By RVal / RIl Descending)
                                Case 2
                                    query2 = (From p In dt _
                                              Where p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                    AndAlso p!WarunekWysylki.ToString.Equals("W1") _
                                              Group By KK = p!KlKraj _
                                              Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                              Order By RVal / RIl Descending)
                                Case 3
                                    query2 = (From p In dt _
                                              Where p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                    AndAlso p!WarunekWysylki.ToString.Equals("W2") _
                                              Group By KK = p!KlKraj _
                                              Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                              Order By RVal / RIl Descending)
                                Case 4
                                    query2 = (From p In dt _
                                              Where p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                    AndAlso p!WarunekWysylki.ToString.Equals("W4") _
                                              Group By KK = p!KlKraj _
                                              Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                              Order By RVal / RIl Descending)
                                Case 5
                                    query2 = (From p In dt _
                                              Group By KK = p!KlKraj _
                                              Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                              Order By RVal / RIl Descending)
                                Case 6
                                    query2 = (From p In dt _
                                              Where p!WarunekWysylki.ToString.Equals("W1") _
                                              Group By KK = p!KlKraj _
                                              Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                              Order By RVal / RIl Descending)
                                Case 7
                                    query2 = (From p In dt _
                                              Where p!WarunekWysylki.ToString.Equals("W2") _
                                              Group By KK = p!KlKraj _
                                              Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                              Order By RVal / RIl Descending)
                                Case 8
                                    query2 = (From p In dt _
                                              Where p!WarunekWysylki.ToString.Equals("W4") _
                                              Group By KK = p!KlKraj _
                                              Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                              Order By RVal / RIl Descending)
                            End Select
                            i = 0
                            For Each q In query2
                                i += 1
                                aDane(0, i) = q.KK
                                If i.Equals(10) Then Exit For
                            Next q
                            Select Case k
                                Case 1
                                    query3 = (From p In dt _
                                              Where p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                              Group By ID = p!Spedytor, KK = p!KlKraj _
                                              Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                              Order By RVal / RIl Descending)
                                Case 2
                                    query3 = (From p In dt _
                                              Where p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                    AndAlso p!WarunekWysylki.ToString.Equals("W1") _
                                              Group By ID = p!Spedytor, KK = p!KlKraj _
                                              Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                              Order By RVal / RIl Descending)
                                Case 3
                                    query3 = (From p In dt _
                                              Where p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                    AndAlso p!WarunekWysylki.ToString.Equals("W2") _
                                              Group By ID = p!Spedytor, KK = p!KlKraj _
                                              Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                              Order By RVal / RIl Descending)
                                Case 4
                                    query3 = (From p In dt _
                                              Where p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                    AndAlso p!WarunekWysylki.ToString.Equals("W4") _
                                              Group By ID = p!Spedytor, KK = p!KlKraj _
                                              Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                              Order By RVal / RIl Descending)
                                Case 5
                                    query3 = (From p In dt _
                                              Group By ID = p!Spedytor, KK = p!KlKraj _
                                              Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                              Order By RVal / RIl Descending)
                                Case 6
                                    query3 = (From p In dt _
                                              Where p!WarunekWysylki.ToString.Equals("W1") _
                                              Group By ID = p!Spedytor, KK = p!KlKraj _
                                              Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                              Order By RVal / RIl Descending)
                                Case 7
                                    query3 = (From p In dt _
                                              Where p!WarunekWysylki.ToString.Equals("W2") _
                                              Group By ID = p!Spedytor, KK = p!KlKraj _
                                              Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                              Order By RVal / RIl Descending)
                                Case 8
                                    query3 = (From p In dt _
                                              Where p!WarunekWysylki.ToString.Equals("W4") _
                                              Group By ID = p!Spedytor, KK = p!KlKraj _
                                              Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                              Order By RVal / RIl Descending)
                            End Select
                            For Each q In query3
                                For i = 1 To 50
                                    If aDane(i, 0).Equals(q.ID) Then Exit For
                                Next i
                                For j = 1 To 10
                                    If aDane(0, j).Equals(q.KK) Then Exit For
                                Next j
                                aVal(i, j, 0) += q.Rval
                                aVal(i, j, 1) += q.RIl
                                aVal(i, 12, 0) += q.Rval
                                aVal(i, 12, 1) += q.RIl
                                aVal(52, j, 0) += q.Rval
                                aVal(52, j, 1) += q.RIl
                                aVal(52, 12, 0) += q.Rval
                                aVal(52, 12, 1) += q.RIl
                            Next q
                            For i = 1 To 52
                                For j = 1 To 12
                                    If Not (aVal(i, j, 0) Is Nothing OrElse aVal(i, j, 1) Is Nothing) Then
                                        aDane(i, j) = aVal(i, j, 0) / aVal(i, j, 1)
                                    End If
                                Next j
                            Next i
                            With XE
                                With .oSheet
                                    .Activate()
                                    .Range(Rng).Value = aDane
                                End With
                                .oApp.Run("PodzielStrone")
                            End With
                        Next k
                End Select
            Next m
            aDane = Nothing
            aVal = Nothing
            Dim cc As String = ""
            Dim _Key As String
            Dim oDane(0 To 50, 0 To 2) As Object
            For n = 0 To 3
                Dim query0 As IEnumerable = Nothing
                If n < 2 Then
                    query0 = (From p In dt Select p!AHOfZWW).Distinct
                Else
                    query0 = (From p In dt Select p!Wydzial).Distinct
                End If
                For Each w As String In query0
                    _Key = w
                    Select Case n
                        Case 0
                            cc = "Country"
                            For k = 1 To 8
                                Dim query As IEnumerable = Nothing
                                Select Case k
                                    Case 1
                                        Rng = "B4:D54"
                                        query = (From p In dt _
                                                 Where p!AHOfZWW.Equals(_Key) AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                 Group By ID = p!KlKraj _
                                                 Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                                 Order By RIl Descending)
                                    Case 2
                                        Rng = "H4:J54"
                                        query = (From p In dt _
                                                 Where p!AHOfZWW.Equals(_Key) AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                        AndAlso p!WarunekWysylki.ToString.Equals("W1") _
                                                 Group By ID = p!KlKraj _
                                                 Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                                 Order By RIl Descending)
                                    Case 3
                                        Rng = "N4:P54"
                                        query = (From p In dt _
                                                 Where p!AHOfZWW.Equals(_Key) AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                        AndAlso p!WarunekWysylki.ToString.Equals("W2") _
                                                 Group By ID = p!KlKraj _
                                                 Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                                 Order By RIl Descending)
                                    Case 4
                                        Rng = "T4:V54"
                                        query = (From p In dt _
                                                 Where p!AHOfZWW.Equals(_Key) AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                        AndAlso p!WarunekWysylki.ToString.Equals("W4") _
                                                 Group By ID = p!KlKraj _
                                                 Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                                 Order By RIl Descending)
                                    Case 5
                                        Rng = "B60:D110"
                                        query = (From p In dt _
                                                 Where p!AHOfZWW.Equals(_Key) _
                                                 Group By ID = p!KlKraj _
                                                 Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                                 Order By RIl Descending)
                                    Case 6
                                        Rng = "H60:J110"
                                        query = (From p In dt _
                                                 Where p!AHOfZWW.Equals(_Key) AndAlso p!WarunekWysylki.ToString.Equals("W1") _
                                                 Group By ID = p!KlKraj _
                                                 Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                                 Order By RIl Descending)
                                    Case 7
                                        Rng = "N60:P110"
                                        query = (From p In dt _
                                                 Where p!AHOfZWW.Equals(_Key) AndAlso p!WarunekWysylki.ToString.Equals("W2") _
                                                 Group By ID = p!KlKraj _
                                                 Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                                 Order By RIl Descending)
                                    Case 8
                                        Rng = "T60:V110"
                                        query = (From p In dt _
                                                 Where p!AHOfZWW.Equals(_Key) AndAlso p!WarunekWysylki.ToString.Equals("W4") _
                                                 Group By ID = p!KlKraj _
                                                 Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                                 Order By RIl Descending)
                                End Select
                                Try
                                    With XE
                                        .oSheet = CType(.oBook.Sheets("Data" + w + cc), XL._Worksheet)
                                    End With
                                Catch ex As Exception
                                    GoTo NastepneW
                                End Try
                                i = 0
                                ReDim oDane(0 To 50, 0 To 2)
                                For Each q In query
                                    If i > 49 Then
                                        oDane(50, 1) += q.ril
                                        oDane(50, 2) += q.rval
                                    Else
                                        oDane(i, 0) = q.ID
                                        oDane(i, 1) = q.ril
                                        oDane(i, 2) = q.rval
                                    End If
                                    i += 1
                                Next q
                                oDane(50, 0) = "Other countries"
                                XE.oSheet.Range(Rng).Value = oDane
                            Next k
                        Case 1
                            cc = "Company"
                            For k = 1 To 8
                                Dim query As IEnumerable = Nothing
                                Select Case k
                                    Case 1
                                        Rng = "B4:D54"
                                        query = (From p In dt _
                                                 Where p!AHOfZWW.Equals(_Key) AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                 Group By ID = p!Spedytor _
                                                 Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                                 Order By RIl Descending)
                                    Case 2
                                        Rng = "H4:J54"
                                        query = (From p In dt _
                                                 Where p!AHOfZWW.Equals(_Key) AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                        AndAlso p!WarunekWysylki.ToString.Equals("W1") _
                                                 Group By ID = p!Spedytor _
                                                 Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                                 Order By RIl Descending)
                                    Case 3
                                        Rng = "N4:P54"
                                        query = (From p In dt _
                                                 Where p!AHOfZWW.Equals(_Key) AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                        AndAlso p!WarunekWysylki.ToString.Equals("W2") _
                                                 Group By ID = p!Spedytor _
                                                 Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                                 Order By RIl Descending)
                                    Case 4
                                        Rng = "T4:V54"
                                        query = (From p In dt _
                                                 Where p!AHOfZWW.Equals(_Key) AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                        AndAlso p!WarunekWysylki.ToString.Equals("W4") _
                                                 Group By ID = p!Spedytor _
                                                 Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                                 Order By RIl Descending)
                                    Case 5
                                        Rng = "B60:D110"
                                        query = (From p In dt _
                                                 Where p!AHOfZWW.Equals(_Key) _
                                                 Group By ID = p!Spedytor _
                                                 Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                                 Order By RIl Descending)
                                    Case 6
                                        Rng = "H60:J110"
                                        query = (From p In dt _
                                                 Where p!AHOfZWW.Equals(_Key) AndAlso p!WarunekWysylki.ToString.Equals("W1") _
                                                 Group By ID = p!Spedytor _
                                                 Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                                 Order By RIl Descending)
                                    Case 7
                                        Rng = "N60:P110"
                                        query = (From p In dt _
                                                 Where p!AHOfZWW.Equals(_Key) AndAlso p!WarunekWysylki.ToString.Equals("W2") _
                                                 Group By ID = p!Spedytor _
                                                 Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                                 Order By RIl Descending)
                                    Case 8
                                        Rng = "T60:V110"
                                        query = (From p In dt _
                                                 Where p!AHOfZWW.Equals(_Key) AndAlso p!WarunekWysylki.ToString.Equals("W4") _
                                                 Group By ID = p!Spedytor _
                                                 Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                                 Order By RIl Descending)
                                End Select
                                Try
                                    With XE
                                        .oSheet = CType(.oBook.Sheets("Data" + w + cc), XL._Worksheet)
                                    End With
                                Catch ex As Exception
                                    GoTo NastepneW
                                End Try
                                i = 0
                                ReDim oDane(0 To 50, 0 To 2)
                                For Each q In query
                                    If i > 49 Then
                                        oDane(50, 1) += q.ril
                                        oDane(50, 2) += q.rval
                                    Else
                                        oDane(i, 0) = q.ID
                                        oDane(i, 1) = q.ril
                                        oDane(i, 2) = q.rval
                                    End If
                                    i += 1
                                Next q
                                oDane(50, 0) = "Other companies"
                                XE.oSheet.Range(Rng).Value = oDane
                            Next k
                        Case 2
                            cc = "Country"
                            For k = 1 To 8
                                Dim query As IEnumerable = Nothing
                                Select Case k
                                    Case 1
                                        Rng = "B4:D54"
                                        query = (From p In dt _
                                                 Where p!Wydzial.Equals(_Key) AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                 Group By ID = p!KlKraj _
                                                 Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                                 Order By RIl Descending)
                                    Case 2
                                        Rng = "H4:J54"
                                        query = (From p In dt _
                                                 Where p!Wydzial.Equals(_Key) AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                        AndAlso p!WarunekWysylki.ToString.Equals("W1") _
                                                 Group By ID = p!KlKraj _
                                                 Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                                 Order By RIl Descending)
                                    Case 3
                                        Rng = "N4:P54"
                                        query = (From p In dt _
                                                 Where p!Wydzial.Equals(_Key) AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                        AndAlso p!WarunekWysylki.ToString.Equals("W2") _
                                                 Group By ID = p!KlKraj _
                                                 Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                                 Order By RIl Descending)
                                    Case 4
                                        Rng = "T4:V54"
                                        query = (From p In dt _
                                                 Where p!Wydzial.Equals(_Key) AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                        AndAlso p!WarunekWysylki.ToString.Equals("W4") _
                                                 Group By ID = p!KlKraj _
                                                 Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                                 Order By RIl Descending)
                                    Case 5
                                        Rng = "B60:D110"
                                        query = (From p In dt _
                                                 Where p!Wydzial.Equals(_Key) _
                                                 Group By ID = p!KlKraj _
                                                 Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                                 Order By RIl Descending)
                                    Case 6
                                        Rng = "H60:J110"
                                        query = (From p In dt _
                                                 Where p!Wydzial.Equals(_Key) AndAlso p!WarunekWysylki.ToString.Equals("W1") _
                                                 Group By ID = p!KlKraj _
                                                 Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                                 Order By RIl Descending)
                                    Case 7
                                        Rng = "N60:P110"
                                        query = (From p In dt _
                                                 Where p!Wydzial.Equals(_Key) AndAlso p!WarunekWysylki.ToString.Equals("W2") _
                                                 Group By ID = p!KlKraj _
                                                 Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                                 Order By RIl Descending)
                                    Case 8
                                        Rng = "T60:V110"
                                        query = (From p In dt _
                                                 Where p!Wydzial.Equals(_Key) AndAlso p!WarunekWysylki.ToString.Equals("W4") _
                                                 Group By ID = p!KlKraj _
                                                 Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                                 Order By RIl Descending)
                                End Select
                                Try
                                    With XE
                                        .oSheet = CType(.oBook.Sheets("Data" + w + cc), XL._Worksheet)
                                    End With
                                Catch ex As Exception
                                    GoTo NastepneW
                                End Try
                                i = 0
                                ReDim oDane(0 To 50, 0 To 2)
                                For Each q In query
                                    If i > 49 Then
                                        oDane(50, 1) += q.ril
                                        oDane(50, 2) += q.rval
                                    Else
                                        oDane(i, 0) = q.ID
                                        oDane(i, 1) = q.ril
                                        oDane(i, 2) = q.rval
                                    End If
                                    i += 1
                                Next q
                                oDane(50, 0) = "Other countries"
                                XE.oSheet.Range(Rng).Value = oDane
                            Next k
                        Case 3
                            cc = "Company"
                            For k = 1 To 8
                                Dim query As IEnumerable = Nothing
                                Select Case k
                                    Case 1
                                        Rng = "B4:D54"
                                        query = (From p In dt _
                                                 Where p!Wydzial.Equals(_Key) AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                 Group By ID = p!Spedytor _
                                                 Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                                 Order By RIl Descending)
                                    Case 2
                                        Rng = "H4:J54"
                                        query = (From p In dt _
                                                 Where p!Wydzial.Equals(_Key) AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                        AndAlso p!WarunekWysylki.ToString.Equals("W1") _
                                                 Group By ID = p!Spedytor _
                                                 Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                                 Order By RIl Descending)
                                    Case 3
                                        Rng = "N4:P54"
                                        query = (From p In dt _
                                                 Where p!Wydzial.Equals(_Key) AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                        AndAlso p!WarunekWysylki.ToString.Equals("W2") _
                                                 Group By ID = p!Spedytor _
                                                 Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                                 Order By RIl Descending)
                                    Case 4
                                        Rng = "T4:V54"
                                        query = (From p In dt _
                                                 Where p!Wydzial.Equals(_Key) AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                        AndAlso p!WarunekWysylki.ToString.Equals("W4") _
                                                 Group By ID = p!Spedytor _
                                                 Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                                 Order By RIl Descending)
                                    Case 5
                                        Rng = "B60:D110"
                                        query = (From p In dt _
                                                 Where p!Wydzial.Equals(_Key) _
                                                 Group By ID = p!Spedytor _
                                                 Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                                 Order By RIl Descending)
                                    Case 6
                                        Rng = "H60:J110"
                                        query = (From p In dt _
                                                 Where p!Wydzial.Equals(_Key) AndAlso p!WarunekWysylki.ToString.Equals("W1") _
                                                 Group By ID = p!Spedytor _
                                                 Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                                 Order By RIl Descending)
                                    Case 7
                                        Rng = "N60:P110"
                                        query = (From p In dt _
                                                 Where p!Wydzial.Equals(_Key) AndAlso p!WarunekWysylki.ToString.Equals("W2") _
                                                 Group By ID = p!Spedytor _
                                                 Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                                 Order By RIl Descending)
                                    Case 8
                                        Rng = "T60:V110"
                                        query = (From p In dt _
                                                 Where p!Wydzial.Equals(_Key) AndAlso p!WarunekWysylki.ToString.Equals("W4") _
                                                 Group By ID = p!Spedytor _
                                                 Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                                 Order By RIl Descending)
                                End Select
                                Try
                                    With XE
                                        .oSheet = CType(.oBook.Sheets("Data" + w + cc), XL._Worksheet)
                                    End With
                                Catch ex As Exception
                                    GoTo NastepneW
                                End Try
                                i = 0
                                ReDim oDane(0 To 50, 0 To 2)
                                For Each q In query
                                    If i > 49 Then
                                        oDane(50, 1) += q.ril
                                        oDane(50, 2) += q.rval
                                    Else
                                        oDane(i, 0) = q.ID
                                        oDane(i, 1) = q.ril
                                        oDane(i, 2) = q.rval
                                    End If
                                    i += 1
                                Next q
                                oDane(50, 0) = "Other companies"
                                XE.oSheet.Range(Rng).Value = oDane
                            Next k
                    End Select
NastepneW:
                Next w
            Next n
            'SD - zakład
            For n = 0 To 1
                If n.Equals(0) Then
                    For k = 1 To 8
                        Dim query As IEnumerable = Nothing
                        Select Case k
                            Case 1
                                Rng = "B4:D54"
                                query = (From p In dt _
                                         Where p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                         Group By ID = p!KlKraj _
                                         Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                         Order By RIl Descending)
                            Case 2
                                Rng = "H4:J54"
                                query = (From p In dt _
                                         Where p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                AndAlso p!WarunekWysylki.ToString.Equals("W1") _
                                         Group By ID = p!KlKraj _
                                         Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                         Order By RIl Descending)
                            Case 3
                                Rng = "N4:P54"
                                query = (From p In dt _
                                         Where p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                AndAlso p!WarunekWysylki.ToString.Equals("W2") _
                                         Group By ID = p!KlKraj _
                                         Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                         Order By RIl Descending)
                            Case 4
                                Rng = "T4:V54"
                                query = (From p In dt _
                                         Where p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                AndAlso p!WarunekWysylki.ToString.Equals("W4") _
                                         Group By ID = p!KlKraj _
                                         Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                         Order By RIl Descending)
                            Case 5
                                Rng = "B60:D110"
                                query = (From p In dt _
                                         Group By ID = p!KlKraj _
                                         Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                         Order By RIl Descending)
                            Case 6
                                Rng = "H60:J110"
                                query = (From p In dt _
                                         Where p!WarunekWysylki.ToString.Equals("W1") _
                                         Group By ID = p!KlKraj _
                                         Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                         Order By RIl Descending)
                            Case 7
                                Rng = "N60:P110"
                                query = (From p In dt _
                                         Where p!WarunekWysylki.ToString.Equals("W2") _
                                         Group By ID = p!KlKraj _
                                         Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                         Order By RIl Descending)
                            Case 8
                                Rng = "T60:V110"
                                query = (From p In dt _
                                         Where p!WarunekWysylki.ToString.Equals("W4") _
                                         Group By ID = p!KlKraj _
                                         Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                         Order By RIl Descending)
                        End Select
                        With XE
                            .oSheet = CType(.oBook.Sheets("DataSDCountry"), XL._Worksheet)
                        End With
                        i = 0
                        ReDim oDane(0 To 50, 0 To 2)
                        For Each q In query
                            If i > 49 Then
                                oDane(50, 1) += q.ril
                                oDane(50, 2) += q.rval
                            Else
                                oDane(i, 0) = q.ID
                                oDane(i, 1) = q.ril
                                oDane(i, 2) = q.rval
                            End If
                            i += 1
                        Next q
                        oDane(50, 0) = "Other countries"
                        XE.oSheet.Range(Rng).Value = oDane
                    Next k
                Else
                    For k = 1 To 8
                        Dim query As IEnumerable = Nothing
                        Select Case k
                            Case 1
                                Rng = "B4:D54"
                                query = (From p In dt _
                                         Where p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                         Group By ID = p!Spedytor _
                                         Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                         Order By RIl Descending)
                            Case 2
                                Rng = "H4:J54"
                                query = (From p In dt _
                                         Where p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                AndAlso p!WarunekWysylki.ToString.Equals("W1") _
                                         Group By ID = p!Spedytor _
                                         Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                         Order By RIl Descending)
                            Case 3
                                Rng = "N4:P54"
                                query = (From p In dt _
                                         Where p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                AndAlso p!WarunekWysylki.ToString.Equals("W2") _
                                         Group By ID = p!Spedytor _
                                         Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                         Order By RIl Descending)
                            Case 4
                                Rng = "T4:V54"
                                query = (From p In dt _
                                         Where p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                AndAlso p!WarunekWysylki.ToString.Equals("W4") _
                                         Group By ID = p!Spedytor _
                                         Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                         Order By RIl Descending)
                            Case 5
                                Rng = "B60:D110"
                                query = (From p In dt _
                                         Group By ID = p!Spedytor _
                                         Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                         Order By RIl Descending)
                            Case 6
                                Rng = "H60:J110"
                                query = (From p In dt _
                                         Where p!WarunekWysylki.ToString.Equals("W1") _
                                         Group By ID = p!Spedytor _
                                         Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                         Order By RIl Descending)
                            Case 7
                                Rng = "N60:P110"
                                query = (From p In dt _
                                         Where p!WarunekWysylki.ToString.Equals("W2") _
                                         Group By ID = p!Spedytor _
                                         Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                         Order By RIl Descending)
                            Case 8
                                Rng = "T60:V110"
                                query = (From p In dt _
                                         Where p!WarunekWysylki.ToString.Equals("W4") _
                                         Group By ID = p!Spedytor _
                                         Into RVal = Sum(p.Field(Of Double)("Kwota")), RIl = Sum(p.Field(Of Double)("Mg")) _
                                         Order By RIl Descending)
                        End Select
                        With XE
                            .oSheet = CType(.oBook.Sheets("DataSDCompany"), XL._Worksheet)
                        End With
                        i = 0
                        ReDim oDane(0 To 50, 0 To 2)
                        For Each q In query
                            If i > 49 Then
                                oDane(50, 1) += q.ril
                                oDane(50, 2) += q.rval
                            Else
                                oDane(i, 0) = q.ID
                                oDane(i, 1) = q.ril
                                oDane(i, 2) = q.rval
                            End If
                            i += 1
                        Next q
                        oDane(50, 0) = "Other companies"
                        XE.oSheet.Range(Rng).Value = oDane
                    Next k
                End If
            Next n
Pokaz:
            frmMain.HideScreen()
            XE.Show()
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message, frmMain.tsslMain.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Using
        Finally
            da = Nothing
            dt = Nothing
            dtW = Nothing
            dvW = Nothing
            frmMain.HideScreen(False)
        End Try
    End Sub

    Private Sub RozdzielTnPln(ByVal dt As DataTable, ByVal Sufix As String, ByVal KW As String, ByVal XE As Cl4Excel.Excel)

        Dim i, j, k, m As Integer
        Dim Rng As String = ""
        Dim aDane(,) As Object
        For m = 0 To 2
            Select Case m
                Case 0
                    With XE
                        .oSheet = CType(.oBook.Sheets("Domestic_" + Sufix), XL._Worksheet)
                    End With
                    For k = 1 To 8
                        Dim query As IEnumerable = Nothing
                        Dim query2 As IEnumerable = Nothing
                        Dim query3 As IEnumerable = Nothing
                        Select Case k
                            Case 1
                                Rng = "B3:M54"
                                query = (From p In dt _
                                         Where p!GrDekretacji.ToString.Equals("01") AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                         Group By ID = p!Spedytor _
                                         Into RIl = Sum(p.Field(Of Double)(KW)) _
                                         Order By RIl Descending)
                            Case 2
                                Rng = "B59:M110"
                                query = (From p In dt _
                                         Where p!GrDekretacji.ToString.Equals("01") AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                AndAlso p!WarunekWysylki.ToString.Equals("W1") _
                                         Group By ID = p!Spedytor _
                                         Into RIl = Sum(p.Field(Of Double)(KW)) _
                                         Order By RIl Descending)
                            Case 3
                                Rng = "B115:M166"
                                query = (From p In dt _
                                         Where p!GrDekretacji.ToString.Equals("01") AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                AndAlso p!WarunekWysylki.ToString.Equals("W2") _
                                         Group By ID = p!Spedytor _
                                         Into RIl = Sum(p.Field(Of Double)(KW)) _
                                         Order By RIl Descending)
                            Case 4
                                Rng = "B171:M222"
                                query = (From p In dt _
                                         Where p!GrDekretacji.ToString.Equals("01") AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                AndAlso p!WarunekWysylki.ToString.Equals("W4") _
                                         Group By ID = p!Spedytor _
                                         Into RIl = Sum(p.Field(Of Double)(KW)) _
                                         Order By RIl Descending)
                            Case 5
                                Rng = "B227:M278"
                                query = (From p In dt _
                                         Where p!GrDekretacji.ToString.Equals("01") _
                                         Group By ID = p!Spedytor _
                                         Into RIl = Sum(p.Field(Of Double)(KW)) _
                                         Order By RIl Descending)
                            Case 6
                                Rng = "B283:M334"
                                query = (From p In dt _
                                         Where p!GrDekretacji.ToString.Equals("01") _
                                                AndAlso p!WarunekWysylki.ToString.Equals("W1") _
                                         Group By ID = p!Spedytor _
                                         Into RIl = Sum(p.Field(Of Double)(KW)) _
                                         Order By RIl Descending)
                            Case 7
                                Rng = "B339:M390"
                                query = (From p In dt _
                                         Where p!GrDekretacji.ToString.Equals("01") _
                                                AndAlso p!WarunekWysylki.ToString.Equals("W2") _
                                         Group By ID = p!Spedytor _
                                         Into RIl = Sum(p.Field(Of Double)(KW)) _
                                         Order By RIl Descending)
                            Case 8
                                Rng = "B395:M446"
                                query = (From p In dt _
                                         Where p!GrDekretacji.ToString.Equals("01") _
                                                AndAlso p!WarunekWysylki.ToString.Equals("W4") _
                                         Group By ID = p!Spedytor _
                                         Into RIl = Sum(p.Field(Of Double)(KW)) _
                                         Order By RIl Descending)
                        End Select
                        i = 0
                        ReDim aDane(0 To 51, 0 To 11)
                        For Each q In query
                            i += 1
                            aDane(i, 0) = q.ID
                            If i.Equals(50) Then Exit For
                        Next q
                        aDane(51, 0) = "Other Transport Companies"
                        aDane(0, 11) = "Other Countries"
                        Select Case k
                            Case 1
                                query2 = (From p In dt _
                                          Where p!GrDekretacji.ToString.Equals("01") AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                          Group By KK = p!KlKraj _
                                          Into RIl = Sum(p.Field(Of Double)(KW)) _
                                          Order By RIl Descending)
                            Case 2
                                query2 = (From p In dt _
                                          Where p!GrDekretacji.ToString.Equals("01") AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                AndAlso p!WarunekWysylki.ToString.Equals("W1") _
                                          Group By KK = p!KlKraj _
                                          Into RIl = Sum(p.Field(Of Double)(KW)) _
                                          Order By RIl Descending)
                            Case 3
                                query2 = (From p In dt _
                                          Where p!GrDekretacji.ToString.Equals("01") AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                AndAlso p!WarunekWysylki.ToString.Equals("W2") _
                                          Group By KK = p!KlKraj _
                                          Into RIl = Sum(p.Field(Of Double)(KW)) _
                                          Order By RIl Descending)
                            Case 4
                                query2 = (From p In dt _
                                          Where p!GrDekretacji.ToString.Equals("01") AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                AndAlso p!WarunekWysylki.ToString.Equals("W4") _
                                          Group By KK = p!KlKraj _
                                          Into RIl = Sum(p.Field(Of Double)(KW)) _
                                          Order By RIl Descending)
                            Case 5
                                query2 = (From p In dt _
                                          Where p!GrDekretacji.ToString.Equals("01") _
                                          Group By KK = p!KlKraj _
                                          Into RIl = Sum(p.Field(Of Double)(KW)) _
                                          Order By RIl Descending)
                            Case 6
                                query2 = (From p In dt _
                                          Where p!GrDekretacji.ToString.Equals("01") _
                                                AndAlso p!WarunekWysylki.ToString.Equals("W1") _
                                          Group By KK = p!KlKraj _
                                          Into RIl = Sum(p.Field(Of Double)(KW)) _
                                          Order By RIl Descending)
                            Case 7
                                query2 = (From p In dt _
                                          Where p!GrDekretacji.ToString.Equals("01") _
                                                AndAlso p!WarunekWysylki.ToString.Equals("W2") _
                                          Group By KK = p!KlKraj _
                                          Into RIl = Sum(p.Field(Of Double)(KW)) _
                                          Order By RIl Descending)
                            Case 8
                                query2 = (From p In dt _
                                          Where p!GrDekretacji.ToString.Equals("01") _
                                                AndAlso p!WarunekWysylki.ToString.Equals("W4") _
                                          Group By KK = p!KlKraj _
                                          Into RIl = Sum(p.Field(Of Double)(KW)) _
                                          Order By RIl Descending)
                        End Select
                        i = 0
                        For Each q In query2
                            i += 1
                            aDane(0, i) = q.KK
                            If i.Equals(10) Then Exit For
                        Next q
                        Select Case k
                            Case 1
                                query3 = (From p In dt _
                                          Where p!GrDekretacji.ToString.Equals("01") AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                          Group By ID = p!Spedytor, KK = p!KlKraj _
                                          Into RIl = Sum(p.Field(Of Double)(KW)) _
                                          Order By RIl Descending)
                            Case 2
                                query3 = (From p In dt _
                                          Where p!GrDekretacji.ToString.Equals("01") AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                AndAlso p!WarunekWysylki.ToString.Equals("W1") _
                                          Group By ID = p!Spedytor, KK = p!KlKraj _
                                          Into RIl = Sum(p.Field(Of Double)(KW)) _
                                          Order By RIl Descending)
                            Case 3
                                query3 = (From p In dt _
                                          Where p!GrDekretacji.ToString.Equals("01") AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                AndAlso p!WarunekWysylki.ToString.Equals("W2") _
                                          Group By ID = p!Spedytor, KK = p!KlKraj _
                                          Into RIl = Sum(p.Field(Of Double)(KW)) _
                                          Order By RIl Descending)
                            Case 4
                                query3 = (From p In dt _
                                          Where p!GrDekretacji.ToString.Equals("01") AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                AndAlso p!WarunekWysylki.ToString.Equals("W4") _
                                          Group By ID = p!Spedytor, KK = p!KlKraj _
                                          Into RIl = Sum(p.Field(Of Double)(KW)) _
                                          Order By RIl Descending)
                            Case 5
                                query3 = (From p In dt _
                                          Where p!GrDekretacji.ToString.Equals("01") _
                                          Group By ID = p!Spedytor, KK = p!KlKraj _
                                          Into RIl = Sum(p.Field(Of Double)(KW)) _
                                          Order By RIl Descending)
                            Case 6
                                query3 = (From p In dt _
                                          Where p!GrDekretacji.ToString.Equals("01") _
                                                AndAlso p!WarunekWysylki.ToString.Equals("W1") _
                                          Group By ID = p!Spedytor, KK = p!KlKraj _
                                          Into RIl = Sum(p.Field(Of Double)(KW)) _
                                          Order By RIl Descending)
                            Case 7
                                query3 = (From p In dt _
                                          Where p!GrDekretacji.ToString.Equals("01") _
                                                AndAlso p!WarunekWysylki.ToString.Equals("W2") _
                                          Group By ID = p!Spedytor, KK = p!KlKraj _
                                          Into RIl = Sum(p.Field(Of Double)(KW)) _
                                          Order By RIl Descending)
                            Case 8
                                query3 = (From p In dt _
                                          Where p!GrDekretacji.ToString.Equals("01") _
                                                AndAlso p!WarunekWysylki.ToString.Equals("W4") _
                                          Group By ID = p!Spedytor, KK = p!KlKraj _
                                          Into RIl = Sum(p.Field(Of Double)(KW)) _
                                          Order By RIl Descending)
                        End Select
                        For Each q In query3
                            For i = 1 To 50
                                If aDane(i, 0).Equals(q.ID) Then Exit For
                            Next i
                            For j = 1 To 10
                                If aDane(0, j).Equals(q.KK) Then Exit For
                            Next j
                            aDane(i, j) += q.RIl
                        Next q
                        With XE
                            With .oSheet
                                .Activate()
                                .Range(Rng).Value = aDane
                            End With
                            .oApp.Run("PodzielStrone")
                        End With
                    Next k
                Case 1
                    With XE
                        .oSheet = CType(.oBook.Sheets("Export_" + Sufix), XL._Worksheet)
                    End With
                    For k = 1 To 8
                        Dim query As IEnumerable = Nothing
                        Dim query2 As IEnumerable = Nothing
                        Dim query3 As IEnumerable = Nothing
                        Select Case k
                            Case 1
                                Rng = "B3:M54"
                                query = (From p In dt _
                                         Where Not p!GrDekretacji.ToString.Equals("01") AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                         Group By ID = p!Spedytor _
                                         Into RIl = Sum(p.Field(Of Double)(KW)) _
                                         Order By RIl Descending)
                            Case 2
                                Rng = "B59:M110"
                                query = (From p In dt _
                                         Where Not p!GrDekretacji.ToString.Equals("01") AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                AndAlso p!WarunekWysylki.ToString.Equals("W1") _
                                         Group By ID = p!Spedytor _
                                         Into RIl = Sum(p.Field(Of Double)(KW)) _
                                         Order By RIl Descending)
                            Case 3
                                Rng = "B115:M166"
                                query = (From p In dt _
                                         Where Not p!GrDekretacji.ToString.Equals("01") AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                AndAlso p!WarunekWysylki.ToString.Equals("W2") _
                                         Group By ID = p!Spedytor _
                                         Into RIl = Sum(p.Field(Of Double)(KW)) _
                                         Order By RIl Descending)
                            Case 4
                                Rng = "B171:M222"
                                query = (From p In dt _
                                         Where Not p!GrDekretacji.ToString.Equals("01") AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                AndAlso p!WarunekWysylki.ToString.Equals("W4") _
                                         Group By ID = p!Spedytor _
                                         Into RIl = Sum(p.Field(Of Double)(KW)) _
                                         Order By RIl Descending)
                            Case 5
                                Rng = "B227:M278"
                                query = (From p In dt _
                                         Where Not p!GrDekretacji.ToString.Equals("01") _
                                         Group By ID = p!Spedytor _
                                         Into RIl = Sum(p.Field(Of Double)(KW)) _
                                         Order By RIl Descending)
                            Case 6
                                Rng = "B283:M334"
                                query = (From p In dt _
                                         Where Not p!GrDekretacji.ToString.Equals("01") _
                                                AndAlso p!WarunekWysylki.ToString.Equals("W1") _
                                         Group By ID = p!Spedytor _
                                         Into RIl = Sum(p.Field(Of Double)(KW)) _
                                         Order By RIl Descending)
                            Case 7
                                Rng = "B339:M390"
                                query = (From p In dt _
                                         Where Not p!GrDekretacji.ToString.Equals("01") _
                                                AndAlso p!WarunekWysylki.ToString.Equals("W2") _
                                         Group By ID = p!Spedytor _
                                         Into RIl = Sum(p.Field(Of Double)(KW)) _
                                         Order By RIl Descending)
                            Case 8
                                Rng = "B395:M446"
                                query = (From p In dt _
                                         Where Not p!GrDekretacji.ToString.Equals("01") _
                                                AndAlso p!WarunekWysylki.ToString.Equals("W4") _
                                         Group By ID = p!Spedytor _
                                         Into RIl = Sum(p.Field(Of Double)(KW)) _
                                         Order By RIl Descending)
                        End Select
                        i = 0
                        ReDim aDane(0 To 51, 0 To 11)
                        For Each q In query
                            i += 1
                            aDane(i, 0) = q.ID
                            If i.Equals(50) Then Exit For
                        Next q
                        aDane(51, 0) = "Other Transport Companies"
                        aDane(0, 11) = "Other Countries"
                        Select Case k
                            Case 1
                                query2 = (From p In dt _
                                          Where Not p!GrDekretacji.ToString.Equals("01") AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                          Group By KK = p!KlKraj _
                                          Into RIl = Sum(p.Field(Of Double)(KW)) _
                                          Order By RIl Descending)
                            Case 2
                                query2 = (From p In dt _
                                          Where Not p!GrDekretacji.ToString.Equals("01") AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                AndAlso p!WarunekWysylki.ToString.Equals("W1") _
                                          Group By KK = p!KlKraj _
                                          Into RIl = Sum(p.Field(Of Double)(KW)) _
                                          Order By RIl Descending)
                            Case 3
                                query2 = (From p In dt _
                                          Where Not p!GrDekretacji.ToString.Equals("01") AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                AndAlso p!WarunekWysylki.ToString.Equals("W2") _
                                          Group By KK = p!KlKraj _
                                          Into RIl = Sum(p.Field(Of Double)(KW)) _
                                          Order By RIl Descending)
                            Case 4
                                query2 = (From p In dt _
                                          Where Not p!GrDekretacji.ToString.Equals("01") AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                AndAlso p!WarunekWysylki.ToString.Equals("W4") _
                                          Group By KK = p!KlKraj _
                                          Into RIl = Sum(p.Field(Of Double)(KW)) _
                                          Order By RIl Descending)
                            Case 5
                                query2 = (From p In dt _
                                          Where Not p!GrDekretacji.ToString.Equals("01") _
                                          Group By KK = p!KlKraj _
                                          Into RIl = Sum(p.Field(Of Double)(KW)) _
                                          Order By RIl Descending)
                            Case 6
                                query2 = (From p In dt _
                                          Where Not p!GrDekretacji.ToString.Equals("01") _
                                                AndAlso p!WarunekWysylki.ToString.Equals("W1") _
                                          Group By KK = p!KlKraj _
                                          Into RIl = Sum(p.Field(Of Double)(KW)) _
                                          Order By RIl Descending)
                            Case 7
                                query2 = (From p In dt _
                                          Where Not p!GrDekretacji.ToString.Equals("01") _
                                                AndAlso p!WarunekWysylki.ToString.Equals("W2") _
                                          Group By KK = p!KlKraj _
                                          Into RIl = Sum(p.Field(Of Double)(KW)) _
                                          Order By RIl Descending)
                            Case 8
                                query2 = (From p In dt _
                                          Where Not p!GrDekretacji.ToString.Equals("01") _
                                                AndAlso p!WarunekWysylki.ToString.Equals("W4") _
                                          Group By KK = p!KlKraj _
                                          Into RIl = Sum(p.Field(Of Double)(KW)) _
                                          Order By RIl Descending)
                        End Select
                        i = 0
                        For Each q In query2
                            i += 1
                            aDane(0, i) = q.KK
                            If i.Equals(10) Then Exit For
                        Next q
                        Select Case k
                            Case 1
                                query3 = (From p In dt _
                                          Where Not p!GrDekretacji.ToString.Equals("01") AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                          Group By ID = p!Spedytor, KK = p!KlKraj _
                                          Into RIl = Sum(p.Field(Of Double)(KW)) _
                                          Order By RIl Descending)
                            Case 2
                                query3 = (From p In dt _
                                          Where Not p!GrDekretacji.ToString.Equals("01") AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                AndAlso p!WarunekWysylki.ToString.Equals("W1") _
                                          Group By ID = p!Spedytor, KK = p!KlKraj _
                                          Into RIl = Sum(p.Field(Of Double)(KW)) _
                                          Order By RIl Descending)
                            Case 3
                                query3 = (From p In dt _
                                          Where Not p!GrDekretacji.ToString.Equals("01") AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                AndAlso p!WarunekWysylki.ToString.Equals("W2") _
                                          Group By ID = p!Spedytor, KK = p!KlKraj _
                                          Into RIl = Sum(p.Field(Of Double)(KW)) _
                                          Order By RIl Descending)
                            Case 4
                                query3 = (From p In dt _
                                          Where Not p!GrDekretacji.ToString.Equals("01") AndAlso p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                AndAlso p!WarunekWysylki.ToString.Equals("W4") _
                                          Group By ID = p!Spedytor, KK = p!KlKraj _
                                          Into RIl = Sum(p.Field(Of Double)(KW)) _
                                          Order By RIl Descending)
                            Case 5
                                query3 = (From p In dt _
                                          Where Not p!GrDekretacji.ToString.Equals("01") _
                                          Group By ID = p!Spedytor, KK = p!KlKraj _
                                          Into RIl = Sum(p.Field(Of Double)(KW)) _
                                          Order By RIl Descending)
                            Case 6
                                query3 = (From p In dt _
                                          Where Not p!GrDekretacji.ToString.Equals("01") _
                                                AndAlso p!WarunekWysylki.ToString.Equals("W1") _
                                          Group By ID = p!Spedytor, KK = p!KlKraj _
                                          Into RIl = Sum(p.Field(Of Double)(KW)) _
                                          Order By RIl Descending)
                            Case 7
                                query3 = (From p In dt _
                                          Where Not p!GrDekretacji.ToString.Equals("01") _
                                                AndAlso p!WarunekWysylki.ToString.Equals("W2") _
                                          Group By ID = p!Spedytor, KK = p!KlKraj _
                                          Into RIl = Sum(p.Field(Of Double)(KW)) _
                                          Order By RIl Descending)
                            Case 8
                                query3 = (From p In dt _
                                          Where Not p!GrDekretacji.ToString.Equals("01") _
                                                AndAlso p!WarunekWysylki.ToString.Equals("W4") _
                                          Group By ID = p!Spedytor, KK = p!KlKraj _
                                          Into RIl = Sum(p.Field(Of Double)(KW)) _
                                          Order By RIl Descending)
                        End Select
                        For Each q In query3
                            For i = 1 To 50
                                If aDane(i, 0).Equals(q.ID) Then Exit For
                            Next i
                            For j = 1 To 10
                                If aDane(0, j).Equals(q.KK) Then Exit For
                            Next j
                            aDane(i, j) += q.RIl
                        Next q
                        With XE
                            With .oSheet
                                .Activate()
                                .Range(Rng).Value = aDane
                            End With
                            .oApp.Run("PodzielStrone")
                        End With
                    Next k
                Case 2
                    With XE
                        .oSheet = CType(.oBook.Sheets("Total_" + Sufix), XL._Worksheet)
                    End With
                    For k = 1 To 8
                        Dim query As IEnumerable = Nothing
                        Dim query2 As IEnumerable = Nothing
                        Dim query3 As IEnumerable = Nothing
                        Select Case k
                            Case 1
                                Rng = "B3:M54"
                                query = (From p In dt _
                                         Where p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                         Group By ID = p!Spedytor _
                                         Into RIl = Sum(p.Field(Of Double)(KW)) _
                                         Order By RIl Descending)
                            Case 2
                                Rng = "B59:M110"
                                query = (From p In dt _
                                         Where p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                AndAlso p!WarunekWysylki.ToString.Equals("W1") _
                                         Group By ID = p!Spedytor _
                                         Into RIl = Sum(p.Field(Of Double)(KW)) _
                                         Order By RIl Descending)
                            Case 3
                                Rng = "B115:M166"
                                query = (From p In dt _
                                         Where p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                AndAlso p!WarunekWysylki.ToString.Equals("W2") _
                                         Group By ID = p!Spedytor _
                                         Into RIl = Sum(p.Field(Of Double)(KW)) _
                                         Order By RIl Descending)
                            Case 4
                                Rng = "B171:M222"
                                query = (From p In dt _
                                         Where p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                AndAlso p!WarunekWysylki.ToString.Equals("W4") _
                                         Group By ID = p!Spedytor _
                                         Into RIl = Sum(p.Field(Of Double)(KW)) _
                                         Order By RIl Descending)
                            Case 5
                                Rng = "B227:M278"
                                query = (From p In dt _
                                         Group By ID = p!Spedytor _
                                         Into RIl = Sum(p.Field(Of Double)(KW)) _
                                         Order By RIl Descending)
                            Case 6
                                Rng = "B283:M334"
                                query = (From p In dt _
                                         Where p!WarunekWysylki.ToString.Equals("W1") _
                                         Group By ID = p!Spedytor _
                                         Into RIl = Sum(p.Field(Of Double)(KW)) _
                                         Order By RIl Descending)
                            Case 7
                                Rng = "B339:M390"
                                query = (From p In dt _
                                         Where p!WarunekWysylki.ToString.Equals("W2") _
                                         Group By ID = p!Spedytor _
                                         Into RIl = Sum(p.Field(Of Double)(KW)) _
                                         Order By RIl Descending)
                            Case 8
                                Rng = "B395:M446"
                                query = (From p In dt _
                                         Where p!WarunekWysylki.ToString.Equals("W4") _
                                         Group By ID = p!Spedytor _
                                         Into RIl = Sum(p.Field(Of Double)(KW)) _
                                         Order By RIl Descending)
                        End Select
                        i = 0
                        ReDim aDane(0 To 51, 0 To 11)
                        For Each q In query
                            i += 1
                            aDane(i, 0) = q.ID
                            If i.Equals(50) Then Exit For
                        Next q
                        aDane(51, 0) = "Other Transport Companies"
                        aDane(0, 11) = "Other Countries"
                        Select Case k
                            Case 1
                                query2 = (From p In dt _
                                          Where p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                          Group By KK = p!KlKraj _
                                          Into RIl = Sum(p.Field(Of Double)(KW)) _
                                          Order By RIl Descending)
                            Case 2
                                query2 = (From p In dt _
                                          Where p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                AndAlso p!WarunekWysylki.ToString.Equals("W1") _
                                          Group By KK = p!KlKraj _
                                          Into RIl = Sum(p.Field(Of Double)(KW)) _
                                          Order By RIl Descending)
                            Case 3
                                query2 = (From p In dt _
                                          Where p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                AndAlso p!WarunekWysylki.ToString.Equals("W2") _
                                          Group By KK = p!KlKraj _
                                          Into RIl = Sum(p.Field(Of Double)(KW)) _
                                          Order By RIl Descending)
                            Case 4
                                query2 = (From p In dt _
                                          Where p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                AndAlso p!WarunekWysylki.ToString.Equals("W4") _
                                          Group By KK = p!KlKraj _
                                          Into RIl = Sum(p.Field(Of Double)(KW)) _
                                          Order By RIl Descending)
                            Case 5
                                query2 = (From p In dt _
                                          Group By KK = p!KlKraj _
                                          Into RIl = Sum(p.Field(Of Double)(KW)) _
                                          Order By RIl Descending)
                            Case 6
                                query2 = (From p In dt _
                                          Where p!WarunekWysylki.ToString.Equals("W1") _
                                          Group By KK = p!KlKraj _
                                          Into RIl = Sum(p.Field(Of Double)(KW)) _
                                          Order By RIl Descending)
                            Case 7
                                query2 = (From p In dt _
                                          Where p!WarunekWysylki.ToString.Equals("W2") _
                                          Group By KK = p!KlKraj _
                                          Into RIl = Sum(p.Field(Of Double)(KW)) _
                                          Order By RIl Descending)
                            Case 8
                                query2 = (From p In dt _
                                          Where p!WarunekWysylki.ToString.Equals("W4") _
                                          Group By KK = p!KlKraj _
                                          Into RIl = Sum(p.Field(Of Double)(KW)) _
                                          Order By RIl Descending)
                        End Select
                        i = 0
                        For Each q In query2
                            i += 1
                            aDane(0, i) = q.KK
                            If i.Equals(10) Then Exit For
                        Next q
                        Select Case k
                            Case 1
                                query3 = (From p In dt _
                                          Where p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                          Group By ID = p!Spedytor, KK = p!KlKraj _
                                          Into RIl = Sum(p.Field(Of Double)(KW)) _
                                          Order By RIl Descending)
                            Case 2
                                query3 = (From p In dt _
                                          Where p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                AndAlso p!WarunekWysylki.ToString.Equals("W1") _
                                          Group By ID = p!Spedytor, KK = p!KlKraj _
                                          Into RIl = Sum(p.Field(Of Double)(KW)) _
                                          Order By RIl Descending)
                            Case 3
                                query3 = (From p In dt _
                                          Where p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                AndAlso p!WarunekWysylki.ToString.Equals("W2") _
                                          Group By ID = p!Spedytor, KK = p!KlKraj _
                                          Into RIl = Sum(p.Field(Of Double)(KW)) _
                                          Order By RIl Descending)
                            Case 4
                                query3 = (From p In dt _
                                          Where p!Miesiac.ToString.Equals(frmMain.dtpOkres.Value.Month.ToString) _
                                                AndAlso p!WarunekWysylki.ToString.Equals("W4") _
                                          Group By ID = p!Spedytor, KK = p!KlKraj _
                                          Into RIl = Sum(p.Field(Of Double)(KW)) _
                                          Order By RIl Descending)
                            Case 5
                                query3 = (From p In dt _
                                          Group By ID = p!Spedytor, KK = p!KlKraj _
                                          Into RIl = Sum(p.Field(Of Double)(KW)) _
                                          Order By RIl Descending)
                            Case 6
                                query3 = (From p In dt _
                                          Where p!WarunekWysylki.ToString.Equals("W1") _
                                          Group By ID = p!Spedytor, KK = p!KlKraj _
                                          Into RIl = Sum(p.Field(Of Double)(KW)) _
                                          Order By RIl Descending)
                            Case 7
                                query3 = (From p In dt _
                                          Where p!WarunekWysylki.ToString.Equals("W2") _
                                          Group By ID = p!Spedytor, KK = p!KlKraj _
                                          Into RIl = Sum(p.Field(Of Double)(KW)) _
                                          Order By RIl Descending)
                            Case 8
                                query3 = (From p In dt _
                                          Where p!WarunekWysylki.ToString.Equals("W4") _
                                          Group By ID = p!Spedytor, KK = p!KlKraj _
                                          Into RIl = Sum(p.Field(Of Double)(KW)) _
                                          Order By RIl Descending)
                        End Select
                        For Each q In query3
                            For i = 1 To 50
                                If aDane(i, 0).Equals(q.ID) Then Exit For
                            Next i
                            For j = 1 To 10
                                If aDane(0, j).Equals(q.KK) Then Exit For
                            Next j
                            aDane(i, j) += q.RIl
                        Next q
                        With XE
                            With .oSheet
                                .Activate()
                                .Range(Rng).Value = aDane
                            End With
                            .oApp.Run("PodzielStrone")
                        End With
                    Next k
            End Select
        Next m
    End Sub

End Module
