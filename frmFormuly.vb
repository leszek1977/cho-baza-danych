﻿
#Region "Opcje i referencje"

Imports System.Windows.Forms
Imports System.IO
Imports XL = Microsoft.Office.Interop.Excel

#End Region ' Opcje i referencje

Public Class frmFormuly

    Private oApp As XL.Application
    Private m_Target As String
    Private m_Source As String
    Private WybraneArkusze As Boolean

    Private Sub frmFormuly_Load() Handles Me.Load

        With Me
            .chkLacza.Checked = My.Settings.ZF_Lacza
            .txtZmianaZ.Text = My.Settings.ZF_Source
            .txtZmianaNa.Text = My.Settings.ZF_Target
            .txtFile.Text = My.Settings.ZF_Plik
            .btnLista.Visible = False
            .txtZmianaZ.Focus()
        End With
    End Sub

    Private Sub Startuj() Handles btnStart.Click

        With Me
            With .txtZmianaZ
                .Text = .Text.Trim
                If String.IsNullOrEmpty(.Text) Then .Focus() : Exit Sub
                m_Source = .Text
            End With
            With .txtZmianaNa
                .Text = .Text.Trim
                If String.IsNullOrEmpty(.Text) Then .Focus() : Exit Sub
                m_Target = .Text
            End With
            Dim ofd As New OpenFileDialog
            With ofd
                .Title = "Wybierz folder z plikami źródłowymi"
                .Filter = "Pliki Excel (*.xls;*.xls?)|*.xls;*.xls?"
                Try
                    .InitialDirectory = My.Settings.ZF_Plik.Substring(0, My.Settings.ZF_Plik.LastIndexOf("\"))
                Catch ex As Exception
                End Try
                .Multiselect = False
                .ReadOnlyChecked = True
                If Not .ShowDialog().Equals(Windows.Forms.DialogResult.OK) Then _
                    Exit Sub
                Me.txtFile.Text = My.Computer.FileSystem.GetDirectoryInfo(.FileName).FullName
                If Me.chkLacza.Checked Then Me.chkArkusz.Checked = False
                With My.Settings
                    .ZF_Plik = ofd.FileName
                    .ZF_Source = m_Source
                    .ZF_Target = m_Target
                    .ZF_Lacza = Me.chkLacza.Checked
                    .ZF_Arkusz = Me.chkArkusz.Checked
                    .Save()
                End With
            End With
            ofd = Nothing
        End With
        If Me.chkArkusz.Checked Then
            ZmienFormuly(True)
            WybraneArkusze = False
            Me.btnLista.Visible = True
            Exit Sub
        End If
        ZmienFormuly(False)
    End Sub

    Private Sub Lista_Click() Handles btnLista.Click

        WybraneArkusze = True
        Me.btnLista.Visible = False
        ZmienFormuly(True)
    End Sub

    Private Sub Exit_Click() Handles btnExit.Click

        Try
            Try
                oApp.Quit()
            Catch ex As Exception
            End Try
        Catch ex As Exception
        Finally
            pReleaseObject(oApp)
            Me.Dispose()
        End Try
    End Sub

    Private Sub ZmienFormuly(Optional ByVal CheckSheets As Boolean = False)

        Dim oBook As XL._Workbook = Nothing
        Dim oSheets As XL.Sheets
        Dim oSheet As XL._Worksheet = Nothing
        Dim ac As XL.Range
        Dim strFormula As String = ""
        Dim blnChanged As Boolean = False
        Dim lngLastRow As Integer
        Dim m_Bylo, m_Jest As String
        Const CO_BEZP As String = "Bezpośrednia wymiana łączy"
        Const CO_FORM As String = "Zmiana formuł"
        oApp = New XL.Application
        Try
            With oApp
                oBook = .Workbooks.Open(Filename:=My.Settings.ZF_Plik, UpdateLinks:=False)
                .DisplayAlerts = False
                .Calculation = XL.XlCalculation.xlCalculationManual
            End With
            Try
                oSheets = oBook.Worksheets
                If Not WybraneArkusze AndAlso CheckSheets Then
                    Me.lstArkusze.Items.Clear()
                    For Each oSheet In oSheets
                        If oSheet.Visible = XL.XlSheetVisibility.xlSheetVisible Then
                            Me.lstArkusze.Items.Add(oSheet.Name)
                        End If
                    Next oSheet
                    Exit Try
                End If
                If Me.chkLacza.Checked Then
                    Dim aLinks As Array
                    Dim strLink As Object
                    Try
                        Me.tsslFormuly.Text = CO_BEZP + "..."
                        aLinks = CType(oApp.ActiveWorkbook.LinkSources(Type:=XL.XlLinkType.xlLinkTypeExcelLinks), Array)
                        If aLinks Is Nothing Then
                            MessageBox.Show("Skoroszyt nie posiada łączy", CO_BEZP, MessageBoxButtons.OK, MessageBoxIcon.Information)
                            Exit Try
                        End If
                        With Me.prgAkcja
                            .Minimum = 0
                            .Maximum = aLinks.GetUpperBound(0)
                        End With
                        Me.txtArkusz.Clear()
                        For Each strLink In aLinks
                            m_Bylo = strLink.ToString
                            If m_Bylo.IndexOf(m_Source).Equals(-1) Then GoTo Kolejne
                            m_Jest = m_Bylo.Replace(m_Source, m_Target)
                            If m_Bylo.Equals(m_Jest) Then GoTo Kolejne
                            oApp.ActiveWorkbook.ChangeLink(Name:=m_Bylo, NewName:=m_Jest, _
                                    Type:=XL.XlLinkType.xlLinkTypeExcelLinks)
Kolejne:
                            With Me.prgAkcja
                                .Value += 1
                                If .Value > .Maximum Then .Value = .Maximum
                            End With
                            Application.DoEvents()
                        Next strLink
                        blnChanged = True
                        Me.prgAkcja.Value = 0
                        Application.DoEvents()
                        MessageBox.Show("Wymiana łączy zakończona pomyślnie", CO_BEZP, MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Catch ex As Exception
                        Try
                            oBook.Saved = True
                            oApp.Quit()
                        Catch
                        End Try
                        blnChanged = False
                    End Try
                    Exit Try
                End If
                Me.tsslFormuly.Text = CO_FORM + "..."
                If Me.chkArkusz.Checked Then
                    If Me.lstArkusze.SelectedIndex < 0 Then Exit Try
                    Dim m_item As String
                    For Each m_item In Me.lstArkusze.SelectedItems
                        oSheet = CType(oBook.Sheets(m_item), XL.Worksheet)
                        lngLastRow = oSheet.UsedRange.Row - 1 + oSheet.UsedRange.Rows.Count
                        With Me.prgAkcja
                            .Minimum = 0
                            .Maximum = lngLastRow
                        End With
                        lngLastRow = 1
                        Me.txtArkusz.Text = oSheet.Name
                        For Each ac In oSheet.Cells.SpecialCells(XL.XlCellType.xlCellTypeFormulas)
                            If ac.Row > lngLastRow Then
                                lngLastRow = ac.Row
                                Me.prgAkcja.Value = lngLastRow
                                Application.DoEvents()
                            End If
                            If ac.Address = "" Then GoTo Nastepny2
                            Try
                                strFormula = ac.Formula.ToString.Replace("\\S216-0005\Comun", "W:")
                                If strFormula.ToString.IndexOf(m_Source) > -1 Then
                                    strFormula = strFormula.ToString.Replace(m_Source, m_Target)
                                    ac.Formula = strFormula.ToString
                                    blnChanged = True
                                End If
                            Catch ex As Exception
                                Me.txtWymiana.Text += String.Concat(oSheet.Name, Chr(9), ac.Address, vbCrLf)
                            End Try
Nastepny2:
                        Next ac
                        Me.prgAkcja.Value = 0
                        Application.DoEvents()
                    Next m_item
                    MessageBox.Show("Zmiana formuł wybranych arkuszy zakończona pomyślnie", CO_FORM, MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Exit Try
                End If
                For Each oSheet In oSheets
                    If Not oSheet.Visible = XL.XlSheetVisibility.xlSheetVisible Then GoTo NastepnyArkusz
                    lngLastRow = oSheet.UsedRange.Row - 1 + oSheet.UsedRange.Rows.Count
                    With Me.prgAkcja
                        .Minimum = 0
                        .Maximum = lngLastRow
                    End With
                    lngLastRow = 1
                    Me.txtArkusz.Text = oSheet.Name
                    For Each ac In oSheet.Cells.SpecialCells(XL.XlCellType.xlCellTypeFormulas)
                        Try
                            If ac.Row > lngLastRow Then
                                lngLastRow = ac.Row
                                Me.prgAkcja.Value = lngLastRow
                                Application.DoEvents()
                            End If
                            If ac.Address = "" Then GoTo Nastepny
                            strFormula = ac.Formula.ToString.Replace("\\S216-0005\Comun", "W:")
                            If strFormula.IndexOf(m_Source) > -1 Then
                                strFormula = strFormula.Replace(m_Source, m_Target)
                                ac.Formula = strFormula.ToString
                                blnChanged = True
                            End If
                        Catch ex As Exception
                            Me.txtWymiana.Text += String.Concat(oSheet.Name, Chr(9), ac.Address, vbCrLf)
                        End Try
Nastepny:
                    Next ac
                    Me.prgAkcja.Value = 0
                    Application.DoEvents()
NastepnyArkusz:
                Next oSheet
                MessageBox.Show("Zmiana formuł zakończona pomyślnie", CO_FORM, MessageBoxButtons.OK, MessageBoxIcon.Information)
            Catch ex As Exception
                Try
                    oBook.Saved = True
                    oApp.Quit()
                Catch
                End Try
                blnChanged = False
            End Try
        Catch ex As Exception
            MessageBox.Show(ex.Message, My.Settings.ZF_Plik, MessageBoxButtons.OK, MessageBoxIcon.Error)
            blnChanged = False
        Finally
            Me.tsslFormuly.Text = ""
            Try
                oApp.Calculation = XL.XlCalculation.xlCalculationAutomatic
                If blnChanged Then oBook.Save()
            Catch
            End Try
            Try
                oBook.Close()
            Catch
            End Try
            Try
                oApp.Quit()
            Catch
            End Try
            pReleaseObject(oSheet)
            pReleaseObject(oBook)
            pReleaseObject(oApp)
            If Me.txtWymiana.Text.Length > 0 Then
                Dim sw As StreamWriter
                Dim Plik As String = Me.txtFile.Text.Substring(0, Me.txtFile.Text.LastIndexOf(Path.DirectorySeparatorChar)) & "LogFormula.txt"
                sw = New StreamWriter(Plik, False)
                With sw
                    .Write(Me.txtWymiana.Text)
                    .Close()
                End With
                sw = Nothing
                MessageBox.Show("Sprawdź zapisy w pliku " + Plik, CO_FORM, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
        End Try
    End Sub

End Class
