﻿
' Created   2012-07-06 by KBrzozowski
' Modified: 2015-03-19

Imports System.IO
Imports System.Data.OleDb
Imports System.Deployment.Application
Imports CM = ClHelpFul.CenterMsg
Imports CP = ClHelpFul.Pomocne
Imports XL = Microsoft.Office.Interop.Excel

Public Class frmMain

#Region "Deklaracje"

    Public WybranaTabela As String
    Public Okres As String = ""
    Public cns As New OleDbConnection
    Private cnsPY As New OleDbConnection    ' connection to previous year
    Private WithEvents Initial As TextBox
    Private MNotH As String
    Private FiltrujOkres As Boolean = True

#End Region ' Deklaracje

#Region "Obsługa frmMain"

    Private Sub Main_Load() Handles MyBase.Load

        Try
            pCheckVersion()
            Okres = My.Settings.Okres
            With Me.dtpOkres
                Try
                    .Value = CDate(My.Settings.Okres)
                Catch ex As Exception
                    .Value = Now
                End Try
            End With
            With My.Settings
                If Not .Sciezka.EndsWith(Path.DirectorySeparatorChar) Then _
                    .Sciezka += Path.DirectorySeparatorChar
                If Not My.Computer.FileSystem.FileExists(.Sciezka + .Baza) Then
                    Dim opf As New OpenFileDialog
                    With opf
                        .InitialDirectory = My.Settings.Sciezka
                        .Multiselect = False
                        .FileName = "dbCHO_" + Okres.Substring(0, 4)
                        .Filter = "(*.mdb)|*.mdb"
                        If .ShowDialog.Equals(Windows.Forms.DialogResult.OK) Then
                            My.Settings.Baza = .SafeFileName
                            My.Settings.Sciezka = .FileName.Replace(My.Settings.Baza, "")
                        Else
                            KoniecProgramu()
                        End If
                    End With
                End If
                If Not .Baza.Contains(Okres.Substring(0, 4)) Then
                    Okres = Okres.Replace(Okres.Substring(0, 4), .Baza.Substring(6, 4))
                    Me.dtpOkres.Value = CDate(Okres)
                End If
                If .MainMaximized Then
                    Me.WindowState = FormWindowState.Maximized
                Else
                    Me.Location = .StartOfMain
                    Me.Size = .scrMain
                End If
                Me.dtpOkres.Size = .OkresSize
                Me.dtpOkres.Location = New Point(Me.Width - Me.dtpOkres.Width - 15, 0)
            End With
            Me.KeyPreview = True
            For Each ctrl As Control In Me.Controls
                If TypeOf (ctrl) Is MdiClient Then _
                    ctrl.BackColor = System.Drawing.SystemColors.Control : Exit For
            Next ctrl
            OdswiezHelp()
            ' wybór połączenia
            SetPolaczenie()
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
            KoniecProgramu()
        End Try
    End Sub

    Private Sub Main_ResizeEnd() Handles Me.ResizeEnd

        Me.dtpOkres.Location = New Point(Me.Width - Me.dtpOkres.Width - 15, 0)
    End Sub

    Private Sub Koniec()

        Dim c As New ClADO.CompareMdb
        With My.Settings
            c.KopiaZapasowa(.Sciezka, .Baza, .Myk, "C:\CHO_DataBase\Archiwum\")
        End With
        With My.Settings
            .StartOfMain = Me.Location
            .scrMain = Me.Size
            .MainMaximized = Me.WindowState.Equals(FormWindowState.Maximized)
            .Save()
        End With
    End Sub

    Private Sub KoniecProgramu() Handles tsm_PK.Click

        Koniec()
        Me.Dispose()
        GC.Collect()
        End
    End Sub

    Private Sub OdswiezHelp()

        'CP.CompareFileDate("W:\Controlling\VBA\CHO Baza danych\CHO BazaDanych.chm", _
        '                  "C:\CHO_DataBase\CHO BazaDanych.chm")
    End Sub

    Private Sub SetPolaczenie()

        Dim m_Ado As New ClADO.ADOGet
        Dim i As Integer
        Try
            With m_Ado
                .Baza = My.Settings.Baza
                .Folder = My.Settings.Sciezka
                .Haslo = My.Settings.Myk
                If Not .OpenData(True) Then _
                    Throw New ArgumentException("Brak połączenia z plikiem bazy danych [" + My.Settings.Baza + "]")
                Me.cns.ConnectionString = .CN.ConnectionString
                With .CN
                    i = .ConnectionString.IndexOf("Password")
                    MNotH = .ConnectionString.Substring(i + 9)
                End With
            End With
        Catch ex As Exception
            Throw New ArgumentException(ex.Message)
        Finally
            m_Ado = Nothing
        End Try
    End Sub

    Private Sub PolaczeniePY()

        Dim m_Ado As New ClADO.ADOGet
        Dim i As Integer = Me.dtpOkres.Value.Year - 1
        Try
            With m_Ado
                .Baza = My.Settings.Baza.Replace(Me.dtpOkres.Value.Year.ToString, i.ToString)
                .Folder = My.Settings.Sciezka
                .Haslo = My.Settings.Myk
                If Not .OpenData(True) Then _
                    Throw New ArgumentException("Brak połączenia z plikiem bazy danych [" + .Baza + "]")
                Me.cnsPY.ConnectionString = .CN.ConnectionString
            End With
        Catch ex As Exception
            Throw New ArgumentException(ex.Message)
        Finally
            m_Ado = Nothing
        End Try
    End Sub

    ' Niezbędne do uruchomienia hasła
    Private Sub Initial_TextChanged() Handles Initial.TextChanged

        If Initial.Text.Equals("1") Then Exit Sub
        Initial.Text = "1"
    End Sub

    Private Sub MainFinish() Handles Me.FormClosing

        Koniec()
    End Sub

    ' Pierwsze wyświetlenie ekranu głównego
    Private Sub Main_Shown() Handles MyBase.Shown

        If Initial Is Nothing Then
            Initial = New TextBox
            Initial.Text = "0"
        End If
    End Sub

    Private Sub SzerokoscWyswietlaniaOkresu() Handles tsm_PS.Click

        Dim m_Width As Integer
        Dim m_New As String
        With Me
            m_Width = .dtpOkres.Width
            m_New = InputBox("Wpisz liczbę całkowitą - aktualnie:", "Zmiana szerokości okna okresu", m_Width.ToString, CP.PositionX, CP.PositionY)
            If String.IsNullOrEmpty(m_New) Then Exit Sub
            With .dtpOkres
                .Width = CInt(m_New)
                .Location = New Point(Me.Width - .Width - 15, 0)
                My.Settings.OkresSize = .Size
            End With
        End With
    End Sub

    Private Sub Main_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        If Not e.Control Then Return
        If e.Alt Then e.Handled = True : Return
        Dim frm As Form
        frm = ChildHasFocus()
        If frm Is Nothing Then Exit Sub
        Select Case frm.Name
            Case "Podstawowe"
                'With Podstawowe
                '    Select Case e.KeyCode
                '        Case Keys.D                ' usuń
                '            If .tsbUsun.Enabled Then .Usun()
                '        Case Keys.N                ' dopisz
                '            If .tsbNowy.Enabled Then .Dopisz()
                '        Case Keys.S                ' zapisz
                '            If .tsbZapisz.Enabled Then .Zapisz()
                '        Case Keys.Z                ' zmień
                '            If .tsbZmien.Enabled Then .Zmien()
                '    End Select
                'End With
        End Select
    End Sub

    Private Sub MainMenuClick(ByVal sender As Object, ByVal e As System.EventArgs) _
                                Handles tsmDane.Click, _
                                        tsmExport.Click, _
                                        tsmPomocne.Click, _
                                        tsmAdministrator.Click

        ChildRemove()
        With Me
            .gbZmianaNazwyPlikow.Visible = False
            .gbParametry.Visible = False
            With .txtKomunikat
                .Clear()
                .Visible = False
            End With
        End With
    End Sub

    Private Function ChildRemove(Optional ByVal frm As Form = Nothing) As Boolean

        Dim frmVisible As Boolean
        ' ukryć  widoczne poza frm
        For Each fm As Form In Me.MdiChildren
            If Not frm Is Nothing AndAlso fm.Text.Equals(frm.Text) _
                Then frmVisible = True : fm.Visible = True : Exit For
            fm.Visible = False
        Next fm
        Return frmVisible
    End Function

    Private Function ChildHasFocus() As Form

        For Each frm As Form In Me.MdiChildren
            If frm.ContainsFocus Then Return frm
        Next frm
        Return Nothing
    End Function

    Private Sub ZmianaOkresu() Handles dtpOkres.ValueChanged

        Dim m_Plik As String
        Try
            With Me.dtpOkres
                .Text = DateSerial(.Value.Year, .Value.Month, 1).ToShortDateString
            End With
            With My.Settings
                If Not Me.dtpOkres.Value.Year.Equals(CInt(Okres.Substring(0, 4))) Then
                    m_Plik = "dbCHO_" + Me.dtpOkres.Value.Year.ToString + ".mdb"
                    If Not My.Computer.FileSystem.FileExists(.Sciezka + m_Plik) Then
                        Throw New ArgumentException("Brak dostępu do pliku rocznego [" + m_Plik + "]")
                    End If
                    .Baza = m_Plik
                    SetPolaczenie()
                    Using New CM
                        MessageBox.Show("Przełącznie pliku bazy na " + m_Plik, My.Settings.Sciezka, MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End Using
                    .ToBudget = .ToBudget.Replace(Okres.Substring(0, 4), Me.dtpOkres.Value.Year.ToString)
                    .ToMedia = .ToMedia.Replace(Okres.Substring(0, 4), Me.dtpOkres.Value.Year.ToString)
                    .ToZWKZlecenia = .ToZWKZlecenia.Replace(Okres.Substring(0, 4), Me.dtpOkres.Value.Year.ToString)
                    .ToPlan = .ToPlan.Replace(Okres.Substring(0, 4), Me.dtpOkres.Value.Year.ToString)
                    .ToResult = .ToResult.Replace(Okres.Substring(0, 4), Me.dtpOkres.Value.Year.ToString)
                    .ToRzecz = .ToRzecz.Replace(Okres.Substring(0, 4), Me.dtpOkres.Value.Year.ToString)
                    .ToSales = .ToSales.Replace(Okres.Substring(0, 4), Me.dtpOkres.Value.Year.ToString)
                    .ToStock = .ToStock.Replace(Okres.Substring(0, 4), Me.dtpOkres.Value.Year.ToString)
                    .ToWIP = .ToWIP.Replace(Okres.Substring(0, 4), Me.dtpOkres.Value.Year.ToString)
                    .ToWynikiDane = .ToWynikiDane.Replace(Okres.Substring(0, 4), Me.dtpOkres.Value.Year.ToString)
                    .ToZatrudnienie = .ToZatrudnienie.Replace(Okres.Substring(0, 4), Me.dtpOkres.Value.Year.ToString)
                    .Wyniki = .Wyniki.Replace(Okres.Substring(0, 4), Me.dtpOkres.Value.Year.ToString)
                End If
                .Okres = Me.dtpOkres.Value.ToShortDateString
                .Save()
                Okres = .Okres
            End With
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message, My.Settings.Sciezka, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Using
            With Me.dtpOkres
                .Text = Okres
                .Focus()
            End With
        End Try
    End Sub

#End Region ' Obsługa frmMain

    Private Sub PokazFormuly() Handles tsm_Pf.Click

        Dim m_Form As Form = Nothing
        Try
            m_Form = Formuly
            If Not ChildRemove(m_Form) Then
                With m_Form
                    .MdiParent = Me
                    .Dock = DockStyle.Fill
                    .Show()
                End With
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub PokazWieloletnie() Handles tsm_Pd.Click

        Dim m_Form As Form = Nothing
        Try
            m_Form = Wieloletnie
            If Not ChildRemove(m_Form) Then
                With m_Form
                    .MdiParent = Me
                    .Dock = DockStyle.Fill
                    .Show()
                End With
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub PokazWykonanie() Handles tsm_DPw.Click

        Dim m_Form As Form = Nothing
        Try
            m_Form = PobierzWykonanie
            If Not ChildRemove(m_Form) Then
                With m_Form
                    .MdiParent = Me
                    .Dock = DockStyle.Fill
                    PobierzWykonanie.gbPobierz.Visible = True
                    .Show()
                End With
            End If
        Catch ex As Exception
        End Try
    End Sub

#Region "Menu Dane"

#Region "Plan_i_Budzet"

    ' Sprzedaż ZWK
    Private Sub BudzetSprzedazyZWK(ByVal sender As System.Object, ByVal e As System.EventArgs) _
                                        Handles tsm_DPps.Click, _
                                                tsm_DPbs.Click

        Dim blnBudzet As Boolean = CType(sender, ToolStripMenuItem).Name.Contains("DPb")
        Dim da As OleDbDataAdapter
        Dim dtG As New DataTable, dtK As New DataTable, dt As New DataTable
        Dim r As DataRow
        Dim dvG, dvK As DataView
        Dim m_Action As String
        Dim _Ext As String = ".xls"
        Dim _File As String = "FD_SalesCustomer_" + Me.dtpOkres.Value.Year.ToString
        Dim _Path As String
        Dim _Tbl As String
        Dim i, w As Integer
        Dim aVal As Array
        If blnBudzet Then
            If BlokadaAktualizacji("tblBUSprzedaz") Then Return
            m_Action = "Budżet sprzedaży ZWK"
            _Path = My.Settings.ToBudget
            _File += "Bu"
            _Tbl = "tblBUSprzedaz"
        Else
            If BlokadaAktualizacji("tblPLSprzedaz") Then Return
            m_Action = "Plan sprzedaży ZWK"
            _Path = My.Settings.ToPlan
            _File += "op"
            _Tbl = "tblPLSprzedaz"
        End If
        With Me.dtpOkres.Value
ZmienRok:
            If Not _Path.Contains(.Year.ToString) Then
                i -= 1
                _Path = _Path.Replace(.AddYears(i).Year.ToString, .Year.ToString)
                GoTo ZmienRok
            End If
            If Not _Path.EndsWith(Path.DirectorySeparatorChar) Then _Path += Path.DirectorySeparatorChar
        End With
        _Path = CP.GetSourcePath(_Path + _File, _Ext)
        If String.IsNullOrEmpty(_Path) Then Return
        If blnBudzet Then
            My.Settings.ToBudget = _Path
        Else
            My.Settings.ToPlan = _Path
        End If
        _File += _Ext
        Try
            FillStatusBar(, m_Action + "...")
            With New Cl4Excel.GetFromXls(_Path + _File, "Mc" + Me.dtpOkres.Value.ToShortDateString.Substring(5, 2), "A8:AA")
                .Data2Array() : aVal = .Dane : .Zwolnij()
            End With
            If aVal Is Nothing Then Return
            da = New OleDbDataAdapter("SELECT ID, Wydzial FROM tblGrupyKalk WHERE Wydzial IN ('MS', 'PS', 'MT')", Me.cns)
            da.Fill(dtG)
            da = New OleDbDataAdapter("SELECT ID, GrDekretacji FROM tblKlienci", Me.cns)
            da.Fill(dtK)
            dvG = dtG.DefaultView
            dvG.Sort = "ID"
            dvK = dtK.DefaultView
            dvK.Sort = "ID"
            da = DA_TI(_Tbl, "Miesiac, Material, IDKlienta, GrKalk, Wydzial, GrDekretacji", "Wydzial IN ('MS', 'PS', 'MT') AND Miesiac = " & Me.dtpOkres.Value.Month)
            da.Fill(dt)
            For Each r In dt.Rows
                r.Delete()
            Next r
            For w = 1 To aVal.GetUpperBound(0)
                If aVal(w, 1) Is Nothing OrElse String.IsNullOrEmpty(aVal(w, 1).ToString) Then Exit For
                r = dt.NewRow
                r("Miesiac") = Me.dtpOkres.Value.Month
                r("Material") = aVal(w, 1)
                Try
                    r("IDKlienta") = aVal(w, 6)
                Catch ex As Exception
                    Me.txtKomunikat.Text += vbCrLf + "Klient [" + aVal(w, 7).ToString + "] - błąd odczytu ID"
                    GoTo Nastepny
                End Try
                r("GrMater") = aVal(w, 1)
                r("GrKalk") = aVal(w, 3)
                i = dvG.Find(aVal(w, 3))
                If i.Equals(-1) Then
                    With Me.txtKomunikat
                        If Not .Text.Contains(aVal(w, 3)) Then _
                            .Text += vbCrLf + "Grupa kalkulacyjna [" + aVal(w, 3) + "] nie istnieje"
                    End With
                    GoTo Nastepny
                Else
                    r("Wydzial") = dvG(i)("Wydzial")
                End If
                i = dvK.Find(aVal(w, 6))
                If i.Equals(-1) Then
                    With Me.txtKomunikat
                        If Not .Text.Contains(aVal(w, 6).ToString) Then _
                            .Text += vbCrLf + "Klient [" + aVal(w, 6).ToString + "] nie istnieje"
                    End With
                    GoTo Nastepny
                Else
                    r("GrDekretacji") = "0" + dvK(i)("GrDekretacji").ToString
                End If
                r("Waga") = aVal(w, 8)
                r("Kwota") = aVal(w, 9)
                r("PIP") = aVal(w, 11) + aVal(w, 12)
                r("Transport") = aVal(w, 15)
                r("Other") = aVal(w, 20)
                r("Overhead") = aVal(w, 18)
                r("EBiTDA") = aVal(w, 27)
                dt.Rows.Add(r)
Nastepny:
            Next w
            da.Update(dt.GetChanges)
            ClearStatus()
            Using New CM
                If String.IsNullOrEmpty(Me.txtKomunikat.Text) Then _
                   MessageBox.Show("Operacja zakończona pomyślnie", m_Action, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End Using
        Catch ex As Exception
            ClearStatus()
            Me.txtKomunikat.Text += vbCrLf + ex.Message
        Finally
            With Me.txtKomunikat
                If Not String.IsNullOrEmpty(.Text) Then
                    .Text = "Aktualizacja " + IIf(blnBudzet, "budżetu:", "planu:") + .Text
                    KomunikatShow()
                End If
            End With
            da = Nothing
            dt = Nothing
            dtG = Nothing
            dtK = Nothing
            dvK = Nothing
            dvG = Nothing
        End Try
    End Sub

    ' tu jest pobieranie dla planu za wyjątkiem sprzedaży ZWK
    Private Sub PLBUDane(ByVal sender As Object, ByVal e As System.EventArgs) _
                            Handles tsm_DPbC.Click, _
                                    tsm_DPpC.Click

        Dim blnBudzet As Boolean = CType(sender, ToolStripMenuItem).Name.Contains("DPb")
        Dim da As OleDbDataAdapter
        Dim dt As New DataTable, dtA As New DataTable, dtC As New DataTable
        Dim dtE As New DataTable, dtK As New DataTable, dtMed As New DataTable
        Dim dtSpr As New DataTable, dtZat As New DataTable, dtKos As New DataTable
        Dim r, r1 As DataRow
        Dim dvM, dvS, dvZ, dvK, dvG As DataView
        Dim bs As New BindingSource
        Dim i, i1, i2, j, k, m, p As Integer
        Dim m_Ark, m_Inst, m_Prod As String
        Dim m_WB As String = ""
        Dim m_Mpk As String = ""
        Dim tr As OleDbTransaction = Nothing
        Dim cmd As New OleDbCommand
        Dim m_Ado As New ClADO.ADOGet
        Dim aDane As Array = Nothing
        Dim aMedia As Array = Nothing
        Dim m_Dbl(3, 7) As Double
        Dim strPath As String
        Dim m_Action As String
        If blnBudzet Then
            m_Action = "Budżet: aktualizacja bazy danych"
            strPath = My.Settings.ToBudget
            If BlokadaAktualizacji("tblBUMedia") Then Exit Sub
            If BlokadaAktualizacji("tblBUNaleznosci") Then Exit Sub
            If BlokadaAktualizacji("tblBUSprzedaz") Then Exit Sub
            If BlokadaAktualizacji("tblBUWIP") Then Exit Sub
            If BlokadaAktualizacji("tblBUZapasy") Then Exit Sub
            If BlokadaAktualizacji("tblBUZatrudnienie") Then Exit Sub
            If BlokadaAktualizacji("tblBUZWPC") Then Exit Sub
        Else
            m_Action = "Plan: aktualizacja bazy danych"
            strPath = My.Settings.ToPlan
            If BlokadaAktualizacji("tblPLMedia") Then Exit Sub
            If BlokadaAktualizacji("tblPLNaleznosci") Then Exit Sub
            If BlokadaAktualizacji("tblPLSprzedaz") Then Exit Sub
            If BlokadaAktualizacji("tblPLWIP") Then Exit Sub
            If BlokadaAktualizacji("tblPLZapasy") Then Exit Sub
            If BlokadaAktualizacji("tblPLZatrudnienie") Then Exit Sub
            If BlokadaAktualizacji("tblPLZWPC") Then Exit Sub
        End If
        ' Sprawdzenie obecności plików
SprawdzPlik:
        If Not My.Computer.FileSystem.FileExists(strPath + "ZkSt_" + Okres.Substring(0, 4) + IIf(blnBudzet, "Bu", "op") + ".xls") Then
            Dim ofd As New OpenFileDialog
            With ofd
                .FileName = "ZkSt_" + Okres.Substring(0, 4) + IIf(blnBudzet, "Bu", "op") + ".xls"
                .Multiselect = False
                .InitialDirectory = strPath
                .Filter = IIf(blnBudzet, "Budżet", "Plan operacyjny") + " (*.xls)|*.xls"
                .ShowReadOnly = True
                If Not .ShowDialog.Equals(Windows.Forms.DialogResult.OK) Then Exit Sub
                strPath = .FileName.Replace(.SafeFileName, "")
                If blnBudzet Then
                    My.Settings.ToBudget = strPath.Replace(.SafeFileName, "")
                Else
                    My.Settings.ToPlan = strPath.Replace(.SafeFileName, "")
                End If
                My.Settings.Save()
                GoTo SprawdzPlik
            End With
        End If
        Try
            da = DA_TI("tblCALCO", "ID")
            da.Fill(dtC)
            da = DA_TI("tblGEn")
            da.Fill(dtE)
            da = New OleDbDataAdapter("SELECT ID, KolStyczenPomocniczych, FlOfCalCo FROM tblInstalacje WHERE KolStyczenPomocniczych > 0 ORDER BY KolStyczenPomocniczych", Me.cns)
            da.Fill(dtA)
            da = New OleDbDataAdapter("SELECT T.Skoroszyt, T.Ark, M.Skoroszyt, M.Ark, M.ID, M.Instalacja " _
                                + "FROM tblMPK M INNER JOIN tblInstalacje T ON M.Instalacja = T.ID " _
                                + "WHERE T.Skoroszyt <> '' OR M.Skoroszyt <> '' ORDER BY T.Skoroszyt, M.Skoroszyt, M.Ark, M.ID", Me.cns)
            da.Fill(dtK)
            If dtK.Rows.Count.Equals(0) Then Throw New ArgumentException("Brak danych podstawowych")
            ' modyfikacja Mpk
            For k = dtK.Rows.Count - 1 To 0 Step -1
                r = dtK.Rows(k)
                If Not String.IsNullOrEmpty(r(3).ToString.Trim) Then
                    If r(2).ToString.ToUpper.Equals("PIONDG") Then
                        If r(3).ToString.ToUpper.Equals("TRANSPORT") Then r.Delete()
                    ElseIf r(2).ToString.ToUpper.Equals("PIONDT") Then
                        If r(3).ToString.StartsWith("Ele") Then
                            If Not r(4).ToString.Equals("JTEL01") Then
                                r.Delete()
                            Else
                                r(3) = "Elektr"
                            End If
                        ElseIf r(3).ToString.Equals("OGGS_Inne") Then
                            r(3) = "OGGS_Ogólne"
                        ElseIf r(3).ToString.Equals("OGGS_CO") Then
                            r.Delete()
                        ElseIf r(3).ToString.Equals("Centrala") Then
                            r.Delete()
                        End If
                    ElseIf r(2).ToString.ToUpper.Equals("ZKPOM") Then
                        If r(3).ToString.Equals("Konsultant") Then
                            r(3) = "Zwk_Konsu"
                        End If
                    End If
                End If
            Next k
            dtK.AcceptChanges()
            FillStatusBar(dtK.Rows.Count, m_Action + "...")
            Using cn As New OleDbConnection(Me.cns.ConnectionString)
                Try
                    cn.Open()
                    tr = cn.BeginTransaction
                    With cmd
                        .Connection = cn
                        .Transaction = tr
                        ' Usuwanie poprzednich zapisów
                        For p = 0 To 3
                            Select Case p
                                Case 0
                                    .CommandText = "DELETE FROM tblPLMedia WHERE Mc = " + Me.dtpOkres.Value.Month.ToString
                                Case 1
                                    .CommandText = "DELETE FROM tblPLSprzedaz WHERE NOT Wydzial IN ('MS', 'PS', 'MT') AND Miesiac = " + Me.dtpOkres.Value.Month.ToString
                                Case 2
                                    .CommandText = "DELETE FROM tblPLZatrudnienie WHERE Mc = " + Me.dtpOkres.Value.Month.ToString
                                Case 3
                                    .CommandText = "DELETE FROM tblPLZWPC WHERE Mc = " + Me.dtpOkres.Value.Month.ToString
                            End Select
                            If blnBudzet Then _
                                .CommandText = .CommandText.Replace("tblPL", "tblBU")
                            .ExecuteNonQuery()
                        Next p
                    End With
                    tr.Commit()
                Catch ex As Exception
                    tr.Rollback()
                    If Not Me.txtKomunikat.Text.Contains(ex.Message) Then _
                        Me.txtKomunikat.Text += vbCrLf + ex.Message
                    Throw New ArgumentException(ex.Message)
                End Try
            End Using
            ' puste tabele
            da = New OleDbDataAdapter("SELECT * FROM tblPLMedia WHERE Mc = " + Me.dtpOkres.Value.Month.ToString, Me.cns)
            If blnBudzet Then
                With da.SelectCommand
                    .CommandText = .CommandText.Replace("tblPL", "tblBU")
                End With
            End If
            da.Fill(dtMed)
            dvM = dtMed.DefaultView
            dvM.Sort = "MPK, Media, JM"
            If blnBudzet Then
                da = New OleDbDataAdapter("SELECT * FROM tblBuSprzedaz WHERE Miesiac = " + Me.dtpOkres.Value.Month.ToString, Me.cns)
            Else
                da = New OleDbDataAdapter("SELECT * FROM tblPLSprzedaz WHERE NOT Wydzial IN ('MS', 'PS', 'MT') AND Miesiac = " + Me.dtpOkres.Value.Month.ToString, Me.cns)
            End If
            da.Fill(dtSpr)
            dvS = dtSpr.DefaultView
            dvS.Sort = "Material, Wydzial, GrDekretacji"
            da = New OleDbDataAdapter("SELECT * FROM tblPLZatrudnienie WHERE Mc = " + Me.dtpOkres.Value.Month.ToString, Me.cns)
            If blnBudzet Then
                With da.SelectCommand
                    .CommandText = .CommandText.Replace("tblPL", "tblBU")
                End With
            End If
            da.Fill(dtZat)
            dvZ = dtZat.DefaultView
            dvZ.Sort = "MPK, Grupa"
            da = New OleDbDataAdapter("SELECT * FROM tblPLZWPC WHERE Mc = " + Me.dtpOkres.Value.Month.ToString, Me.cns)
            If blnBudzet Then
                With da.SelectCommand
                    .CommandText = .CommandText.Replace("tblPL", "tblBU")
                End With
            End If
            da.Fill(dtKos)
            dvK = dtKos.DefaultView
            dvK.Sort = "Instalacja, MPK, CalCo"
            i = 0
            Do
                If String.IsNullOrEmpty(Trim(dtK.Rows(i)(0).ToString)) Then GoTo Pomocnicze
                m_WB = dtK.Rows(i)(0).ToString
                m_Mpk = ""
                m_Inst = dtK.Rows(i)(5).ToString
                While i < dtK.Rows.Count AndAlso dtK.Rows(i)(0).ToString.Equals(m_WB)
                    With dtK.Rows(i)(4)
                        If .ToString.LastIndexOf("09") > 0 Then m_Mpk = .ToString
                    End With
                    i += 1
                End While
                If String.IsNullOrEmpty(m_Mpk) Then _
                    Throw New ArgumentException("[" + m_Inst + "] - niezidentyfikowane Mpk")
                m_Ark = "m" + Okres.Substring(5, 2)
                Try
                    ' Koszty
                    With New Cl4Excel.GetFromXls(strPath + m_WB + "_" + Okres.Substring(0, 4) + IIf(blnBudzet, "Bu", "op") + ".xls", _
                                m_Ark, "A5:N320")
                        .Data2Array()
                        aDane = .Dane
                        .Arkusz = "Media" : .Obszar = "A5:O69"
                        .Data2Array()
                        aMedia = .Dane
                        .Zwolnij()
                    End With
                    If aDane Is Nothing Then Throw New ArgumentException("Dane nie istnieją [" + m_WB + "] arkusz [" + m_Ark + "]")
                    For j = 1 To aDane.GetUpperBound(0) - 5
                        If aDane.GetValue(j, 2) IsNot Nothing Then
                            m_Dbl(0, 0) = CDbl(aDane.GetValue(j, 8))
                            If m_Dbl(0, 0) <> 0 Then
                                Try
                                    m_Dbl(0, 1) = CDbl(aDane.GetValue(j, 6)) * 1000
                                Catch ex As Exception
                                    m_Dbl(0, 1) = 0
                                End Try
                                r = dtKos.NewRow
                                r("Mc") = Me.dtpOkres.Value.Month
                                r("Instalacja") = m_Inst
                                r("MPK") = m_Mpk
                                r("CalCo") = aDane.GetValue(j, 2)
                                r("Wartosc") = m_Dbl(0, 0)
                                r("Ilosc") = m_Dbl(0, 1)
                                r("Typ") = "P"
                                m = dvK.Find(New Object() {r("Instalacja"), r("MPK"), r("CalCo")})
                                If m.Equals(-1) Then
                                    dtKos.Rows.Add(r)
                                Else
                                    dvK(m)("Wartosc") += m_Dbl(0, 0)
                                    dvK(m)("Ilosc") += m_Dbl(0, 1)
                                End If
                            End If
                        End If
                    Next j
                    ' Zatrudnienie
                    m_Dbl(0, 0) = CDbl(aDane.GetValue(aDane.GetUpperBound(0), 13))
                    If Not m_Dbl(0, 0).Equals(0.0) Then
                        r = dtZat.NewRow
                        r("Mc") = Me.dtpOkres.Value.Month
                        r("MPK") = m_Mpk
                        r("Grupa") = "P"
                        r("Ilosc") = m_Dbl(0, 0)
                        m = dvZ.Find(New Object() {r("MPK"), r("Grupa")})
                        If m.Equals(-1) Then
                            dtZat.Rows.Add(r)
                        Else
                            dvZ(m)("Ilosc") += m_Dbl(0, 0)
                        End If
                    End If
                Catch ex As Exception
                    If Not Me.txtKomunikat.Text.Contains(ex.Message) Then _
                        Me.txtKomunikat.Text += vbCrLf + ex.Message
                End Try
                ' Media
                Try
                    If aMedia Is Nothing Then Throw New ArgumentException
                    For k = 0 To dtE.Rows.Count - 1
                        Try
                            i1 = CInt(dtE(k)("RowMedia").ToString)
                            If i1 > 0 Then
                                i1 -= 4
                                i2 = CInt(dtE(k)("RowMediaWartosc").ToString)
                                i2 -= 4
                                m_Dbl(0, 0) = CDbl(aMedia.GetValue(i1, 3 + Me.dtpOkres.Value.Month))
                                If Not m_Dbl(0, 0).Equals(0.0) Then
                                    m_Dbl(0, 0) = m_Dbl(0, 0) * CInt(dtE(k)("Podzielnik").ToString)
                                    m_Dbl(0, 1) = CDbl(aMedia.GetValue(i2, 3 + Me.dtpOkres.Value.Month))
                                    r = dtMed.NewRow
                                    r("Mc") = Me.dtpOkres.Value.Month
                                    r("MPK") = m_Mpk
                                    r("Media") = dtE(k)("ID")
                                    r("JM") = dtE(k)("JM")
                                    r("Wartosc") = m_Dbl(0, 0)
                                    r("Ilosc") = m_Dbl(0, 1)
                                    m = dvM.Find(New Object() {r("MPK"), r("Media"), r("JM")})
                                    If m.Equals(-1) Then
                                        dtMed.Rows.Add(r)
                                    Else
                                        dvM(m)("Wartosc") += m_Dbl(0, 0)
                                        dvM(m)("Ilosc") += m_Dbl(0, 1)
                                    End If
                                End If
                            End If
                        Catch ex As Exception
                        End Try
                    Next k
                Catch ex As Exception
                    Throw New ArgumentException(ex.Message)
                End Try
                GoTo Nastepny
Pomocnicze:
                m_WB = dtK(i)(2).ToString
                With New Cl4Excel.GetFromXls(strPath + m_WB + "_" + Okres.Substring(0, 4) + IIf(blnBudzet, "Bu", "op") + ".xls")
                    m_Mpk = ""
                    While i < dtK.Rows.Count AndAlso dtK(i)(2).ToString.Equals(m_WB)
                        r1 = dtK(i)
                        m_Ark = r1(3).ToString
                        m_Mpk = r1(4).ToString
                        m_Inst = r1(5).ToString
                        While i < dtK.Rows.Count AndAlso dtK(i)(2).ToString.Equals(m_WB) AndAlso dtK(i)(3).ToString = m_Ark
                            i += 1
                        End While
                        If String.IsNullOrEmpty(m_Mpk) Then Exit Do
                        .Arkusz = m_Ark
                        .Obszar = "A5:DV318"
                        .Data2Array()
                        aDane = .Dane
                        .Obszar = "A373:O431"
                        .Data2Array()
                        aMedia = .Dane
                        Try
                            If aDane Is Nothing Then Exit Try
                            For j = 1 To aDane.GetUpperBound(0)
                                If Not (aDane.GetValue(j, 2) Is Nothing OrElse String.IsNullOrEmpty(aDane.GetValue(j, 2).ToString)) Then
                                    m_Dbl(0, 0) = CDbl(aDane.GetValue(j, 3 + Me.dtpOkres.Value.Month))
                                    If Not m_Dbl(0, 0).Equals(0.0) Then
                                        r = dtKos.NewRow
                                        r("Mc") = Me.dtpOkres.Value.Month
                                        r("Instalacja") = m_Inst
                                        r("MPK") = m_Mpk
                                        r("CalCo") = aDane.GetValue(j, 2)
                                        r("Wartosc") = m_Dbl(0, 0)
                                        r("Typ") = "A"
                                        m = dvK.Find(New Object() {r("Instalacja"), r("MPK"), r("CalCo")})
                                        If m.Equals(-1) Then
                                            dtKos.Rows.Add(r)
                                        Else
                                            dvK(m)("Wartosc") += m_Dbl(0, 0)
                                        End If
                                    End If
                                    If Not m_WB.ToUpper.Contains("PIONDG") Then
                                        ' koszty przeniesione do produkcyjnych
                                        If m_WB.ToUpper.Contains("PIONDT") AndAlso m_Ark.Substring(0, 2).Equals("OG") Then
                                        Else
                                            For k = 0 To dtA.Rows.Count - 1
                                                If Not dtA(k)(0).ToString.Equals("JAX03") Then
                                                    m_Dbl(0, 0) = CDbl(aDane.GetValue(j, CInt(dtA(k)(1).ToString) + Me.dtpOkres.Value.Month - 1))
                                                    If Not m_Dbl(0, 0).Equals(0.0) Then
                                                        Try
                                                            m_Prod = ZamienCalco(dtA(k), dtC, aDane.GetValue(j, 2).ToString)
                                                            If String.IsNullOrEmpty(m_Prod) Then Throw New ArgumentException("Brak odpowiednika w [" + dtA(k)(0).ToString + "] dla [" + aDane.GetValue(j, 2).ToString + "]")
                                                            r = dtKos.NewRow
                                                            r("Mc") = Me.dtpOkres.Value.Month
                                                            r("Instalacja") = dtA(k)(0)
                                                            r("MPK") = m_Mpk
                                                            r("CalCo") = m_Prod
                                                            r("Wartosc") = m_Dbl(0, 0)
                                                            r("Typ") = "A"
                                                            m = dvK.Find(New Object() {r("Instalacja"), r("MPK"), r("CalCo")})
                                                            If m.Equals(-1) Then
                                                                dtKos.Rows.Add(r)
                                                            Else
                                                                dvK(m)("Wartosc") += m_Dbl(0, 0)
                                                            End If
                                                        Catch ex As Exception
                                                            With Me.txtKomunikat
                                                                If Not .Text.Contains(ex.Message) Then _
                                                                    .Text += vbCrLf + ex.Message
                                                            End With
                                                        End Try
                                                    End If
                                                End If
                                            Next k
                                        End If
                                    End If
                                End If
                            Next j
                            ' Media
                            For k = 0 To dtE.Rows.Count - 1
                                If aMedia Is Nothing Then Exit For
                                r1 = dtE.Rows(k)
                                Try
                                    i1 = CInt(r1("RowMedPom").ToString)
                                    If i1 > 0 Then
                                        i1 -= 372
                                        i2 = CInt(r1("RowMedPomWartosc").ToString)
                                        i2 -= 372
                                        m_Dbl(0, 0) = CDbl(aMedia.GetValue(i1, 3 + Me.dtpOkres.Value.Month))
                                        If Not m_Dbl(0, 0).Equals(0.0) Then
                                            m_Dbl(0, 0) = m_Dbl(0, 0) * CInt(r1("Podzielnik").ToString)
                                            m_Dbl(0, 1) = CDbl(aMedia.GetValue(i2, 3 + Me.dtpOkres.Value.Month))
                                            r = dtMed.NewRow
                                            r("Mc") = Me.dtpOkres.Value.Month
                                            r("MPK") = m_Mpk
                                            r("Media") = r1("ID")
                                            r("JM") = r1("JM")
                                            r("Wartosc") = m_Dbl(0, 0)
                                            r("Ilosc") = m_Dbl(0, 1)
                                            m = dvM.Find(New Object() {r("MPK"), r("Media"), r("JM")})
                                            If m.Equals(-1) Then
                                                dtMed.Rows.Add(r)
                                            Else
                                                dvM(m)("Wartosc") += m_Dbl(0, 0)
                                                dvM(m)("Ilosc") += m_Dbl(0, 1)
                                            End If
                                        End If
                                    End If
                                Catch ex As Exception
                                End Try
                            Next k
                            ' Zatrudnienie
                            .Obszar = "D320:O320"
                            .Data2Array()
                            aDane = .Dane
                            m_Dbl(0, 0) = CDbl(aDane.GetValue(1, Me.dtpOkres.Value.Month))
                            If Not m_Dbl(0, 0).Equals(0.0) Then
                                r = dtZat.NewRow
                                r("Mc") = Me.dtpOkres.Value.Month
                                r("MPK") = m_Mpk
                                r("Grupa") = "R"
                                r("Ilosc") = m_Dbl(0, 0)
                                m = dvZ.Find(New Object() {r("MPK"), r("Grupa")})
                                If m.Equals(-1) Then
                                    dtZat.Rows.Add(r)
                                Else
                                    dvZ(m)("Ilosc") += m_Dbl(0, 0)
                                End If
                            End If
                        Catch ex As Exception
                            If Not Me.txtKomunikat.Text.Contains(ex.Message) Then _
                                Me.txtKomunikat.Text += vbCrLf + ex.Message
                            GoTo Nastepny
                        End Try
                    End While
                    ' przeniesienie kosztów OGGS i OGWS
                    If m_WB.ToUpper.Contains("PIONDT") Then
                        For p = 0 To 1
                            If p.Equals(0) Then
                                .Arkusz = "OGGS"
                                m_Mpk = "JTGN01"
                            Else
                                .Arkusz = "OGWS"
                                m_Mpk = "JTWR01"
                            End If
                            .Obszar = "A5:DV318"
                            .Data2Array()
                            aDane = .Dane
                            For j = 1 To aDane.GetUpperBound(0)
                                If Not aDane.GetValue(j, 2) Is Nothing Then
                                    For k = 0 To dtA.Rows.Count - 1
                                        r1 = dtA.Rows(k)
                                        m_Dbl(0, 0) = CDbl(aDane.GetValue(j, CInt(dtA(k)(1).ToString) + Me.dtpOkres.Value.Month + 1))
                                        If Not m_Dbl(0, 0).Equals(0.0) Then
                                            Try
                                                m_Prod = ZamienCalco(r1, dtC, aDane.GetValue(j, 2).ToString)
                                                If String.IsNullOrEmpty(m_Prod) Then Throw New ArgumentException("Brak odpowiednika w [" + dtA(k)(0).ToString + "] dla [" + aDane.GetValue(j, 2).ToString + "]")
                                                r = dtKos.NewRow
                                                r("Mc") = Me.dtpOkres.Value.Month
                                                r("Instalacja") = r1(0)
                                                r("MPK") = m_Mpk
                                                r("CalCo") = m_Prod
                                                r("Wartosc") = m_Dbl(0, 0)
                                                r("Typ") = "A"
                                                m = dvK.Find(New Object() {r("Instalacja"), r("MPK"), r("CalCo")})
                                                If m.Equals(-1) Then
                                                    dtKos.Rows.Add(r)
                                                Else
                                                    dvK(m)("Wartosc") += m_Dbl(0, 0)
                                                End If
                                            Catch ex As Exception
                                                If Not Me.txtKomunikat.Text.Contains(ex.Message) Then _
                                                    Me.txtKomunikat.Text += vbCrLf + ex.Message
                                            End Try
                                        End If
                                    Next k
                                End If
                            Next j
                        Next p
                    End If
                    .Zwolnij()
                End With
Nastepny:
                Me.tspbMain.Value = i
                Application.DoEvents()
            Loop While i < dtK.Rows.Count
            ' Sprzedaż
            ' plik War_rrrrop.xls
            FillStatusBar(0, m_Action + " - sprzedaż...")
            Dim m_Wsk As Integer = 3 + Me.dtpOkres.Value.Month
            Dim m_Kol As Char
            ' Pozostałe
            ' Wydzial = Other
            ' Material z kol. 3
            ' Kwota       - wiersz materiału +1
            ' PIP         - wiersz materiału +2
            ' Transport   - wiersz materiału +3
            ' Overhead    - wiersz materiału +4
            ' EBitDA      - wiersz materiału +6
            Try
                With New Cl4Excel.GetFromXls(m_WB, "Sale_Other")
                    For j = 153 To 256 Step 8
                        .Obszar = "A" + j.ToString + ":O" + (j + 6).ToString
                        .Data2Array()
                        aDane = .Dane
                        If aDane.GetValue(1, 3) Is Nothing OrElse String.IsNullOrEmpty(aDane.GetValue(1, 3).ToString) Then GoTo NextOtherSale
                        For p = 0 To 7
                            Try
                                m_Dbl(0, p) = CDbl(aDane.GetValue(1, p + 2).ToString)
                            Catch ex As Exception
                                m_Dbl(0, p) = 0
                            End Try
                        Next p
                        r = dtSpr.NewRow
                        r("Miesiac") = Me.dtpOkres.Value.Month
                        r("Material") = aDane.GetValue(1, 3)
                        r("Wydzial") = "OR"
                        r("IDKlienta") = 0
                        r("GrKalk") = ""
                        r("GrDekretacji") = "01"
                        r("Kwota") = m_Dbl(0, 0)
                        r("PIP") = m_Dbl(0, 1)
                        r("Transport") = m_Dbl(0, 2)
                        r("Overhead") = m_Dbl(0, 3)
                        r("EBiTDA") = m_Dbl(0, 5)
                        m = dvS.Find(New Object() {r("Material"), r("Wydzial"), r("GrDekretacji")})
                        If m.Equals(-1) Then
                            dtSpr.Rows.Add(r)
                        Else
                            dvS(m)("Kwota") += m_Dbl(0, 0)
                            dvS(m)("PIP") += m_Dbl(0, 1)
                            dvS(m)("Transport") += m_Dbl(0, 2)
                            dvS(m)("Overhead") += m_Dbl(0, 3)
                            dvS(m)("EBiTDA") += m_Dbl(0, 5)
                        End If
NextOtherSale:
                    Next j
                    .Zwolnij()
                End With
            Catch ex As Exception
                If Not Me.txtKomunikat.Text.Contains(ex.Message) Then _
                    Me.txtKomunikat.Text += vbCrLf + ex.Message
            End Try
            ' wydziały produkcyjne
            ' Miesiac	Material Wydzial
            ' Waga 4 - 9
            ' Kwota 16 - 19
            ' PIP 40 - 43 - to jest koszt produkcji sprzedanej!!!
            ' Transport 64 - 67
            ' Other 76- 79
            ' Overhead 88 - 91
            ' Pozostale 100 - 103 
            ' EBITDA 156 - 159
            m_Kol = Chr(m_Wsk + 64)
            ' uzupełnij nazwę!!!
            da = New OleDbDataAdapter("SELECT DISTINCT ID, PlanOfSales, Wydzial FROM tblGrupyMat WHERE NOT PlanOfSales IS NULL AND NOT Wydzial IN ('MS', 'PS', 'MT')", Me.cns)
            dt = New DataTable
            da.Fill(dt)
            Try
                If dt.Rows.Count.Equals(0) Then Throw New ArgumentException("Brak określenia arkuszy sprzedaży w tabeli grup materiałowych")
                dvG = dt.DefaultView
                dvG.Sort = "PlanOfSales"
                Dim XE As New Cl4Excel.Excel(Nothing)
                For i = 0 To dt.Rows.Count - 1
                    r1 = dt.Rows(i)
                    With XE
                        .oSheet = CType(.oBook.Sheets(r1(1).ToString), XL._Worksheet)
                        For p = 0 To m_Dbl.GetUpperBound(1)
                            Select Case p
                                Case 0
                                    aDane = CType(CType(.oSheet.Range(m_Kol + "4:" + m_Kol + "7"), XL.Range).Value, Array)
                                Case 1
                                    aDane = CType(CType(.oSheet.Range(m_Kol + "16:" + m_Kol + "19"), XL.Range).Value, Array)
                                Case 2
                                    aDane = CType(CType(.oSheet.Range(m_Kol + "40:" + m_Kol + "43"), XL.Range).Value, Array)
                                Case 3
                                    aDane = CType(CType(.oSheet.Range(m_Kol + "64:" + m_Kol + "67"), XL.Range).Value, Array)
                                Case 4
                                    aDane = CType(CType(.oSheet.Range(m_Kol + "76:" + m_Kol + "79"), XL.Range).Value, Array)
                                Case 5
                                    aDane = CType(CType(.oSheet.Range(m_Kol + "88:" + m_Kol + "91"), XL.Range).Value, Array)
                                Case 6
                                    aDane = CType(CType(.oSheet.Range(m_Kol + "100:" + m_Kol + "103"), XL.Range).Value, Array)
                                Case 7
                                    aDane = CType(CType(.oSheet.Range(m_Kol + "156:" + m_Kol + "159"), XL.Range).Value, Array)
                            End Select
                            For k = 0 To 3
                                Try
                                    m_Dbl(k, p) = CDbl(aDane.GetValue(k + 1, 1).ToString)
                                Catch ex As Exception
                                    m_Dbl(k, p) = 0
                                End Try
                            Next k
                        Next p
                    End With
                    For k = 0 To 3
                        For p = 0 To m_Dbl.GetUpperBound(1)
                            If Not m_Dbl(k, p).Equals(0.0) Then GoTo ZapiszSprzedaz
                        Next p
                        GoTo PoZapisie
ZapiszSprzedaz:
                        r = dtSpr.NewRow
                        r("Miesiac") = Me.dtpOkres.Value.Month
                        r("Material") = r1(1)
                        r("Wydzial") = r1(2)
                        r("IDKlienta") = 0
                        m = dvG.Find(r("Material"))
                        If Not m.Equals(-1) Then r("GrMater") = dvG(m)("ID")
                        r("GrKalk") = ""
                        r("GrDekretacji") = "0" + (k + 1).ToString
                        r("Waga") = m_Dbl(k, 0)
                        r("Kwota") = m_Dbl(k, 1)
                        r("PIP") = m_Dbl(k, 2)
                        r("Transport") = m_Dbl(k, 3)
                        r("Other") = m_Dbl(k, 4)
                        r("Overhead") = m_Dbl(k, 5)
                        r("Pozostale") = m_Dbl(k, 6)
                        r("EBiTDA") = m_Dbl(k, 7)
                        m = dvS.Find(New Object() {r("Material"), r("Wydzial"), r("GrDekretacji")})
                        If m.Equals(-1) Then
                            dtSpr.Rows.Add(r)
                        Else
                            dvS(m)("Waga") += m_Dbl(k, 0)
                            dvS(m)("Kwota") += m_Dbl(k, 1)
                            dvS(m)("PIP") += m_Dbl(k, 2)
                            dvS(m)("Transport") += m_Dbl(k, 3)
                            dvS(m)("Other") += m_Dbl(k, 4)
                            dvS(m)("Overhead") += m_Dbl(k, 5)
                            dvS(m)("Pozostale") += m_Dbl(k, 6)
                            dvS(m)("EBiTDA") += m_Dbl(k, 7)
                        End If
PoZapisie:
                    Next k
                Next i
            Catch ex As Exception
                If Not Me.txtKomunikat.Text.Contains(ex.Message) Then _
                    Me.txtKomunikat.Text += vbCrLf + ex.Message
            End Try
            FillStatusBar(0, m_Action + " - zapis zmian...")
            dt = New DataTable
            dt = dtKos.GetChanges
            If Not dt Is Nothing Then
                If blnBudzet Then
                    da = DA_TI("tblBUZWPC", "Mc, Instalacja, MPK, CalCo")
                Else
                    da = DA_TI("tblPLZWPC", "Mc, Instalacja, MPK, CalCo")
                End If
                da.Update(dt)
            End If
            dt = New DataTable
            dt = dtMed.GetChanges
            If Not dt Is Nothing Then
                If blnBudzet Then
                    da = DA_TI("tblBUMedia", "Mc, MPK, Media, JM")
                Else
                    da = DA_TI("tblPLMedia", "Mc, MPK, Media, JM")
                End If
                da.Update(dt)
            End If
            dt = New DataTable
            dt = dtSpr.GetChanges
            If Not dt Is Nothing Then
                If blnBudzet Then
                    da = DA_TI("tblBUSprzedaz", "Miesiac, Material, Wydzial, GrDekretacji")
                Else
                    da = DA_TI("tblPLSprzedaz", "Miesiac, Material, Wydzial, GrDekretacji")
                End If
                da.Update(dt)
            End If
            dt = New DataTable
            dt = dtZat.GetChanges
            If Not dt Is Nothing Then
                If blnBudzet Then
                    da = DA_TI("tblBUZatrudnienie", "Mc, MPK, Grupa")
                Else
                    da = DA_TI("tblPLZatrudnienie", "Mc, MPK, Grupa")
                End If
                da.Update(dt)
            End If
            Using New CM
                If Me.txtKomunikat.Text.Length.Equals(0) Then _
                   MessageBox.Show("Operacja zakończona pomyślnie", m_Action, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End Using
        Finally
            ClearStatus()
            da = Nothing
            dt = Nothing
            dtA = Nothing
            dtK = Nothing
            dtMed = Nothing
            dtSpr = Nothing
            dtZat = Nothing
            dtKos = Nothing
            dvM = Nothing
            dvK = Nothing
            dvZ = Nothing
            dvS = Nothing
            With Me.txtKomunikat
                If Not .Text.Length.Equals(0) Then
                    .Text = "Aktualizacja " + IIf(blnBudzet, "budżet", "plan") + "u:" + .Text
                    KomunikatShow()
                End If
            End With
        End Try
    End Sub

    Private Function ZamienCalco(ByVal drInst As DataRow, ByVal dtCalCo As DataTable, ByVal CalCo As String) As String

        Dim m_New As String = ""
        Dim i As Integer
        Dim dv As DataView
        Dim m_Key As Integer
        dv = dtCalCo.DefaultView
        Try
            With dv
                .Sort = "ID"
                i = .Find(CalCo)
                If i.Equals(-1) Then Throw New ArgumentException
                m_Key = dv(i)(6)
                .RowFilter = "NrWiersza = " + m_Key.ToString
                For Each v As DataRowView In dv
                    If v(0).ToString.StartsWith(drInst(2).ToString) Then
                        m_New = v(0)
                        Exit Try
                    End If
                Next v
                .RowFilter = Nothing
                .RowFilter = "NrWiersza2 = " + m_Key.ToString
                For Each v As DataRowView In dv
                    If v(0).ToString.StartsWith(drInst(2).ToString) Then
                        m_New = v(0)
                        Exit Try
                    End If
                Next v
            End With
        Catch ex As Exception
        Finally
            dv.RowFilter = Nothing
            dv = Nothing
        End Try
        Return m_New
    End Function

#End Region ' Plan_i_Budzet

    Private Sub PobierzKursyWalut() Handles tsm_DPk.Click

        Dim _Rok As String = Me.dtpOkres.Value.Year.ToString
        Dim aVal As Object
        With New Cl4Excel.GetFromXls("W:\Costs\CELSA\Wykon" + _Rok + "\Rzecz" + _Rok + "\Poz_" + _Rok + "rz.xls", "Waluty", "D4:O11")
            .Data2Array() : aVal = .Dane : .Zwolnij()
        End With
        Dim _Mc As Integer = Me.dtpOkres.Value.Month
        _Rok = "0" & _Mc
        _Rok = _Rok.Substring(_Rok.Length - 2)
        Dim tr As OleDbTransaction = Nothing
        Dim cmd As New OleDbCommand
        Using cn As New OleDbConnection(Me.cns.ConnectionString)
            Try
                cn.Open()
                tr = cn.BeginTransaction
                With cmd
                    .Connection = cn
                    .Transaction = tr
                    Try
                        .CommandText = "INSERT INTO tblKursyWalut (MM_dd, EUR, USD, DKK, GBP) VALUES ('" + _Rok + "', " + CStr(aVal(1, _Mc)).Replace(",", ".") + ", " + CStr(aVal(3, _Mc)).Replace(",", ".") + ", " _
                                    + CStr(aVal(5, _Mc)).Replace(",", ".") + ", " + CStr(aVal(7, _Mc)).Replace(",", ".") + ")"
                        .ExecuteNonQuery()
                    Catch ex As Exception
                        .CommandText = "UPDATE tblKursyWalut SET EUR = " + CStr(aVal(1, _Mc)).Replace(",", ".") + ", USD = " + CStr(aVal(3, _Mc)).Replace(",", ".") + ", DKK = " + CStr(aVal(5, _Mc)).Replace(",", ".") _
                                    + ", GBP = " + CStr(aVal(7, _Mc)).Replace(",", ".") + " WHERE MM_dd = '" + _Rok + "'"
                        .ExecuteNonQuery()
                    End Try
                End With
                tr.Commit()
                Using New CM
                    MessageBox.Show("Dane zostały zaktualizowane", Me.tsm_DPk.Text.Replace("&", ""), MessageBoxButtons.OK, MessageBoxIcon.Information)
                End Using
            Catch ex As Exception
                tr.Rollback()
                Using New CM
                    MessageBox.Show(ex.Message, Me.tsm_DPk.Text.Replace("&", ""), MessageBoxButtons.OK, MessageBoxIcon.Error)
                End Using
            End Try
        End Using
        tr = Nothing
        cmd = Nothing
    End Sub

#Region "Edycja i zapis tabel danych"

    Private Sub PokazTabele(ByVal sender As System.Object, ByVal e As System.EventArgs) _
                                    Handles _
                                            tsmtblGrupyKalk.Click, _
                                            tsmtblGrupyMat.Click, _
                                            tsmtblMagazyny.Click, _
                                            tsmtblMaterialy.Click, _
                                            tsmtblRodzajeMat.Click, _
                                            tsmtblKlienci.Click, _
                                            tsmtblSources.Click, _
                                            tsmtblCALCO.Click, _
                                            tsmtblDostawcy.Click, _
                                            tsmtblGrKoszt.Click, _
                                            tsmtblINEF.Click, _
                                            tsmtblInstalacje.Click, _
                                            tsmtblMPK.Click, _
                                            tsmtblRK.Click, _
                                            tsmtblGEn.Click, _
                                            tsmtblSprzedaz.Click, _
                                            tsmtblPLSprzedaz.Click, _
                                            tsmtblBUSprzedaz.Click, _
                                            tsmtblZWPC.Click, _
                                            tsmtblPLZWPC.Click, _
                                            tsmtblBUZWPC.Click, _
                                            tsmtblMedia.Click, _
                                            tsmtblPLMedia.Click, _
                                            tsmtblBUMedia.Click, _
                                            tsmtblZapasy.Click, _
                                            tsmtblZWKZlecenia.Click, _
                                            tsmtblWIP.Click, _
                                            tsmtblZatrudnienie.Click, _
                                            tsmtblPLZatrudnienie.Click, _
                                            tsmtblBUZatrudnienie.Click, _
                                            tsmtblKursyWalut.Click, _
                                            tsmtblFormuly.Click, _
                                            tsmtblBlokady.Click, _
                                            tsmtblAmortyzacja.Click, _
                                            tsmtblNaleznosci.Click, _
                                            tsmtblTransport.Click

        Dim ctrl As ToolStripMenuItem = DirectCast(sender, ToolStripMenuItem)
        Dim m_Dane As New ClDane.frmDane
        Dim da As New OleDbDataAdapter
        Dim dt As New DataTable
        Dim m_Filtr As String = ""
        Dim i As Integer
        Dim dtF As New DataTable
        Const CO_OKRESOWE As String = "tblZWKZlecenia_tblPLNaleznosci_tblBUNaleznosci_tblNaleznosci_" _
                                    + "tblSprzedaz_tblPLSprzedaz_tblBUSprzedaz_" _
                                    + "tblWIP_tblPLWIP_tblBUWIP_" _
                                    + "tblZapasy_tblPLZapasy_tblBUZapasy_" _
                                    + "tblZWPC_tblPLZWPC_tblBUZWPC_" _
                                    + "tblMedia_tblPLMedia_tblBUMedia_" _
                                    + "tblZatrudnienie_tblPLZatrudnienie_tblBUZatrudnienie_" _
                                    + "tblBlokady_tblAmortyzacja_tblTransport"
        Try
            With dtF.Columns
                .Add("Column")
                .Add("Format")
                .Add("Alignment")
            End With
            WybranaTabela = ctrl.Name.Substring(3)
            With Me
                .tsslMain.Text = "Odczyt danych " + WybranaTabela + "..."
                .Cursor = Cursors.WaitCursor
            End With
            Application.DoEvents()
            With m_Dane
                Select Case WybranaTabela
                    Case "tblAmortyzacja"
                        da = DA_TI(WybranaTabela, "Wydzial, Mc")
                        m_Filtr = "Mc = " + Me.dtpOkres.Value.Month.ToString
                        .SizeOfScreen = My.Settings.scrAmortyzacja
                        dtF.Rows.Add(New Object() {"Stawka", "##0.0000", CType(DataGridViewContentAlignment.TopRight, Integer)})
                    Case "tblBlokady"
                        da = DA_TI(WybranaTabela, "Miesiac, Obiekt")
                        m_Filtr = "Miesiac = " + Me.dtpOkres.Value.Month.ToString
                        .SizeOfScreen = My.Settings.scrBlokady
                        .ColToFroze = 1
                    Case "tblCALCO"
                        da = DA_TI(WybranaTabela, "ID")
                        .SizeOfScreen = My.Settings.scrCALCO
                        .ColToFroze = 0
                        .FreshFromXls = True
                    Case "tblDostawcy"
                        da = DA_TI(WybranaTabela, "ID")
                        .SizeOfScreen = My.Settings.scrDostawcy
                        .ColToFroze = 0
                        .FreshFromXls = True
                    Case "tblFormuly"
                        da = DA_TI(WybranaTabela, "ID")
                        .SizeOfScreen = My.Settings.scrFormuly
                    Case "tblGEn"
                        da = DA_TI(WybranaTabela, "ID")
                        .SizeOfScreen = My.Settings.scrGEn
                        .FreshFromXls = True
                    Case "tblGrKoszt"
                        da = DA_TI(WybranaTabela, "ID")
                        .SizeOfScreen = My.Settings.scrGrKoszt
                        .ColToFroze = 0
                        .FreshFromXls = True
                    Case "tblGrupyKalk"
                        da = DA_TI(WybranaTabela, "ID")
                        .SizeOfScreen = My.Settings.scrGrKalk
                        .tsbCopy.Enabled = True
                        .ColToFroze = 0
                        .FreshFromXls = True
                    Case "tblGrupyMat"
                        da = DA_TI(WybranaTabela, "ID")
                        .SizeOfScreen = My.Settings.scrGrMat
                        .tsbCopy.Enabled = True
                        .ColToFroze = 0
                        .FreshFromXls = True
                    Case "tblINEF"
                        da = DA_TI(WybranaTabela, "ID")
                        .SizeOfScreen = My.Settings.scrINEF
                        .ColToFroze = 0
                        .FreshFromXls = True
                    Case "tblInstalacje"
                        da = DA_TI(WybranaTabela, "ID")
                        .FreshFromXls = True
                    Case "tblKlienci"
                        da = DA_TI(WybranaTabela, "ID")
                        .SizeOfScreen = My.Settings.scrKlienci
                        .ColToFroze = 0
                        .FreshFromXls = True
                    Case "tblKursyWalut"
                        da = DA_TI(WybranaTabela, "MM_dd DESC")
                        .SizeOfScreen = My.Settings.scrKursyWalut
                        With dtF.Rows
                            .Add(New Object() {"EUR", "##0.0000", CType(DataGridViewContentAlignment.TopRight, Integer)})
                            .Add(New Object() {"USD", "##0.0000", CType(DataGridViewContentAlignment.TopRight, Integer)})
                            .Add(New Object() {"GBP", "##0.0000", CType(DataGridViewContentAlignment.TopRight, Integer)})
                            .Add(New Object() {"DKK", "##0.0000", CType(DataGridViewContentAlignment.TopRight, Integer)})
                        End With
                    Case "tblMagazyny"
                        da = DA_TI(WybranaTabela, "ID")
                        .SizeOfScreen = My.Settings.scrMagazyny
                        .tsbCopy.Enabled = True
                        .ColToFroze = 0
                        .FreshFromXls = True
                    Case "tblMaterialy"
                        da = DA_TI(WybranaTabela, "ID")
                        .SizeOfScreen = My.Settings.scrMaterialy
                        .tsbCopy.Enabled = True
                        .ColToFroze = 0
                        .FreshFromXls = True
                    Case "tblMedia", "tblPLMedia", "tblBUMedia"
                        da = DA_TI(WybranaTabela, "Mc DESC, MPK, Media, JM")
                        m_Filtr = "Mc = " + Me.dtpOkres.Value.Month.ToString
                        .SizeOfScreen = My.Settings.scrMedia
                        With dtF.Rows
                            .Add(New Object() {"Ilosc", "# ##0.000", CType(DataGridViewContentAlignment.TopRight, Integer)})
                            .Add(New Object() {"Wartosc", "# ### ##0.00", CType(DataGridViewContentAlignment.TopRight, Integer)})
                        End With
                        .FreshFromXls = True
                    Case "tblMPK"
                        da = DA_TI(WybranaTabela, "ID")
                        .SizeOfScreen = My.Settings.scrMPK
                        .ColToFroze = 0
                        .FreshFromXls = True
                    Case "tblNaleznosci", "tblPLNaleznosci", "tblBUNaleznosci"
                        da = DA_TI(WybranaTabela, "Miesiac DESC, IDKlienta")
                        m_Filtr = "Miesiac = " + Me.dtpOkres.Value.Month.ToString
                        .SizeOfScreen = My.Settings.scrNaleznosci
                        dtF.Rows.Add(New Object() {"Kwota", "# ### ##0.00", CType(DataGridViewContentAlignment.TopRight, Integer)})
                        .ColToFroze = 0
                    Case "tblRK"
                        da = DA_TI(WybranaTabela, "ID")
                        .SizeOfScreen = My.Settings.scrRK
                        .ColToFroze = 0
                        .FreshFromXls = True
                    Case "tblRodzajeMat"
                        da = DA_TI(WybranaTabela, "ID")
                        .SizeOfScreen = My.Settings.scrRodzajeMat
                        .ColToFroze = 0
                        .FreshFromXls = True
                    Case "tblSources"
                        da = DA_TI("tblSources", "Source, NrKol")
                        .SizeOfScreen = My.Settings.scrSources
                        .ColToFroze = 0
                        .FreshFromXls = True
                    Case "tblSprzedaz", "tblPLSprzedaz", "tblBUSprzedaz"
                        da = DA_TI(WybranaTabela, "Miesiac DESC, Material, IDKlienta")
                        m_Filtr = "Miesiac = " + Me.dtpOkres.Value.Month.ToString
                        .SizeOfScreen = My.Settings.scrSprzedaz
                        With dtF.Rows
                            .Add(New Object() {"Waga", "# ##0.000", CType(DataGridViewContentAlignment.TopRight, Integer)})
                            .Add(New Object() {"Kwota", "# ### ##0.00", CType(DataGridViewContentAlignment.TopRight, Integer)})
                            .Add(New Object() {"Rabat", "# ### ##0.00", CType(DataGridViewContentAlignment.TopRight, Integer)})
                            .Add(New Object() {"PIP", "# ### ##0.00", CType(DataGridViewContentAlignment.TopRight, Integer)})
                            .Add(New Object() {"Transport", "# ### ##0.00", CType(DataGridViewContentAlignment.TopRight, Integer)})
                            .Add(New Object() {"Other", "# ### ##0.00", CType(DataGridViewContentAlignment.TopRight, Integer)})
                            .Add(New Object() {"Overhead", "# ### ##0.00", CType(DataGridViewContentAlignment.TopRight, Integer)})
                            .Add(New Object() {"Pozostale", "# ### ##0.00", CType(DataGridViewContentAlignment.TopRight, Integer)})
                            .Add(New Object() {"PIP_SAP", "# ### ##0.00", CType(DataGridViewContentAlignment.TopRight, Integer)})
                            .Add(New Object() {"EBiTDA", "# ### ##0.00", CType(DataGridViewContentAlignment.TopRight, Integer)})
                        End With
                        .ColToFroze = 1
                    Case "tblTransport"
                        da = DA_TI(WybranaTabela, "Miesiac DESC, GrMater, IDKlienta, IDDostawcy")
                        m_Filtr = "Miesiac = " + Me.dtpOkres.Value.Month.ToString
                        .SizeOfScreen = My.Settings.scrTransport
                        With dtF.Rows
                            .Add(New Object() {"Waga", "# ##0.000", CType(DataGridViewContentAlignment.TopRight, Integer)})
                            .Add(New Object() {"Kwota", "# ### ##0.00", CType(DataGridViewContentAlignment.TopRight, Integer)})
                        End With
                        .ColToFroze = 1
                    Case "tblWIP", "tblPLWIP", "tblBUWIP"
                        da = DA_TI(WybranaTabela, "Okres DESC, NrZlecenia, Partia")
                        m_Filtr = "Okres = '" + Me.dtpOkres.Value.ToShortDateString.Substring(0, 7) + "'"
                        .SizeOfScreen = My.Settings.scrWIP
                        With dtF.Rows
                            .Add(New Object() {"Waga", "# ##0.000", CType(DataGridViewContentAlignment.TopRight, Integer)})
                            .Add(New Object() {"Kwota", "# ##0.00", CType(DataGridViewContentAlignment.TopRight, Integer)})
                            .Add(New Object() {"WagaOdkuwki", "# ##0.000", CType(DataGridViewContentAlignment.TopRight, Integer)})
                            .Add(New Object() {"WagaKoncowa", "# ##0.000", CType(DataGridViewContentAlignment.TopRight, Integer)})
                        End With
                        .ColToFroze = 1
                    Case "tblZapasy", "tblPLZapasy", "tblBUZapasy"
                        da = DA_TI(WybranaTabela, "Miesiac DESC, Material")
                        m_Filtr = "Miesiac = " + Me.dtpOkres.Value.Month.ToString
                        .SizeOfScreen = My.Settings.scrZapasy
                        With dtF.Rows
                            .Add(New Object() {"Ilosc", "# ##0", CType(DataGridViewContentAlignment.TopRight, Integer)})
                            .Add(New Object() {"Waga", "# ##0.000", CType(DataGridViewContentAlignment.TopRight, Integer)})
                            .Add(New Object() {"Kwota", "# ##0.00", CType(DataGridViewContentAlignment.TopRight, Integer)})
                        End With
                        .ColToFroze = 1
                    Case "tblZatrudnienie", "tblPLZatrudnienie", "tblBUZatrudnienie"
                        da = DA_TI(WybranaTabela, "Mc DESC, MPK")
                        m_Filtr = "Mc = " + Me.dtpOkres.Value.Month.ToString
                        .SizeOfScreen = My.Settings.scrZatrudnienie
                        dtF.Rows.Add(New Object() {"Ilosc", "# ##0", CType(DataGridViewContentAlignment.TopRight, Integer)})
                    Case "tblZWKZlecenia"
                        da = DA_TI(WybranaTabela, "Mc DESC, NrZlecenia, RK, MPK")
                        m_Filtr = "Mc = " + Me.dtpOkres.Value.Month.ToString
                        .SizeOfScreen = My.Settings.scrZWKZlecenia
                        With dtF.Rows
                            .Add(New Object() {"Waga", "# ##0", CType(DataGridViewContentAlignment.TopRight, Integer)})
                            .Add(New Object() {"IloscRzecz", "# ##0.000", CType(DataGridViewContentAlignment.TopRight, Integer)})
                            .Add(New Object() {"WartoscRzecz", "# ##0.00", CType(DataGridViewContentAlignment.TopRight, Integer)})
                            .Add(New Object() {"TnProdukcjiReal", "# ##0.000", CType(DataGridViewContentAlignment.TopRight, Integer)})
                            .Add(New Object() {"IloscPlan", "# ##0.000", CType(DataGridViewContentAlignment.TopRight, Integer)})
                            .Add(New Object() {"WartoscPlan", "# ##0.00", CType(DataGridViewContentAlignment.TopRight, Integer)})
                            .Add(New Object() {"TnProdukcjiPlan", "# ##0.000", CType(DataGridViewContentAlignment.TopRight, Integer)})
                        End With
                        .ColToFroze = 1
                    Case "tblZWPC", "tblPLZWPC", "tblBUZWPC"
                        da = DA_TI(WybranaTabela, "Mc DESC, Instalacja, MPK, CalCo, RK, Material, GrKoszt, Dostawca, JM")
                        m_Filtr = "Mc = " + Me.dtpOkres.Value.Month.ToString
                        .SizeOfScreen = My.Settings.scrZWPC
                        With dtF.Rows
                            .Add(New Object() {"Ilosc", "# ##0.000", CType(DataGridViewContentAlignment.TopRight, Integer)})
                            .Add(New Object() {"Wartosc", "# ##0.00", CType(DataGridViewContentAlignment.TopRight, Integer)})
                        End With
                End Select
                .NazwaTabeli = WybranaTabela
                If CO_OKRESOWE.Contains(.NazwaTabeli) Then
                    .Zapytania = True
                    .BezEdycji = BlokadaAktualizacji(WybranaTabela, False)
                End If
                If FiltrujOkres Then
                    If Not String.IsNullOrEmpty(m_Filtr) Then
                        With da.SelectCommand
                            i = .CommandText.IndexOf("ORDER BY")
                            If i > 0 Then
                                .CommandText = .CommandText.Substring(0, i) + " WHERE " + m_Filtr + " " + .CommandText.Substring(i - 1)
                            Else
                                .CommandText += " WHERE " + m_Filtr
                            End If
                        End With
                    End If
                Else
                    .FiltrOkresu = m_Filtr
                End If
                .Okres = Me.dtpOkres.Value.ToShortDateString.Substring(0, 7)
                .Polaczenie = Me.cns.ConnectionString
                .PolecenieSelect = da.SelectCommand.CommandText.ToString
                .Formatowanie = dtF
                .ResizeColumnsOff = True
                HideScreen()
                .ShowDialog()
                With My.Settings
                    Select Case WybranaTabela
                        Case "tblAmortyzacja"
                            .scrAmortyzacja = m_Dane.SizeOfScreen
                        Case "tblBlokady"
                            .scrBlokady = m_Dane.SizeOfScreen
                        Case "tblCALCO"
                            .scrCALCO = m_Dane.SizeOfScreen
                        Case "tblDostawcy"
                            .scrDostawcy = m_Dane.SizeOfScreen
                        Case "tblFormuly"
                            .scrFormuly = m_Dane.SizeOfScreen
                        Case "tblGEn"
                            .scrGEn = m_Dane.SizeOfScreen
                        Case "tblGrKoszt"
                            .scrGrKoszt = m_Dane.SizeOfScreen
                        Case "tblGrupyKalk"
                            .scrGrKalk = m_Dane.SizeOfScreen
                        Case "tblGrupyMat"
                            .scrGrMat = m_Dane.SizeOfScreen
                        Case "tblINEF"
                            .scrINEF = m_Dane.SizeOfScreen
                        Case "tblKlienci"
                            .scrKlienci = m_Dane.SizeOfScreen
                        Case "tblKursyWalut"
                            .scrKursyWalut = m_Dane.SizeOfScreen
                        Case "tblMagazyny"
                            .scrMagazyny = m_Dane.SizeOfScreen
                        Case "tblMaterialy"
                            .scrMaterialy = m_Dane.SizeOfScreen
                        Case "tblMedia", "tblPLMedia", "tblBUMedia"
                            .scrMedia = m_Dane.SizeOfScreen
                        Case "tblMPK"
                            .scrMPK = m_Dane.SizeOfScreen
                        Case "tblNaleznosci", "tblPLNaleznosci", "tblBUNaleznosci"
                            .scrNaleznosci = m_Dane.SizeOfScreen
                        Case "tblRK"
                            .scrRK = m_Dane.SizeOfScreen
                        Case "tblRodzajeMat"
                            .scrRodzajeMat = m_Dane.SizeOfScreen
                        Case "tblSources"
                            .scrSources = m_Dane.SizeOfScreen
                        Case "tblSprzedaz", "tblPLSprzedaz", "tblBUSprzedaz"
                            .scrSprzedaz = m_Dane.SizeOfScreen
                        Case "tblTransport"
                            .scrSprzedaz = m_Dane.SizeOfScreen
                        Case "tblWIP", "tblPLWIP", "tblBUWIP"
                            .scrWIP = m_Dane.SizeOfScreen
                        Case "tblZapasy", "tblPLZapasy", "tblBUZapasy"
                            .scrZapasy = m_Dane.SizeOfScreen
                        Case "tblZatrudnienie", "tblPLZatrudnienie", "tblBUZatrudnienie"
                            .scrZatrudnienie = m_Dane.SizeOfScreen
                        Case "tblZWKZlecenia"
                            .scrZWKZlecenia = m_Dane.SizeOfScreen
                        Case "tblZWPC", "tblPLZWPC", "tblBUZWPC"
                            .scrZWPC = m_Dane.SizeOfScreen
                    End Select
                    .Save()
                End With
                HideScreen(False)
                If .Zapisane.Equals(0) Then Exit Try
                dt = New DataTable
                dt = .Dane.GetChanges
                If dt Is Nothing OrElse dt.Rows.Count.Equals(0) Then Exit Try
                If BlokadaAktualizacji(WybranaTabela, False) Then Exit Try
                Try
                    If .FreshFromXls Then UzupelnienieRocznych(dt, da.SelectCommand.CommandText)
                    da.Update(dt)
                Catch ex As Exception
                    Using New CM
                        MessageBox.Show(ex.Message, WybranaTabela, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    End Using
                End Try
            End With
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message, "Podgląd tabeli [" + WybranaTabela + "]", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Using
        Finally
            dt = Nothing
            m_Dane.Dispose()
            m_Dane = Nothing
            ClearStatus()
        End Try
    End Sub

    Private Sub FiltrOkresu_Click() Handles tsm_Df.Click

        FiltrujOkres = Not FiltrujOkres
        Using New CM
            If FiltrujOkres Then
                MessageBox.Show("Dostępne zapisy dla aktywnego okresu", "Dane w tabeli", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                MessageBox.Show("Wszystkie zapisy dostępne", "Pokaż tabelę", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        End Using
    End Sub

    Private Sub UzupelnienieRocznych(ByVal dtChange As DataTable, ByVal m_Select As String)

        ' Uzupełnienie danych stałych rok wcześniej i do ostatniego istniejącego
        Dim m_Year As String = (CInt(Me.Okres.Substring(0, 4)) - 1).ToString
        Dim m_File As String
        Dim i As Integer
        Dim obj(0 To dtChange.Columns.Count - 1) As Object
        Dim m_cns As New OleDbConnection
        Dim dtG As New DataTable
        Dim dtC As New DataTable
        Dim bs As New BindingSource
        Dim daC As OleDbDataAdapter = New OleDbDataAdapter
        Dim m_Ado As New ClADO.ADOGet
        Dim cb As OleDbCommandBuilder
        Using New CM
            If MessageBox.Show("Uzupełnić dane z poprzedniego roku i lat następnych?", "Aktualizacja danych stałych", MessageBoxButtons.YesNo, MessageBoxIcon.Question).Equals(Windows.Forms.DialogResult.No) Then Exit Sub
        End Using
        Try
            While True
                If m_Year.Equals(Me.dtpOkres.Value.Year.ToString) Then GoTo Nastepny
                m_File = My.Settings.Baza.Replace(Me.dtpOkres.Value.Year.ToString, m_Year)
                Try
                    With m_Ado
                        .Baza = m_File
                        .Folder = My.Settings.Sciezka
                        .Haslo = My.Settings.Myk
                        If Not .OpenData(True) Then Throw New ArgumentException
                        m_cns.ConnectionString = .CN.ConnectionString
                    End With
                    daC = New OleDbDataAdapter
                    daC.SelectCommand = New OleDbCommand(m_Select, m_cns)
                    cb = New OleDbCommandBuilder(daC)
                    daC.InsertCommand = cb.GetInsertCommand
                    daC.UpdateCommand = cb.GetUpdateCommand
                    daC.DeleteCommand = cb.GetDeleteCommand
                    dtG = New DataTable
                    daC.Fill(dtG)
                    bs.DataSource = dtG
                    For Each r As DataRow In dtChange.Rows
                        If r.RowState.Equals(DataRowState.Added) OrElse r.RowState.Equals(DataRowState.Modified) Then
                            With dtG.Columns(0)
                                i = bs.Find(.ColumnName, r(.ColumnName))
                            End With
                            obj = r.ItemArray
                            If i.Equals(-1) Then
                                dtG.Rows.Add(obj)
                            Else
                                dtG(i).ItemArray = obj
                            End If
                        ElseIf r.RowState.Equals(DataRowState.Deleted) Then
                            With dtG.Columns(0)
                                i = bs.Find(.ColumnName, r(.ColumnName, DataRowVersion.Original))
                            End With
                            If Not i.Equals(-1) Then dtG(i).Delete()
                        End If
                    Next r
                    dtC = dtG.GetChanges
                    If Not dtC Is Nothing Then daC.Update(dtC)
                Catch ex As Exception
                    Using New CM
                        MessageBox.Show(ex.Message, m_File, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    End Using
                End Try
Nastepny:
                m_Year = (CInt(m_Year) + 1).ToString
                If CInt(m_Year) > Now.Year Then Exit While
            End While
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message, "Przenoszenie danych stałych", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Using
        Finally
            bs = Nothing
            dtC = Nothing
            dtG = Nothing
            daC = Nothing
            m_Ado = Nothing
            cb = Nothing
        End Try
    End Sub

    Function DA_TI(ByVal Tabela As String, _
                   Optional ByVal Indeks As String = "", _
                   Optional ByVal Warunek As String = "") As OleDbDataAdapter

        Dim da As OleDbDataAdapter = New OleDbDataAdapter
        Dim cb As OleDbCommandBuilder
        Dim txt As String = "SELECT * FROM " + Tabela
        If Not String.IsNullOrEmpty(Warunek) Then txt += " WHERE " + Warunek
        If Not String.IsNullOrEmpty(Indeks) Then txt += " ORDER BY " + Indeks
        da.SelectCommand = New OleDbCommand(txt, Me.cns)
        cb = New OleDbCommandBuilder(da)
        da.InsertCommand = cb.GetInsertCommand
        da.UpdateCommand = cb.GetUpdateCommand
        da.DeleteCommand = cb.GetDeleteCommand
        cb = Nothing
        Return da
    End Function

#End Region ' Obsługa tabel danych

    Private Sub TworzenieBazyRocznej() Handles tsmTworzenieDB.Click

        Dim cmd As New OleDbCommand
        Dim tr As OleDbTransaction = Nothing
        Dim m_Plik As String
        Dim m_New As String = (CInt(My.Settings.Okres.Substring(0, 4)) + 1).ToString
        Const CO_ACTION As String = "Tworzenie pliku rocznego "
        Try
            With My.Settings
                If Not My.Computer.FileSystem.FileExists(.Sciezka + .Baza) Then
                    Throw New ArgumentException("Brak dostępu do pliku [" + .Sciezka + .Baza + "]")
                End If
                m_Plik = .Baza.Replace(.Okres.Substring(0, 4), m_New)
            End With
            With My.Settings
                ' kopiowanie
                If My.Computer.FileSystem.FileExists(.Sciezka + m_Plik) Then
                    Throw New ArgumentException("Plik [" + m_Plik + "] już istnieje!!!")
                Else
                    Using New CM
                        If MessageBox.Show("Rzeczywiście chcesz utworzyć plik [" + m_Plik + "]?", CO_ACTION, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2).Equals(Windows.Forms.DialogResult.No) Then Return
                    End Using
                    Me.tsslMain.Text = CO_ACTION
                    Application.DoEvents()
                    My.Computer.FileSystem.CopyFile(.Sciezka + .Baza, .Sciezka + m_Plik)
                End If
                .Baza = m_Plik
                .Okres = .Okres.Replace(.Okres.Substring(0, 4), m_New)
                .Save()
            End With
            SetPolaczenie()
            ' usuwanie zapisów z tabel
            Using cn As New OleDbConnection(Me.cns.ConnectionString)
                Try
                    cn.Open()
                    tr = cn.BeginTransaction
                    With cmd
                        .Connection = cn
                        .Transaction = tr
                        .CommandText = "DELETE FROM tblAmortyzacja"
                        .ExecuteNonQuery()
                        .CommandText = "UPDATE tblBlokady SET Zablokowane = FALSE, Pobrane = NULL"
                        .ExecuteNonQuery()
                        .CommandText = "DELETE FROM tblBUMedia"
                        .ExecuteNonQuery()
                        .CommandText = "DELETE FROM tblBUNaleznosci"
                        .ExecuteNonQuery()
                        .CommandText = "DELETE FROM tblBUSprzedaz"
                        .ExecuteNonQuery()
                        .CommandText = "DELETE FROM tblBUWIP"
                        .ExecuteNonQuery()
                        .CommandText = "DELETE FROM tblBUZapasy"
                        .ExecuteNonQuery()
                        .CommandText = "DELETE FROM tblBUZatrudnienie"
                        .ExecuteNonQuery()
                        .CommandText = "DELETE FROM tblBUZWPC"
                        .ExecuteNonQuery()
                        .CommandText = "DELETE FROM tblMedia"
                        .ExecuteNonQuery()
                        .CommandText = "DELETE FROM tblNaleznosci"
                        .ExecuteNonQuery()
                        .CommandText = "DELETE FROM tblPLMedia"
                        .ExecuteNonQuery()
                        .CommandText = "DELETE FROM tblPLNaleznosci"
                        .ExecuteNonQuery()
                        .CommandText = "DELETE FROM tblPLSprzedaz"
                        .ExecuteNonQuery()
                        .CommandText = "DELETE FROM tblPLWIP"
                        .ExecuteNonQuery()
                        .CommandText = "DELETE FROM tblPLZapasy"
                        .ExecuteNonQuery()
                        .CommandText = "DELETE FROM tblPLZatrudnienie"
                        .ExecuteNonQuery()
                        .CommandText = "DELETE FROM tblPLZWPC"
                        .ExecuteNonQuery()
                        .CommandText = "DELETE FROM tblSprzedaz"
                        .ExecuteNonQuery()
                        .CommandText = "DELETE FROM tblTransport"
                        .ExecuteNonQuery()
                        .CommandText = "DELETE FROM tblWIP"
                        .ExecuteNonQuery()
                        .CommandText = "DELETE FROM tblZapasy"
                        .ExecuteNonQuery()
                        .CommandText = "DELETE FROM tblZatrudnienie"
                        .ExecuteNonQuery()
                        .CommandText = "DELETE FROM tblZWKZlecenia"
                        .ExecuteNonQuery()
                        .CommandText = "DELETE FROM tblZWPC"
                        .ExecuteNonQuery()
                    End With
                    tr.Commit()
                    Using New CM
                        MessageBox.Show(CO_ACTION + m_New + " zakończone pomyślnie", "Baza Danych CHO", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End Using
                Catch ex As Exception
                    tr.Rollback()
                    Throw New ArgumentException(ex.Message + vbCrLf + ex.StackTrace)
                End Try
            End Using
            DBKompaktuj()
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message, CO_ACTION + m_New, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
        Finally
            cmd = Nothing
            tr = Nothing
            ClearStatus()
        End Try
        KoniecProgramu()
    End Sub

    Private Sub DBKompaktuj() Handles tsm_Dm.Click

        Dim c As New ClADO.CompareMdb
        With Me
            .Cursor = Cursors.WaitCursor
            .tsslMain.Text = "Kompaktowanie bazy danych " + Okres.Substring(0, 4) + " roku"
        End With
        Application.DoEvents()
        With My.Settings
            c.CompactMdb(Path2Db:=.Sciezka, _
                         DbFile:=.Baza, _
                         Dostep:=.Myk, _
                         Path2Arch:="C:\CHO_DataBase\Archiwum\")
        End With
        ClearStatus()
    End Sub

    Private Sub BlokujOkres() Handles tsm_DB.Click

        Dim dt As New DataTable
        Dim tr As OleDbTransaction = Nothing
        Dim cmd As New OleDbCommand
        Const CO_ACTION As String = "Blokada okresu"
        Using New CM
            If MessageBox.Show("Zablokować procesy odświeżania danych dla okresu [" + Me.dtpOkres.Text.Trim + "]?", CO_ACTION, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2).Equals(Windows.Forms.DialogResult.No) Then Return
        End Using
        Using cn As New OleDbConnection(Me.cns.ConnectionString)
            Try
                cn.Open()
                tr = cn.BeginTransaction
                With cmd
                    .Connection = cn
                    .Transaction = tr
                    .CommandText = "SELECT DISTINCT Obiekt, Opis FROM tblBlokady WHERE NOT Opis IS NULL"
                    dt.Load(.ExecuteReader)
                    For Each r In dt.Rows
                        Try
                            .CommandText = "INSERT INTO tblBlokady (Miesiac, Obiekt, Opis) VALUES (" + Me.dtpOkres.Value.Month.ToString + ", '" + r("Obiekt") + "', '" + r("Opis") + "')"
                            .ExecuteNonQuery()
                        Catch ex As Exception
                            .CommandText = "UPDATE tblBlokady SET Opis = '" + r("Opis") + "' WHERE Obiekt = '" + r("Obiekt") + "'"
                            .ExecuteNonQuery()
                        End Try
                    Next r
                    .CommandText = "UPDATE tblBlokady SET Zablokowane = TRUE WHERE Miesiac <= " + Me.dtpOkres.Value.Month.ToString
                    .ExecuteNonQuery()
                End With
                tr.Commit()
                Using New CM
                    MessageBox.Show("Operacja zakończona pomyślnie", CO_ACTION, MessageBoxButtons.OK, MessageBoxIcon.Information)
                End Using
            Catch ex As Exception
                tr.Rollback()
                Using New CM
                    MessageBox.Show(ex.Message, CO_ACTION, MessageBoxButtons.OK, MessageBoxIcon.Error)
                End Using
            End Try
        End Using
        tr = Nothing
        cmd = Nothing
        dt = Nothing
    End Sub

    Sub RZSprzedazVsZS(ByVal Rok As String, ByVal Mc As Integer, ByRef Kontrol As Integer)

        ' Kontrola prawidłowości danych z zeszytami sprzedaży
        ' wiersz 33, col. 3+m-c - sprzedaż
        ' wiersz 117, col. 3+m-c - koszty
        Dim _Path As String = My.Settings.ToSales
        Dim m_Key As String = ""
        Dim m_Txt As String
        Dim i As Integer
        Dim oVal As Object
        Dim m_Kwota, _dbl As Double
        Dim dv As DataView
        Dim dtT As New DataTable
        Dim dt As New DataTable
        Dim da As OleDbDataAdapter = New OleDbDataAdapter("SELECT Wydzial, SUM(Kwota), SUM(Rabat), SUM(PIP), SUM(Transport), SUM(Other), SUM(Overhead), SUM(Pozostale) " _
                                + "FROM tblSprzedaz WHERE Miesiac = " + Mc.ToString _
                                + " GROUP BY Wydzial ORDER BY Wydzial", Me.cns)
        FillStatusBar(7, "Kontrola pobranych danych z zeszytami sprzedaży...")
        da.Fill(dt)
        dv = dt.DefaultView
        dv.Sort = "Wydzial"
        Me.txtKomunikat.Clear()
        Dim _Baza As String = "", _Ext As String = ".xls"
        _Path = CP.GetSourcePath(_Path + "ZkSt_" + Rok + "sprz", _Ext)
        If String.IsNullOrEmpty(_Path) Then Return
        With My.Settings
            If Not .ToSales.Equals(_Path) Then
                .ToSales = _Path
                .Save()
            End If
        End With
        For i = 1 To 7
Poczatek:
            Select Case i
                Case 1
                    _Baza = "ZkSt_"
                    m_Key = "MS"
                Case 2
                    _Baza = "ZkPras_"
                    m_Key = "PS"
                Case 3
                    _Baza = "ZkWom_"
                    m_Key = "MT"
                Case 4
                    _Baza = "ZwSt_"
                    m_Key = "WS"
                Case 5
                    _Baza = "ZwWD_"
                    m_Key = "R1"
                Case 6
                    _Baza = "ZwWS_"
                    m_Key = "R2"
                Case 7
                    _Baza = "ZPoz_"
                    m_Key = "OR"
            End Select
            _Baza += Rok + "sprz"
            With New Cl4Excel.GetFromXls(_Path + _Baza + _Ext, "Total", "D4:O117")
                .Data2Array() : oVal = .Dane : .Zwolnij()
            End With
            Kontrol = 1
            dv.RowFilter = "Wydzial = '" + m_Key + "'"
            If dv.Count.Equals(1) Then
                m_Kwota = CP.Str2Dbl(dv(0)(1).ToString) - CP.Str2Dbl(dv(0)(2).ToString)
            Else
                m_Kwota = 0
            End If
            Try
                _dbl = oVal(30, Mc)
            Catch ex As Exception
                _dbl = 0
            End Try
            If Math.Abs(_dbl - m_Kwota) > 1 Then
                m_Txt = "Różnica sprzedaży " + m_Key + vbCrLf + Format((m_Kwota - _dbl), "# ###.00")
                If Not Me.txtKomunikat.Text.Contains(m_Txt) Then _
                    Me.txtKomunikat.Text += vbCrLf + m_Txt
            End If
            If dv.Count.Equals(1) Then
                m_Kwota = CP.Str2Dbl(dv(0)(3).ToString) + CP.Str2Dbl(dv(0)(4).ToString) + CP.Str2Dbl(dv(0)(5).ToString) _
                        + CP.Str2Dbl(dv(0)(6).ToString) + CP.Str2Dbl(dv(0)(7).ToString)
            Else
                m_Kwota = 0
            End If
            Try
                _dbl = oVal(oVal.GetUpperBound(0), Mc)
            Catch ex As Exception
                _dbl = 0
            End Try
            If Math.Abs(_dbl - m_Kwota) > 1 Then
                m_Txt = "Różnica kosztów " + m_Key + vbCrLf + Format((m_Kwota - _dbl), "# ###.00")
                If Not Me.txtKomunikat.Text.Contains(m_Txt) Then Me.txtKomunikat.Text += vbCrLf + m_Txt
            End If
            Me.tspbMain.Value = i : Me.Refresh()
        Next i
Finish:
        ClearStatus()
        dv = Nothing
        dt = Nothing
        da = Nothing
    End Sub

    Sub RZSprzedazVsZS(ByVal Rok As String, ByVal Mc As Integer, ByRef Kontrol As Integer, ByRef Poprzedni As Boolean)

        ' Kontrola prawidłowości danych z zeszytami sprzedaży
        ' wiersz 33, col. 3+m-c - sprzedaż
        ' wiersz 117, col. 3+m-c - koszty
        Dim strPath As String = My.Settings.ToSales
        Dim m_Key As String = ""
        Dim m_Txt As String
        Dim i As Integer
        Dim m_Kwota As Double
        Dim m_Ado As New ClADO.ADOGet
        Dim dv As DataView
        Dim dtT As New DataTable
        Dim dt As New DataTable
        Dim da As OleDbDataAdapter = New OleDbDataAdapter("SELECT Wydzial, SUM(Kwota), SUM(Rabat), SUM(PIP), SUM(Transport), SUM(Other), SUM(Overhead), SUM(Pozostale) " _
                                + "FROM tblSprzedaz WHERE Miesiac = " + Mc.ToString _
                                + " GROUP BY Wydzial ORDER BY Wydzial", Me.cns)
        FillStatusBar(7, "Kontrola pobranych danych z zeszytami sprzedaży...")
        da.Fill(dt)
        dv = dt.DefaultView
        dv.Sort = "Wydzial"
        Me.txtKomunikat.Clear()
        For i = 1 To 7
Poczatek:
            With m_Ado
                .Folder = strPath
                .HDR = True
                Select Case i
                    Case 1
                        .Baza = "ZkSt_"
                        m_Key = "MS"
                    Case 2
                        .Baza = "ZkPras_"
                        m_Key = "PS"
                    Case 3
                        .Baza = "ZkWom_"
                        m_Key = "MT"
                    Case 4
                        .Baza = "ZwSt_"
                        m_Key = "WS"
                    Case 5
                        .Baza = "ZwWD_"
                        m_Key = "R1"
                    Case 6
                        .Baza = "ZwWS_"
                        m_Key = "R2"
                    Case 7
                        .Baza = "ZPoz_"
                        m_Key = "OR"
                End Select
                .Baza += Rok + "sprz.xls"
                If Not .OpenData(True) Then
                    Dim ofd As New OpenFileDialog
                    With ofd
                        .FileName = m_Ado.Baza
                        .Multiselect = False
                        .InitialDirectory = strPath
                        .Filter = "Pliki Excela (*.xl*)|*.xl*"
                        .ShowReadOnly = True
                        If Not .ShowDialog.Equals(Windows.Forms.DialogResult.OK) Then _
                            GoTo Finish
                        strPath = .FileName.Replace(.SafeFileName, "")
                        My.Settings.ToSales = strPath.Replace(.SafeFileName, "")
                        My.Settings.Save()
                        GoTo Poczatek
                    End With
                End If
            End With
            Kontrol = 1
            da = New OleDbDataAdapter("SELECT * FROM [TOTAL$D4:O117]  ", m_Ado.CN)
            dtT = New DataTable
            da.Fill(dtT)
            dv.RowFilter = "Wydzial = '" + m_Key + "'"
            If dv.Count.Equals(1) Then
                m_Kwota = CP.Str2Dbl(dv(0)(1).ToString) - CP.Str2Dbl(dv(0)(2).ToString)
            Else
                m_Kwota = 0
            End If
            If Math.Abs(CP.Str2Dbl(dtT(28)(Mc - 1).ToString) - m_Kwota) > 1 Then
                m_Txt = "Różnica sprzedaży " + m_Key + vbCrLf + Format((m_Kwota - CP.Str2Dbl(dtT(28)(Mc - 1).ToString)), "# ###.00")
                If Not Me.txtKomunikat.Text.Contains(m_Txt) Then _
                    Me.txtKomunikat.Text += vbCrLf + m_Txt
            End If
            If dv.Count.Equals(1) Then
                m_Kwota = CP.Str2Dbl(dv(0)(3).ToString) + CP.Str2Dbl(dv(0)(4).ToString) + CP.Str2Dbl(dv(0)(5).ToString) _
                        + CP.Str2Dbl(dv(0)(6).ToString) + CP.Str2Dbl(dv(0)(7).ToString)
            Else
                m_Kwota = 0
            End If
            If Math.Abs(CP.Str2Dbl(dtT(dtT.Rows.Count - 1)(Mc - 1).ToString) - m_Kwota) > 1 Then
                m_Txt = "Różnica kosztów " + m_Key + vbCrLf + Format(m_Kwota - CP.Str2Dbl(dtT(dtT.Rows.Count - 1)(Mc - 1).ToString), "# ###.00")
                If Not Me.txtKomunikat.Text.Contains(m_Txt) Then
                    Me.txtKomunikat.Text += vbCrLf + m_Txt
                End If
            End If
            Me.tspbMain.Value = i : Me.Refresh()
        Next i
Finish:
        ClearStatus()
        m_Ado = Nothing
        dv = Nothing
        dtT = Nothing
        dt = Nothing
        da = Nothing
    End Sub

#End Region ' Menu Dane

#Region "Menu Export"

    Private Sub AktualizacjaZeszytowSprzedazy() Handles tsmZeszytySprzedazy.Click

        Dim strPath As String = My.Settings.ToResult
        Dim m_File As String = "CHO_Result_" + Okres.Substring(0, 7) + ".xls"
        Const CO_ACTION As String = "Aktualizacja zeszytów sprzedaży"
        If BlokadaAktualizacji("ZeszytySprzedazy") Then Exit Sub
        ' Sprawdzenie obecności plików
SprawdzPlik:
        If Not My.Computer.FileSystem.FileExists(strPath + "CHO_Result_" + Okres.Substring(0, 7) + ".xls") Then
            Dim ofd As New OpenFileDialog
            With ofd
                .FileName = m_File
                .Multiselect = False
                .InitialDirectory = strPath
                .Filter = "CHO_Result (*.xls)|*.xls"
                .ShowReadOnly = True
                If Not .ShowDialog.Equals(Windows.Forms.DialogResult.OK) Then Exit Sub
                strPath = .FileName.Replace(.SafeFileName, "")
                My.Settings.ToResult = strPath.Replace(.SafeFileName, "")
                My.Settings.Save()
                GoTo SprawdzPlik
            End With
        End If
        Try
            With New Cl4Excel.Excel(m_File)
                .oApp.Run("AktualizujWydzialy")
                .Zwolnij()
            End With
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message, CO_ACTION, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
        End Try
    End Sub

    Private Sub AktualizacjaZesztowBezWOT() Handles tsmAktualizacjaZesztowBezWOT.Click

        Dim strPath As String = My.Settings.ToRzecz
        Dim da As OleDbDataAdapter
        Dim dt As New DataTable
        Dim dtE As New DataTable
        Dim dtZ As New DataTable
        Dim bs As New BindingSource
        Dim drv As DataRowView
        Dim h, i, j, k, iW, iMa As Integer
        Dim m_Instalacja, strZeszyt As String
        Dim m_File As String = ""
        Dim m_Blad As String = ""
        Dim aKoszty(0, 11) As Object
        Dim aMedia(0, 6) As Object
        Dim aZatrud(0, 1) As Object
        Dim m_Uzyte As String = ""
        Dim m_NieCzysc As Boolean = False
        Const CO_ACTION As String = "Aktualizacja danych WOM i Prasowni bez WOT"
        If BlokadaAktualizacji("ZeszytyWydzialowe") Then Exit Sub
        ' Sprawdzenie obecności plików
SprawdzPlik:
        If Not My.Computer.FileSystem.FileExists(strPath + "ZkSt_" + Okres.Substring(0, 4) + "rz.xls") Then
            Dim ofd As New OpenFileDialog
            With ofd
                .FileName = "ZkSt_" + Okres.Substring(0, 4) + "rz.xls"
                .Multiselect = False
                .InitialDirectory = strPath
                .Filter = "Koszty rzeczywiste (*.xls)|*.xls"
                .ShowReadOnly = True
                If Not .ShowDialog.Equals(Windows.Forms.DialogResult.OK) Then Exit Sub
                strPath = .FileName.Replace(.SafeFileName, "")
                My.Settings.ToRzecz = strPath.Replace(.SafeFileName, "")
                My.Settings.Save()
                GoTo SprawdzPlik
            End With
        End If
        Try
            Dim m_WB As String = ""
            If NotExportFile(m_WB) Then Exit Try
            da = New OleDbDataAdapter("SELECT T.ID, M.Skoroszyt, M.Ark, Z.CalCo, Z.MPK, M.Typ, SUM(Z.Ilosc) AS [Ilosc], SUM(Z.Wartosc) AS [Wartosc], T.Ark, T.Skoroszyt, Z.JM, C.Waga2Zuzycie " _
                                + "FROM ((tblZWPC Z INNER JOIN tblMPK M ON Z.MPK = M.ID) INNER JOIN tblInstalacje T ON Z.Instalacja = T.ID) " _
                                + "INNER JOIN tblCalCo C ON Z.CalCo = C.ID " _
                                + "WHERE Mc = " + Okres.Substring(5, 2) + " AND NOT Z.Typ IN ('F', 'O', 'R', 'S') " _
                                + "AND T.ID IN ('JMT01','JPS01') AND Z.MPK NOT IN ('JFHT01','JFHT02','JFHT03','JFHT04','JFHT05','JFHT06','JFHT07','JFHT08','JFHT09','JFHT19','JFRC03') " _
                                + "GROUP BY T.ID, M.Skoroszyt, T.Ark, M.Ark, Z.CalCo, Z.MPK, M.Typ, T.Skoroszyt, Z.JM, C.Waga2Zuzycie " _
                                + "ORDER BY T.ID, T.Skoroszyt, M.Skoroszyt, T.Ark, M.Ark, Z.CalCo", Me.cns)
            dt = New DataTable
            da.Fill(dt)
            If dt.Rows.Count = 0 Then Throw New ArgumentException("Brak danych kosztowych")
            da = New OleDbDataAdapter("SELECT T.ID, M.Skoroszyt, M.Ark, Z.Media, E.RowZuzycie, E.RowMedia, E.Podzielnik, SUM(Z.Ilosc) AS [Ilosc], E.Opis, T.Ark, T.Skoroszyt, E.RowMedPom " _
               + "FROM ((tblMedia Z INNER JOIN tblMPK M ON Z.MPK = M.ID) INNER JOIN tblInstalacje T ON M.Instalacja = T.ID) " _
               + "INNER JOIN tblGEn E ON Z.Media = E.ID " _
               + "WHERE  Mc = " + Okres.Substring(5, 2) _
               + " AND T.ID IN ('JMT01','JPS01') " _
               + "GROUP BY T.ID, M.Skoroszyt, T.Ark, M.Ark, Z.Media, E.RowZuzycie, E.RowMedia, E.Podzielnik, E.Opis, T.Skoroszyt, E.RowMedPom " _
               + "ORDER BY T.ID, T.Skoroszyt, M.Skoroszyt, T.Ark, M.Ark, Z.Media, E.RowZuzycie, E.RowMedia, E.RowMedPom, E.Podzielnik", Me.cns)
            da.Fill(dtE)
            bs.DataSource = dtE
            FillStatusBar(dt.Rows.Count, CO_ACTION + "...")
            With New Cl4Excel.Excel(m_WB)
                .ScreenRefresh(False)
                .oApp.Run("AdresDB", My.Settings.Sciezka, My.Settings.Baza, MNotH)
                'ZkPras_BezWot_2012rz
                'ZkWOM_BezWot_2012rz
                Do
                    If String.IsNullOrEmpty(dt.Rows(i)(9).ToString) Then
                        If String.IsNullOrEmpty(dt.Rows(i)(1).ToString) Then
                            i += 1
                            GoTo Nastepny
                        End If
                        k = 1
                        strZeszyt = dt.Rows(i)(1).ToString
                    Else
                        k = 9
                        strZeszyt = dt.Rows(i)(9).ToString + "_BezWOT"
                    End If
                    iW = i
                    iMa = 0
                    m_Instalacja = dt.Rows(i)(0).ToString
                    While i < dt.Rows.Count AndAlso dt.Rows(i)(0).ToString.Equals(m_Instalacja)
                        iMa += 1
                        i += 1
                    End While
                    With Me
                        .tspbMain.Value = i
                        .tsslLicznik.Text = strZeszyt
                    End With
                    Application.DoEvents()
                    ReDim aKoszty(iMa - 1, 11)
                    For h = 0 To iMa - 1
                        For j = 0 To 11
                            If j.Equals(6) OrElse j.Equals(7) Then
                                Try
                                    aKoszty(h, j) = CDbl(dt.Rows(iW)(j).ToString)
                                Catch ex As Exception
                                    aKoszty(h, j) = 0
                                End Try
                            Else
                                aKoszty(h, j) = dt.Rows(iW)(j).ToString
                            End If
                        Next j
                        iW += 1
                    Next h
                    bs.Filter = Nothing
                    bs.Filter = dtE.Columns(0).ColumnName.ToString + " = '" + aKoszty(0, 0).ToString + "'"
                    ReDim aMedia(bs.Count - 1, dtE.Columns.Count - 1)
                    For h = 0 To bs.Count - 1
                        With bs
                            .Position = h
                            drv = CType(.Current, DataRowView)
                        End With
                        For j = 0 To dtE.Columns.Count - 1
                            If j.Equals(6) OrElse j.ToString(7) Then
                                Try
                                    aMedia(h, j) = CDbl(drv(j).ToString)
                                Catch ex As Exception
                                    aMedia(h, j) = 0
                                End Try
                            Else
                                aMedia(h, j) = drv(j).ToString
                            End If
                        Next j
                    Next h
                    m_NieCzysc = True
                    If m_Instalacja.Equals("JMS03") Then
                        m_File = strPath + "ZwSt_" + Okres.Substring(0, 4) + "rz.xls"
                        m_NieCzysc = False
                    Else
                        m_File = (strPath + strZeszyt + "_" + Okres.Substring(0, 4) + "rz.xls").ToUpper
                        If Not m_Uzyte.Contains(m_File) Then
                            m_Uzyte += m_File + "#"
                            m_NieCzysc = False
                        End If
                    End If
                    With .oApp
                        .Run("WstawDane", m_File, Me.dtpOkres.Value.Month, aKoszty, aMedia, m_NieCzysc)
                        m_Blad = .Range("Blad").Value2
                    End With
                    If Not String.IsNullOrEmpty(m_Blad) Then Exit Do
Nastepny:
                Loop Until i.Equals(dt.Rows.Count)
                .Zwolnij()
            End With
            If Not String.IsNullOrEmpty(m_Blad) Then Throw New ArgumentException(m_Blad)
            Using New CM
                MessageBox.Show("Operacja zakończona pomyślnie", CO_ACTION, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End Using
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message, CO_ACTION, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
        Finally
            ClearStatus()
            da = Nothing
            dt = Nothing
            dtE = Nothing
            dtZ = Nothing
            bs = Nothing
            drv = Nothing
        End Try
    End Sub

    Private Sub AktualizacjaRzeczWyn() Handles tsm_Et.Click

        Dim _Path As String = My.Settings.ToRzecz
        Dim _File As String = ""
        Dim dt As New DataTable, dtE As New DataTable
        Dim dtU As New DataTable, dtZ As New DataTable
        Dim dv As New DataView
        Dim bs As New BindingSource
        Dim v As DataRowView
        Dim r As DataRow
        Dim h, i, j, k, iW, iMa, _Mod, _Mc As Integer
        Dim m_Instalacja, strZeszyt As String
        Dim aKoszty(0, 11) As Object
        Dim aMedia(0, 6) As Object
        Dim aZatrud(0, 1) As Object
        Dim aUslugi(,) As Object
        Dim aMpkControl(,) As Object
        Dim m_Blad As String = ""
        Dim m_Uzyte As String = ""
        Dim m_NieCzysc As Boolean = False
        Const CO_ACTION As String = "Aktualizacja danych rzeczywistych"
        If BlokadaAktualizacji("ZeszytyWydzialowe") Then Return
        _Mc = Me.dtpOkres.Value.Month
        'strPath = "C:\Celsa\Wykon2014\"   '<- do testowania
        Try
            Dim m_WB As String = Nothing
            Dim _Ext As String = ".xlsx"
            ' Sprawdzenie obecności plików
            _Path = CP.GetSourcePath(_Path + "Koszty_" + Okres.Substring(0, 4) + "rz", _Ext)
            If String.IsNullOrEmpty(_Path) Then Exit Try
            If NotExportFile(m_WB) Then Exit Try
            ' dane kontrolne z arkusza MpkControl
            With New Cl4Excel.GetFromXls("W:\Costs\CHO Wyniki\Wynik " & Me.dtpOkres.Value.Year & "\CHO_Result_" & Me.dtpOkres.Value.ToShortDateString.Substring(0, 7) & ".xlsm", "MpkControl", "F2:O")
                .Data2Array() : aMpkControl = .Dane : .Zwolnij()
            End With
            Dim tr As OleDbTransaction = Nothing
            Dim cmd As New OleDbCommand
            Using cn As New OleDbConnection(Me.cns.ConnectionString)
                Try
                    cn.Open()
                    tr = cn.BeginTransaction
                    With cmd
                        .Connection = cn
                        .Transaction = tr
                        .CommandText = "SELECT I.Skoroszyt, Z.Instalacja, Z.MPK, M.Opis, Mc, SUM(Wartosc) AS [Wartosc] FROM (tblZWPC Z INNER JOIN tblInstalacje I ON Z.Instalacja = I.ID) " _
                                        + "INNER JOIN tblMPK M ON Z.MPK = M.ID WHERE NOT LEFT(Z.Instalacja,3) = 'JAX' AND LEFT(M.Instalacja,3) = 'JAX' " _
                                        + "GROUP BY I.Skoroszyt, Z.Instalacja, Z.MPK, M.Opis, Mc ORDER BY Z.Instalacja, Z.MPK"
                        dtU.Load(.ExecuteReader)
                        dv = dtU.DefaultView
                        dv.Sort = "Instalacja, MPK"
                        .CommandText = "SELECT T.ID, M.Skoroszyt, M.Ark, Z.CalCo, Z.MPK, M.Typ, SUM(Z.Ilosc) AS [Ilosc], SUM(Z.Wartosc) AS [Wartosc], T.Ark, T.Skoroszyt AS [Wydz], Z.JM, C.Waga2Zuzycie " _
                                        + "FROM ((tblZWPC Z INNER JOIN tblMPK M ON Z.MPK = M.ID) INNER JOIN tblInstalacje T ON Z.Instalacja = T.ID) " _
                                        + "INNER JOIN tblCalCo C ON Z.CalCo = C.ID " _
                                        + "WHERE Mc = " + Okres.Substring(5, 2) + " AND M.getMPK = true " _
                                        + "GROUP BY T.ID, M.Skoroszyt, T.Ark, M.Ark, Z.CalCo, Z.MPK, M.Typ, T.Skoroszyt, Z.JM, C.Waga2Zuzycie " _
                                        + "ORDER BY T.ID, T.Skoroszyt, M.Skoroszyt, T.Ark, M.Ark, Z.CalCo"
                        '"WHERE Mc = " + Okres.Substring(5, 2) + " AND NOT Z.Typ IN ('F', 'O', 'R', 'S') " _
                        '"WHERE Mc = " + Okres.Substring(5, 2) + " AND M.getMPK = true " _
                        dt.Load(.ExecuteReader)
                        If dt.Rows.Count.Equals(0) Then Throw New ArgumentException("Brak danych kosztowych")
                        .CommandText = "SELECT T.ID, M.Skoroszyt, M.Ark, Z.Media, E.RowZuzycie, E.RowMedia, E.Podzielnik, SUM(Z.Ilosc) AS [Ilosc], E.Opis, T.Ark, T.Skoroszyt AS [Wydz], E.RowMedPom " _
                                        + "FROM ((tblMedia Z INNER JOIN tblMPK M ON Z.MPK = M.ID) INNER JOIN tblInstalacje T ON M.Instalacja = T.ID) " _
                                        + "INNER JOIN tblGEn E ON Z.Media = E.ID WHERE  Mc = " + Okres.Substring(5, 2) _
                                        + " GROUP BY T.ID, M.Skoroszyt, T.Ark, M.Ark, Z.Media, E.RowZuzycie, E.RowMedia, E.Podzielnik, E.Opis, T.Skoroszyt, E.RowMedPom " _
                                        + "ORDER BY T.ID, T.Skoroszyt, M.Skoroszyt, T.Ark, M.Ark, Z.Media, E.RowZuzycie, E.RowMedia, E.RowMedPom, E.Podzielnik"
                        dtE.Load(.ExecuteReader)
                        If dtE.Rows.Count.Equals(0) Then Throw New ArgumentException("Brak danych o mediach")
                    End With
                Catch ex As Exception
                    Throw New ArgumentException(ex.Message)
                End Try
            End Using
            tr = Nothing
            cmd = Nothing
            i = 0
            bs.DataSource = dtE
            FillStatusBar(dt.Rows.Count, CO_ACTION + "...")
            _Mod = dt.Rows.Count \ 20
            Dim xE As New Cl4Excel.Excel(m_WB)
            With xE
                .oApp.Run("AdresDB", My.Settings.Sciezka, My.Settings.Baza, MNotH, aMpkControl)
                Do
                    r = dt.Rows(i)
                    If String.IsNullOrEmpty(r(9).ToString) Then
                        If String.IsNullOrEmpty(r(1).ToString) Then
                            i += 1
                            GoTo Nastepny
                        End If
                        k = 1
                        strZeszyt = r(1).ToString
                    Else
                        k = 9
                        strZeszyt = r(9).ToString
                    End If
                    iW = i
                    iMa = 0
                    m_Instalacja = r(0).ToString
                    While i < dt.Rows.Count AndAlso dt.Rows(i)(0).ToString.Equals(m_Instalacja) _
                          AndAlso dt.Rows(i)(k).ToString.Equals(strZeszyt)
                        iMa += 1
                        If (i Mod _Mod).Equals(0) Then
                            With Me
                                .tspbMain.Value = i
                                .tsslLicznik.Text = strZeszyt
                            End With
                            Application.DoEvents()
                        End If
                        i += 1
                    End While
                    ReDim aKoszty(iMa - 1, 11)
                    For h = 0 To iMa - 1
                        For j = 0 To 11
                            If j.Equals(6) OrElse j.Equals(7) Then
                                Try
                                    aKoszty(h, j) = CDbl(dt.Rows(iW)(j).ToString)
                                Catch ex As Exception
                                    aKoszty(h, j) = 0
                                End Try
                            Else
                                aKoszty(h, j) = dt.Rows(iW)(j).ToString
                            End If
                        Next j
                        iW += 1
                    Next h
                    bs.Filter = Nothing
                    If m_Instalacja.Equals("JMS03") Then
                        bs.Filter = dtE.Columns(0).ColumnName.ToString + " = 'JMS03'"
                    ElseIf k.Equals(1) Then
                        bs.Filter = dtE.Columns(1).ColumnName.ToString + " = '" + strZeszyt + "'"
                    Else
                        bs.Filter = dtE.Columns(10).ColumnName.ToString + " = '" + strZeszyt + "'"
                    End If
                    ReDim aMedia(bs.Count - 1, dtE.Columns.Count - 1)
                    For h = 0 To bs.Count - 1
                        With bs
                            .Position = h
                            v = CType(.Current, DataRowView)
                        End With
                        For j = 0 To dtE.Columns.Count - 1
                            If j.Equals(6) OrElse j.Equals(7) Then
                                Try
                                    aMedia(h, j) = CDbl(v(j).ToString)
                                Catch ex As Exception
                                    aMedia(h, j) = 0
                                End Try
                            Else
                                aMedia(h, j) = v(j).ToString
                            End If
                        Next j
                    Next h
                    ReDim aUslugi(0, 0)
                    dv.RowFilter = Nothing
                    Try
                        dv.RowFilter = "Instalacja = '" + m_Instalacja + "'"
                        If dv.Count.Equals(0) Then Throw New ArgumentException
                    Catch ex As Exception
                        GoTo PoUslugach
                    End Try
                    ReDim aUslugi(dv.Count - 1, 4)
                    For h = 0 To dv.Count - 1
                        For j = 0 To 4
                            aUslugi(h, j) = dv(h)(1 + j)
                        Next j
                    Next h
PoUslugach:
                    m_NieCzysc = True
                    If m_Instalacja.Equals("JMS03") Then
                        _File = _Path + "ZwSt_" + Okres.Substring(0, 4) + "rz.xlsx"
                        m_NieCzysc = False
                    Else
                        _File = (_Path + strZeszyt + "_" + Okres.Substring(0, 4) + "rz.xlsx").ToUpper
                        If Not m_Uzyte.Contains(_File) Then
                            m_Uzyte += _File + "#"
                            m_NieCzysc = False
                        End If
                    End If
                    With .oApp
                        .Run("WstawDane", _File, _Mc, aKoszty, aMedia, m_NieCzysc, aUslugi)
                        m_Blad = .Range("Blad").Value2
                    End With
                    If Not String.IsNullOrEmpty(m_Blad) Then Exit Do
Nastepny:
                Loop Until i.Equals(dt.Rows.Count)
                .Zwolnij()
            End With
            ClearStatus()
            If Not String.IsNullOrEmpty(m_Blad) Then Throw New ArgumentException(m_Blad)
            Using New CM
                MessageBox.Show("Operacja zakończona pomyślnie", CO_ACTION, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End Using
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message, CO_ACTION, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
        Finally
            ClearStatus()
            dt = Nothing
            dtE = Nothing
            dtZ = Nothing
            bs = Nothing
            v = Nothing
            dv = Nothing
        End Try
    End Sub

    Private Sub AnalizaSprzedazyZWW() Handles tsm_Ehs.Click

        Dim da As OleDbDataAdapter = New OleDbDataAdapter("SELECT Miesiac, S.Wydzial, M.AHOfZWW, K.Kraj, K.Nazwa, K.KK, SUM(Waga) AS [Mg], SUM(Kwota-Rabat-Transport-Pozostale) AS [Sprzedaz], SUM(EBiTDA) AS [EBiTDA] " _
                                                        + "FROM (tblSprzedaz S LEFT JOIN tblKlienci K ON S.IdKlienta = K.ID) LEFT JOIN tblGrupyMat M ON S.GrMater = M.ID " _
                                                        + "WHERE S.Wydzial IN ('WS', 'R1', 'R2') AND Miesiac <= " + Me.dtpOkres.Value.Month.ToString _
                                                        + " GROUP BY Miesiac, S.Wydzial, M.AHOfZWW, K.Kraj, K.Nazwa, K.KK ORDER BY M.AHOfZWW, Miesiac", Me.cns)
        Dim dt As New DataTable, dtK As New DataTable, dtO As New DataTable
        Dim dtW As New DataTable
        Dim dv, dvK, dvO, dvW As DataView
        Dim aDane(0 To 10, 0 To 12) As Object
        Dim i, j, k, m As Integer
        Dim _WB As String = "W:\Costs\Celsa\Raporty kwartalne ZWW\Quarterly Report_" + Me.dtpOkres.Value.Year.ToString + ".XLS"
        Dim txt As String = ""
        Dim Najwieksi As String = ""
        Dim _Mc, _LongMc, M_c As String
        Dim _Kw As Integer = CP.Kwartal(Me.dtpOkres.Value)
        Dim _Mg, _Sprzedaz, _EBiTDA As Double
        Dim fs As XL.Range = Nothing
        Dim Rng As String = ""
        Dim _EUR As Boolean
        Try
            _Mc = "0" + Me.dtpOkres.Value.Month.ToString
            _Mc = _Mc.Substring(_Mc.Length - 2, 2)
            _LongMc = CP.FullEnglishDate(Me.dtpOkres.Value, True, True)
            FillStatusBar(0, Me.tsm_Eh.Text.Replace("&", "") + " - sprzedaż")
            da.Fill(dt)
            If dt.Rows.Count.Equals(0) Then Throw New ArgumentException("Brak danych")
            dvW = Nothing
            Using New CM
                _EUR = MessageBox.Show("EUR version?", Me.tsm_Eh.Text.Replace("&", "") + " - sprzedaż", MessageBoxButtons.YesNo, MessageBoxIcon.Question).Equals(Windows.Forms.DialogResult.Yes)
            End Using
            If _EUR Then
                da = New OleDbDataAdapter("SELECT TOP " + Me.dtpOkres.Value.Month.ToString + " MM_dd, EUR FROM tblKursyWalut WHERE INT(MM_dd) <= " & _Mc & " ORDER BY MM_dd", Me.cns)
                da.Fill(dtW)
                If Not dtW.Rows.Count.Equals(Me.dtpOkres.Value.Month) Then Throw New ArgumentException("Sprawdź kursy EUR")
                dvW = dtW.DefaultView
                dvW.Sort = "MM_dd"
                _WB = _WB.Replace("ZWW\Quarterly Report_", "ZWW\Quarterly Report_EUR_")
            End If
            With dt.Columns
                .Add("Kwart", GetType(Integer))
                .Add("EUR", GetType(Double))
            End With
            For Each r In dt.Rows
                r("Kwart") = CP.Kwartal(r("Miesiac"))
                M_c = "0" + r("Miesiac").ToString
                M_c = M_c.Substring(M_c.Length - 2)
                If _EUR Then
                    i = dvW.Find(M_c)
                    If i.Equals(-1) Then _
                        Throw New ArgumentException("Brak przelicznika EUR dla [" + M_c + "]")
                    r("EUR") = dvW(i)("EUR")
                End If
                r("Nazwa") = Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(r("Nazwa").ToString.ToLower)
                If Not String.IsNullOrEmpty(r("KK").ToString) Then _
                    r("Nazwa") = r("Nazwa").ToString + " (" + r("KK").ToString + ")"
            Next r
            With dtO.Columns
                .Add("Opis")
                .Add("Grupa")
                .Add("Mg", GetType(Double))
                .Add("Sprzedaz", GetType(Double))
                .Add("EBiTDA", GetType(Double))
                .Add("YTD_Mg", GetType(Double))
                .Add("YTD_Sprzedaz", GetType(Double))
                .Add("YTD_EBiTDA", GetType(Double))
                .Add("Q_Mg", GetType(Double))
                .Add("Q_Sprzedaz", GetType(Double))
                .Add("Q_EBiTDA", GetType(Double))
            End With
            dtK = dtO.Clone
            Dim xE As New Cl4Excel.Excel(_WB, , , "x")
            With xE
                .ScreenRefresh(False)
                ' czyszczenie
                For j = 1 To .oBook.Worksheets.Count
                    .oSheet = CType(.oBook.Sheets(j), XL._Worksheet)
                    Try
                        If Not .oSheet.Tab.ColorIndex.ToString.Equals("41") Then Exit Try
                        If .oSheet.Name.Equals("Total") Then
                            For i = 1 To 3
                                Select Case i
                                    Case 1
                                        Rng = "B6"
                                    Case 2
                                        Rng = "B23"
                                    Case 3
                                        Rng = "B40"
                                End Select
                                Try
                                    With .oSheet.Range(Rng).CurrentRegion
                                        .Offset(2, 1).Resize(10, 1).ClearContents()
                                        .Offset(2, 2).Resize(11).SpecialCells(Microsoft.Office.Interop.Excel.XlCellType.xlCellTypeConstants).ClearContents()
                                    End With
                                Catch ex As Exception
                                End Try
                            Next i
                            With .oSheet
                                .Cells(2, 2) = _LongMc
                                .Cells(2, 1) = _Mc
                                .Cells(19, 2) = _LongMc + " YTD"
                                .Cells(19, 1) = _Mc
                                .Cells(36, 2) = _Kw.ToString + " Q " + Me.dtpOkres.Value.Year.ToString
                                .Cells(36, 1) = _Kw & "Q"
                            End With
                        ElseIf Not .oSheet.Name.Contains("Data") Then
                            For i = 1 To 6
                                Select Case i
                                    Case 1
                                        Rng = "B6"
                                    Case 2
                                        Rng = "B23"
                                    Case 3
                                        Rng = "B40"
                                    Case 4
                                        Rng = "B57"
                                    Case 5
                                        Rng = "B73"
                                    Case 6
                                        Rng = "B89"
                                End Select
                                Try
                                    With .oSheet.Range(Rng).CurrentRegion
                                        .Offset(1, 1).Resize(10, 1).ClearContents()
                                        .Offset(1, 2).Resize(11, 6).SpecialCells(Microsoft.Office.Interop.Excel.XlCellType.xlCellTypeConstants).ClearContents()
                                    End With
                                Catch ex As Exception
                                End Try
                            Next i
                            With .oSheet
                                .Cells(2, 2) = _LongMc
                                .Cells(54, 2) = _LongMc
                                .Cells(2, 1) = _Mc
                                .Cells(19, 2) = _LongMc + " YTD"
                                .Cells(70, 2) = _LongMc + " YTD"
                                .Cells(19, 1) = _Mc
                                .Cells(36, 2) = _Kw.ToString + " Q " + Me.dtpOkres.Value.Year.ToString
                                .Cells(86, 2) = _Kw.ToString + " Q " + Me.dtpOkres.Value.Year.ToString
                                .Cells(36, 1) = _Kw & "Q"
                            End With
                        End If
                    Catch ex As Exception
                    End Try
                Next j
                dv = dt.DefaultView
                ' Total
                .oSheet = CType(.oBook.Sheets("Total"), XL._Worksheet)
                dv.RowFilter = Nothing
                dv.Sort = "Nazwa"
                For m = 0 To 2
                    txt = ""
                    i = 0
                    dvO = dtO.DefaultView
                    dvO.Sort = "Opis, Grupa"
                    Select Case m
                        Case 0
                            Dim query = (From p In dt _
                                         Where p!Miesiac.ToString = Me.dtpOkres.Value.Month.ToString _
                                         Group By ID = p!Nazwa _
                                         Into RIl = Sum(p.Field(Of Double)("Mg")) _
                                         Order By RIl Descending)
                            For Each q In query
                                txt += q.ID + "|"
                                i += 1
                                .oSheet.Cells(5 + i, 3) = q.ID
                                If i.Equals(10) Then Exit For
                            Next q
                            dv.RowFilter = "Miesiac = " + Me.dtpOkres.Value.Month.ToString
                            Rng = "C6:C16"
                        Case 1
                            Dim query = (From p In dt _
                                         Group By ID = p!Nazwa _
                                         Into RIl = Sum(p.Field(Of Double)("Mg")) _
                                         Order By RIl Descending)
                            For Each q In query
                                txt += q.ID + "|"
                                i += 1
                                .oSheet.Cells(22 + i, 3) = q.ID
                                If i.Equals(10) Then Exit For
                            Next q
                            Rng = "C23:C33"
                        Case 2
                            Dim query = (From p In dt _
                                         Where p!Kwart = _Kw _
                                         Group By ID = p!Nazwa _
                                         Into RIl = Sum(p.Field(Of Double)("Mg")) _
                                         Order By RIl Descending)
                            For Each q In query
                                txt += q.ID + "|"
                                i += 1
                                .oSheet.Cells(39 + i, 3) = q.ID
                                If i.Equals(10) Then Exit For
                            Next q
                            dv.RowFilter = "Kwart = " + _Kw.ToString
                            Rng = "C40:C50"
                    End Select
                    For Each v In dv
                        If txt.Contains(v("Nazwa") + "|") Then
                            i = dvO.Find(New Object() {v("Nazwa"), v("AHOfZWW")})
                            If i.Equals(-1) Then _
                                dtO.Rows.Add(New Object() {v("Nazwa"), v("AHOfZWW"), 0, 0, 0})
                            i = dvO.Find(New Object() {v("Nazwa"), v("AHOfZWW")})
                        Else
                            i = dvO.Find(New Object() {"Other Customers", v("AHOfZWW")})
                            If i.Equals(-1) Then _
                                dtO.Rows.Add(New Object() {"Other Customers", v("AHOfZWW"), 0, 0, 0})
                            i = dvO.Find(New Object() {"Other Customers", v("AHOfZWW")})
                        End If
                        dvO(i)("Mg") = CDbl(dvO(i)("Mg").ToString) + v("Mg")
                        If _EUR Then
                            dvO(i)("Sprzedaz") = CDbl(dvO(i)("Sprzedaz").ToString) + v("Sprzedaz") / v("EUR")
                            dvO(i)("EBiTDA") = CDbl(dvO(i)("EBiTDA").ToString) + v("EBiTDA") / v("EUR")
                        Else
                            dvO(i)("Sprzedaz") = CDbl(dvO(i)("Sprzedaz").ToString) + v("Sprzedaz")
                            dvO(i)("EBiTDA") = CDbl(dvO(i)("EBiTDA").ToString) + v("EBiTDA")
                        End If
                    Next v
                    For Each v In dvO
                        fs = .oSheet.Rows(4).Find(v("Grupa"), LookAt:=XL.XlLookAt.xlWhole, LookIn:=XL.XlFindLookIn.xlValues)
                        If fs Is Nothing Then GoTo Nastepny
                        k = fs.Column
                        fs = .oSheet.Range(Rng).Find(v("Opis"), LookAt:=XL.XlLookAt.xlWhole, LookIn:=XL.XlFindLookIn.xlValues)
                        If fs Is Nothing Then GoTo Nastepny
                        i = fs.Row
                        With .oSheet
                            .Cells(i, k) = v("Mg")
                            .Cells(i, k + 1) = v("Sprzedaz")
                            .Cells(i, k + 3) = v("EBiTDA")
                        End With
Nastepny:
                    Next v
                    dv.RowFilter = Nothing
                    dtO.Clear()
                Next m
                dvO = Nothing
                dtO.Clear()
                dtK.Columns.Remove("Grupa")
                dtO.Columns.Remove("Grupa")
                Dim oArk() As Object = Nothing
                For m = 0 To 2
                    dv.RowFilter = Nothing
                    dv.Sort = Nothing
                    Select Case m
                        Case 0
                            dv.Sort = "AHOfZWW"
                            oArk = (From v As DataRowView In dv Select v!AHOfZWW Distinct).ToArray
                        Case 1
                            dv.Sort = "Wydzial"
                            oArk = (From v As DataRowView In dv Select v!Wydzial Distinct).ToArray
                    End Select
                    For Each o In oArk
                        dv.RowFilter = Nothing
                        If m.Equals(0) Then
                            dv.RowFilter = "AHOfZWW = '" + o.ToString + "'"
                        Else
                            If Not o.Equals("WS") Then _
                                dv.RowFilter = "Wydzial = '" + o.ToString + "'"
                        End If
                        dvK = Nothing
                        dvO = Nothing
                        dtK.Clear()
                        dtO.Clear()
                        dvK = dtK.DefaultView
                        dvK.Sort = "Opis"
                        dvO = dtO.DefaultView
                        dvO.Sort = "Opis"
                        For Each v As DataRowView In dv
                            k = dvK.Find(v("Kraj"))
                            If k.Equals(-1) Then _
                                dtK.Rows.Add(New Object() {v("Kraj"), 0, 0, 0, 0, 0, 0, 0, 0, 0})
                            k = dvK.Find(v("Kraj"))
                            i = dvO.Find(v("Nazwa"))
                            If i.Equals(-1) Then _
                                dtO.Rows.Add(New Object() {v("Nazwa"), 0, 0, 0, 0, 0, 0, 0, 0, 0})
                            i = dvO.Find(v("Nazwa"))
                            ' miesiąc
                            If CInt(v("Miesiac").ToString).Equals(Me.dtpOkres.Value.Month) Then
                                dvK(k)("Mg") = CDbl(dvK(k)("Mg").ToString) + v("Mg")
                                dvO(i)("Mg") = CDbl(dvO(i)("Mg").ToString) + v("Mg")
                                If _EUR Then
                                    dvK(k)("Sprzedaz") = CDbl(dvK(k)("Sprzedaz").ToString) + v("Sprzedaz") / v("EUR")
                                    dvK(k)("EBiTDA") = CDbl(dvK(k)("EBiTDA").ToString) + v("EBiTDA") / v("EUR")
                                    dvO(i)("Sprzedaz") = CDbl(dvO(i)("Sprzedaz").ToString) + v("Sprzedaz") / v("EUR")
                                    dvO(i)("EBiTDA") = CDbl(dvO(i)("EBiTDA").ToString) + v("EBiTDA") / v("EUR")
                                Else
                                    dvK(k)("Sprzedaz") = CDbl(dvK(k)("Sprzedaz").ToString) + v("Sprzedaz")
                                    dvK(k)("EBiTDA") = CDbl(dvK(k)("EBiTDA").ToString) + v("EBiTDA")
                                    dvO(i)("Sprzedaz") = CDbl(dvO(i)("Sprzedaz").ToString) + v("Sprzedaz")
                                    dvO(i)("EBiTDA") = CDbl(dvO(i)("EBiTDA").ToString) + v("EBiTDA")
                                End If
                            End If
                            ' YTD
                            dvK(k)("YTD_Mg") = CDbl(dvK(k)("YTD_Mg").ToString) + v("Mg")
                            dvO(i)("YTD_Mg") = CDbl(dvO(i)("YTD_Mg").ToString) + v("Mg")
                            If _EUR Then
                                dvK(k)("YTD_Sprzedaz") = CDbl(dvK(k)("YTD_Sprzedaz").ToString) + v("Sprzedaz") / v("EUR")
                                dvK(k)("YTD_EBiTDA") = CDbl(dvK(k)("YTD_EBiTDA").ToString) + v("EBiTDA") / v("EUR")
                                dvO(i)("YTD_Sprzedaz") = CDbl(dvO(i)("YTD_Sprzedaz").ToString) + v("Sprzedaz") / v("EUR")
                                dvO(i)("YTD_EBiTDA") = CDbl(dvO(i)("YTD_EBiTDA").ToString) + v("EBiTDA") / v("EUR")
                            Else
                                dvK(k)("YTD_Sprzedaz") = CDbl(dvK(k)("YTD_Sprzedaz").ToString) + v("Sprzedaz")
                                dvK(k)("YTD_EBiTDA") = CDbl(dvK(k)("YTD_EBiTDA").ToString) + v("EBiTDA")
                                dvO(i)("YTD_Sprzedaz") = CDbl(dvO(i)("YTD_Sprzedaz").ToString) + v("Sprzedaz")
                                dvO(i)("YTD_EBiTDA") = CDbl(dvO(i)("YTD_EBiTDA").ToString) + v("EBiTDA")
                            End If
                            ' kwartał
                            If v("Kwart").Equals(_Kw) Then
                                dvK(k)("Q_Mg") = CDbl(dvK(k)("Q_Mg").ToString) + v("Mg")
                                dvO(i)("Q_Mg") = CDbl(dvO(i)("Q_Mg").ToString) + v("Mg")
                                If _EUR Then
                                    dvK(k)("Q_Sprzedaz") = CDbl(dvK(k)("Q_Sprzedaz").ToString) + v("Sprzedaz") / v("EUR")
                                    dvK(k)("Q_EBiTDA") = CDbl(dvK(k)("Q_EBiTDA").ToString) + v("EBiTDA") / v("EUR")
                                    dvO(i)("Q_Sprzedaz") = CDbl(dvO(i)("Q_Sprzedaz").ToString) + v("Sprzedaz") / v("EUR")
                                    dvO(i)("Q_EBiTDA") = CDbl(dvO(i)("Q_EBiTDA").ToString) + v("EBiTDA") / v("EUR")
                                Else
                                    dvK(k)("Q_Sprzedaz") = CDbl(dvK(k)("Q_Sprzedaz").ToString) + v("Sprzedaz")
                                    dvK(k)("Q_EBiTDA") = CDbl(dvK(k)("Q_EBiTDA").ToString) + v("EBiTDA")
                                    dvO(i)("Q_Sprzedaz") = CDbl(dvO(i)("Q_Sprzedaz").ToString) + v("Sprzedaz")
                                    dvO(i)("Q_EBiTDA") = CDbl(dvO(i)("Q_EBiTDA").ToString) + v("EBiTDA")
                                End If
                            End If
                        Next v
                        dvK.Sort = Nothing
                        dvO.Sort = Nothing
                        ' przenoszenie do Excel-a
                        If m.Equals(0) Then
                            .oSheet = CType(.oBook.Sheets(o), XL._Worksheet)
                        Else
                            If o.Equals("R1") Then
                                .oSheet = CType(.oBook.Sheets("RM1"), XL._Worksheet)
                            ElseIf o.Equals("R2") Then
                                .oSheet = CType(.oBook.Sheets("RM2"), XL._Worksheet)
                            Else
                                .oSheet = CType(.oBook.Sheets("SD"), XL._Worksheet)
                            End If
                        End If
                        ' ranking miesięczny
                        dvK.Sort = "Mg DESC"
                        dvO.Sort = "Mg DESC"
                        _Mg = 0 : _Sprzedaz = 0 : _EBiTDA = 0 : k = 0
                        For i = 0 To dvO.Count - 1
                            If k.Equals(10) Then
                                _Mg += CDbl(dvO(i)("Mg").ToString)
                                _Sprzedaz += CDbl(dvO(i)("Sprzedaz").ToString)
                                _EBiTDA += CDbl(dvO(i)("EBiTDA").ToString)
                            Else
                                k += 1
                                With .oSheet
                                    .Cells(5 + k, 3) = dvO(i)("Opis")
                                    .Cells(5 + k, 4) = dvO(i)("Mg")
                                    .Cells(5 + k, 5) = dvO(i)("Sprzedaz")
                                    .Cells(5 + k, 7) = dvO(i)("EBiTDA")
                                End With
                            End If
                        Next i
                        With .oSheet
                            .Cells(16, 4) = _Mg
                            .Cells(16, 5) = _Sprzedaz
                            .Cells(16, 7) = _EBiTDA
                        End With
                        _Mg = 0 : _Sprzedaz = 0 : _EBiTDA = 0 : k = 0
                        For i = 0 To dvK.Count - 1
                            If k.Equals(10) Then
                                _Mg += CDbl(dvK(i)("Mg").ToString)
                                _Sprzedaz += CDbl(dvK(i)("Sprzedaz").ToString)
                                _EBiTDA += CDbl(dvK(i)("EBiTDA").ToString)
                            Else
                                k += 1
                                With .oSheet
                                    .Cells(56 + k, 3) = dvK(i)("Opis")
                                    .Cells(56 + k, 4) = dvK(i)("Mg")
                                    .Cells(56 + k, 5) = dvK(i)("Sprzedaz")
                                    .Cells(56 + k, 7) = dvK(i)("EBiTDA")
                                End With
                            End If
                        Next i
                        With .oSheet
                            .Cells(67, 4) = _Mg
                            .Cells(67, 5) = _Sprzedaz
                            .Cells(67, 7) = _EBiTDA
                        End With
                        ' ranking roczny
                        dvK.Sort = Nothing
                        dvO.Sort = Nothing
                        dvK.Sort = "YTD_Mg DESC"
                        dvO.Sort = "YTD_Mg DESC"
                        _Mg = 0 : _Sprzedaz = 0 : _EBiTDA = 0 : k = 0
                        For i = 0 To dvO.Count - 1
                            If k.Equals(10) Then
                                _Mg += CDbl(dvO(i)("YTD_Mg").ToString)
                                _Sprzedaz += CDbl(dvO(i)("YTD_Sprzedaz").ToString)
                                _EBiTDA += CDbl(dvO(i)("YTD_EBiTDA").ToString)
                            Else
                                k += 1
                                With .oSheet
                                    .Cells(22 + k, 3) = dvO(i)("Opis")
                                    .Cells(22 + k, 4) = dvO(i)("YTD_Mg")
                                    .Cells(22 + k, 5) = dvO(i)("YTD_Sprzedaz")
                                    .Cells(22 + k, 7) = dvO(i)("YTD_EBiTDA")
                                End With
                            End If
                        Next i
                        With .oSheet
                            .Cells(33, 4) = _Mg
                            .Cells(33, 5) = _Sprzedaz
                            .Cells(33, 7) = _EBiTDA
                        End With
                        _Mg = 0 : _Sprzedaz = 0 : _EBiTDA = 0 : k = 0
                        For i = 0 To dvK.Count - 1
                            If k.Equals(10) Then
                                _Mg += CDbl(dvK(i)("YTD_Mg").ToString)
                                _Sprzedaz += CDbl(dvK(i)("YTD_Sprzedaz").ToString)
                                _EBiTDA += CDbl(dvK(i)("YTD_EBiTDA").ToString)
                            Else
                                k += 1
                                With .oSheet
                                    .Cells(72 + k, 3) = dvK(i)("Opis")
                                    .Cells(72 + k, 4) = dvK(i)("YTD_Mg")
                                    .Cells(72 + k, 5) = dvK(i)("YTD_Sprzedaz")
                                    .Cells(72 + k, 7) = dvK(i)("YTD_EBiTDA")
                                End With
                            End If
                        Next i
                        With .oSheet
                            .Cells(83, 4) = _Mg
                            .Cells(83, 5) = _Sprzedaz
                            .Cells(83, 7) = _EBiTDA
                        End With
                        ' ranking kwartalny
                        dvK.Sort = Nothing
                        dvO.Sort = Nothing
                        dvK.Sort = "Q_Mg DESC"
                        dvO.Sort = "Q_Mg DESC"
                        _Mg = 0 : _Sprzedaz = 0 : _EBiTDA = 0 : k = 0
                        For i = 0 To dvO.Count - 1
                            If k.Equals(10) Then
                                _Mg += CDbl(dvO(i)("Q_Mg").ToString)
                                _Sprzedaz += CDbl(dvO(i)("Q_Sprzedaz").ToString)
                                _EBiTDA += CDbl(dvO(i)("Q_EBiTDA").ToString)
                            Else
                                k += 1
                                With .oSheet
                                    .Cells(39 + k, 3) = dvO(i)("Opis")
                                    .Cells(39 + k, 4) = dvO(i)("Q_Mg")
                                    .Cells(39 + k, 5) = dvO(i)("Q_Sprzedaz")
                                    .Cells(39 + k, 7) = dvO(i)("Q_EBiTDA")
                                End With
                            End If
                        Next i
                        With .oSheet
                            .Cells(50, 4) = _Mg
                            .Cells(50, 5) = _Sprzedaz
                            .Cells(50, 7) = _EBiTDA
                        End With
                        _Mg = 0 : _Sprzedaz = 0 : _EBiTDA = 0 : k = 0
                        For i = 0 To dvK.Count - 1
                            If k.Equals(10) Then
                                _Mg += CDbl(dvK(i)("Q_Mg").ToString)
                                _Sprzedaz += CDbl(dvK(i)("Q_Sprzedaz").ToString)
                                _EBiTDA += CDbl(dvK(i)("Q_EBiTDA").ToString)
                            Else
                                k += 1
                                With .oSheet
                                    .Cells(88 + k, 3) = dvK(i)("Opis")
                                    .Cells(88 + k, 4) = dvK(i)("Q_Mg")
                                    .Cells(88 + k, 5) = dvK(i)("Q_Sprzedaz")
                                    .Cells(88 + k, 7) = dvK(i)("Q_EBiTDA")
                                End With
                            End If
                        Next i
                        With .oSheet
                            .Cells(99, 4) = _Mg
                            .Cells(99, 5) = _Sprzedaz
                            .Cells(99, 7) = _EBiTDA
                        End With
                    Next o
                Next m
Pokaz:
                .ScreenRefresh(True)
                .Show()
            End With
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message, Me.tsslMain.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Using
        Finally
            da = Nothing
            dt = Nothing
            dtK = Nothing
            dtO = Nothing
            dtW = Nothing
            dv = Nothing
            dvK = Nothing
            dvO = Nothing
            dvW = Nothing
            ClearStatus()
        End Try
    End Sub

    Private Sub AnalizaTransportuZWW() Handles tsm_Est.Click

        ZWW_AnalizaTransportu()
    End Sub

    Private Sub KosztyOverheads() Handles tsm_Edo.Click

        Dim da As OleDbDataAdapter
        Dim dt As New DataTable, dtW As New DataTable
        Dim r As DataRow
        Dim dv As DataView
        Dim w, i, _Lat As Integer
        Dim SQL As String = ""
        Dim m_Language As String = "PL"
        Dim _Mat, _DesMat As String
        Dim dm1, dm2 As Date
        Dim _Wart, _Suma As Double
        Const CO_ACTION As String = "Zestawienie kosztów overheads"
        Try
            Dim FM As New ZaOkres
            With FM
                If .ShowDialog.Equals(Windows.Forms.DialogResult.Cancel) Then Return
                dm1 = .dtpOd.Value
                dm2 = .dtpDo.Value
            End With
            FM = Nothing
            _Lat = dm2.Year - dm1.Year
            If _Lat > 1 Then Throw New ArgumentException("Max. okres to poprzedni i bieżący rok")
            If dm1.AddYears(1).Year.Equals(Me.dtpOkres.Value.Year) Then
                PolaczeniePY()
                _Lat = 1    ' poprzedni rok
                If dm1.AddYears(1).Year.Equals(dm2.Year) Then _Lat += 1 ' dwa lata
            ElseIf dm1.Year.Equals(Me.dtpOkres.Value.Year) Then
                _Lat = 0    ' bieżący rok
            Else
                Throw New ArgumentException("Nie te lata")
            End If
            With dtW.Columns
                .Add("Insta")
                .Add("InstaOpis")
                .Add("MPK")
                .Add("MPKOpis")
                .Add("RK")
                .Add("INEF")
                .Add("INEFOpis")
                .Add("CalCo")
                .Add("CalCoOpis")
                .Add("Dostawca")
                .Add("DostawcaOpis")
                .Add("Material")
                .Add("MaterialOpis")
                .Add("Pion")
                If _Lat.Equals(2) Then
                    For i = 1 To dm2.Month
                        .Add("2_" + i.ToString, GetType(Double))
                    Next i
                    .Add("2_Total", GetType(Double))
                    For i = dm1.Month To 12
                        .Add("1_" + i.ToString, GetType(Double))
                    Next i
                    .Add("1_Total", GetType(Double))
                Else
                    For i = dm1.Month To dm2.Month
                        .Add("2_" + i.ToString, GetType(Double))
                    Next i
                    .Add("2_Total", GetType(Double))
                End If
            End With
            Using New CM
                If MessageBox.Show("Czy chcesz w angielskiej wersji językowej?", CO_ACTION, MessageBoxButtons.YesNo, MessageBoxIcon.Question, _
                                    MessageBoxDefaultButton.Button2).Equals(Windows.Forms.DialogResult.Yes) Then m_Language = "EN"
            End Using
            FillStatusBar(, CO_ACTION)
            dtW.Clear()
            dv = dtW.DefaultView
            dv.Sort = "Insta, MPK, INEF, CalCo, Dostawca, Material, RK"
            If _Lat < 2 Then GoTo JedenRok
            ' 2014-11-17
            ' czemu ma służyć sprawdzanie miesięcy w MPK?!?
            SQL = "SELECT 'Overheads' AS [INSTA], Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, SUM(Z.Wartosc) AS [Wartosc], 'Overheads', M.Pion " _
                  + "FROM (((((tblZWPC Z INNER JOIN tblCalCo C ON Z.CalCo = C.ID) INNER JOIN tblINEF I ON C.INEF = I.ID) " _
                  + "INNER JOIN tblMPK M ON Z.MPK = M.ID) LEFT OUTER JOIN tblMaterialy R ON Z.Material = R.ID) LEFT OUTER JOIN tblDostawcy D ON Z.Dostawca = D.ID) " _
                  + "LEFT OUTER JOIN tblRK A ON Z.RK = A.ID " _
                  + "WHERE Z.Instalacja = 'JAX04' AND Mc >= " + dm1.Month.ToString _
                  + " AND (M.DoOverheads AND Mc >= M.StartOverheads AND Mc <= M.FinishOverheads) " _
                  + "GROUP BY Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, M.Pion " _
                  + "ORDER BY Z.MPK DESC, C.INEF, Z.CalCo, Z.Dostawca, Z.Material, RK, Z.Mc"
            If m_Language.Equals("EN") Then SQL = SQL.Replace(".Opis", ".Description").Replace("R.MaterialPL", "R.MaterialEN")
            da = New OleDbDataAdapter(SQL, Me.cnsPY)
            da.Fill(dt)
            If dt.Rows.Count.Equals(0) Then Throw New ArgumentException("Brak danych")
            With Me
                With .tspbMain
                    .Minimum = 0
                    .Maximum = dt.Rows.Count
                    .Value = 0
                End With
            End With
            Application.DoEvents()
            w = 0
            Do Until w.Equals(dt.Rows.Count)
                r = dt.Rows(w)
                i = dv.Find(New String() {r(0).ToString, r(1).ToString, r(3).ToString, r(5).ToString, r(7).ToString, r(9).ToString, r(11).ToString})
                If i.Equals(-1) Then
                    _Mat = r(9).ToString
                    _DesMat = r(10).ToString
                    If String.IsNullOrEmpty(_Mat) Then _DesMat = r(12).ToString
                    dtW.Rows.Add(New Object() {r(0).ToString, r(15).ToString, r(1).ToString, r(2).ToString, r(11).ToString, r(3).ToString, r(4).ToString, r(5).ToString, _
                                               r(6).ToString, r(7).ToString, r(8).ToString, _Mat, _DesMat, ""})
                    i = dv.Find(New String() {r(0).ToString, r(1).ToString, r(3).ToString, r(5).ToString, r(7).ToString, r(9).ToString, r(11).ToString})
                End If
                With dv(i)
                    Try
                        _Wart = CDbl(r("Wartosc").ToString)
                    Catch ex As Exception
                        _Wart = 0
                    End Try
                    Try
                        _Suma = CDbl(.Item("1_" + r("Mc").ToString))
                    Catch ex As Exception
                        _Suma = 0
                    End Try
                    .Item("1_" + r("Mc").ToString) = _Suma + _Wart
                    Try
                        _Suma = CDbl(.Item("1_Total").ToString)
                    Catch ex As Exception
                        _Suma = 0
                    End Try
                    .Item("1_Total") = _Suma + _Wart
                    .EndEdit()
                End With
                If (w Mod 500).Equals(0) Then _
                    Me.tspbMain.Value = w : Application.DoEvents()
                w += 1
            Loop
JedenRok:
            If Not _Lat.Equals(2) Then dtW.Clear()
            If dm1.Year.Equals(dm2.Year) Then
                SQL = "SELECT 'Overheads' AS [INSTA], Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, SUM(Z.Wartosc) AS [Wartosc], 'Overheads', M.Pion " _
                      + "FROM (((((tblZWPC Z INNER JOIN tblCalCo C ON Z.CalCo = C.ID) INNER JOIN tblINEF I ON C.INEF = I.ID) " _
                      + "INNER JOIN tblMPK M ON Z.MPK = M.ID) LEFT OUTER JOIN tblMaterialy R ON Z.Material = R.ID) LEFT OUTER JOIN tblDostawcy D ON Z.Dostawca = D.ID) " _
                      + "LEFT OUTER JOIN tblRK A ON Z.RK = A.ID " _
                      + "WHERE Z.Instalacja = 'JAX04' AND Mc >= " + dm1.Month.ToString + " AND Mc <= " + dm2.Month.ToString _
                      + " AND (M.DoOverheads AND Mc >= M.StartOverheads AND Mc <= M.FinishOverheads) " _
                      + "GROUP BY Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, M.Pion " _
                      + "ORDER BY Z.MPK DESC, C.INEF, Z.CalCo, Z.Dostawca, Z.Material, RK, Z.Mc"
            Else
                SQL = "SELECT 'Overheads' AS [INSTA], Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, SUM(Z.Wartosc) AS [Wartosc], 'Overheads', M.Pion " _
                      + "FROM (((((tblZWPC Z INNER JOIN tblCalCo C ON Z.CalCo = C.ID) INNER JOIN tblINEF I ON C.INEF = I.ID) " _
                      + "INNER JOIN tblMPK M ON Z.MPK = M.ID) LEFT OUTER JOIN tblMaterialy R ON Z.Material = R.ID) LEFT OUTER JOIN tblDostawcy D ON Z.Dostawca = D.ID) " _
                      + "LEFT OUTER JOIN tblRK A ON Z.RK = A.ID " _
                      + "WHERE Z.Instalacja = 'JAX04' AND Mc <= " + dm2.Month.ToString _
                      + " AND (M.DoOverheads AND Mc >= M.StartOverheads AND Mc <= M.FinishOverheads) " _
                      + "GROUP BY Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, M.Pion " _
                      + "ORDER BY Z.MPK DESC, C.INEF, Z.CalCo, Z.Dostawca, Z.Material, RK, Z.Mc"
            End If
            If m_Language.Equals("EN") Then SQL = SQL.Replace(".Opis", ".Description").Replace("R.MaterialPL", "R.MaterialEN")
            dt = New DataTable
            da = New OleDbDataAdapter(SQL, Me.cns)
            da.Fill(dt)
            If dt.Rows.Count.Equals(0) Then Throw New ArgumentException("Brak danych")
            With Me
                With .tspbMain
                    .Minimum = 0
                    .Maximum = dt.Rows.Count
                    .Value = 0
                End With
            End With
            Application.DoEvents()
            w = 0
            Do Until w.Equals(dt.Rows.Count)
                r = dt.Rows(w)
                i = dv.Find(New String() {r(0).ToString, r(1).ToString, r(3).ToString, r(5).ToString, r(7).ToString, r(9).ToString, r(11).ToString})
                If i.Equals(-1) Then
                    _Mat = r(9).ToString
                    _DesMat = r(10).ToString
                    If String.IsNullOrEmpty(_Mat) Then _DesMat = r(12).ToString
                    dtW.Rows.Add(New Object() {r(0).ToString, r(15).ToString, r(1).ToString, r(2).ToString, r(11).ToString, r(3).ToString, r(4).ToString, r(5).ToString, _
                                               r(6).ToString, r(7).ToString, r(8).ToString, _Mat, _DesMat, ""})
                    i = dv.Find(New String() {r(0).ToString, r(1).ToString, r(3).ToString, r(5).ToString, r(7).ToString, r(9).ToString, r(11).ToString})
                End If
                With dv(i)
                    Try
                        _Wart = CDbl(r("Wartosc").ToString)
                    Catch ex As Exception
                        _Wart = 0
                    End Try
                    Try
                        _Suma = CDbl(.Item("2_" + r("Mc").ToString))
                    Catch ex As Exception
                        _Suma = 0
                    End Try
                    .Item("2_" + r("Mc").ToString) = _Suma + _Wart
                    Try
                        _Suma = CDbl(.Item("2_Total").ToString)
                    Catch ex As Exception
                        _Suma = 0
                    End Try
                    .Item("2_Total") = _Suma + _Wart
                    .EndEdit()
                End With
                If (w Mod 500).Equals(0) Then _
                    Me.tspbMain.Value = w : Application.DoEvents()
                w += 1
            Loop
            Dim m_WB As String = Nothing
            If NotExportFile(m_WB) Then Exit Try
            Dim XE As New Cl4Excel.Excel(m_WB, , , "m")
            XLKosztyDostawcami(dv, m_Language, dm1.Year, dm2.Year, XE)
            With XE
                With .oBook
                    .Saved = True
                    .Close()
                End With
                .oBook = .oApp.Workbooks.Open("W:\Costs\CELSA\Koszty wg Dostawcy\Overheads.xls")
                HideScreen()
                .Show()
            End With
            HideScreen(False)
        Catch ex As Exception
            ClearStatus()
            Using New CM
                MessageBox.Show(ex.Message, CO_ACTION, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
        Finally
            da = Nothing
            dt = Nothing
        End Try
    End Sub

    Private Sub KosztyDostawcow(ByVal sender As System.Object, ByVal e As System.EventArgs) _
                                Handles tsm_Edp.Click, _
                                        tsm_EdI.Click, _
                                        tsm_EdC.Click

        Dim da As OleDbDataAdapter
        Dim dt As New DataTable, dtW As New DataTable
        Dim dv As DataView
        Dim r As DataRow
        Dim vF() As DataRowView
        Dim g, i, w, _Lat, _Mod As Integer
        Dim SQL As String = ""
        Dim ctrl As ToolStripMenuItem = CType(sender, ToolStripMenuItem)
        Dim m_Language As String = "PL"
        Dim _Mat, _DesMat As String
        Dim dm1, dm2 As Date
        Dim _Wart, _Suma As Double
        Const CO_ACTION As String = "Zestawienie kosztów w podziale na dostawców"
        Try
            Dim FM As New ZaOkres
            With FM
                If .ShowDialog.Equals(Windows.Forms.DialogResult.Cancel) Then Return
                dm1 = .dtpOd.Value
                dm2 = .dtpDo.Value
            End With
            FM = Nothing
            _Lat = dm2.Year - dm1.Year
            If _Lat > 1 Then Throw New ArgumentException("Max. okres to poprzedni i bieżący rok")
            If dm1.AddYears(1).Year.Equals(Me.dtpOkres.Value.Year) Then
                PolaczeniePY()
                _Lat = 1    ' poprzedni rok
                If dm1.AddYears(1).Year.Equals(dm2.Year) Then _Lat += 1 ' dwa lata
            ElseIf dm1.Year.Equals(Me.dtpOkres.Value.Year) Then
                _Lat = 0    ' bieżący rok
            Else
                Throw New ArgumentException("Nie te lata")
            End If
            Dim m_WB As String = Nothing
            If NotExportFile(m_WB) Then Exit Try
            Using New CM
                If MessageBox.Show("Czy chcesz w angielskiej wersji językowej?", CO_ACTION, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2).Equals( _
                                Windows.Forms.DialogResult.Yes) Then m_Language = "EN"
            End Using
            With dtW.Columns
                .Add("Insta")
                .Add("InstaOpis")
                .Add("MPK")
                .Add("MPKOpis")
                .Add("RK")
                .Add("INEF")
                .Add("INEFOpis")
                .Add("CalCo")
                .Add("CalCoOpis")
                .Add("Dostawca")
                .Add("DostawcaOpis")
                .Add("Material")
                .Add("MaterialOpis")
                .Add("Pion")
                If _Lat.Equals(2) Then
                    
                    For i = 1 To dm2.Month
                        .Add("2_" + i.ToString, GetType(Double))
                    Next i
                    .Add("2_Total", GetType(Double))
                    For i = dm1.Month To 12
                        .Add("1_" + i.ToString, GetType(Double))
                    Next i
                    .Add("1_Total", GetType(Double))

                Else
                    For i = dm1.Month To dm2.Month
                        .Add("2_" + i.ToString, GetType(Double))
                    Next i
                    .Add("2_Total", GetType(Double))
                End If
            End With
            Me.Cursor = Cursors.WaitCursor
            Dim XE As New Cl4Excel.Excel(m_WB)
            With ctrl.Name
                If .EndsWith("p") OrElse .EndsWith("C") Then GoTo Podstawowe
            End With
            For g = 0 To 5
                FillStatusBar(, CO_ACTION + " - cz. " & g + 1 & "/6")
                dtW.Clear()
                dv = dtW.DefaultView
                dv.Sort = "Insta, MPK, INEF, CalCo, Dostawca, Material, RK"
                If _Lat < 2 Then GoTo JedenRok
                Select Case g
                    Case 0  ' produkcyjne + JAX03 (wykluczone JAX04, JMS02 i JMS03)
                        SQL = "SELECT Z.Instalacja, Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, SUM(Z.Wartosc) AS [Wartosc], S.Opis, M.Pion " _
                          + "FROM ((((((tblZWPC Z INNER JOIN tblCalCo C ON Z.CalCo = C.ID) INNER JOIN tblINEF I ON C.INEF = I.ID) " _
                          + "INNER JOIN tblMPK M ON Z.MPK = M.ID) LEFT JOIN tblMaterialy R ON Z.Material = R.ID) " _
                          + "INNER JOIN tblInstalacje S ON Z.Instalacja = S.ID) LEFT JOIN tblDostawcy D ON Z.Dostawca = D.ID) " _
                          + "LEFT JOIN tblRK A ON Z.RK = A.ID " _
                          + "WHERE Mc >= " + dm1.Month.ToString + " AND NOT Z.Typ IN ('A', 'F', 'O', 'R', 'S') " _
                          + "AND NOT Z.Instalacja IN ('JAX04', 'JMS02', 'JMS03') AND I.Wybrane " _
                          + "GROUP BY Z.Instalacja, S.Opis, Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, M.Pion " _
                          + "ORDER BY Z.Instalacja, Z.MPK DESC, C.INEF, Z.CalCo, Z.Dostawca, Z.Material, RK, Z.Mc"
                    Case 1  ' JAX04
                        SQL = "SELECT Z.Instalacja, Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, SUM(Z.Wartosc) AS [Wartosc], S.Opis, M.Pion " _
                          + "FROM ((((((tblZWPC Z INNER JOIN tblCalCo C ON Z.CalCo = C.ID) INNER JOIN tblINEF I ON C.INEF = I.ID) " _
                          + "INNER JOIN tblMPK M ON Z.MPK = M.ID) LEFT JOIN tblMaterialy R ON Z.Material = R.ID) " _
                          + "INNER JOIN tblInstalacje S ON Z.Instalacja = S.ID) LEFT JOIN tblDostawcy D ON Z.Dostawca = D.ID) " _
                          + "LEFT JOIN tblRK A ON Z.RK = A.ID " _
                          + "WHERE Mc >= " + dm1.Month.ToString + " AND NOT Z.Typ IN ('F', 'O', 'R', 'S') " _
                          + "AND Z.Instalacja = 'JAX04' AND NOT (Z.MPK LIKE 'JTEL%' OR Z.MPK LIKE 'JTGN%' OR Z.MPK LIKE 'JTWR%') AND I.Wybrane " _
                          + "GROUP BY Z.Instalacja, S.Opis, Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, M.Pion " _
                          + "ORDER BY Z.Instalacja, Z.MPK DESC, C.INEF, Z.CalCo, Z.Dostawca, Z.Material, RK, Z.Mc"
                    Case 2  ' JTEL
                        SQL = "SELECT 'JTEL', Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, SUM(Z.Wartosc) AS [Wartosc], 'Elektryczny', M.Pion " _
                          + "FROM (((((tblZWPC Z INNER JOIN tblCalCo C ON Z.CalCo = C.ID) INNER JOIN tblINEF I ON C.INEF = I.ID) " _
                          + "INNER JOIN tblMPK M ON Z.MPK = M.ID) LEFT JOIN tblMaterialy R ON Z.Material = R.ID) " _
                          + "LEFT JOIN tblDostawcy D ON Z.Dostawca = D.ID) " _
                          + "LEFT JOIN tblRK A ON Z.RK = A.ID " _
                          + "WHERE Mc >= " + dm1.Month.ToString + " AND NOT Z.Typ IN ('F', 'O', 'R', 'S') " _
                          + "AND Z.Instalacja = 'JAX04' AND Z.MPK LIKE 'JTEL%' AND I.Wybrane " _
                          + "GROUP BY Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, M.Pion " _
                          + "ORDER BY Z.MPK DESC, C.INEF, Z.CalCo, Z.Dostawca, Z.Material, RK, Z.Mc"
                    Case 3  ' JTGN
                        SQL = "SELECT 'JTGN', Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, SUM(Z.Wartosc) AS [Wartosc], 'OGGS', M.Pion " _
                          + "FROM (((((tblZWPC Z INNER JOIN tblCalCo C ON Z.CalCo = C.ID) INNER JOIN tblINEF I ON C.INEF = I.ID) " _
                          + "INNER JOIN tblMPK M ON Z.MPK = M.ID) LEFT JOIN tblMaterialy R ON Z.Material = R.ID) " _
                          + "LEFT JOIN tblDostawcy D ON Z.Dostawca = D.ID) " _
                          + "LEFT JOIN tblRK A ON Z.RK = A.ID " _
                          + "WHERE Mc >= " + dm1.Month.ToString + " AND NOT Z.Typ IN ('F', 'O', 'R', 'S') " _
                          + "AND Z.Instalacja = 'JAX04' AND Z.MPK LIKE 'JTGN%' AND I.Wybrane " _
                          + "GROUP BY Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, M.Pion " _
                          + "ORDER BY Z.MPK DESC, C.INEF, Z.CalCo, Z.Dostawca, Z.Material, RK, Z.Mc"
                    Case 4  ' JTWR
                        SQL = "SELECT 'JTWR', Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, SUM(Z.Wartosc) AS [Wartosc], 'OGWS', M.Pion " _
                          + "FROM (((((tblZWPC Z INNER JOIN tblCalCo C ON Z.CalCo = C.ID) INNER JOIN tblINEF I ON C.INEF = I.ID) " _
                          + "INNER JOIN tblMPK M ON Z.MPK = M.ID) LEFT JOIN tblMaterialy R ON Z.Material = R.ID) " _
                          + "LEFT JOIN tblDostawcy D ON Z.Dostawca = D.ID) " _
                          + "LEFT JOIN tblRK A ON Z.RK = A.ID " _
                          + "WHERE Mc >= " + dm1.Month.ToString + " AND NOT Z.Typ IN ('F', 'O', 'R', 'S') " _
                          + "AND Z.Instalacja = 'JAX04' AND Z.MPK LIKE 'JTWR%' AND I.Wybrane " _
                          + "GROUP BY Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, M.Pion " _
                          + "ORDER BY Z.MPK DESC, C.INEF, Z.CalCo, Z.Dostawca, Z.Material, RK, Z.Mc"
                    Case 5  'Stalownia ZWK
                        SQL = "SELECT 'JMS02' AS [INSTA], Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, SUM(Z.Wartosc) AS [Wartosc], 'Stalownia ZWK', M.Pion " _
                          + "FROM (((((tblZWPC Z INNER JOIN tblCalCo C ON Z.CalCo = C.ID) INNER JOIN tblINEF I ON C.INEF = I.ID) " _
                          + "INNER JOIN tblMPK M ON Z.MPK = M.ID) LEFT JOIN tblMaterialy R ON Z.Material = R.ID) LEFT JOIN tblDostawcy D ON Z.Dostawca = D.ID) " _
                          + "LEFT JOIN tblRK A ON Z.RK = A.ID " _
                          + "WHERE Mc >= " + dm1.Month.ToString + " AND NOT Z.Typ IN ('A', 'F', 'O', 'R', 'S') " _
                          + "AND Z.Instalacja IN ('JMS02', 'JMS03') AND I.Wybrane " _
                          + "GROUP BY Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, M.Pion " _
                          + "ORDER BY Z.MPK DESC, C.INEF, Z.CalCo, Z.Dostawca, Z.Material, RK, Z.Mc"
                End Select
                If m_Language.Equals("EN") Then SQL = SQL.Replace(".Opis", ".Description").Replace("R.MaterialPL", "R.MaterialEN").Replace("Walcownia Średnia", "Rolling Mill 2").Replace("Stalownia ZWK", "Melt Shop FD")
                dt = New DataTable
                da = New OleDbDataAdapter(SQL, Me.cnsPY)
                da.Fill(dt)
                With Me
                    With .tspbMain
                        .Minimum = 0
                        .Maximum = dt.Rows.Count
                        .Value = 0
                        _Mod = .Maximum \ 20
                        If _Mod.Equals(0) Then _Mod = 1
                    End With
                End With
                Application.DoEvents()
                w = 0
                Do Until w.Equals(dt.Rows.Count)
                    r = dt.Rows(w)
                    vF = dv.FindRows(New String() {r(0).ToString, r(1).ToString, r(3).ToString, r(5).ToString, r(7).ToString, r(9).ToString, r(11).ToString})
                    If vF.Length.Equals(0) Then
                        _Mat = r(9).ToString
                        _DesMat = r(10).ToString
                        If String.IsNullOrEmpty(_Mat) Then _DesMat = r(12).ToString
                        dtW.Rows.Add(New Object() {r(0).ToString, r(15).ToString, r(1).ToString, r(2).ToString, r(11).ToString, r(3).ToString, r(4).ToString, r(5).ToString, _
                                                   r(6).ToString, r(7).ToString, r(8).ToString, _Mat, _DesMat, r(16).ToString})
                        vF = dv.FindRows(New String() {r(0).ToString, r(1).ToString, r(3).ToString, r(5).ToString, r(7).ToString, r(9).ToString, r(11).ToString})
                    End If
                    With vF(0)
                        Try
                            _Wart = CDbl(r("Wartosc").ToString)
                        Catch ex As Exception
                            _Wart = 0
                        End Try
                        Try
                            _Suma = CDbl(.Item("1_" + r("Mc").ToString))
                        Catch ex As Exception
                            _Suma = 0
                        End Try
                        .Item("1_" + r("Mc").ToString) = _Suma + _Wart
                        Try
                            _Suma = CDbl(.Item("1_Total").ToString)
                        Catch ex As Exception
                            _Suma = 0
                        End Try
                        .Item("1_Total") = _Suma + _Wart
                        .EndEdit()
                    End With
                    If (w Mod _Mod).Equals(0) Then _
                        Me.tspbMain.Value = w : Application.DoEvents()
                    w += 1
                Loop
JedenRok:
                If Not _Lat.Equals(2) Then dtW.Clear()
                If dm1.Year.Equals(dm2.Year) Then
                    Select Case g
                        Case 0  ' produkcyjne + JAX03 (wykluczone JAX04, JMS02 i JMS03)
                            SQL = "SELECT Z.Instalacja, Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, SUM(Z.Wartosc) AS [Wartosc], S.Opis, M.Pion " _
                              + "FROM ((((((tblZWPC Z INNER JOIN tblCalCo C ON Z.CalCo = C.ID) INNER JOIN tblINEF I ON C.INEF = I.ID) " _
                              + "INNER JOIN tblMPK M ON Z.MPK = M.ID) LEFT JOIN tblMaterialy R ON Z.Material = R.ID) " _
                              + "INNER JOIN tblInstalacje S ON Z.Instalacja = S.ID) LEFT JOIN tblDostawcy D ON Z.Dostawca = D.ID) " _
                              + "LEFT JOIN tblRK A ON Z.RK = A.ID " _
                              + "WHERE Mc >= " + dm1.Month.ToString + " AND Mc <= " + dm2.Month.ToString + " AND NOT Z.Typ IN ('A', 'F', 'O', 'R', 'S') " _
                              + "AND NOT Z.Instalacja IN ('JAX04', 'JMS02', 'JMS03') AND I.Wybrane " _
                              + "GROUP BY Z.Instalacja, S.Opis, Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, M.Pion " _
                              + "ORDER BY Z.Instalacja, Z.MPK DESC, C.INEF, Z.CalCo, Z.Dostawca, Z.Material, RK, Z.Mc"
                        Case 1  ' JAX04
                            SQL = "SELECT Z.Instalacja, Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, SUM(Z.Wartosc) AS [Wartosc], S.Opis " _
                              + "FROM ((((((tblZWPC Z INNER JOIN tblCalCo C ON Z.CalCo = C.ID) INNER JOIN tblINEF I ON C.INEF = I.ID) " _
                              + "INNER JOIN tblMPK M ON Z.MPK = M.ID) LEFT JOIN tblMaterialy R ON Z.Material = R.ID) " _
                              + "INNER JOIN tblInstalacje S ON Z.Instalacja = S.ID) LEFT JOIN tblDostawcy D ON Z.Dostawca = D.ID) " _
                              + "LEFT JOIN tblRK A ON Z.RK = A.ID " _
                              + "WHERE Mc >= " + dm1.Month.ToString + " AND Mc <= " + dm2.Month.ToString + " AND NOT Z.Typ IN ('F', 'O', 'R', 'S') " _
                              + "AND Z.Instalacja = 'JAX04' AND NOT (Z.MPK LIKE 'JTEL%' OR Z.MPK LIKE 'JTGN%' OR Z.MPK LIKE 'JTWR%') AND I.Wybrane " _
                              + "GROUP BY Z.Instalacja, S.Opis, Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, M.Pion " _
                              + "ORDER BY Z.Instalacja, Z.MPK DESC, C.INEF, Z.CalCo, Z.Dostawca, Z.Material, RK, Z.Mc"
                        Case 2  ' JTEL
                            SQL = "SELECT 'JTEL', Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, SUM(Z.Wartosc) AS [Wartosc], 'Elektryczny', M.Pion " _
                              + "FROM (((((tblZWPC Z INNER JOIN tblCalCo C ON Z.CalCo = C.ID) INNER JOIN tblINEF I ON C.INEF = I.ID) " _
                              + "INNER JOIN tblMPK M ON Z.MPK = M.ID) LEFT JOIN tblMaterialy R ON Z.Material = R.ID) " _
                              + "LEFT JOIN tblDostawcy D ON Z.Dostawca = D.ID) " _
                              + "LEFT JOIN tblRK A ON Z.RK = A.ID " _
                              + "WHERE Mc >= " + dm1.Month.ToString + " AND Mc <= " + dm2.Month.ToString + " AND NOT Z.Typ IN ('F', 'O', 'R', 'S') " _
                              + "AND Z.Instalacja = 'JAX04' AND Z.MPK LIKE 'JTEL%' AND I.Wybrane " _
                              + "GROUP BY Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, M.Pion " _
                              + "ORDER BY Z.MPK DESC, C.INEF, Z.CalCo, Z.Dostawca, Z.Material, RK, Z.Mc"
                        Case 3  ' JTGN
                            SQL = "SELECT 'JTGN', Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, SUM(Z.Wartosc) AS [Wartosc], 'OGGS', M.Pion " _
                              + "FROM (((((tblZWPC Z INNER JOIN tblCalCo C ON Z.CalCo = C.ID) INNER JOIN tblINEF I ON C.INEF = I.ID) " _
                              + "INNER JOIN tblMPK M ON Z.MPK = M.ID) LEFT JOIN tblMaterialy R ON Z.Material = R.ID) " _
                              + "LEFT JOIN tblDostawcy D ON Z.Dostawca = D.ID) " _
                              + "LEFT JOIN tblRK A ON Z.RK = A.ID " _
                              + "WHERE Mc >= " + dm1.Month.ToString + " AND Mc <= " + dm2.Month.ToString + " AND NOT Z.Typ IN ('F', 'O', 'R', 'S') " _
                              + "AND Z.Instalacja = 'JAX04' AND Z.MPK LIKE 'JTGN%' AND I.Wybrane " _
                              + "GROUP BY Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, M.Pion " _
                              + "ORDER BY Z.MPK DESC, C.INEF, Z.CalCo, Z.Dostawca, Z.Material, RK, Z.Mc"
                        Case 4  ' JTWR
                            SQL = "SELECT 'JTWR', Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, SUM(Z.Wartosc) AS [Wartosc], 'OGWS', M.Pion " _
                              + "FROM (((((tblZWPC Z INNER JOIN tblCalCo C ON Z.CalCo = C.ID) INNER JOIN tblINEF I ON C.INEF = I.ID) " _
                              + "INNER JOIN tblMPK M ON Z.MPK = M.ID) LEFT JOIN tblMaterialy R ON Z.Material = R.ID) " _
                              + "LEFT JOIN tblDostawcy D ON Z.Dostawca = D.ID) " _
                              + "LEFT JOIN tblRK A ON Z.RK = A.ID " _
                              + "WHERE Mc >= " + dm1.Month.ToString + " AND Mc <= " + dm2.Month.ToString + " AND NOT Z.Typ IN ('F', 'O', 'R', 'S') " _
                              + "AND Z.Instalacja = 'JAX04' AND Z.MPK LIKE 'JTWR%' AND I.Wybrane " _
                              + "GROUP BY Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, M.Pion " _
                              + "ORDER BY Z.MPK DESC, C.INEF, Z.CalCo, Z.Dostawca, Z.Material, RK, Z.Mc"
                        Case 5  'Stalownia ZWK
                            SQL = "SELECT 'JMS02' AS [INSTA], Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, SUM(Z.Wartosc) AS [Wartosc], 'Stalownia ZWK', M.Pion " _
                              + "FROM (((((tblZWPC Z INNER JOIN tblCalCo C ON Z.CalCo = C.ID) INNER JOIN tblINEF I ON C.INEF = I.ID) " _
                              + "INNER JOIN tblMPK M ON Z.MPK = M.ID) LEFT JOIN tblMaterialy R ON Z.Material = R.ID) LEFT JOIN tblDostawcy D ON Z.Dostawca = D.ID) " _
                              + "LEFT JOIN tblRK A ON Z.RK = A.ID " _
                              + "WHERE Mc >= " + dm1.Month.ToString + " AND Mc <= " + dm2.Month.ToString + " AND NOT Z.Typ IN ('A', 'F', 'O', 'R', 'S') " _
                              + "AND Z.Instalacja IN ('JMS02', 'JMS03') AND I.Wybrane " _
                              + "GROUP BY Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, M.Pion " _
                              + "ORDER BY Z.MPK DESC, C.INEF, Z.CalCo, Z.Dostawca, Z.Material, RK, Z.Mc"
                    End Select
                Else
                    Select Case g
                        Case 0  ' produkcyjne + JAX03 (wykluczone JAX04, JMS02 i JMS03)
                            SQL = "SELECT Z.Instalacja, Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, SUM(Z.Wartosc) AS [Wartosc], S.Opis, M.Pion " _
                              + "FROM ((((((tblZWPC Z INNER JOIN tblCalCo C ON Z.CalCo = C.ID) INNER JOIN tblINEF I ON C.INEF = I.ID) " _
                              + "INNER JOIN tblMPK M ON Z.MPK = M.ID) LEFT JOIN tblMaterialy R ON Z.Material = R.ID) " _
                              + "INNER JOIN tblInstalacje S ON Z.Instalacja = S.ID) LEFT JOIN tblDostawcy D ON Z.Dostawca = D.ID) " _
                              + "LEFT JOIN tblRK A ON Z.RK = A.ID " _
                              + "WHERE Mc <= " + dm2.Month.ToString + " AND NOT Z.Typ IN ('A', 'F', 'O', 'R', 'S') " _
                              + "AND NOT Z.Instalacja IN ('JAX04', 'JMS02', 'JMS03') AND I.Wybrane " _
                              + "GROUP BY Z.Instalacja, S.Opis, Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, M.Pion " _
                              + "ORDER BY Z.Instalacja, Z.MPK DESC, C.INEF, Z.CalCo, Z.Dostawca, Z.Material, RK, Z.Mc"
                        Case 1  ' JAX04
                            SQL = "SELECT Z.Instalacja, Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, SUM(Z.Wartosc) AS [Wartosc], S.Opis, M.Pion " _
                              + "FROM ((((((tblZWPC Z INNER JOIN tblCalCo C ON Z.CalCo = C.ID) INNER JOIN tblINEF I ON C.INEF = I.ID) " _
                              + "INNER JOIN tblMPK M ON Z.MPK = M.ID) LEFT JOIN tblMaterialy R ON Z.Material = R.ID) " _
                              + "INNER JOIN tblInstalacje S ON Z.Instalacja = S.ID) LEFT JOIN tblDostawcy D ON Z.Dostawca = D.ID) " _
                              + "LEFT JOIN tblRK A ON Z.RK = A.ID " _
                              + "WHERE Mc <= " + dm2.Month.ToString + " AND NOT Z.Typ IN ('F', 'O', 'R', 'S') " _
                              + "AND Z.Instalacja = 'JAX04' AND NOT (Z.MPK LIKE 'JTEL%' OR Z.MPK LIKE 'JTGN%' OR Z.MPK LIKE 'JTWR%') AND I.Wybrane " _
                              + "GROUP BY Z.Instalacja, S.Opis, Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, M.Pion " _
                              + "ORDER BY Z.Instalacja, Z.MPK DESC, C.INEF, Z.CalCo, Z.Dostawca, Z.Material, RK, Z.Mc"
                        Case 2  ' JTEL
                            SQL = "SELECT 'JTEL', Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, SUM(Z.Wartosc) AS [Wartosc], 'Elektryczny', M.Pion " _
                              + "FROM (((((tblZWPC Z INNER JOIN tblCalCo C ON Z.CalCo = C.ID) INNER JOIN tblINEF I ON C.INEF = I.ID) " _
                              + "INNER JOIN tblMPK M ON Z.MPK = M.ID) LEFT JOIN tblMaterialy R ON Z.Material = R.ID) " _
                              + "LEFT JOIN tblDostawcy D ON Z.Dostawca = D.ID) " _
                              + "LEFT JOIN tblRK A ON Z.RK = A.ID " _
                              + "WHERE Mc <= " + dm2.Month.ToString + " AND NOT Z.Typ IN ('F', 'O', 'R', 'S') " _
                              + "AND Z.Instalacja = 'JAX04' AND Z.MPK LIKE 'JTEL%' AND I.Wybrane " _
                              + "GROUP BY Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, M.Pion " _
                              + "ORDER BY Z.MPK DESC, C.INEF, Z.CalCo, Z.Dostawca, Z.Material, RK, Z.Mc"
                        Case 3  ' JTGN
                            SQL = "SELECT 'JTGN', Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, SUM(Z.Wartosc) AS [Wartosc], 'OGGS', M.Pion " _
                              + "FROM (((((tblZWPC Z INNER JOIN tblCalCo C ON Z.CalCo = C.ID) INNER JOIN tblINEF I ON C.INEF = I.ID) " _
                              + "INNER JOIN tblMPK M ON Z.MPK = M.ID) LEFT JOIN tblMaterialy R ON Z.Material = R.ID) " _
                              + "LEFT JOIN tblDostawcy D ON Z.Dostawca = D.ID) " _
                              + "LEFT JOIN tblRK A ON Z.RK = A.ID " _
                              + "WHERE Mc <= " + dm2.Month.ToString + " AND NOT Z.Typ IN ('F', 'O', 'R', 'S') " _
                              + "AND Z.Instalacja = 'JAX04' AND Z.MPK LIKE 'JTGN%' AND I.Wybrane " _
                              + "GROUP BY Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, M.Pion " _
                              + "ORDER BY Z.MPK DESC, C.INEF, Z.CalCo, Z.Dostawca, Z.Material, RK, Z.Mc"
                        Case 4  ' JTWR
                            SQL = "SELECT 'JTWR', Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, SUM(Z.Wartosc) AS [Wartosc], 'OGWS', M.Pion " _
                              + "FROM (((((tblZWPC Z INNER JOIN tblCalCo C ON Z.CalCo = C.ID) INNER JOIN tblINEF I ON C.INEF = I.ID) " _
                              + "INNER JOIN tblMPK M ON Z.MPK = M.ID) LEFT JOIN tblMaterialy R ON Z.Material = R.ID) " _
                              + "LEFT JOIN tblDostawcy D ON Z.Dostawca = D.ID) " _
                              + "LEFT JOIN tblRK A ON Z.RK = A.ID " _
                              + "WHERE Mc <= " + dm2.Month.ToString + " AND NOT Z.Typ IN ('F', 'O', 'R', 'S') " _
                              + "AND Z.Instalacja = 'JAX04' AND Z.MPK LIKE 'JTWR%' AND I.Wybrane " _
                              + "GROUP BY Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, M.Pion " _
                              + "ORDER BY Z.MPK DESC, C.INEF, Z.CalCo, Z.Dostawca, Z.Material, RK, Z.Mc"
                        Case 5  'Stalownia ZWK
                            SQL = "SELECT 'JMS02' AS [INSTA], Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, SUM(Z.Wartosc) AS [Wartosc], 'Stalownia ZWK', M.Pion " _
                              + "FROM (((((tblZWPC Z INNER JOIN tblCalCo C ON Z.CalCo = C.ID) INNER JOIN tblINEF I ON C.INEF = I.ID) " _
                              + "INNER JOIN tblMPK M ON Z.MPK = M.ID) LEFT JOIN tblMaterialy R ON Z.Material = R.ID) LEFT JOIN tblDostawcy D ON Z.Dostawca = D.ID) " _
                              + "LEFT JOIN tblRK A ON Z.RK = A.ID " _
                              + "WHERE Mc <= " + dm2.Month.ToString + " AND NOT Z.Typ IN ('A', 'F', 'O', 'R', 'S') " _
                              + "AND Z.Instalacja IN ('JMS02', 'JMS03') AND I.Wybrane " _
                              + "GROUP BY Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, M.Pion " _
                              + "ORDER BY Z.MPK DESC, C.INEF, Z.CalCo, Z.Dostawca, Z.Material, RK, Z.Mc"
                    End Select
                End If
                If m_Language.Equals("EN") Then SQL = SQL.Replace(".Opis", ".Description").Replace("R.MaterialPL", "R.MaterialEN").Replace("Walcownia Średnia", "Rolling Mill 2").Replace("Stalownia ZWK", "Melt Shop FD")
                dt = New DataTable
                da = New OleDbDataAdapter(SQL, Me.cns)
                da.Fill(dt)
                With Me
                    With .tspbMain
                        .Minimum = 0
                        .Maximum = dt.Rows.Count
                        .Value = 0
                        _Mod = .Maximum \ 20
                        If _Mod.Equals(0) Then _Mod = 1
                    End With
                End With
                Application.DoEvents()
                w = 0
                Do Until w.Equals(dt.Rows.Count)
                    r = dt.Rows(w)
                    vF = dv.FindRows(New String() {r(0).ToString, r(1).ToString, r(3).ToString, r(5).ToString, r(7).ToString, r(9).ToString, r(11).ToString})
                    If vF.Length.Equals(0) Then
                        _Mat = r(9).ToString
                        _DesMat = r(10).ToString
                        If String.IsNullOrEmpty(_Mat) Then _DesMat = r(12).ToString
                        dtW.Rows.Add(New Object() {r(0).ToString, r(15).ToString, r(1).ToString, r(2).ToString, r(11).ToString, r(3).ToString, r(4).ToString, r(5).ToString, _
                                                   r(6).ToString, r(7).ToString, r(8).ToString, _Mat, _DesMat, r(16).ToString})
                        vF = dv.FindRows(New String() {r(0).ToString, r(1).ToString, r(3).ToString, r(5).ToString, r(7).ToString, r(9).ToString, r(11).ToString})
                    End If
                    With vF(0)
                        Try
                            _Wart = CDbl(r("Wartosc").ToString)
                        Catch ex As Exception
                            _Wart = 0
                        End Try
                        Try
                            _Suma = CDbl(.Item("2_" + r("Mc").ToString))
                        Catch ex As Exception
                            _Suma = 0
                        End Try
                        .Item("2_" + r("Mc").ToString) = _Suma + _Wart
                        Try
                            _Suma = CDbl(.Item("2_Total").ToString)
                        Catch ex As Exception
                            _Suma = 0
                        End Try
                        .Item("2_Total") = _Suma + _Wart
                        .EndEdit()
                    End With
                    If (w Mod _Mod).Equals(0) Then _
                        Me.tspbMain.Value = w : Application.DoEvents()
                    w += 1
                Loop
                XLKosztyDostawcami(dv, m_Language, dm1.Year, dm2.Year, XE)
            Next g
            GoTo NastepnaInstalacja
Podstawowe:
            For g = 0 To 5
                FillStatusBar(, CO_ACTION + " - cz. " & g + 1 & "/6")
                dtW.Clear()
                dv = dtW.DefaultView
                dv.Sort = "Insta, MPK, INEF, CalCo, Dostawca, Material, RK"
                If _Lat < 2 Then GoTo JedenRokPodstawowe
                Select Case g
                    Case 0  ' produkcyjne + JAX03 (wykluczone JAX04, JMS02 i JMS03)
                        SQL = "SELECT Z.Instalacja, Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, SUM(Z.Wartosc) AS [Wartosc], S.Opis, M.Pion " _
                          + "FROM ((((((tblZWPC Z INNER JOIN tblCalCo C ON Z.CalCo = C.ID) INNER JOIN tblINEF I ON C.INEF = I.ID) " _
                          + "INNER JOIN tblMPK M ON Z.MPK = M.ID) LEFT JOIN tblMaterialy R ON Z.Material = R.ID) " _
                          + "INNER JOIN tblInstalacje S ON Z.Instalacja = S.ID) LEFT JOIN tblDostawcy D ON Z.Dostawca = D.ID) " _
                          + "LEFT JOIN tblRK A ON Z.RK = A.ID " _
                          + "WHERE Mc >= " + dm1.Month.ToString + " AND NOT Z.Typ IN ('A', 'F', 'O', 'R', 'S') " _
                          + "AND NOT Z.Instalacja IN ('JAX04', 'JMS02', 'JMS03') " _
                          + "GROUP BY Z.Instalacja, S.Opis, Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, M.Pion " _
                          + "ORDER BY Z.Instalacja, Z.MPK DESC, C.INEF, Z.CalCo, Z.Dostawca, Z.Material, RK, Z.Mc"
                    Case 1  ' JAX04
                        SQL = "SELECT Z.Instalacja, Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, SUM(Z.Wartosc) AS [Wartosc], S.Opis, M.Pion " _
                          + "FROM ((((((tblZWPC Z INNER JOIN tblCalCo C ON Z.CalCo = C.ID) INNER JOIN tblINEF I ON C.INEF = I.ID) " _
                          + "INNER JOIN tblMPK M ON Z.MPK = M.ID) LEFT JOIN tblMaterialy R ON Z.Material = R.ID) " _
                          + "INNER JOIN tblInstalacje S ON Z.Instalacja = S.ID) LEFT JOIN tblDostawcy D ON Z.Dostawca = D.ID) " _
                          + "LEFT JOIN tblRK A ON Z.RK = A.ID " _
                          + "WHERE Mc >= " + dm1.Month.ToString + " AND NOT Z.Typ IN ('F', 'O', 'R', 'S') " _
                          + "AND Z.Instalacja = 'JAX04' AND NOT (Z.MPK LIKE 'JTEL%' OR Z.MPK LIKE 'JTGN%' OR Z.MPK LIKE 'JTWR%') " _
                          + "GROUP BY Z.Instalacja, S.Opis, Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, M.Pion " _
                          + "ORDER BY Z.Instalacja DESC, C.INEF, Z.CalCo, Z.Dostawca, Z.Material, RK, Z.Mc"
                    Case 2  ' JTEL
                        SQL = "SELECT 'JTEL', Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, SUM(Z.Wartosc) AS [Wartosc], 'Elektryczny', M.Pion " _
                          + "FROM (((((tblZWPC Z INNER JOIN tblCalCo C ON Z.CalCo = C.ID) INNER JOIN tblINEF I ON C.INEF = I.ID) " _
                          + "INNER JOIN tblMPK M ON Z.MPK = M.ID) LEFT JOIN tblMaterialy R ON Z.Material = R.ID) " _
                          + "LEFT JOIN tblDostawcy D ON Z.Dostawca = D.ID) " _
                          + "LEFT JOIN tblRK A ON Z.RK = A.ID " _
                          + "WHERE Mc >= " + dm1.Month.ToString + " AND NOT Z.Typ IN ('F', 'O', 'R', 'S') " _
                          + "AND Z.Instalacja = 'JAX04' AND Z.MPK LIKE 'JTEL%' " _
                          + "GROUP BY Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, M.Pion " _
                          + "ORDER BY Z.MPK DESC, C.INEF, Z.CalCo, Z.Dostawca, Z.Material, RK, Z.Mc"
                    Case 3  ' JTGN
                        SQL = "SELECT 'JTGN', Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, SUM(Z.Wartosc) AS [Wartosc], 'OGGS', M.Pion " _
                          + "FROM (((((tblZWPC Z INNER JOIN tblCalCo C ON Z.CalCo = C.ID) INNER JOIN tblINEF I ON C.INEF = I.ID) " _
                          + "INNER JOIN tblMPK M ON Z.MPK = M.ID) LEFT JOIN tblMaterialy R ON Z.Material = R.ID) " _
                          + "LEFT JOIN tblDostawcy D ON Z.Dostawca = D.ID) " _
                          + "LEFT JOIN tblRK A ON Z.RK = A.ID " _
                          + "WHERE Mc >= " + dm1.Month.ToString + " AND NOT Z.Typ IN ('F', 'O', 'R', 'S') " _
                          + "AND Z.Instalacja = 'JAX04' AND Z.MPK LIKE 'JTGN%' " _
                          + "GROUP BY Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, M.Pion " _
                          + "ORDER BY Z.MPK DESC, C.INEF, Z.CalCo, Z.Dostawca, Z.Material, RK, Z.Mc"
                    Case 4  ' JTWR
                        SQL = "SELECT 'JTWR', Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, SUM(Z.Wartosc) AS [Wartosc], 'OGWS', M.Pion " _
                          + "FROM (((((tblZWPC Z INNER JOIN tblCalCo C ON Z.CalCo = C.ID) INNER JOIN tblINEF I ON C.INEF = I.ID) " _
                          + "INNER JOIN tblMPK M ON Z.MPK = M.ID) LEFT JOIN tblMaterialy R ON Z.Material = R.ID) " _
                          + "LEFT JOIN tblDostawcy D ON Z.Dostawca = D.ID) " _
                          + "LEFT JOIN tblRK A ON Z.RK = A.ID " _
                          + "WHERE Mc >= " + dm1.Month.ToString + " AND NOT Z.Typ IN ('F', 'O', 'R', 'S') " _
                          + "AND Z.Instalacja = 'JAX04' AND Z.MPK LIKE 'JTWR%' " _
                          + "GROUP BY Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, M.Pion " _
                          + "ORDER BY Z.MPK DESC, C.INEF, Z.CalCo, Z.Dostawca, Z.Material, RK, Z.Mc"
                    Case 5  'Stalownia ZWK
                        SQL = "SELECT 'JMS02' AS [INSTA], Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, SUM(Z.Wartosc) AS [Wartosc], 'Stalownia ZWK', M.Pion " _
                          + "FROM (((((tblZWPC Z INNER JOIN tblCalCo C ON Z.CalCo = C.ID) INNER JOIN tblINEF I ON C.INEF = I.ID) " _
                          + "INNER JOIN tblMPK M ON Z.MPK = M.ID) LEFT JOIN tblMaterialy R ON Z.Material = R.ID) LEFT JOIN tblDostawcy D ON Z.Dostawca = D.ID) " _
                          + "LEFT JOIN tblRK A ON Z.RK = A.ID " _
                          + "WHERE Mc >= " + dm1.Month.ToString + " AND NOT Z.Typ IN ('A', 'F', 'O', 'R', 'S') " _
                          + "AND Z.Instalacja IN ('JMS02', 'JMS03') " _
                          + "GROUP BY Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, M.Pion " _
                          + "ORDER BY Z.MPK DESC, C.INEF, Z.CalCo, Z.Dostawca, Z.Material, RK, Z.Mc"
                End Select
                If m_Language.Equals("EN") Then SQL = SQL.Replace(".Opis", ".Description").Replace("R.MaterialPL", "R.MaterialEN").Replace("Walcownia Średnia", "Rolling Mill 2").Replace("Stalownia ZWK", "Melt Shop FD")
                If ctrl.Name.EndsWith("C") Then _
                    SQL = SQL.Replace("WHERE", "WHERE C.UR AND")
                dt = New DataTable
                da = New OleDbDataAdapter(SQL, Me.cnsPY)
                da.Fill(dt)
                w = 0
                With Me
                    With .tspbMain
                        .Minimum = 0
                        .Maximum = dt.Rows.Count
                        .Value = 0
                        _Mod = .Maximum \ 20
                        If _Mod.Equals(0) Then _Mod = 1
                    End With
                End With
                Application.DoEvents()
                Do Until w.Equals(dt.Rows.Count)
                    r = dt.Rows(w)
                    vF = dv.FindRows(New String() {r(0).ToString, r(1).ToString, r(3).ToString, r(5).ToString, r(7).ToString, r(9).ToString, r(11).ToString})
                    If vF.Length.Equals(0) Then
                        _Mat = r(9).ToString
                        _DesMat = r(10).ToString
                        If String.IsNullOrEmpty(_Mat) Then _DesMat = r(12).ToString
                        dtW.Rows.Add(New Object() {r(0).ToString, r(15).ToString, r(1).ToString, r(2).ToString, r(11).ToString, r(3).ToString, r(4).ToString, r(5).ToString, _
                                                   r(6).ToString, r(7).ToString, r(8).ToString, _Mat, _DesMat, r(16).ToString})
                        vF = dv.FindRows(New String() {r(0).ToString, r(1).ToString, r(3).ToString, r(5).ToString, r(7).ToString, r(9).ToString, r(11).ToString})
                    End If
                    With vF(0)
                        Try
                            _Wart = CDbl(r("Wartosc").ToString)
                        Catch ex As Exception
                            _Wart = 0
                        End Try
                        Try
                            _Suma = CDbl(.Item("1_" + r("Mc").ToString))
                        Catch ex As Exception
                            _Suma = 0
                        End Try
                        .Item("1_" + r("Mc").ToString) = _Suma + _Wart
                        Try
                            _Suma = CDbl(.Item("1_Total").ToString)
                        Catch ex As Exception
                            _Suma = 0
                        End Try
                        .Item("1_Total") = _Suma + _Wart
                        .EndEdit()
                    End With
                    If (w Mod _Mod).Equals(0) Then _
                        Me.tspbMain.Value = w : Application.DoEvents()
                    w += 1
                Loop
JedenRokPodstawowe:
                If Not _Lat.Equals(2) Then dtW.Clear()
                If dm1.Year.Equals(dm2.Year) Then
                    Select Case g
                        Case 0  ' produkcyjne + JAX03 (wykluczone JAX04, JMS02 i JMS03)
                            SQL = "SELECT Z.Instalacja, Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, SUM(Z.Wartosc) AS [Wartosc], S.Opis, M.Pion " _
                              + "FROM ((((((tblZWPC Z INNER JOIN tblCalCo C ON Z.CalCo = C.ID) INNER JOIN tblINEF I ON C.INEF = I.ID) " _
                              + "INNER JOIN tblMPK M ON Z.MPK = M.ID) LEFT JOIN tblMaterialy R ON Z.Material = R.ID) " _
                              + "INNER JOIN tblInstalacje S ON Z.Instalacja = S.ID) LEFT JOIN tblDostawcy D ON Z.Dostawca = D.ID) " _
                              + "LEFT JOIN tblRK A ON Z.RK = A.ID " _
                              + "WHERE Mc >= " + dm1.Month.ToString + " AND Mc <= " + dm2.Month.ToString + " AND NOT Z.Typ IN ('A', 'F', 'O', 'R', 'S') " _
                              + "AND NOT Z.Instalacja IN ('JAX04', 'JMS02', 'JMS03') " _
                              + "GROUP BY Z.Instalacja, S.Opis, Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, M.Pion " _
                              + "ORDER BY Z.Instalacja, Z.MPK DESC, C.INEF, Z.CalCo, Z.Dostawca, Z.Material, RK, Z.Mc"
                        Case 1  ' JAX04
                            SQL = "SELECT Z.Instalacja, Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, SUM(Z.Wartosc) AS [Wartosc], S.Opis, M.Pion " _
                              + "FROM ((((((tblZWPC Z INNER JOIN tblCalCo C ON Z.CalCo = C.ID) INNER JOIN tblINEF I ON C.INEF = I.ID) " _
                              + "INNER JOIN tblMPK M ON Z.MPK = M.ID) LEFT JOIN tblMaterialy R ON Z.Material = R.ID) " _
                              + "INNER JOIN tblInstalacje S ON Z.Instalacja = S.ID) LEFT JOIN tblDostawcy D ON Z.Dostawca = D.ID) " _
                              + "LEFT JOIN tblRK A ON Z.RK = A.ID " _
                              + "WHERE Mc >= " + dm1.Month.ToString + " AND Mc <= " + dm2.Month.ToString + " AND NOT Z.Typ IN ('F', 'O', 'R', 'S') " _
                              + "AND Z.Instalacja = 'JAX04' AND NOT (Z.MPK LIKE 'JTEL%' OR Z.MPK LIKE 'JTGN%' OR Z.MPK LIKE 'JTWR%') " _
                              + "GROUP BY Z.Instalacja, S.Opis, Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, M.Pion " _
                              + "ORDER BY Z.Instalacja DESC, C.INEF, Z.CalCo, Z.Dostawca, Z.Material, RK, Z.Mc"
                        Case 2  ' JTEL
                            SQL = "SELECT 'JTEL', Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, SUM(Z.Wartosc) AS [Wartosc], 'Elektryczny', M.Pion " _
                              + "FROM (((((tblZWPC Z INNER JOIN tblCalCo C ON Z.CalCo = C.ID) INNER JOIN tblINEF I ON C.INEF = I.ID) " _
                              + "INNER JOIN tblMPK M ON Z.MPK = M.ID) LEFT JOIN tblMaterialy R ON Z.Material = R.ID) " _
                              + "LEFT JOIN tblDostawcy D ON Z.Dostawca = D.ID) " _
                              + "LEFT JOIN tblRK A ON Z.RK = A.ID " _
                              + "WHERE Mc >= " + dm1.Month.ToString + " AND Mc <= " + dm2.Month.ToString + " AND NOT Z.Typ IN ('F', 'O', 'R', 'S') " _
                              + "AND Z.Instalacja = 'JAX04' AND Z.MPK LIKE 'JTEL%' " _
                              + "GROUP BY Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, M.Pion " _
                              + "ORDER BY Z.MPK DESC, C.INEF, Z.CalCo, Z.Dostawca, Z.Material, RK, Z.Mc"
                        Case 3  ' JTGN
                            SQL = "SELECT 'JTGN', Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, SUM(Z.Wartosc) AS [Wartosc], 'OGGS', M.Pion " _
                              + "FROM (((((tblZWPC Z INNER JOIN tblCalCo C ON Z.CalCo = C.ID) INNER JOIN tblINEF I ON C.INEF = I.ID) " _
                              + "INNER JOIN tblMPK M ON Z.MPK = M.ID) LEFT JOIN tblMaterialy R ON Z.Material = R.ID) " _
                              + "LEFT JOIN tblDostawcy D ON Z.Dostawca = D.ID) " _
                              + "LEFT JOIN tblRK A ON Z.RK = A.ID " _
                              + "WHERE Mc >= " + dm1.Month.ToString + " AND Mc <= " + dm2.Month.ToString + " AND NOT Z.Typ IN ('F', 'O', 'R', 'S') " _
                              + "AND Z.Instalacja = 'JAX04' AND Z.MPK LIKE 'JTGN%' " _
                              + "GROUP BY Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, M.Pion " _
                              + "ORDER BY Z.MPK DESC, C.INEF, Z.CalCo, Z.Dostawca, Z.Material, RK, Z.Mc"
                        Case 4  ' JTWR
                            SQL = "SELECT 'JTWR', Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, SUM(Z.Wartosc) AS [Wartosc], 'OGWS', M.Pion " _
                              + "FROM (((((tblZWPC Z INNER JOIN tblCalCo C ON Z.CalCo = C.ID) INNER JOIN tblINEF I ON C.INEF = I.ID) " _
                              + "INNER JOIN tblMPK M ON Z.MPK = M.ID) LEFT JOIN tblMaterialy R ON Z.Material = R.ID) " _
                              + "LEFT JOIN tblDostawcy D ON Z.Dostawca = D.ID) " _
                              + "LEFT JOIN tblRK A ON Z.RK = A.ID " _
                              + "WHERE Mc >= " + dm1.Month.ToString + " AND Mc <= " + dm2.Month.ToString + " AND NOT Z.Typ IN ('F', 'O', 'R', 'S') " _
                              + "AND Z.Instalacja = 'JAX04' AND Z.MPK LIKE 'JTWR%' " _
                              + "GROUP BY Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, M.Pion " _
                              + "ORDER BY Z.MPK DESC, C.INEF, Z.CalCo, Z.Dostawca, Z.Material, RK, Z.Mc"
                        Case 5  'Stalownia ZWK
                            SQL = "SELECT 'JMS02' AS [INSTA], Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, SUM(Z.Wartosc) AS [Wartosc], 'Stalownia ZWK', M.Pion " _
                              + "FROM (((((tblZWPC Z INNER JOIN tblCalCo C ON Z.CalCo = C.ID) INNER JOIN tblINEF I ON C.INEF = I.ID) " _
                              + "INNER JOIN tblMPK M ON Z.MPK = M.ID) LEFT JOIN tblMaterialy R ON Z.Material = R.ID) LEFT JOIN tblDostawcy D ON Z.Dostawca = D.ID) " _
                              + "LEFT JOIN tblRK A ON Z.RK = A.ID " _
                              + "WHERE Mc >= " + dm1.Month.ToString + " AND Mc <= " + dm2.Month.ToString + " AND NOT Z.Typ IN ('A', 'F', 'O', 'R', 'S') " _
                              + "AND Z.Instalacja IN ('JMS02', 'JMS03') " _
                              + "GROUP BY Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, M.Pion " _
                              + "ORDER BY Z.MPK DESC, C.INEF, Z.CalCo, Z.Dostawca, Z.Material, RK, Z.Mc"
                    End Select
                Else
                    Select Case g
                        Case 0  ' produkcyjne + JAX03 (wykluczone JAX04, JMS02 i JMS03)
                            SQL = "SELECT Z.Instalacja, Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, SUM(Z.Wartosc) AS [Wartosc], S.Opis, M.Pion " _
                              + "FROM ((((((tblZWPC Z INNER JOIN tblCalCo C ON Z.CalCo = C.ID) INNER JOIN tblINEF I ON C.INEF = I.ID) " _
                              + "INNER JOIN tblMPK M ON Z.MPK = M.ID) LEFT JOIN tblMaterialy R ON Z.Material = R.ID) " _
                              + "INNER JOIN tblInstalacje S ON Z.Instalacja = S.ID) LEFT JOIN tblDostawcy D ON Z.Dostawca = D.ID) " _
                              + "LEFT JOIN tblRK A ON Z.RK = A.ID " _
                              + "WHERE Mc <= " + dm2.Month.ToString + " AND NOT Z.Typ IN ('A', 'F', 'O', 'R', 'S') " _
                              + "AND NOT Z.Instalacja IN ('JAX04', 'JMS02', 'JMS03') " _
                              + "GROUP BY Z.Instalacja, S.Opis, Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, M.Pion " _
                              + "ORDER BY Z.Instalacja, Z.MPK DESC, C.INEF, Z.CalCo, Z.Dostawca, Z.Material, RK, Z.Mc"
                        Case 1  ' JAX04
                            SQL = "SELECT Z.Instalacja, Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, SUM(Z.Wartosc) AS [Wartosc], S.Opis, M.Pion " _
                              + "FROM ((((((tblZWPC Z INNER JOIN tblCalCo C ON Z.CalCo = C.ID) INNER JOIN tblINEF I ON C.INEF = I.ID) " _
                              + "INNER JOIN tblMPK M ON Z.MPK = M.ID) LEFT JOIN tblMaterialy R ON Z.Material = R.ID) " _
                              + "INNER JOIN tblInstalacje S ON Z.Instalacja = S.ID) LEFT JOIN tblDostawcy D ON Z.Dostawca = D.ID) " _
                              + "LEFT JOIN tblRK A ON Z.RK = A.ID " _
                              + "WHERE Mc <= " + dm2.Month.ToString + " AND NOT Z.Typ IN ('F', 'O', 'R', 'S') " _
                              + "AND Z.Instalacja = 'JAX04' AND NOT (Z.MPK LIKE 'JTEL%' OR Z.MPK LIKE 'JTGN%' OR Z.MPK LIKE 'JTWR%') " _
                              + "GROUP BY Z.Instalacja, S.Opis, Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, M.Pion " _
                              + "ORDER BY Z.Instalacja DESC, C.INEF, Z.CalCo, Z.Dostawca, Z.Material, RK, Z.Mc"
                        Case 2  ' JTEL
                            SQL = "SELECT 'JTEL', Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, SUM(Z.Wartosc) AS [Wartosc], 'Elektryczny', M.Pion " _
                              + "FROM (((((tblZWPC Z INNER JOIN tblCalCo C ON Z.CalCo = C.ID) INNER JOIN tblINEF I ON C.INEF = I.ID) " _
                              + "INNER JOIN tblMPK M ON Z.MPK = M.ID) LEFT JOIN tblMaterialy R ON Z.Material = R.ID) " _
                              + "LEFT JOIN tblDostawcy D ON Z.Dostawca = D.ID) " _
                              + "LEFT JOIN tblRK A ON Z.RK = A.ID " _
                              + "WHERE Mc <= " + dm2.Month.ToString + " AND NOT Z.Typ IN ('F', 'O', 'R', 'S') " _
                              + "AND Z.Instalacja = 'JAX04' AND Z.MPK LIKE 'JTEL%' " _
                              + "GROUP BY Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, M.Pion " _
                              + "ORDER BY Z.MPK DESC, C.INEF, Z.CalCo, Z.Dostawca, Z.Material, RK, Z.Mc"
                        Case 3  ' JTGN
                            SQL = "SELECT 'JTGN', Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, SUM(Z.Wartosc) AS [Wartosc], 'OGGS', M.Pion " _
                              + "FROM (((((tblZWPC Z INNER JOIN tblCalCo C ON Z.CalCo = C.ID) INNER JOIN tblINEF I ON C.INEF = I.ID) " _
                              + "INNER JOIN tblMPK M ON Z.MPK = M.ID) LEFT JOIN tblMaterialy R ON Z.Material = R.ID) " _
                              + "LEFT JOIN tblDostawcy D ON Z.Dostawca = D.ID) " _
                              + "LEFT JOIN tblRK A ON Z.RK = A.ID " _
                              + "WHERE Mc <= " + dm2.Month.ToString + " AND NOT Z.Typ IN ('F', 'O', 'R', 'S') " _
                              + "AND Z.Instalacja = 'JAX04' AND Z.MPK LIKE 'JTGN%' " _
                              + "GROUP BY Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, M.Pion " _
                              + "ORDER BY Z.MPK DESC, C.INEF, Z.CalCo, Z.Dostawca, Z.Material, RK, Z.Mc"
                        Case 4  ' JTWR
                            SQL = "SELECT 'JTWR', Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, SUM(Z.Wartosc) AS [Wartosc], 'OGWS', M.Pion " _
                              + "FROM (((((tblZWPC Z INNER JOIN tblCalCo C ON Z.CalCo = C.ID) INNER JOIN tblINEF I ON C.INEF = I.ID) " _
                              + "INNER JOIN tblMPK M ON Z.MPK = M.ID) LEFT JOIN tblMaterialy R ON Z.Material = R.ID) " _
                              + "LEFT JOIN tblDostawcy D ON Z.Dostawca = D.ID) " _
                              + "LEFT JOIN tblRK A ON Z.RK = A.ID " _
                              + "WHERE Mc <= " + dm2.Month.ToString + " AND NOT Z.Typ IN ('F', 'O', 'R', 'S') " _
                              + "AND Z.Instalacja = 'JAX04' AND Z.MPK LIKE 'JTWR%' " _
                              + "GROUP BY Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, M.Pion " _
                              + "ORDER BY Z.MPK DESC, C.INEF, Z.CalCo, Z.Dostawca, Z.Material, RK, Z.Mc"
                        Case 5  'Stalownia ZWK
                            SQL = "SELECT 'JMS02' AS [INSTA], Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, SUM(Z.Wartosc) AS [Wartosc], 'Stalownia ZWK', M.Pion " _
                              + "FROM (((((tblZWPC Z INNER JOIN tblCalCo C ON Z.CalCo = C.ID) INNER JOIN tblINEF I ON C.INEF = I.ID) " _
                              + "INNER JOIN tblMPK M ON Z.MPK = M.ID) LEFT JOIN tblMaterialy R ON Z.Material = R.ID) LEFT JOIN tblDostawcy D ON Z.Dostawca = D.ID) " _
                              + "LEFT JOIN tblRK A ON Z.RK = A.ID " _
                              + "WHERE Mc <= " + dm2.Month.ToString + " AND NOT Z.Typ IN ('A', 'F', 'O', 'R', 'S') " _
                              + "AND Z.Instalacja IN ('JMS02', 'JMS03') " _
                              + "GROUP BY Z.MPK, M.Opis, C.INEF, I.Opis, Z.CalCo, C.Opis, Z.Dostawca, D.Nazwa, Z.Material, R.MaterialPL, RK, A.Opis, Z.Mc, M.Pion " _
                              + "ORDER BY Z.MPK DESC, C.INEF, Z.CalCo, Z.Dostawca, Z.Material, RK, Z.Mc"
                    End Select
                End If
                If m_Language.Equals("EN") Then SQL = SQL.Replace(".Opis", ".Description").Replace("R.MaterialPL", "R.MaterialEN").Replace("Walcownia Średnia", "Rolling Mill 2").Replace("Stalownia ZWK", "Melt Shop FD")
                If ctrl.Name.EndsWith("C") Then _
                    SQL = SQL.Replace("WHERE", "WHERE C.UR AND")
                dt = New DataTable
                da = New OleDbDataAdapter(SQL, Me.cns)
                da.Fill(dt)
                With Me
                    With .tspbMain
                        .Minimum = 0
                        .Maximum = dt.Rows.Count
                        .Value = 0
                        _Mod = .Maximum \ 20
                        If _Mod.Equals(0) Then _Mod = 1
                    End With
                End With
                Application.DoEvents()
                w = 0
                Do Until w.Equals(dt.Rows.Count)
                    r = dt.Rows(w)
                    vF = dv.FindRows(New String() {r(0).ToString, r(1).ToString, r(3).ToString, r(5).ToString, r(7).ToString, r(9).ToString, r(11).ToString})
                    If vF.Length.Equals(0) Then
                        _Mat = r(9).ToString
                        _DesMat = r(10).ToString
                        If String.IsNullOrEmpty(_Mat) Then _DesMat = r(12).ToString
                        dtW.Rows.Add(New Object() {r(0).ToString, r(15).ToString, r(1).ToString, r(2).ToString, r(11).ToString, r(3).ToString, r(4).ToString, r(5).ToString, _
                                                   r(6).ToString, r(7).ToString, r(8).ToString, _Mat, _DesMat, r(16).ToString})
                        vF = dv.FindRows(New String() {r(0).ToString, r(1).ToString, r(3).ToString, r(5).ToString, r(7).ToString, r(9).ToString, r(11).ToString})
                    End If
                    With vF(0)
                        Try
                            _Wart = CDbl(r("Wartosc").ToString)
                        Catch ex As Exception
                            _Wart = 0
                        End Try
                        Try
                            _Suma = CDbl(.Item("2_" + r("Mc").ToString))
                        Catch ex As Exception
                            _Suma = 0
                        End Try
                        .Item("2_" + r("Mc").ToString) = _Suma + _Wart
                        Try
                            _Suma = CDbl(.Item("2_Total").ToString)
                        Catch ex As Exception
                            _Suma = 0
                        End Try
                        .Item("2_Total") = _Suma + _Wart
                        .EndEdit()
                    End With
                    If (w Mod _Mod).Equals(0) Then _
                        Me.tspbMain.Value = w : Application.DoEvents()
                    w += 1
                Loop
                XLKosztyDostawcami(dv, m_Language, dm1.Year, dm2.Year, XE)
            Next g
NastepnaInstalacja:
            XE.Zwolnij()
            Using New CM
                MessageBox.Show("Generowanie skoroszytów zakończone pomyślnie", CO_ACTION, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End Using
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message, CO_ACTION, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
        Finally
            ClearStatus()
            da = Nothing
            dt = Nothing
            dtW = Nothing
            dv = Nothing
        End Try
    End Sub

    Private Sub XLKosztyDostawcami(ByVal dv As DataView, _
                                   ByVal Jezyk As String, _
                                   ByVal Rok1 As Integer, _
                                   ByVal Rok2 As Integer, _
                                   ByVal XE As Cl4Excel.Excel)

        Dim i, j As Integer
        Dim m_Rok As String = ""
        Dim txt As String
        Dim _Instalacja() As Object = (From v As DataRowView In dv Select v!Insta Distinct).ToArray
        Try
            m_Rok = Rok1.ToString
            If Not Rok1.Equals(Rok2) Then m_Rok += ", " + Rok2.ToString
            ' przeniesienie Rk do pustych materiałów
            For Each v As DataRowView In dv
                If String.IsNullOrEmpty(v(11)) Then v(11) = v(4)
            Next v
            For Each _Inst In _Instalacja
                dv.RowFilter = "Insta = '" + _Inst.ToString + "'"
                dv.Sort = "Pion DESC, MPK DESC, INEF, CalCo, DostawcaOpis, Material"
                Dim obj(dv.Count, dv.Table.Columns.Count - 2)
                i = 0
                For j = 0 To 12
                    obj(0, j) = dv.Table.Columns(j).ColumnName
                Next j
                For j = 14 To dv.Table.Columns.Count - 1
                    obj(0, j - 1) = dv.Table.Columns(j).ColumnName
                Next j
                For Each v As DataRowView In dv
                    i += 1
                    For j = 0 To 12
                        obj(i, j) = v(j)
                    Next j
                    For j = 14 To dv.Table.Columns.Count - 1
                        obj(i, j - 1) = v(j)
                    Next j
                Next v
                For j = 13 To obj.GetUpperBound(1)
                    txt = obj(0, j)
                    If txt.Contains("Total") Then
                        If txt.StartsWith("1") Then
                            txt = "Total " + Rok1.ToString
                        Else
                            txt = "Total " + Rok2.ToString
                        End If
                    Else
                        txt = "0" + txt.Substring(2)
                        txt = "'" + txt.Substring(txt.Length - 2)
                    End If
                    obj(0, j) = txt
                Next j
                XE.oApp.Run("KosztyDostawcami", obj, Jezyk, m_Rok)
                dv.RowFilter = Nothing
            Next _Inst
        Catch ex As Exception
            Throw New ArgumentException(ex.Message)
        End Try
    End Sub

    Private Sub SumaDlaDostawcy() Handles tsm_Eds.Click

        Dim da As OleDbDataAdapter
        Dim dt As New DataTable, dtW As New DataTable
        Dim dv As DataView
        Dim _Kwota As Double
        Dim i, w As Integer
        Const CO_ACTION As String = "Koszty sumaryczne dostawców"
        Try
            FillStatusBar(, CO_ACTION + " - pobieranie danych...")
            da = New OleDbDataAdapter("SELECT Dostawca, D.Nazwa AS [Suppliers], D.Kraj AS [Country], D.Miasto AS [City], Mc, SUM(Wartosc) AS [Costs] " _
                                      + "FROM (tblZWPC Z INNER JOIN tblMPK P ON Z.MPK = P.ID) INNER JOIN tblDostawcy D ON Z.Dostawca = D.ID " _
                                      + "WHERE Z.Instalacja = P.Instalacja GROUP BY Mc, Dostawca, D.Nazwa, D.Kraj, D.Miasto ORDER BY Dostawca, Mc", Me.cns)
            da.Fill(dt)
            If dt.Rows.Count.Equals(0) Then Throw New ArgumentException("Brak danych")
            With dtW.Columns
                .Add("ID")
                .Add("Suppliers")
                .Add("Country")
                .Add("City")
                For i = 1 To 12
                    .Add(i.ToString, GetType(Double))
                Next i
            End With
            dv = dtW.DefaultView
            dv.Sort = "ID"
            FillStatusBar(dt.Rows.Count, CO_ACTION)
            For Each r As DataRow In dt.Rows
                i = dv.Find(r("Dostawca").ToString)
                If i.Equals(-1) Then
                    dtW.Rows.Add(New Object() {r("Dostawca"), r("Suppliers"), r("Country"), r("City")})
                    i = dv.Find(r("Dostawca").ToString)
                End If
                Try
                    _Kwota = CDbl(r("Costs").ToString)
                    dv(i)(3 + CInt(r("Mc").ToString)) = _Kwota
                Catch ex As Exception
                    _Kwota = 0
                End Try
                With Me.tspbMain
                    If (w Mod 100).Equals(0) Then
                        .Value = w
                        Application.DoEvents()
                    End If
                End With
                w += 1
            Next r
            dv.Sort = "Suppliers"
            Dim XE As New Cl4Excel.Excel(Nothing)
            With XE
                With .oSheet
                    .Range("A4:Q4").Value = New String() {"ID", "Name", "Country", "City", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec", "Total"}
                    For Each v As DataRowView In dv
                        i += 1
                        .Range("A" & i & ":P" & i).Value = New Object() {v("ID"), v("Suppliers"), v("Country"), v("City"), v("1"), v("2"), v("3"), v("4"), v("5"), v("6"), v("7"), v("8"), v("9"), _
                                                              v("10"), v("11"), v("12")}
                    Next v
                    .Range("Q5:Q" & (i - 1)).FormulaR1C1 = "=SUMA(WK5:WK16)"
                    .Range("E:Q").NumberFormat = "### ### ##0"
                    .Range("A:Q").Columns.AutoFit()
                    .Range("A2").Value = "Analyse costs by suppliers " & Me.dtpOkres.Value.Year & " Year"
                    With .Range("A2:L2")
                        .HorizontalAlignment = XL.XlHAlign.xlHAlignCenter
                        .Merge()
                    End With
                    .Range("E4:Q4").HorizontalAlignment = XL.XlHAlign.xlHAlignCenter
                    XE.Obramowanie(.Range("A4").CurrentRegion)
                    .Range("A4").CurrentRegion.Offset(1).Borders(XL.XlBordersIndex.xlEdgeTop).ColorIndex = XL.XlColorIndex.xlColorIndexAutomatic
                    With .Range("B1")
                        .Value = Now
                        .HorizontalAlignment = XL.XlHAlign.xlHAlignLeft
                    End With
                    .Range("A5").Select()
                End With
                With .oApp.ActiveWindow
                    .DisplayGridlines = False
                    .DisplayZeros = False
                    .FreezePanes = True
                    .Zoom = 80
                End With
                .oBook.Saved = True
                ClearStatus()
                .Show()
            End With
            i = 4
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message, CO_ACTION, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
            ClearStatus()
        Finally
            da = Nothing
            dt = Nothing
            dv = Nothing
        End Try
    End Sub

    Private Sub SuppliersAnalyse() Handles tsm_En.Click

        Dim drv As DataRowView = Nothing
        Dim dtZ As New DataTable
        Dim dtF As New DataTable
        Dim dtG As New DataTable
        Dim dtC As New DataTable
        Dim da As OleDbDataAdapter
        Dim bsC As New BindingSource
        Dim bsG As New BindingSource
        Dim bs As New BindingSource
        Dim dv As DataView = Nothing
        Dim r As DataRow
        Dim oList As New List(Of Object)
        Dim i, j, k As Integer
        Dim dbl As Double
        Dim oDane(,) As Object
        Dim oZWM(), oFbl() As Object
        Const CO_ACTION As String = "Analiza dostawców"
        Try
            With New Cl4Excel.GetFromXls("C:\Tmp\ZWMM080.xls")
                .Data2Array()
                oZWM = .Dane    ' dtZ
                .Zwolnij()
            End With
            If oZWM Is Nothing Then Return
            With New Cl4Excel.GetFromXls("C:\Tmp\FBL1N.xls")
                .Data2Array()
                oFbl = .Dane    ' dtF
                .Zwolnij()
            End With
            If oFbl Is Nothing Then Return
            Dim m_WB As String
            With New Cl4Excel.CheckFile(My.Settings.Wzorce + "Suppliers", True, , My.Settings.Wyniki)
                If .NOK Then Exit Try
                My.Settings.Wzorce = .Wzorce
                My.Settings.Wyniki = .Wyniki
                m_WB = .Plik
            End With
            'If dtZ.Rows.Count.Equals(0) Then Throw New ArgumentException("[" + .Baza + "]: brak danych")
            ' dodanie kolumny
            With dtZ.Columns
                .Add("FValue", GetType(Double))
                .Add("ForWho")
            End With
            dtZ.Columns(9).ColumnName = "DokMater"
            With bs
                .DataSource = dtZ
                .Sort = "DokMater, " + dtZ.Columns(8).ColumnName
                dv = DirectCast(.List, DataView)
            End With
            FillStatusBar(, CO_ACTION + "...")
            da = DA_TI("tblMPK", "ID")
            da.Fill(dtC)
            With bsC
                .DataSource = dtC   ' Description
                .Sort = "ID"
            End With
            da = DA_TI("tblGrupyMat", "ID")
            da.Fill(dtG)
            With bsG
                .DataSource = dtG   ' GrMaterEN
                .Sort = "ID"
            End With
            ' połączenie z FI
            For i = 0 To dtF.Rows.Count - 1
                Try
                    oList.Clear()
                    r = dtF.Rows(i)
                    dbl = -1 * CDbl(r(16).ToString)
                    oList.Add(r(7).ToString.Substring(2, 8))
                    oList.Add(r(7).ToString.Substring(10))
                    j = dv.Find(oList.ToArray)
                    If j < 0 Then
                        dtZ.Rows.Add()
                        With dtZ.Rows(dtZ.Rows.Count - 1)
                            .Item(1) = r(10).ToString
                            .Item(8) = r(0).ToString.Substring(0, 4)
                            .Item(9) = r(7).ToString.Substring(2, 8)
                            .Item("FValue") = dbl
                            .Item(5) = r(8).ToString
                        End With
                    Else
                        With bs
                            .Position = j
                            drv = CType(.Current, DataRowView)
                            drv("FValue") = dbl + CP.Str2Dbl(drv("FValue").ToString)
                        End With
                    End If
                Catch ex As Exception
                    dbl = 0
                End Try
Nastepny:
            Next i
            ' wstawienie opisów
            ReDim oDane(dtZ.Rows.Count, dtZ.Columns.Count - 1)
            For k = 0 To dtZ.Columns.Count - 1
                oDane(0, k) = dtZ.Columns(k).ColumnName
            Next k
            For i = 0 To dtZ.Rows.Count - 1
                r = dtZ.Rows(i)
                r("ForWho") = "Other"
                If r(15).ToString.Trim <> "" Then
                    j = bsC.Find("ID", r(15).ToString.Trim)
                    If j > -1 Then
                        bsC.Position = j
                        drv = CType(bsC.Current, DataRowView)
                        r(16) = drv("Description").ToString
                        If "JML01_JML02_JML03_JMS01".Contains(drv("Instalacja").ToString) Then
                            r("ForWho") = "Steel"
                        ElseIf "JHT01_JMS02_JMT01_JPS01".Contains(drv("Instalacja").ToString) Then
                            r("ForWho") = "Forge"
                        End If
                    End If
                ElseIf Not String.IsNullOrEmpty(r(17).ToString.Trim) Then
                    Select Case r(17).ToString.Substring(0, 2)
                        Case "JW"
                            r("ForWho") = "Steel"
                        Case "JK"
                            r("ForWho") = "Forge"
                    End Select
                End If
                If Not String.IsNullOrEmpty(r(13).ToString.Trim) Then
                    j = bsG.Find("ID", r(13).ToString.Trim)
                    If Not j.Equals(-1) Then
                        bsG.Position = j
                        drv = CType(bsG.Current, DataRowView)
                        r(14) = drv("GrMaterEN").ToString
                    End If
                End If
                For k = 0 To dtZ.Columns.Count - 1
                    r = dtZ.Rows(i)
                    If k > 23 AndAlso k < 27 Then
                        oDane(i + 1, k) = CP.Str2Dbl(r(k).ToString)
                    ElseIf k = 20 Then
                        oDane(i + 1, k) = CP.Str2Dbl(r(k).ToString)
                    Else
                        oDane(i + 1, k) = r(k).ToString
                    End If
                Next k
            Next i
            With New Cl4Excel.Excel(m_WB)
                .ScreenRefresh(False)
                .oApp.Run("PrepareSheets", oDane)
                .ScreenRefresh(True)
                HideScreen()
                .Show()
            End With
            HideScreen(False)
        Catch ex As Exception
            ClearStatus()
            Using New CM
                MessageBox.Show(ex.Message, CO_ACTION, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
        Finally
            oDane = Nothing
            bsC = Nothing
            bsG = Nothing
            bs = Nothing
            da = Nothing
            dtC = Nothing
            dtG = Nothing
            dtF = Nothing
            dtZ = Nothing
            r = Nothing
            oZWM = Nothing
            oFbl = Nothing
            GC.Collect()
        End Try
    End Sub

    Private Sub WieloletnieDane(ByVal sender As System.Object, ByVal e As System.EventArgs) _
                                Handles tsm_Es.Click, _
                                        tsm_Ep.Click, _
                                        tsmWeryfSprzWydz.Click, _
                                        tsm_Eu.Click

        Dim da As OleDbDataAdapter = New OleDbDataAdapter("SELECT DISTINCT Wydzial FROM tblInstalacje ORDER BY Wydzial", Me.cns)
        Dim dt As New DataTable
        Dim i As Integer
        Dim txt As String = ""
        Dim ctrl As ToolStripMenuItem = CType(sender, ToolStripMenuItem)
        da.Fill(dt)
        With Me
            .lstPaWybor.Items.Clear()
            For i = 0 To dt.Rows.Count - 1
                With dt(i)
                    If Not txt.Contains(.Item("Wydzial").ToString) Then
                        Me.lstPaWybor.Items.Add(.Item("Wydzial").ToString)
                        txt += .Item("Wydzial").ToString + "^^"
                    End If
                End With
            Next i
            .lstPaWybor.Items.Add("OR")
            With .gbParametry
                .Size = New Size(352, 342)
                .Location = New Point(32, 97)
                .Visible = True
            End With
            .lblPaParent.Text = ctrl.Name.Substring(3)
            .cmbPaMiesiace1.Enabled = False
            .cmbPaMiesiace2.Enabled = False
            .txtPaRok1.Enabled = False
            .txtPaRok2.Enabled = False
            Select Case .lblPaParent.Text
                Case "AnalizaSprzedazy"
                    .cmbPaMiesiace1.Enabled = True
                    .cmbPaMiesiace2.Enabled = True
                    .txtPaRok1.Enabled = True
                    .txtPaRok2.Enabled = True
                Case "ListaSprzedazy"
                    .txtPaRok1.Enabled = True
                    .txtPaRok2.Enabled = True
            End Select
            If .cmbPaMiesiace1.Enabled Then
                .cmbPaMiesiace1.Focus()
            ElseIf .txtPaRok1.Enabled Then
                .txtPaRok1.Focus()
            End If
        End With
        dt = Nothing
        da = Nothing
    End Sub

    Private Sub AnalizaSprzedazyWykonanie()

        Dim i, k As Integer
        Dim j As Integer = -1
        Dim da As New OleDbDataAdapter
        Dim dt As New DataTable
        Dim r As DataRow
        Dim aDane(6, 0) As Object
        Const CO_ACTION As String = "Analiza sprzedaży"
        Try
            Dim m_WB As String = Nothing
            If NotExportFile(m_WB) Then Exit Try
            FillStatusBar(, CO_ACTION + "...")
            With Me
                If .cmbPaMiesiace1.SelectedIndex.Equals(-1) Then .cmbPaMiesiace1.SelectedIndex = 0
                If .cmbPaMiesiace2.SelectedIndex.Equals(-1) Then .cmbPaMiesiace2.SelectedIndex = 11
                Try
                    i = CInt(.txtPaRok1.Text)
                Catch ex As Exception
                    .txtPaRok1.Text = Okres.Substring(0, 4)
                End Try
                Try
                    i = CInt(.txtPaRok2.Text)
                    If i < CInt(.txtPaRok1.Text) Then Throw New ArgumentException
                Catch ex As Exception
                    .txtPaRok2.Text = .txtPaRok1.Text
                End Try
            End With
            If String.IsNullOrEmpty(Me.txtPaWydzialy.Text) Then Throw New ArgumentException("Wybierz wydział(y)")
            For i = CInt(Me.txtPaRok1.Text) To CInt(Me.txtPaRok2.Text)
                With Me.dtpOkres
                    .Text = .Text.Replace(Okres.Substring(0, 4), i.ToString)
                End With
                da = New OleDbDataAdapter("SELECT Material, MaterialPL, GrMater, SUM(S.Waga) AS [Waga], SUM(Kwota) AS [Kwota], SUM(Rabat) AS [Rabat] FROM tblSprzedaz S INNER JOIN tblMaterialy M ON S.Material = M.ID " _
                                        + " WHERE Wydzial IN (" + Me.txtPaWydzialy.Text + ") GROUP BY Material, MaterialPL, GrMater", Me.cns)
                dt = New DataTable
                da.Fill(dt)
                If dt.Rows.Count.Equals(0) Then GoTo Nastepny
                ReDim Preserve aDane(6, aDane.GetUpperBound(1) + dt.Rows.Count)
                For k = 0 To dt.Rows.Count - 1
                    j += 1
                    aDane(0, j) = i
                    r = dt.Rows(k)
                    aDane(1, j) = r(0).ToString
                    aDane(2, j) = r(1).ToString
                    aDane(3, j) = r(2).ToString
                    aDane(4, j) = CP.Str2Dbl(r(3).ToString)
                    aDane(5, j) = CP.Str2Dbl(r(4).ToString)
                    aDane(6, j) = CP.Str2Dbl(r(5).ToString)
                Next k
Nastepny:
            Next i
            With New Cl4Excel.Excel(m_WB)
                .oApp.Run("AnalizaSprzedazy", CO_ACTION + " " + Me.txtPaWydzialy.Text.Replace("'", "") + " " + Me.txtPaRok1.Text + " - " + Me.txtPaRok2.Text, aDane)
                HideScreen()
                .Show()
            End With
            HideScreen(False)
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message, CO_ACTION, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
        Finally
            da = Nothing
            dt = Nothing
            aDane = Nothing
            r = Nothing
            GC.Collect()
        End Try
    End Sub

    Private Sub ListaSprzedazyWykonanie()

        Dim m_Ado As New ClADO.ADOGet
        Dim i, k As Integer
        Dim j As Integer = -1
        Dim da As New OleDbDataAdapter
        Dim dt As New DataTable
        Dim r As DataRow
        Dim aDane(,) As Object
        Dim m_File As String = "SD_Sales.xls"
        Const CO_ACTION As String = "List of sales"
        Try
            Dim m_WB As String
            With New Cl4Excel.CheckFile(My.Settings.Wzorce + "CHO_SALES", True, _
                                    Me.txtPaRok1.Text + "-" + Me.txtPaRok2.Text, My.Settings.Wyniki)
                If .NOK Then Exit Try
                My.Settings.Wzorce = .Wzorce
                My.Settings.Wyniki = .Wyniki
                m_WB = .Plik
            End With
            FillStatusBar(, CO_ACTION + "...")
            Dim XE As New Cl4Excel.Excel(m_WB)
            For i = CInt(Me.txtPaRok1.Text) To CInt(Me.txtPaRok2.Text)
                With Me.dtpOkres
                    .Text = .Text.Replace(Okres.Substring(0, 4), i.ToString)
                End With
                da = New OleDbDataAdapter("SELECT Miesiac, S.GrMater, G.GrMaterEN, S.GrDekretacji AS [AcctAssgGr], S.GrDekretacji AS [AcctAssgGrDesc], K.Kraj, IdKlienta, K.Nazwa, SUM(Waga) AS [Weight], SUM(Kwota) AS [Amount], SUM(Rabat) AS [Rabat] " _
                                          + "FROM (tblSprzedaz S INNER JOIN tblGrupyMat G ON s.GrMater = G.ID) INNER JOIN tblKLienci K ON S.IdKlienta = K.ID " _
                                    + "WHERE S.Wydzial IN (" + Me.txtPaWydzialy.Text + ") GROUP BY Miesiac, IdKlienta, K.Nazwa, K.Kraj, S.GrDekretacji, S.GrDekretacji, GrMater, G.GrMaterEN", Me.cns)
                dt = New DataTable
                da.Fill(dt)
                If dt.Rows.Count.Equals(0) Then GoTo Nastepny
                ReDim aDane(dt.Rows.Count, dt.Columns.Count - 1)
                For k = 0 To dt.Columns.Count - 1
                    aDane(0, k) = dt.Columns(k).ColumnName
                Next k
                For j = 1 To dt.Rows.Count
                    r = dt.Rows(j - 1)
                    aDane(j, 0) = r(0).ToString
                    aDane(j, 1) = r(1).ToString
                    aDane(j, 2) = r(2).ToString
                    aDane(j, 3) = r(3).ToString
                    aDane(j, 4) = r(4).ToString
                    aDane(j, 5) = r(5).ToString
                    If String.IsNullOrEmpty(aDane(j, 5)) Then aDane(j, 5) = "Poland"
                    aDane(j, 6) = r(6).ToString
                    aDane(j, 7) = r(7).ToString
                    aDane(j, 8) = CP.Str2Dbl(r(8).ToString)
                    aDane(j, 9) = CP.Str2Dbl(r(9).ToString)
                    aDane(j, 10) = CP.Str2Dbl(r(10).ToString)
                Next j
                XE.oApp.Run("Sprzedane", i.ToString, aDane)
Nastepny:
            Next i
            XE.oApp.Run("TabeleListySprzedazy")
            HideScreen()
            XE.Show()
            HideScreen(False)
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message, CO_ACTION, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
        Finally
            ClearStatus()
            da = Nothing
            dt = Nothing
            aDane = Nothing
            r = Nothing
        End Try
    End Sub

    ' Uruchomienie parametrów wieloletnich
    Private Sub WykonajDlaParametrow() Handles btnPaOk.Click

        Dim i As Integer
        Try
            With Me
                .txtPaWydzialy.Clear()
                With .txtPaRok1
                    If .Enabled Then
                        Try
                            i = CInt(.Text)
                        Catch ex As Exception
                            .Text = Okres.Substring(0, 4)
                        End Try
                    End If
                End With
                If .txtPaRok1.Enabled Then
                    Try
                        i = CInt(.txtPaRok2.Text)
                        If i < CInt(.txtPaRok1.Text) Then Throw New ArgumentException
                    Catch ex As Exception
                        .txtPaRok2.Text = .txtPaRok1.Text
                    End Try
                End If
                .gbParametry.Enabled = False
                Application.DoEvents()
                i = 0
                For Each m_Item As String In .lstPaWybor.CheckedItems
                    .txtPaWydzialy.Text += ", '" + m_Item.ToString + "'"
                    i += 1
                Next m_Item
                With .txtPaWydzialy
                    If String.IsNullOrEmpty(.Text) Then Throw New ArgumentException("Wybierz wydział(y)")
                    .Text = .Text.Substring(2)
                End With
                Select Case .lblPaParent.Text
                    Case "AnalizaSprzedazy"
                        If .cmbPaMiesiace1.SelectedIndex.Equals(-1) Then .cmbPaMiesiace1.SelectedIndex = 0
                        If .cmbPaMiesiace2.SelectedIndex.Equals(-1) Then .cmbPaMiesiace2.SelectedIndex = 11
                        AnalizaSprzedazyWykonanie()
                    Case "ListaSprzedazy"
                        ListaSprzedazyWykonanie()
                    Case "WeryfSprzWydz"
                        WeryfikacjaSprzedazyWydzialowej()
                    Case "AnalizaUzysku"
                        If Not i.Equals(1) Then _
                            Throw New ArgumentException("Analiza uzysku odbywa się dla pojedyńczych wydziałów")
                        AnalizaUzysku()
                End Select
            End With
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message, Me.lblPaParent.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
        End Try
        Me.gbParametry.Enabled = True
    End Sub

    Private Sub KosztyLogistyki() Handles tsm_Eg.Click

        Dim da As OleDbDataAdapter
        Dim dt As New DataTable
        Dim i, j As Integer
        Dim SQL As String = ""
        Dim aKoszty(0, 0) As Object
        Const CO_ACTION As String = "Koszty logistyki"
        Try
            Dim m_WB As String = Nothing
            If NotExportFile(m_WB) Then Exit Try
            FillStatusBar(dt.Rows.Count, CO_ACTION)
            SQL = "SELECT C.Logistic, Z.MPK, C.MpkOfLogistic, SUM(Z.Ilosc) AS [Ilosc], SUM(Z.Wartosc) AS [Wartosc] " _
                  + "FROM tblZWPC Z INNER JOIN tblCalCo C ON Z.CalCo = C.ID " _
                  + "WHERE Mc = " + Me.dtpOkres.Value.Month.ToString + " AND NOT C.Logistic IS NULL " _
                  + "GROUP BY C.Logistic, Z.MPK, C.MpkOfLogistic ORDER BY C.Logistic, Z.MPK"
            dt = New DataTable
            da = New OleDbDataAdapter(SQL, Me.cns)
            da.Fill(dt)
            If dt.Rows.Count.Equals(0) Then Throw New ArgumentException("Brak danych kosztowych")
            ReDim aKoszty(dt.Rows.Count - 1, dt.Columns.Count - 1)
            For i = 0 To dt.Rows.Count - 1
                For j = 0 To 4
                    aKoszty(i, j) = dt.Rows(i)(j)
                Next j
            Next i
            With New Cl4Excel.Excel(m_WB)
                .oApp.Run("KosztyLogistyki", aKoszty, Me.dtpOkres.Value.Month, Okres.Substring(0, 4))
                HideScreen()
                .Show()
            End With
            HideScreen(False)
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message, CO_ACTION, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
        Finally
            da = Nothing
            dt = Nothing
            aKoszty = Nothing
        End Try
    End Sub

    Private Sub AnalizaUzysku()

        Dim iMc As Integer
        Dim iRok As Integer
        Dim da As OleDbDataAdapter = New OleDbDataAdapter("SELECT M.ID, GrMaterID, INEFGroup FROM tblMaterialy M INNER JOIN tblGrupyMat G ON M.GrMaterID = G.ID", Me.cns)
        Dim dtM As New DataTable
        Dim dtB As New DataTable
        Dim dvB As New DataView
        Dim i, j, iR As Integer
        Dim rd As DataRow = Nothing
        Dim m_Txt As String
        Dim m_Ark As String
        Dim aDane As Array
        Dim bs As New BindingSource
        Const CO_ACTION As String = "Analiza uzysku walcowni"
        Try
            With Me
                With .txtKomunikat
                    .Clear()
                    .Visible = False
                End With
                iRok = .dtpOkres.Value.Year
            End With
            FillStatusBar(, "Odczyt danych źródłowych...")
            da.Fill(dtM)
            bs.DataSource = dtM
            dtB = New DataTable
            With dtB
                With .Columns
                    .Add("Wydzial")
                    .Add("Grupa")
                    .Add("Fi")
                    .Add("Material")
                    .Add("Opis")
                    For i = 1 To 12
                        .Add("Waga_" + i.ToString, GetType(Double))
                        .Add("Wsad_" + i.ToString, GetType(Double))
                        .Add("Uzysk_" + i.ToString, GetType(Double))
                    Next i
                End With
                dvB = .DefaultView
            End With
            dvB.Sort = "Wydzial, Material"
            For Each m_Item As String In Me.lstPaWybor.CheckedItems
                For Each m_File As String In My.Computer.FileSystem.GetFiles("W:\Costs\Celsa\Produkcja SAP\", _
                                                                            FileIO.SearchOption.SearchAllSubDirectories, m_Item + "_" + Okres.Substring(0, 4) + "*.xl*")
                    m_Ark = CP.GetYearMonth(m_File, "_")
                    iMc = CInt(m_Ark.Substring(5, 2))
                    With New Cl4Excel.GetFromXls(m_File, m_Item + "_" + m_Ark, "D7")
                        .Data2Array() : aDane = .Dane : .Zwolnij()
                    End With
                    If aDane Is Nothing Then GoTo Nastepny
                    i = 1
                    Do Until i > aDane.GetLength(0) OrElse aDane.GetValue(i, 1) Is Nothing
                        rd = dtB.NewRow
                        For j = 1 To 12
                            rd("Waga_" + j.ToString) = 0
                            rd("Wsad_" + j.ToString) = 0
                            rd("Uzysk_" + j.ToString) = 0
                        Next j
                        m_Txt = aDane.GetValue(i, 3).ToString.Replace("-", "").Substring(0, 6)
                        iR = dvB.Find(New Object() {m_Item, m_Txt})
                        rd("Wydzial") = m_Item
                        rd("Material") = m_Txt
                        If m_Item = "R2" Then
                            rd("Fi") = ""
                        Else
                            rd("Fi") = m_Txt.Substring(4)
                        End If
                        rd("Opis") = aDane.GetValue(i, 4)
                        Try
                            rd("Waga_" + iMc.ToString) = aDane.GetValue(i, 2)
                        Catch ex As Exception
                            rd("Waga_" + iMc.ToString) = 0
                        End Try
                        Try
                            rd("Wsad_" + iMc.ToString) = aDane.GetValue(i, 1)
                        Catch ex As Exception
                            rd("Wsad_" + iMc.ToString) = 0
                        End Try
                        If iR < 0 Then
                            j = bs.Find("ID", aDane.GetValue(i, 3))
                            If j > -1 Then
                                bs.Position = j
                                rd("Grupa") = CType(bs.Current, DataRowView)("INEFGroup").ToString
                            End If
                            dtB.Rows.Add(rd)
                        Else
                            Try
                                dvB(iR)("Waga_" + iMc.ToString) += rd("Waga_" + iMc.ToString)
                            Catch ex As Exception
                            End Try
                            Try
                                dvB(iR)("Wsad_" + iMc.ToString) += rd("Wsad_" + iMc.ToString)
                            Catch ex As Exception
                            End Try
                        End If
                        i += 1
                    Loop
                Next m_File
Nastepny:
            Next m_Item
            For i = 0 To dtB.Rows.Count - 1
                For j = 1 To 12
                    With dtB.Rows(i)
                        Try
                            .Item("Uzysk_" + j.ToString) = 100 * (1 - (.Item("Waga_" + j.ToString) / .Item("Wsad_" + j.ToString)))
                        Catch ex As Exception
                        End Try
                    End With
                Next j
            Next i
            dvB.Sort = "Wydzial, Grupa, Fi"
            Dim dt As New DataTable
            dt = dtB.Copy
            dt.Clear()
            With dt.Columns
                .Remove("Material")
                .Remove("Opis")
            End With
            i = 0
            iR = -1
            Do
                With dvB(i)
                    m_Txt = .Item("Grupa") + .Item("Fi")
                    dt.Rows.Add(New Object() {.Item("Wydzial"), .Item("Grupa"), .Item("Fi")})
                    iR += 1
                    With dt.Rows(iR)
                        For j = 1 To 12
                            .Item("Waga_" + j.ToString) = 0
                            .Item("Wsad_" + j.ToString) = 0
                            .Item("Uzysk_" + j.ToString) = 0
                        Next j
                    End With
                End With
                While i < dtB.Rows.Count AndAlso m_Txt.Equals(dvB(i)("Grupa") + dvB(i)("Fi"))
                    With dvB(i)
                        For j = 1 To 12
                            Try
                                dt(iR)("Waga_" + j.ToString) += .Item("Waga_" + j.ToString)
                            Catch ex As Exception
                            End Try
                            Try
                                dt(iR)("Wsad_" + j.ToString) += .Item("Wsad_" + j.ToString)
                            Catch ex As Exception
                            End Try
                        Next j
                    End With
                    i += 1
                End While
            Loop Until i.Equals(dtB.Rows.Count)
            For i = 0 To dt.Rows.Count - 1
                With dt.Rows(i)
                    For j = 1 To 12
                        Try
                            If .Item("Wsad_" + j.ToString) = 0 Then Throw New ArgumentException
                            .Item("Uzysk_" + j.ToString) = 100 * (1 - (.Item("Waga_" + j.ToString) / .Item("Wsad_" + j.ToString)))
                        Catch ex As Exception
                            .Item("Uzysk_" + j.ToString) = 0
                        End Try
                    Next j
                End With
            Next i
            ClearStatus()
            With My.Computer.FileSystem
                If .FileExists("C:\Celsa\Uzysk.xls") Then _
                    .DeleteFile("C:\Celsa\Uzysk.xls")
            End With
            Dim aVal(dt.Rows.Count * 12, 5) As Object
            aVal(0, 0) = "Month"
            aVal(0, 1) = "Group"
            aVal(0, 2) = "Fi"
            aVal(0, 3) = "Weight"
            aVal(0, 4) = "Billets"
            If "MS_WS".Contains(dt(0)(0).ToString) Then
                aVal(0, 4) = "Charge"
            End If
            aVal(0, 5) = "Loss (%)"
            For i = 0 To dt.Rows.Count - 1
                With dt.Rows(i)
                    For j = 1 To 12
                        aVal(i * 12 + j, 0) = j
                        aVal(i * 12 + j, 1) = .Item(1)
                        aVal(i * 12 + j, 2) = .Item(2)
                        aVal(i * 12 + j, 3) = .Item(3 * j)
                        aVal(i * 12 + j, 4) = .Item(3 * j + 1)
                        aVal(i * 12 + j, 5) = .Item(3 * j + 2)
                    Next j
                End With
            Next i
            With New Cl4Excel.Excel(Nothing)
                With .oSheet
                    .Range("A1").Resize(aVal.GetUpperBound(0) + 1, aVal.GetUpperBound(1) + 1).Value = aVal
                    .Name = "Uzysk"
                End With
                With .oApp
                    .ActiveWindow.Zoom = 75
                    .ActiveSheet.Range("A2").Select()
                    .ActiveWindow.FreezePanes = True
                End With
                HideScreen()
                .Show()
            End With
            HideScreen(False)
            aVal = Nothing
        Catch ex As Exception
            ClearStatus()
            Using New CM
                MessageBox.Show(ex.Message, CO_ACTION, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
        Finally
            aDane = Nothing
            dtB = Nothing
            dvB = Nothing
        End Try
    End Sub

    Private Sub DaneMonitoringu() Handles tsm_Em.Click

        Dim da As New OleDbDataAdapter
        Dim dt As New DataTable
        Dim dv As DataView
        Dim i, j As Integer
        Dim aDane(,) As Object
        Dim _Blad As String
        Const CO_ACTION As String = "Export danych monitoringu"
        Try
            Dim m_WB As String
            With New Cl4Excel.CheckFile(My.Settings.Wzorce + "CHO_MONITOR.xls", True, , My.Settings.Wyniki)
                If .NOK Then Exit Try
                My.Settings.Wyniki = .Wyniki
                My.Settings.Wzorce = .Wzorce
                m_WB = .Plik
            End With
            FillStatusBar(, CO_ACTION + " - sprzedaż...")
            ' Sprzedaż: rok ubiegły mc 4
            With Me.dtpOkres
                .Text = .Text.Replace(Okres.Substring(0, 4), (.Value.Year - 1).ToString)
            End With
            da = New OleDbDataAdapter("SELECT GrMater, GrDekretacji, '4' AS [Kol], SUM(Waga) AS [Mg], SUM(Kwota-Rabat) AS [Sprzedaz], SUM(PIP) AS [PIP], SUM(Transport) AS [Transport], SUM(Other) AS [Other], SUM(Overhead) AS [Overhead], SUM(Pozostale) AS [Pozostale], SUM(EBiTDA) AS [EBiTDA] " _
                                     + "FROM tblSprzedaz WHERE Miesiac = " + Me.dtpOkres.Value.Month.ToString + " GROUP BY GrMater, GrDekretacji", Me.cns)
            da.Fill(dt)
            ' Sprzedaż: rok ubiegły 1 - mc 5
            da = New OleDbDataAdapter("SELECT GrMater, GrDekretacji, '5' AS [Kol], SUM(Waga) AS [Mg], SUM(Kwota-Rabat) AS [Sprzedaz], SUM(PIP) AS [PIP], SUM(Transport) AS [Transport], SUM(Other) AS [Other], SUM(Overhead) AS [Overhead], SUM(Pozostale) AS [Pozostale], SUM(EBiTDA) AS [EBiTDA] " _
                                     + "FROM tblSprzedaz WHERE Miesiac <= " + Me.dtpOkres.Value.Month.ToString + " GROUP BY GrMater, GrDekretacji", Me.cns)
            da.Fill(dt)
            ' Sprzedaż: aktualny mc - plan 6
            With Me.dtpOkres
                .Text = .Text.Replace(Okres.Substring(0, 4), (.Value.Year + 1).ToString)
            End With
            da = New OleDbDataAdapter("SELECT GrMater, GrDekretacji, '6' AS [Kol], SUM(Waga) AS [Mg], SUM(Kwota-Rabat) AS [Sprzedaz], SUM(PIP) AS [PIP], SUM(Transport) AS [Transport], SUM(Other) AS [Other], SUM(Overhead) AS [Overhead], SUM(Pozostale) AS [Pozostale], SUM(EBiTDA) AS [EBiTDA] " _
                                     + "FROM tblPLSprzedaz WHERE Miesiac = " + Me.dtpOkres.Value.Month.ToString + " GROUP BY GrMater, GrDekretacji", Me.cns)
            da.Fill(dt)
            ' Sprzedaż: aktualny mc - wykon 7
            da = New OleDbDataAdapter("SELECT GrMater, GrDekretacji, '7' AS [Kol], SUM(Waga) AS [Mg], SUM(Kwota-Rabat) AS [Sprzedaz], SUM(PIP) AS [PIP], SUM(Transport) AS [Transport], SUM(Other) AS [Other], SUM(Overhead) AS [Overhead], SUM(Pozostale) AS [Pozostale], SUM(EBiTDA) AS [EBiTDA] " _
                                     + "FROM tblSprzedaz WHERE Miesiac = " + Me.dtpOkres.Value.Month.ToString + " GROUP BY GrMater, GrDekretacji", Me.cns)
            da.Fill(dt)
            ' Sprzedaż: narastająco - budżet 12
            da = New OleDbDataAdapter("SELECT GrMater, GrDekretacji, '12' AS [Kol], SUM(Waga) AS [Mg], SUM(Kwota-Rabat) AS [Sprzedaz], SUM(PIP) AS [PIP], SUM(Transport) AS [Transport], SUM(Other) AS [Other], SUM(Overhead) AS [Overhead], SUM(Pozostale) AS [Pozostale], SUM(EBiTDA) AS [EBiTDA] " _
                                     + "FROM tblBUSprzedaz WHERE Miesiac <= " + Me.dtpOkres.Value.Month.ToString + " GROUP BY GrMater, GrDekretacji", Me.cns)
            da.Fill(dt)
            ' Sprzedaż: narastająco - wykon 13
            da = New OleDbDataAdapter("SELECT GrMater, GrDekretacji, '13' AS [Kol], SUM(Waga) AS [Mg], SUM(Kwota-Rabat) AS [Sprzedaz], SUM(PIP) AS [PIP], SUM(Transport) AS [Transport], SUM(Other) AS [Other], SUM(Overhead) AS [Overhead], SUM(Pozostale) AS [Pozostale], SUM(EBiTDA) AS [EBiTDA] " _
                                     + "FROM tblSprzedaz WHERE Miesiac <= " + Me.dtpOkres.Value.Month.ToString + " GROUP BY GrMater, GrDekretacji", Me.cns)
            da.Fill(dt)
            dv = dt.DefaultView
            dv.Sort = "GrMater, GrDekretacji, Kol"
            dv.RowFilter = "GrMater <> '' AND GrMater <> '#'"
            With dt
                ReDim aDane(dv.Count, .Columns.Count - 1)
                For j = 0 To .Columns.Count - 1
                    aDane(0, j) = .Columns(j).ColumnName
                Next j
            End With
            i = 0
            For Each drv As DataRowView In dv
                i += 1
                aDane(i, 0) = drv(0)
                aDane(i, 1) = CInt(drv(1))
                aDane(i, 2) = CInt(drv(2))
                For j = 3 To dt.Columns.Count - 1
                    Try
                        aDane(i, j) = CDbl(drv(j))
                    Catch ex As Exception
                        aDane(i, j) = 0
                    End Try
                Next j
            Next drv
            With New Cl4Excel.Excel(m_WB)
                With .oApp
                    .Run("BiezacyOkres", Okres.Substring(0, 4), Me.dtpOkres.Value.Month)
                    .Run("Sprzedaz", aDane)
                    _Blad = .Range("Blad").Value2
                End With
                HideScreen()
                .Show()
            End With
            If Not String.IsNullOrEmpty(_Blad) Then Throw New ArgumentException(_Blad)
        Catch ex As Exception
            ClearStatus()
            Using New CM
                MessageBox.Show(ex.Message, CO_ACTION, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
        Finally
            HideScreen(False)
            da = Nothing
            dt = Nothing
            dv = Nothing
        End Try
    End Sub

    Private Sub RankingKlientow() Handles tsm_ER.Click

        Dim i, k As Integer
        Dim j As Integer = -1
        Dim da As New OleDbDataAdapter
        Dim dt As New DataTable
        Dim dv As DataView
        Dim v As DataRowView
        Dim aDane1(9, 3) As Object
        Dim aDane2(9, 3) As Object
        Dim aDane3(9, 3) As Object
        Dim aDane4(9, 3) As Object
        Dim aDane5(3, 1) As Double
        Const CO_ACTION As String = "Ranking klientów"
        Try
            Dim m_WB As String
            With New Cl4Excel.CheckFile(My.Settings.Wzorce + "CHO_RANKING", True, , My.Settings.Wyniki)
                If .NOK Then Exit Try
                My.Settings.Wzorce = .Wzorce
                My.Settings.Wyniki = .Wyniki
                m_WB = .Plik
            End With
            FillStatusBar(, CO_ACTION + "...")
            For i = 0 To 3
                Select Case i
                    Case 0  ' FD top 10
                        da = New OleDbDataAdapter("SELECT IdKlienta, K.Nazwa, SUM(Waga), SUM(Kwota) AS [Kwota], SUM(Rabat) FROM tblSprzedaz S INNER JOIN tblKlienci K ON S.IdKlienta = K.ID " _
                                                    + " WHERE Wydzial IN ('MS','PS','MT','HT') AND Miesiac = " + Me.dtpOkres.Value.Month.ToString + " GROUP BY IdKlienta, K.Nazwa", Me.cns)
                    Case 1  ' FD miesięczne
                        da = New OleDbDataAdapter("SELECT IdKlienta, K.Nazwa, SUM(Waga), SUM(Kwota) AS [Kwota], SUM(Rabat) FROM tblSprzedaz S INNER JOIN tblKlienci K ON S.IdKlienta = K.ID " _
                                                    + " WHERE Wydzial IN ('MS','PS','MT','HT') GROUP BY IdKlienta, K.Nazwa", Me.cns)
                    Case 2  ' SD top 10
                        da = New OleDbDataAdapter("SELECT IdKlienta, K.Nazwa, SUM(Waga), SUM(Kwota) AS [Kwota], SUM(Rabat) FROM tblSprzedaz S INNER JOIN tblKlienci K ON S.IdKlienta = K.ID " _
                                                    + " WHERE Wydzial IN ('WS','R1','R2') AND Miesiac = " + Me.dtpOkres.Value.Month.ToString + " GROUP BY IdKlienta, K.Nazwa", Me.cns)
                    Case 3  ' SD miesięczne
                        da = New OleDbDataAdapter("SELECT IdKlienta, K.Nazwa, SUM(Waga), SUM(Kwota) AS [Kwota], SUM(Rabat) FROM tblSprzedaz S INNER JOIN tblKlienci K ON S.IdKlienta = K.ID " _
                                                    + " WHERE Wydzial IN ('WS','R1','R2') GROUP BY IdKlienta, K.Nazwa", Me.cns)
                End Select
                dt = New DataTable
                da.Fill(dt)
                If dt.Rows.Count.Equals(0) Then GoTo Nastepny
                For Each r As DataRow In dt.Rows
                    r(3) = r(3) + r(4)
                Next r
                dv = dt.DefaultView
                dv.Sort = "Kwota DESC"
                j = 0
                For k = 0 To dv.Count - 1
                    v = dv(k)
                    If j > 9 Then GoTo Total
                    Select Case i
                        Case 0
                            aDane1(j, 0) = v(0).ToString
                            aDane1(j, 1) = v(1).ToString
                            aDane1(j, 2) = v(2)
                            aDane1(j, 3) = v(3)
                        Case 1
                            aDane2(j, 0) = v(0).ToString
                            aDane2(j, 1) = v(1).ToString
                            aDane2(j, 2) = v(2)
                            aDane2(j, 3) = v(3)
                        Case 2
                            aDane3(j, 0) = v(0).ToString
                            aDane3(j, 1) = v(1).ToString
                            aDane3(j, 2) = v(2)
                            aDane3(j, 3) = v(3)
                        Case 3
                            aDane4(j, 0) = v(0).ToString
                            aDane4(j, 1) = v(1).ToString
                            aDane4(j, 2) = v(2)
                            aDane4(j, 3) = v(3)
                    End Select
                    aDane5(i, 1) += v(3)
Total:
                    aDane5(i, 0) += v(3)
                    j += 1
                Next k
Nastepny:
            Next i
            Dim XE As New Cl4Excel.Excel(m_WB)
            Dim m_Date As Date = Now
            Dim m_Txt As String = CP.FullEnglishDate(dm:=Me.dtpOkres.Value, BezDnia:=True)
            For i = 0 To 1
                Select Case i
                    Case 0
                        With XE
                            .oSheet = CType(.oBook.Sheets("FD_Top10"), XL._Worksheet)
                            With .oSheet
                                .Range("B4").Value = Now.ToShortDateString
                                .Range("D4").Value = m_Txt
                                .Range("D17").Value = "January - " + m_Txt
                                .Range("B6:E15").Value = aDane1
                                .Range("B19:E28").Value = aDane2
                                .Range("H6:I6").Value = New Object() {aDane5(0, 0), aDane5(0, 1)}
                                .Range("H19:I19").Value = New Object() {aDane5(1, 0), aDane5(1, 1)}
                            End With
                        End With
                    Case 1
                        With XE
                            .oSheet = CType(.oBook.Sheets("SD_Top10"), XL._Worksheet)
                            With .oSheet
                                .Range("B4").Value = Now.ToShortDateString
                                .Range("D4").Value = m_Txt
                                .Range("D17").Value = "January - " + m_Txt
                                .Range("B6:E15").Value = aDane3
                                .Range("B19:E28").Value = aDane4
                                .Range("H6:I6").Value = New Object() {aDane5(2, 0), aDane5(2, 1)}
                                .Range("H19:I19").Value = New Object() {aDane5(3, 0), aDane5(3, 1)}
                            End With
                        End With
                End Select
            Next i
            HideScreen()
            XE.Show()
            HideScreen(False)
        Catch ex As Exception
            ClearStatus()
            Using New CM
                MessageBox.Show(ex.Message, CO_ACTION, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
        Finally
            da = Nothing
            dt = Nothing
            dv = Nothing
            aDane1 = Nothing
            aDane2 = Nothing
            aDane3 = Nothing
            aDane4 = Nothing
            aDane5 = Nothing
            v = Nothing
        End Try
    End Sub

    Private Sub PrzestarzaleZapasy() Handles tsm_Ea.Click
        ' Analiza zapasów na podstawie MB51
        'Materiał	Zakł	Skł.	RR	S	Dok. mat.	Poz.	Data księg	Il. w jed. wpr.	Kwota w WKr	JWp
        ' i ZWMM003Q
        'Zakł	Rodz	Grupa mat.	Gr. mater.	Skład	Materiał	Krótki tekst materiału	Gr.koszt.	Opis reguły	Okres.pocz	Okres końc	JM	Końcowa ilość na skł	Końcowa wartość na s
        Static _Path As String = "C:\Tmp\Dane\"
        Dim da As OleDbDataAdapter = New OleDbDataAdapter("SELECT ID, MaterialPL FROM tblMaterialy WHERE LEFT(ID,2) <> '35'", Me.cns)
        Dim dtT As New DataTable, dt As New DataTable, dtM As New DataTable
        Dim dv, dvM As DataView
        Dim r As DataRow
        Dim v As DataRowView
        Dim _Ado As New ClADO.ADOGet
        Dim Tytul As String = Me.tsm_Ea.Text.Replace("&", "")
        Dim m_File As String = "ZWMM003Q_*.xls"
        Dim m_Path As String = InputBox("Dane znajdują się:", Tytul, _Path, CP.PositionX, CP.PositionY)
        Dim i, j, w, _Mod As Integer
        Dim _P, _R, _Pw, _Rw, _Ms, _Ilosc, _Wart As Double
        Dim _dmP, _dmR, _dm, _dmS As Date
        If String.IsNullOrEmpty(m_Path) Then Return
        Dim txt As String = InputBox("Podaj datę krytyczną:", Tytul, DateSerial(Me.dtpOkres.Value.Year, 1, 1).ToShortDateString, CP.PositionX, CP.PositionY)
        If String.IsNullOrEmpty(txt) Then Return
        Try
            _Path = m_Path
            _dmS = CDate(txt)
            With dtT.Columns
                .Add("Magazyn")
                .Add("Symbol")
                .Add("MaterialOpis")
                .Add("Jm")
                .Add("PoczIlosc", GetType(Double))
                .Add("PoczWart", GetType(Double))
                .Add("PrzychIlosc", GetType(Double))
                .Add("PrzychWart", GetType(Double))
                .Add("PrzychData", GetType(Date))
                .Add("RozchIlosc", GetType(Double))
                .Add("RozchWart", GetType(Double))
                .Add("RozchData", GetType(Date))
                .Add("PrzesIlosc", GetType(Double))
                .Add("StKoncowy", GetType(Double))
            End With
            dv = dtT.DefaultView
            dv.Sort = "Magazyn, Symbol"
            da.Fill(dtM)
            dvM = dtM.DefaultView
            dvM.Sort = "ID"
            For Each _File In My.Computer.FileSystem.GetFiles(m_Path, FileIO.SearchOption.SearchTopLevelOnly, m_File)
                m_File = _File.Substring(m_Path.Length)
                With _Ado
                    .Baza = m_File
                    .Folder = m_Path
                    .HDR = True
                    If Not .OpenData(True) Then
                        Using New CM
                            MessageBox.Show("Brak dostępu do pliku [" + .Baza + "]", Tytul, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        End Using
                        Exit Sub
                    End If
                End With
                FillStatusBar(0, m_File + " - odczyt danych...")
                da = New OleDbDataAdapter("SELECT * FROM [Arkusz1$]  ", _Ado.CN)
                dt = New DataTable
                da.Fill(dt)
                For Each r In dt.Rows
                    If Not CDbl(r(13).ToString).Equals(0.0) Then
                        dtT.Rows.Add(New Object() {r(4), r(5), r(6), r(11), r(12), r(13)})
                    End If
                Next r
            Next _File
            m_File = "MB51_*.xls"
            For Each _File In My.Computer.FileSystem.GetFiles(m_Path, FileIO.SearchOption.SearchTopLevelOnly, m_File)
                m_File = _File.Substring(m_Path.Length)
                With _Ado
                    .Baza = m_File
                    .Folder = m_Path
                    .HDR = True
                    If Not .OpenData(True) Then
                        Using New CM
                            MessageBox.Show("Brak dostępu do pliku [" + .Baza + "]", Tytul, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        End Using
                        Exit Sub
                    End If
                End With
                da = New OleDbDataAdapter("SELECT * FROM [Arkusz1$]  ", _Ado.CN)
                dt = New DataTable
                da.Fill(dt)
                w = 0
                _Mod = dt.Rows.Count \ 100
                FillStatusBar(dt.Rows.Count, m_File + " - odczyt danych...")
                For Each r In dt.Rows
                    _dm = CDate(r(7).ToString)
                    _Ilosc = CDbl(r(8).ToString)
                    _Wart = CDbl(r(9).ToString)
                    i = dv.Find(New Object() {r(2), r(0)})
                    If i.Equals(-1) Then
                        j = dvM.Find(r(0).ToString)
                        If j.Equals(-1) Then GoTo Nastepny
                        dtT.Rows.Add(New Object() {r(2), r(0), dvM(j)("MaterialPL"), r(10), 0, 0})
                        i = dv.Find(New Object() {r(2), r(0)})
                    End If
                    v = dv(i)
                    Try
                        _P = CDbl(v("PrzychIlosc").ToString)
                    Catch ex As Exception
                        _P = 0
                    End Try
                    Try
                        _Pw = CDbl(v("PrzychWart").ToString)
                    Catch ex As Exception
                        _Pw = 0
                    End Try
                    Try
                        _dmP = CDate(v("PrzychData").ToString)
                    Catch ex As Exception
                        _dmP = CDate("1900-01-01")
                    End Try
                    Try
                        _R = CDbl(v("RozchIlosc").ToString)
                    Catch ex As Exception
                        _R = 0
                    End Try
                    Try
                        _Rw = CDbl(v("RozchWart").ToString)
                    Catch ex As Exception
                        _Rw = 0
                    End Try
                    Try
                        _dmR = CDate(v("RozchData").ToString)
                    Catch ex As Exception
                        _dmR = CDate("1900-01-01")
                    End Try
                    Try
                        _Ms = CDbl(v("PrzesIlosc").ToString)
                    Catch ex As Exception
                        _Ms = 0
                    End Try
                    Select Case r(3).ToString
                        Case "101", "102"
                            v("PrzychIlosc") = _P + _Ilosc
                            v("PrzychWart") = _Pw + _Wart
                            If _dm.CompareTo(_dmP).Equals(1) Then _
                                v("PrzychData") = _dm
                        Case "201", "202", "261", "262"
                            v("RozchIlosc") = _R - _Ilosc
                            v("RozchWart") = _Rw - _Wart
                            If _dm.CompareTo(_dmR).Equals(1) Then _
                                v("RozchData") = _dm
                        Case Else
                            v("PrzesIlosc") = _Ms + _Ilosc
                    End Select
Nastepny:
                    If (w Mod _Mod).Equals(0) Then _
                        Me.tspbMain.Value = w : Application.DoEvents()
                    w += 1
                Next r
            Next _File
            For i = dtT.Rows.Count - 1 To 0 Step -1
                r = dtT.Rows(i)
                Try
                    _P = CDbl(r("PrzychIlosc").ToString)
                Catch ex As Exception
                    _P = 0
                End Try
                Try
                    _dmP = CDate(r("PrzychData").ToString)
                Catch ex As Exception
                    _dmP = CDate("1900-01-01")
                End Try
                Try
                    _R = CDbl(r("RozchIlosc").ToString)
                Catch ex As Exception
                    _R = 0
                End Try
                Try
                    _dmR = CDate(r("RozchData").ToString)
                Catch ex As Exception
                    _dmR = CDate("1900-01-01")
                End Try
                Try
                    _Ms = CDbl(r("PrzesIlosc").ToString)
                Catch ex As Exception
                    _Ms = 0
                End Try
                _Ilosc = CDbl(r("PoczIlosc").ToString)
                If (_Ilosc + _P - _R + _Ms).Equals(0.0) Then
                    r.Delete()
                Else
                    If _dmP.CompareTo(_dmS) > 0 OrElse _dmR.CompareTo(_dmS) > 0 Then
                        r.Delete()
                    Else
                        r("StKoncowy") = _Ilosc + _P - _R + _Ms
                    End If
                End If
            Next i
            dtT.AcceptChanges()
            Dim aVal(dtT.Rows.Count, dtT.Columns.Count - 1) As Object
            For i = 0 To dtT.Columns.Count - 1
                aVal(0, i) = dtT.Columns(i).ColumnName
            Next i
            For j = 0 To dv.Count - 1
                For i = 0 To dtT.Columns.Count - 1
                    aVal(j + 1, i) = dv(j)(i)
                Next i
            Next j
            With New Cl4Excel.Excel(Nothing)
                .oSheet.Range("A1").Resize(aVal.GetUpperBound(0) + 1, aVal.GetUpperBound(1) + 1).Value = aVal
                HideScreen()
                .Show()
            End With
            HideScreen(False)
            aVal = Nothing
        Catch ex As Exception
            ClearStatus()
            Using New CM
                MessageBox.Show(ex.Message, Tytul, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
        Finally
            ClearStatus()
            dtT = Nothing
            dtM = Nothing
            dt = Nothing
            da = Nothing
            dv = Nothing
            dvM = Nothing
        End Try
    End Sub

    Private Sub PodzialZapasow_Click() Handles tsm_Eo.Click

        Dim da As OleDbDataAdapter = New OleDbDataAdapter("SELECT Division, K.Description, K.GroupOfStock, K.Waga, G.Department, SUM(Z.Waga) AS [T], SUM(Kwota) AS [PLN] " _
                                            + "FROM ((tblZapasy Z INNER JOIN tblMaterialy M ON Z.Material = M.ID) " _
                                            + "INNER JOIN tblGrKoszt K ON M.GrKosztID = K.ID) LEFT JOIN tblMagazyny G ON Z.Sklad = G.ID " _
                                            + "WHERE Miesiac = " + Me.dtpOkres.Value.Month.ToString + " AND NOT (K.GroupOfStock IS NULL OR K.GroupOfStock = 0) " _
                                            + "GROUP BY Division, K.Description, K.GroupOfStock, K.Waga, G.Department ORDER BY Division, K.GroupOfStock", Me.cns)
        Dim dt As New DataTable
        Dim i, j As Integer
        Dim _Key As String
        Try
            Dim XE As New Cl4Excel.Excel("W:\Costs\CHO OBROTY\S35_OBROTY_" & Me.dtpOkres.Value.Year & "\S35_Mater32_" & Me.dtpOkres.Value.Year & "rz.xls")
            FillStatusBar(, "Pobieranie danych...")
            da.Fill(dt)
            If dt.Rows.Count.Equals(0) Then Throw New ArgumentException("Brak danych miesięcznych")
            Dim fs As XL.Range
            j = Me.dtpOkres.Value.Month
            FillStatusBar(, "Aktualizacja arkuszy...")
            Dim query = (From p In dt.AsEnumerable Where p("Division").Equals("FD"))
            With XE
                .oSheet = CType(.oBook.Sheets("FD"), XL.Worksheet)
                'oczyść
                .oSheet.Range("A1").CurrentRegion.Offset(1, 2 + j).Resize(, 1).ClearContents()
                For Each q In query
                    With .oSheet
                        fs = .Columns(2).Find(q(1), LookAt:=XL.XlLookAt.xlPart, lookin:=XL.XlFindLookIn.xlValues)
                        If Not fs Is Nothing Then
                            i = fs.Row
                            _Key = .Range("B" & i).Value2.ToString.Trim
                            If Not _Key.Equals(q(1).ToString) Then
                                If Not _Key.Contains(q(4).ToString) Then i += 1
                            End If
                            If q(3) Then
                                .Cells(i, j + 3) = q(5) / 1000
                                .Cells(i + 1, j + 3) = q(6)
                            Else
                                .Cells(i, j + 3) = q(6)
                            End If
                        End If
                    End With
                Next q
            End With
            query = Nothing
            query = (From p In dt Where p("Division").Equals("SD"))
            With XE
                .oSheet = CType(.oBook.Sheets("SD"), XL.Worksheet)
                'oczyść
                .oSheet.Range("A1").CurrentRegion.Offset(1, 2 + j).Resize(, 1).ClearContents()
                For Each q In query
                    If IsDBNull(q(1)) Then GoTo Nastepny
                    With .oSheet
                        fs = .Columns(2).Find(q(1), LookAt:=XL.XlLookAt.xlPart, lookin:=XL.XlFindLookIn.xlValues)
                        If Not fs Is Nothing Then
                            i = fs.Row
                            _Key = .Range("B" & i).Value2.ToString.Trim
                            If Not _Key.Equals(q(1).ToString) Then
                                If Not _Key.Contains(q(4).ToString) Then i += 1
                            End If
                            If q(3) Then
                                .Cells(i, j + 3) = q(5) / 1000
                                .Cells(i + 1, j + 3) = q(6)
                            Else
                                .Cells(i, j + 3) = q(6)
                            End If
                        End If
                    End With
Nastepny:
                Next q
                .oBook.Save()
                .Zwolnij()
            End With
        Catch ex As Exception
            ClearStatus()
            Using New CM
                MessageBox.Show(ex.Message, Me.tsm_Eo.Text.Replace("&", ""), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Using
        Finally
            dt = Nothing
            da = Nothing
        End Try
    End Sub

    Private Sub NowiKlienci() Handles tsm_Ew.Click

        Dim da As OleDbDataAdapter = New OleDbDataAdapter("SELECT DISTINCT IDKlienta, Miesiac FROM tblSprzedaz WHERE Miesiac <= " _
                                                          & Me.dtpOkres.Value.Month & " AND Wydzial IN ('MS', 'PS', 'MT')", Me.cns)
        Dim dt As New DataTable
        Dim dv As DataView
        Dim _Key, i, j As Integer
        Dim Wybrani As String = ""
        Dim bln As Boolean
        Try
            FillStatusBar(, Me.tsm_Ew.Text.Replace("&", "") + "...")
            da.Fill(dt)
            dv = dt.DefaultView
            dv.Sort = "IDKlienta, Miesiac DESC"
            Do Until i.Equals(dv.Count)
                If CInt(dv(i)("Miesiac").ToString).Equals(Me.dtpOkres.Value.Month) Then
                    _Key = dv(i)("IDKlienta")
                    bln = True
                    Do While _Key = dv(i)("IDKlienta")
                        If Not CInt(dv(i)("Miesiac").ToString).Equals(Me.dtpOkres.Value.Month) Then bln = False
                        i += 1
                        If i.Equals(dv.Count) Then Exit Do
                    Loop
                    If bln Then Wybrani += ", " & _Key.ToString
                Else
                    i += 1
                End If
            Loop
            If String.IsNullOrEmpty(Wybrani) Then Throw New ArgumentException("Brak nowych klientów")
            Wybrani = Wybrani.Substring(2)
            ' poprzedni rok
            PolaczeniePY()
            dt = New DataTable
            da = New OleDbDataAdapter("SELECT DISTINCT IDKlienta FROM tblSprzedaz WHERE IDKlienta IN (" _
                                       + Wybrani + ") AND Wydzial IN ('MS', 'PS', 'MT')", Me.cnsPY)
            da.Fill(dt)
            If dt.Rows.Count.Equals(0) Then GoTo DaneZaMc
            Wybrani = ", " + Wybrani
            For Each r As DataRow In dt.Rows
                Wybrani = Wybrani.Replace(", " + r("IDKlienta").ToString, "")
            Next r
            If String.IsNullOrEmpty(Wybrani) Then Throw New ArgumentException("Brak nowych klientów")
DaneZaMc:
            Wybrani = Wybrani.Substring(2)
            dt = New DataTable
            da = New OleDbDataAdapter("SELECT IDKlienta, K.Nazwa AS [Klient], SUM(Waga) AS [Waga], SUM(Kwota - Rabat) AS [Kwota] FROM tblSprzedaz S LEFT JOIN tblKlienci K ON S.IDKlienta = K.ID " _
                                      + "WHERE IDKlienta IN (" + Wybrani + ") AND Wydzial IN ('MS', 'PS', 'MT') GROUP BY IDKlienta, K.Nazwa ORDER BY K.Nazwa", Me.cns)
            da.Fill(dt)
            Dim aVal(dt.Rows.Count, dt.Columns.Count - 1) As Object
            For i = 0 To dt.Columns.Count - 1
                aVal(0, i) = dt.Columns(i).ColumnName
            Next i
            For j = 0 To dt.Rows.Count - 1
                For i = 0 To dt.Columns.Count - 1
                    aVal(j + 1, i) = dt.Rows(j)(i)
                Next i
            Next j
            With New Cl4Excel.Excel(Nothing)
                With .oSheet
                    .Range("A3").Resize(aVal.GetUpperBound(0) + 1, aVal.GetUpperBound(1) + 1).Value = aVal
                    .Range("A3:D3").Interior.ColorIndex = 15
                    .Columns("C:C").NumberFormat = "# ##0,000"
                    .Columns("D:D").NumberFormat = "# ##0"
                    .Range("A:D").Columns.AutoFit()
                    .Range("A1").Value = Me.tsm_Ew.Text.Replace("&", "") + " za " + Format(Me.dtpOkres.Value, "MMMM yyyy") + " roku"
                End With
                .oBook.Saved = True
                HideScreen()
                .Show()
            End With
            HideScreen(False)
        Catch ex As Exception
            ClearStatus()
            Using New CM
                MessageBox.Show(ex.Message, Me.tsm_Ew.Text.Replace("&", ""), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Using
        Finally
            da = Nothing
            dt = Nothing
            dv = Nothing
        End Try
    End Sub

    Private Sub BudzetNastepnegoRoku(ByVal sender As Object, ByVal e As System.EventArgs) _
                        Handles tsm_Edb.Click, _
                                tsm_Edu.Click

        Dim tsm As ToolStripMenuItem = CType(sender, ToolStripMenuItem)
        Dim _Rok As String = (Me.dtpOkres.Value.Year + 1).ToString
        Dim Tytul As String = "Generowanie danych o dostawcach i materiałach: " + tsm.Text.Replace("&", "")
        Dim _Path As String = "W:\Costs\Celsa\Wykon" + _Rok + "\Budget_" + _Rok + "\"
        Dim dt As New DataTable
        Dim _Blad As String = ""
        Dim _Files As String = ""
        Dim _Plik, _Skor As String
        Dim _Prev As String = ""
        Dim SkQuery As Object = Nothing
        Dim i, j As Integer
        Dim aDane1(,) As Object = Nothing
        Dim aDane2(,) As Object = Nothing
        Dim aDane3(,) As Object = Nothing
        Try
            Dim m_WB As String = Nothing
            If NotExportFile(m_WB) Then Exit Try
            Dim tr As OleDbTransaction = Nothing
            Dim cmd As New OleDbCommand
            FillStatusBar(0, Tytul + "...")
            Using cn As New OleDbConnection(Me.cns.ConnectionString)
                Try
                    cn.Open()
                    tr = cn.BeginTransaction
                    With cmd
                        .Connection = cn
                        .Transaction = tr
                        .CommandText = "SELECT Z.MPK, M.Skoroszyt, I.Skoroszyt, M.ArkBud, Mc, Calco, Dostawca, Material, D.Nazwa, T.MaterialPL, RK, R.Opis, SUM(Wartosc) AS [PLN] " _
                                      + "FROM ((((tblZWPC Z INNER JOIN tblInstalacje I ON Z.Instalacja = I.ID) INNER JOIN tblMPK M ON Z.MPK = M.ID) " _
                                      + "INNER JOIN tblRK R ON Z.RK = R.ID) LEFT JOIN tblDostawcy D ON Z.Dostawca = D.ID) LEFT JOIN tblMaterialy T ON Z.Material = T.ID " _
                                      + "WHERE Z.Instalacja = M.Instalacja AND NOT (M.Skoroszyt IS NULL AND I.Skoroszyt IS NULL AND M.ArkBud IS NULL AND CalCo IS NULL) "
                        If tsm.Name.EndsWith("u") Then
                            .CommandText += "AND Mc = " + Me.dtpOkres.Value.Month.ToString
                        End If
                        .CommandText += " GROUP BY Z.MPK, M.Skoroszyt, I.Skoroszyt, M.ArkBud, Mc, Calco, RK, Dostawca, Material, D.Nazwa, T.MaterialPL, R.Opis"
                        dt.Load(.ExecuteReader)
                    End With
                Catch ex As Exception
                End Try
            End Using
            tr = Nothing
            cmd = Nothing
            If dt.Rows.Count.Equals(0) Then Throw New ArgumentException("Brak danych")
            SkQuery = (From r In dt.AsEnumerable _
                       Select SkorM = r(1), SkorI = r(2)).Distinct().ToArray()
            Dim XE As New Cl4Excel.Excel(m_WB)
            For Each q0 In SkQuery
                If String.IsNullOrEmpty(q0.SkorM.ToString) Then
                    _Skor = q0.SkorI.ToString
                    If _Files.Contains(_Skor) Then GoTo Nastepny
                    _Files += _Skor + "|"
                    _Plik = _Path + _Skor + "_" + _Rok + "Bu.xlsx"
                    'If _Plik = "W:\Costs\Celsa\Wykon2018\Budget_2018\ZwWd1_2018Bu.xlsx" Then
                    If Not My.Computer.FileSystem.FileExists(_Plik) Then Throw New ArgumentException("Plik [" + _Plik + "] nie istnieje")

                    For i = 1 To 3
                        Select Case i
                            Case 1
                                Dim query = _
                                        (From r In dt.AsEnumerable _
                                         Where r(2).ToString.Equals(_Skor) AndAlso Not r(6).Equals(0) _
                                         Group By Ark = r(3).ToString, Mc = r(4), CalCo = r(5).ToString, ID = r(6).ToString, Opis = r(8).ToString _
                                         Into PLN = Sum(r.Field(Of Double)("PLN")) _
                                         Order By Ark, CalCo, ID Descending)
                                ReDim aDane1(query.Count - 1, 5)
                                j = 0
                                For Each q In query
                                    aDane1(j, 0) = q.Ark
                                    aDane1(j, 1) = q.Mc
                                    aDane1(j, 2) = q.CalCo
                                    aDane1(j, 3) = q.ID
                                    aDane1(j, 4) = q.Opis
                                    aDane1(j, 5) = q.PLN
                                    j += 1
                                Next q
                            Case 2
                                Dim query = _
                                        (From r In dt.AsEnumerable _
                                         Where r(2).ToString.Equals(_Skor) AndAlso r(6).Equals(0) AndAlso Not String.IsNullOrEmpty(r(7).ToString) _
                                         Group By Ark = r(3).ToString, Mc = r(4), CalCo = r(5).ToString, ID = r(7).ToString, Opis = r(9).ToString _
                                         Into PLN = Sum(r.Field(Of Double)("PLN")) _
                                         Order By Ark, CalCo, ID Descending)
                                ReDim aDane2(query.Count - 1, 5)
                                j = 0
                                For Each q In query
                                    aDane2(j, 0) = q.Ark
                                    aDane2(j, 1) = q.Mc
                                    aDane2(j, 2) = q.CalCo
                                    aDane2(j, 3) = q.ID
                                    aDane2(j, 4) = q.Opis
                                    aDane2(j, 5) = q.PLN
                                    j += 1
                                Next q
                            Case 3
                                Dim query = _
                                        (From r In dt.AsEnumerable _
                                         Where r(2).ToString.Equals(_Skor) AndAlso r(6).Equals(0) AndAlso String.IsNullOrEmpty(r(7).ToString) _
                                         Group By Ark = r(3).ToString, Mc = r(4), CalCo = r(5).ToString, ID = r(10).ToString, Opis = r(11).ToString _
                                         Into PLN = Sum(r.Field(Of Double)("PLN")) _
                                         Order By Ark, CalCo, ID Descending)
                                ReDim aDane3(query.Count - 1, 5)
                                j = 0
                                For Each q In query
                                    aDane3(j, 0) = q.Ark
                                    aDane3(j, 1) = q.Mc
                                    aDane3(j, 2) = q.CalCo
                                    aDane3(j, 3) = q.ID
                                    aDane3(j, 4) = q.Opis
                                    aDane3(j, 5) = q.PLN
                                    j += 1
                                Next q
                        End Select
                    Next i
                    With XE.oApp
                        .Run("BudzetNYProd", _Plik, aDane1, aDane2, aDane3, tsm.Name.EndsWith("u"))
                        _Blad = .Range("Blad").Value2
                        If Not String.IsNullOrEmpty(_Blad) Then Exit For
                    End With
                Else
                    Dim _Arkusze As String = ""
                    _Skor = q0.SkorM.ToString
                    If _Files.Contains(_Skor) Then GoTo Nastepny
                    _Files += _Skor + "|"
                    _Plik = _Path + _Skor + "_" + _Rok + "Bu.xlsx"
                    If Not My.Computer.FileSystem.FileExists(_Plik) Then Throw New ArgumentException("Plik [" + _Plik + "] nie istnieje")
                    For i = 1 To 3
                        Select Case i
                            Case 1
                                Dim query = _
                                        (From r In dt.AsEnumerable _
                                         Where r(1).ToString.Equals(_Skor) AndAlso Not r(6).Equals(0) _
                                         Group By Ark = r(3).ToString, Mc = r(4), CalCo = r(5).ToString, ID = r(6).ToString, Opis = r(8).ToString _
                                         Into PLN = Sum(r.Field(Of Double)("PLN")) _
                                         Order By Ark, CalCo, ID Descending)
                                ReDim aDane1(query.Count - 1, 5)
                                j = 0
                                For Each q In query
                                    If Not _Arkusze.Contains(q.Ark) Then _Arkusze += "|" + q.Ark
                                    aDane1(j, 0) = q.Ark
                                    aDane1(j, 1) = q.Mc
                                    aDane1(j, 2) = q.CalCo
                                    aDane1(j, 3) = q.ID
                                    aDane1(j, 4) = q.Opis
                                    aDane1(j, 5) = q.PLN
                                    j += 1
                                Next q
                            Case 2
                                Dim query = _
                                        (From r In dt.AsEnumerable _
                                         Where r(1).ToString.Equals(_Skor) AndAlso r(6).Equals(0) AndAlso Not String.IsNullOrEmpty(r(7).ToString) _
                                         Group By Ark = r(3).ToString, Mc = r(4), CalCo = r(5).ToString, ID = r(7).ToString, Opis = r(9).ToString _
                                         Into PLN = Sum(r.Field(Of Double)("PLN")) _
                                         Order By Ark, CalCo, ID Descending)
                                ReDim aDane2(query.Count - 1, 5)
                                j = 0
                                For Each q In query
                                    If Not _Arkusze.Contains(q.Ark) Then _Arkusze += "|" + q.Ark
                                    aDane2(j, 0) = q.Ark
                                    aDane2(j, 1) = q.Mc
                                    aDane2(j, 2) = q.CalCo
                                    aDane2(j, 3) = q.ID
                                    aDane2(j, 4) = q.Opis
                                    aDane2(j, 5) = q.PLN
                                    j += 1
                                Next q
                            Case 3
                                Dim query = _
                                        (From r In dt.AsEnumerable _
                                         Where r(1).ToString.Equals(_Skor) AndAlso r(6).Equals(0) AndAlso String.IsNullOrEmpty(r(7).ToString) _
                                         Group By Ark = r(3).ToString, Mc = r(4), CalCo = r(5).ToString, ID = r(10).ToString, Opis = r(11).ToString _
                                         Into PLN = Sum(r.Field(Of Double)("PLN")) _
                                         Order By Ark, CalCo, ID Descending)
                                ReDim aDane3(query.Count - 1, 5)
                                j = 0
                                For Each q In query
                                    If Not _Arkusze.Contains(q.Ark) Then _Arkusze += "|" + q.Ark
                                    aDane3(j, 0) = q.Ark
                                    aDane3(j, 1) = q.Mc
                                    aDane3(j, 2) = q.CalCo
                                    aDane3(j, 3) = q.ID
                                    aDane3(j, 4) = q.Opis
                                    aDane3(j, 5) = q.PLN
                                    j += 1
                                Next q
                        End Select
                    Next i
                    With XE.oApp
                        .Run("BudzetNYPomoc", _Plik, _Arkusze, aDane1, aDane2, aDane3, tsm.Name.EndsWith("u"))
                        _Blad = .Range("Blad").Value2
                        If Not String.IsNullOrEmpty(_Blad) Then Exit For
                    End With
                End If
Nastepny:
                ' End If
            Next q0

            With XE
                .oBook.Save()
                .Zwolnij()
            End With
            If Not String.IsNullOrEmpty(_Blad) Then Throw New ArgumentException(_Blad + vbLf + "Proces został przerwany")
            ClearStatus()
            Using New CM
                MessageBox.Show("Operacja zakończona pomyślnie", Tytul, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End Using
        Catch ex As Exception
            ClearStatus()
            Using New CM
                MessageBox.Show(ex.Message, Tytul, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
        Finally
            dt = Nothing
        End Try
    End Sub

#End Region ' Menu Export

#Region "Menu Pomocne"

    Private Sub ZmianaFragmentuNazwyPlikow() Handles tsm_Pn.Click

        Dim fbd As New FolderBrowserDialog
        Const CO_ACTION As String = "Zmiana nazwy plików"
        Static Katalog As String = "W:\Costs"
        Try
            With fbd
                .RootFolder = Environment.SpecialFolder.MyComputer
                .ShowNewFolderButton = False
                .SelectedPath = Katalog
                If .ShowDialog.Equals(Windows.Forms.DialogResult.OK) Then
                    Katalog = .SelectedPath
                    Me.lblZFNP.Text = .SelectedPath
                Else
                    Throw New ArgumentException("Folder nie został wybrany")
                End If
            End With
            With Me.gbZmianaNazwyPlikow
                .Size = New Size(317, 169)
                .Location = New Point(363, 218)
                .Visible = True
                .Text = CO_ACTION
            End With
            Me.txtZFNP1.Focus()
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message, CO_ACTION, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
        End Try
    End Sub

    ' Wykonanie ZmianyNazwy plików
    Private Sub ZFNP() Handles btnZFNP.Click

        Dim m_Prev, m_New As String
        Try
            If Me.txtZFNP1.Text = "" Then Throw New ArgumentException("Podaj tekst do zmiany")

            For Each m_File As String In My.Computer.FileSystem.GetFiles(Me.lblZFNP.Text)
                m_Prev = m_File.Replace(Me.lblZFNP.Text, "")
                m_New = m_Prev.Replace(Me.txtZFNP1.Text, Me.txtZFNP2.Text)
                If Not String.Compare(m_Prev, m_New).Equals(0) Then _
                    Rename(Me.lblZFNP.Text + m_Prev, Me.lblZFNP.Text + m_New)
            Next m_File
            Using New CM
                MessageBox.Show("Operacja wykonana pomyślnie", Me.gbZmianaNazwyPlikow.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End Using
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message, Me.gbZmianaNazwyPlikow.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
        Finally
            Me.gbZmianaNazwyPlikow.Visible = False
        End Try
    End Sub

#End Region  ' Menu Pomocne

#Region "Administrator"

    Private Sub RZSprzedaz() Handles tsmOdswiezAmortyzacje.Click

        Dim FW As New PobierzWykonanie
        FW.RZSprzedaz(1)
    End Sub

    Private Sub ZmianaUstawienDostepu(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tscmbDostepy.SelectedIndexChanged

        Dim m_Tytul As String = "Zmiana ustawienia dostępu do " + Me.tscmbDostepy.Text
        Dim m_Txt As String = ""
        With My.Settings
            Select Case Me.tscmbDostepy.SelectedIndex
                Case 0
                    m_Txt = .ToBudget
                Case 1
                    m_Txt = .ToMedia
                Case 2
                    m_Txt = .ToNaleznosci
                Case 3
                    m_Txt = .ToPlan
                Case 4
                    m_Txt = .ToSales
                Case 5
                    m_Txt = .ToRzecz
                Case 6
                    m_Txt = .ToStock
                Case 7
                    m_Txt = .ToZatrudnienie
                Case 8
                    m_Txt = .ToWIP
                Case 9
                    m_Txt = .ToZWKZlecenia
                Case 10
                    m_Txt = .ToResult
                Case 11
                    m_Txt = .Sciezka
                Case Else
                    Exit Sub
            End Select
        End With
        Dim fbd As New FolderBrowserDialog
        Try
            With fbd
                .Description = m_Tytul
                .RootFolder = Environment.SpecialFolder.MyComputer
                .SelectedPath = m_Txt
                .ShowNewFolderButton = False
                If .ShowDialog.Equals(Windows.Forms.DialogResult.OK) Then
                    m_Txt = .SelectedPath + Path.DirectorySeparatorChar
                Else
                    Exit Try
                End If
            End With
            With My.Settings
                Select Case Me.tscmbDostepy.SelectedIndex
                    Case 0
                        .ToBudget = m_Txt
                    Case 1
                        .ToMedia = m_Txt
                    Case 2
                        .ToNaleznosci = m_Txt
                    Case 3
                        .ToPlan = m_Txt
                    Case 4
                        .ToSales = m_Txt
                    Case 5
                        .ToRzecz = m_Txt
                    Case 6
                        .ToStock = m_Txt
                    Case 7
                        .ToZatrudnienie = m_Txt
                    Case 8
                        .ToWIP = m_Txt
                    Case 9
                        .ToZWKZlecenia = m_Txt
                    Case 10
                        .ToResult = m_Txt
                    Case 11
                        .Sciezka = m_Txt
                End Select
                .Save()
            End With
        Catch ex As Exception
        Finally
            fbd = Nothing
            Me.tscmbDostepy.SelectedIndex = -1
        End Try
    End Sub

    Private Sub WeryfikacjaMaterialow() Handles tsmWeryfikacjaMaterialow.Click

        Dim m_File, strFile As String
        Dim m_Path As String = "W:\Costs\CHO_SAP_SYSTEM\"
        Dim tr As OleDbTransaction = Nothing
        Dim cmd As New OleDbCommand
        Dim da As OleDbDataAdapter = New OleDbDataAdapter("SELECT TOP 1 * FROM tblMaterialy", Me.cns)
        Dim dtPL As New DataTable, dtEN As New DataTable
        Dim dt As New DataTable
        Dim m_Obj() As Object
        Dim i As Integer
        Dim m_Ado As New ClADO.ADOGet
        Dim dm1 As Date
        Const CO_TITLE As String = "Weryfikacja materiałów"
        Try
            Using New CM
                If Not MessageBox.Show("Weryfikuje materiały na podstawie plików BWD" + vbCrLf _
                                    + "znajdujących się w katalogu " + m_Path + """." + vbCrLf + vbCrLf _
                                    + "Kontynuujesz?", CO_TITLE, MessageBoxButtons.YesNo, MessageBoxIcon.Question, _
                                    MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then Return
            End Using
            da.Fill(dt)
            dt.Clear()
            m_File = "BWD_Materiały*_PL.XLS"
            With dt.Columns
                .Add("RodzMaterPL")
                .Add("RodzMaterEN")
                .Add("GrMaterPL")
                .Add("GrMaterEN")
                .Add("GrKalkPL")
                .Add("GrKalkEN")
            End With
            For Each strFile In My.Computer.FileSystem.GetFiles(m_Path, FileIO.SearchOption.SearchTopLevelOnly, m_File)
                m_File = strFile.Substring(m_Path.Length)
                dm1 = My.Computer.FileSystem.GetFileInfo(strFile).LastWriteTime
                If Not CP.CompareFileDate(FileSpec:=strFile.Replace("_PL", "_EN"), Dtm2Compare:=dm1, AreEqual:=True, OnlyDate:=True) _
                    Then Throw New ArgumentException("Niekompatybilne wersje dla [" + strFile + "]")
                ' otwórz plik PL
                With m_Ado
                    .Baza = m_File
                    .Folder = m_Path
                    .HDR = True
                    If Not .OpenData(True) Then
                        Using New CM
                            MessageBox.Show("Brak dostępu do pliku [" + .Baza + "]", CO_TITLE, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        End Using
                        Exit Sub
                    End If
                End With
                FillStatusBar(0, m_File.Replace("_PL", "") + " - odczyt danych...")
                da = New OleDbDataAdapter("SELECT * FROM [BWD$F15:AK60000]  ", m_Ado.CN)
                dtEN = New DataTable
                dtPL = New DataTable
                da.Fill(dtPL)
                ' otwórz plik EN
                With m_Ado
                    .Baza = m_File.Replace("_PL", "_EN")
                    .Folder = m_Path
                    .HDR = True
                    If Not .OpenData(True) Then
                        Using New CM
                            MessageBox.Show("Brak dostępu do pliku [" + .Baza + "]", CO_TITLE, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        End Using
                        Exit Sub
                    End If
                End With
                da = New OleDbDataAdapter("SELECT * FROM [BWD$F15:AK60000]  ", m_Ado.CN)
                da.Fill(dtEN)
                If dtPL Is Nothing OrElse dtEN Is Nothing Then Throw New ArgumentException("Plik [" + m_File + "] nie zawiera danych lub nie są kompletne wersje językowe")
                dtPL.Columns(2).ColumnName = "Opis"
                dtEN.Columns(2).ColumnName = "Opis"
                dtPL.Columns(5).ColumnName = "RodzMater"
                dtPL.Columns(6).ColumnName = "RodzMaterPL"
                dtEN.Columns(6).ColumnName = "RodzMaterEN"
                dtPL.Columns(7).ColumnName = "GrKalk"
                dtPL.Columns(8).ColumnName = "GrKalkPL"
                dtEN.Columns(8).ColumnName = "GrKalkEN"
                dtPL.Columns(9).ColumnName = "GrMater"
                dtPL.Columns(10).ColumnName = "GrMaterPL"
                dtEN.Columns(10).ColumnName = "GrMaterEN"
                dtPL.Columns(17).ColumnName = "JednostkaWagi"
                dtPL.Columns(31).ColumnName = "Waga"
                Dim query = (From pl In dtPL.AsEnumerable Join en In dtEN.AsEnumerable _
                                On pl.Field(Of String)("Materiał") _
                                Equals en.Field(Of String)("Material") _
                            Select New With {.ID = pl!Materiał, _
                                            .MaterialPL = pl!Opis, _
                                            .MaterialEN = en!Opis, _
                                            .RodzMaterID = pl!RodzMater, _
                                            .GrMaterID = pl!GrMater, _
                                            .GrKalkID = pl!GrKalk, _
                                            .Jm = pl!JednostkaWagi, _
                                            .Waga = pl!Waga, _
                                            .RodzMaterPL = pl!RodzMaterPL, _
                                            .RodzMaterEN = en!RodzMaterEN, _
                                            .GrMaterPL = pl!GrMaterPL, _
                                            .GrMaterEN = en!GrMaterEN, _
                                            .GrKalkPL = pl!GrKalkPL, _
                                            .GrKalkEN = en!GrKalkEN})
                For Each q In query
                    With dt.Rows
                        'ID	MaterialPL	MaterialEN	RodzMaterID	GrMaterID	GrKalkID GrKosztID	Jm	WierszMediow	WierszMedPom	Waga
                        .Add(New Object() {q.ID, q.MaterialPL.ToString.Replace("'", ""), _
                                           q.MaterialEN.ToString.Replace("'", ""), _
                                           q.RodzMaterID.ToString.Replace("#", ""), _
                                           q.GrMaterID.ToString.Replace("#", ""), q.GrKalkID.ToString.Replace("#", ""), "", q.Jm, 0, 0, q.Waga / 1000, _
                                           q.RodzMaterPL.ToString.Replace("#", ""), _
                                           q.RodzMaterEN.ToString.Replace("#", ""), _
                                           q.GrMaterPL.ToString.Replace("#", ""), _
                                           q.GrMaterEN.ToString.Replace("#", ""), _
                                           q.GrKalkPL.ToString.Replace("#", ""), _
                                           q.GrKalkEN.ToString.Replace("#", "")})
                    End With
                Next q
Nastepny:
            Next strFile
            FillStatusBar(dt.Rows.Count, CO_TITLE + "...")
            i = 0
            Using cn As New OleDbConnection(Me.cns.ConnectionString)
                Try
                    cn.Open()
                    tr = cn.BeginTransaction
                    With cmd
                        .Connection = cn
                        .Transaction = tr
                        For Each dr In dt.Rows
                            Try
                                .CommandText = "INSERT INTO tblMaterialy (ID, MaterialPL, MaterialEN, RodzMaterID, GrMaterID, GrKalkID, Jm, Waga) VALUES ('" _
                                                + dr("ID") + "', '" + dr("MaterialPL") + "', '" + dr("MaterialEN") + "', '" + dr("RodzMaterID") + "', '" + dr("GrMaterID") _
                                                + "', '" + dr("GrKalkID") + "', '" + dr("Jm") + "', " + dr("Waga").ToString.Replace(",", ".") + ")"
                                .ExecuteNonQuery()
                            Catch ex As Exception
                                .CommandText = "UPDATE tblMaterialy SET MaterialPL = '" + dr("MaterialPL") + "', MaterialEN = '" + dr("MaterialEN") + "', RodzMaterID = '" + dr("RodzMaterID") + "', GrMaterID = '" + dr("GrMaterID") _
                                                + "', GrKalkID = '" + dr("GrKalkID") + "', Jm = '" + dr("Jm") + "', Waga = " + dr("Waga").ToString.Replace(",", ".") + " WHERE ID = '" + dr("ID") + "'"
                                .ExecuteNonQuery()
                            End Try
                            If (i Mod 500).Equals(0) Then _
                                Me.tspbMain.Value = i : Application.DoEvents()
                            i += 1
                        Next
                    End With
                    ' rodzaj materiałów
                    m_Obj = (From v In dt.AsEnumerable _
                             Where Not (IsDBNull(v!RodzMaterID) OrElse String.IsNullOrEmpty(v!RodzMaterID)) _
                             Select ID = v!RodzMaterID, PL = v!RodzMaterPL, EN = v!RodzMaterEN).Distinct().ToArray()
                    FillStatusBar(0, "Uzupełnianie rodzajów materiału...")
                    For Each o In m_Obj
                        With cmd
                            Try
                                .CommandText = "INSERT INTO tblRodzajeMat (ID, RodzMaterPL, RodzMaterEN) " _
                                     + "VALUES ('" + o.ID + "', '" + o.PL + "', '" + o.EN + "')"
                                .ExecuteNonQuery()
                            Catch ex As Exception
                                .CommandText = "UPDATE tblRodzajeMat SET RodzMaterPL = '" + o.PL + "', RodzMaterEN = '" + o.EN + "' WHERE ID = '" + o.ID + "'"
                                .ExecuteNonQuery()
                            End Try
                        End With
                    Next o
                    ClearStatus()
                    ' grupa materiałowa
                    m_Obj = (From v In dt.AsEnumerable _
                             Where Not (IsDBNull(v!GrMaterID) OrElse String.IsNullOrEmpty(v!GrMaterID)) _
                             Select ID = v!GrMaterID, PL = v!GrMaterPL, EN = v!GrMaterEN).Distinct().ToArray()
                    FillStatusBar(0, "Uzupełnianie grup materiałowych...")
                    For Each o In m_Obj
                        With cmd
                            Try
                                .CommandText = "INSERT INTO tblGrupyMat (ID, GrMaterPL, GrMaterEN) " _
                                     + "VALUES ('" + o.ID + "', '" + o.PL + "', '" + o.EN + "')"
                                .ExecuteNonQuery()
                            Catch ex As Exception
                                .CommandText = "UPDATE tblGrupyMat SET GrMaterPL = '" + o.PL + "', GrMaterEN = '" + o.EN + "' WHERE ID = '" + o.ID + "'"
                                .ExecuteNonQuery()
                            End Try
                        End With
                    Next o
                    ClearStatus()
                    m_Obj = (From v In dt.AsEnumerable _
                             Where Not (IsDBNull(v("GrKalkID")) OrElse String.IsNullOrEmpty(v("GrKalkID"))) _
                             Select ID = v!GrKalkID, GrMater = v!GrMaterID, PL = v!GrKalkPL, EN = v!GrKalkEN).Distinct().ToArray()
                    FillStatusBar(0, "Uzupełnianie grup kalkulacyjnych...")
                    For Each o In m_Obj
                        With cmd
                            Try
                                .CommandText = "INSERT INTO tblGrupyKalk (ID, GrMater, GrKalkPL, GrKalkEN) " _
                                     + "VALUES ('" + o.ID + "', '" + o.GrMater + "', '" + o.PL + "', '" + o.EN + "')"
                                .ExecuteNonQuery()
                            Catch ex As Exception
                                .CommandText = "UPDATE tblGrupyKalk SET GrMater = '" + o.GrMater + "', GrKalkPL = '" + o.PL + "', GrKalkEN = '"
                                If o.en.Equals(o.ID) Then
                                    .CommandText += ""
                                Else
                                    .CommandText += o.EN
                                End If
                                .CommandText += "' WHERE ID = '" + o.ID + "'"
                                .ExecuteNonQuery()
                            End Try
                        End With
                    Next o
                    ClearStatus()
                    tr.Commit()
                Catch ex As Exception
                    tr.Rollback()
                    ClearStatus()
                    Throw New ArgumentException(ex.Message + vbCrLf + ex.StackTrace)
                End Try
            End Using
            Using New CM
                MessageBox.Show("Operacja zakończona pomyślnie", CO_TITLE, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End Using
        Catch ex As Exception
            ClearStatus()
            Using New CM
                MessageBox.Show(ex.Message, CO_TITLE, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Using
        Finally
            tr = Nothing
            cmd = Nothing
            da = Nothing
            dtEN = Nothing
            dtPL = Nothing
            m_Ado = Nothing
            m_Obj = Nothing
        End Try
    End Sub

    Private Sub WeryfikacjaMagazynow() Handles tsmWeryfikacjaMagazynow.Click

        Dim mPath As String = "W:\Controlling\Controlling Dpt\Obowiązki\"
        Dim mFile As String = "PlanInwentaryzacji_2012_1.xls"
        Dim dt As New DataTable
        Dim dtM As New DataTable
        Dim bs As New BindingSource
        Dim da As OleDbDataAdapter
        Dim m_Ado As New ClADO.ADOGet
        Dim i, j As Integer
        Const CO_TITLE As String = "Weryfikacja magazynów"
        Try
            da = DA_TI("tblMagazyny", "ID")
            da.Fill(dtM)
            bs.DataSource = dtM
            If Not My.Computer.FileSystem.FileExists(mPath + mFile) Then
                Throw New ArgumentException("Plik [" + mPath + mFile + "] nie istnieje")
            End If
            ' otwórz plik
            With m_Ado
                .Baza = mFile
                .Folder = mPath
                .HDR = True
                If Not .OpenData(True) Then
                    Throw New ArgumentException("Brak dostępu do pliku [" + mFile + "]")
                End If
            End With
            da = New OleDbDataAdapter("SELECT * FROM [InventoryPlan$A1:V10000]  ", m_Ado.CN)
            da.Fill(dt)
            If dt Is Nothing Then
                Throw New ArgumentException("Plik [" + mFile + "] nie zawiera danych")
            End If
            FillStatusBar(dt.Rows.Count, mFile)
            For Each dr As DataRow In dt.Rows
                i += 1
                j = bs.Find("ID", dr(0).ToString)
                If j < 0 Then
                    dtM.Rows.Add(New Object() {dr(0).ToString, dr(1).ToString, dr(3).ToString})
                Else
                    With dtM(j)
                        .Item("Description") = dr(3).ToString
                    End With
                End If
                With Me.tspbMain
                    If i <= .Maximum AndAlso (i Mod 5).Equals(0) Then
                        .Value = i
                        Application.DoEvents()
                    End If
                End With
            Next dr
            dt = New DataTable
            dt = dtM.GetChanges
            da = DA_TI("tblMagazyny", "ID")
            da.Update(dt)
            dtM.AcceptChanges()
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message, CO_TITLE, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
        Finally
            ClearStatus()
            dt = Nothing
            dtM = Nothing
            bs = Nothing
            da = Nothing
            m_Ado = Nothing
        End Try
    End Sub

    Private Sub WeryfikacjaINEFCalCo() Handles tsmWeryfikacjaINEFCalCo.Click

        Dim mPath As String = "W:\Costs\Celsa\Opisy\"
        Dim mFile As String = "BazaOpisCALCO.xlsx"
        Dim dt As New DataTable
        Dim dtC As New DataTable
        Dim dtI As New DataTable
        Dim bsC As New BindingSource
        Dim bsI As New BindingSource
        Dim da As OleDbDataAdapter
        Dim m_Ado As New ClADO.ADOGet
        Dim i, j, k As Integer
        Dim m_INEF, m_CalCo As String
        Dim aArk() As String = New String() {"StZww", "WD", "WD_Poz", "StZwk", "Zwk", "Pom"}
        Const CO_TITLE As String = "Weryfikacja INEF/CalCo"
        Try
            da = DA_TI("tblINEF", "ID")
            da.Fill(dtI)
            bsI.DataSource = dtI
            da = DA_TI("tblCALCO", "ID")
            dtC = New DataTable
            da.Fill(dtC)
            bsC.DataSource = dtC
            If Not My.Computer.FileSystem.FileExists(mPath + mFile) Then
                mFile = mFile.Replace(".xlsx", ".xls")
                If Not My.Computer.FileSystem.FileExists(mPath + mFile) Then _
                    Throw New ArgumentException("Plik [" + mPath + mFile + "/.xlsx] nie istnieje")
            End If
            ' otwórz plik
            With m_Ado
                .Baza = mFile
                .Folder = mPath
                .HDR = True
                If Not .OpenData(True) Then
                    Throw New ArgumentException("Brak dostępu do pliku [" + mFile + "]")
                End If
            End With
            For i = aArk.GetLowerBound(0) To aArk.GetUpperBound(0)
                dt = New DataTable
                da = New OleDbDataAdapter("SELECT * FROM [" + aArk(i) + "$A4:F400]  ", m_Ado.CN)
                da.Fill(dt)
                If dt Is Nothing Then GoTo NastepnyArkusz
                FillStatusBar(dt.Rows.Count, aArk(i))
                k = 0
                m_INEF = ""
                For Each dr As DataRow In dt.Rows
                    k += 1
                    If Not String.IsNullOrEmpty(dr(0).ToString) Then
                        m_INEF = dr(0).ToString.Trim
                        j = bsI.Find("ID", m_INEF)
                        If j < 0 Then
                            dtI.Rows.Add(New Object() {m_INEF, dr(2).ToString, dr(3).ToString, "", k + 4})
                        Else
                            With dtI(j)
                                .Item("Opis") = dr(2).ToString
                                .Item("Description") = dr(3).ToString
                                .Item("NrWiersza") = k + 4
                            End With
                        End If
                    End If
                    If Not String.IsNullOrEmpty(dr(1).ToString) Then
                        m_CalCo = dr(1).ToString.Trim
                        j = bsC.Find("ID", m_CalCo)
                        If j < 0 Then
                            dtC.Rows.Add(New Object() {m_CalCo, dr(4).ToString, dr(5).ToString, m_INEF, "", False, k + 4})
                        Else
                            With dtC(j)
                                .Item("Opis") = dr(4).ToString
                                .Item("Description") = dr(5).ToString
                                .Item("INEF") = m_INEF
                                .Item("NrWiersza") = k + 4
                            End With
                        End If
                    End If
                    With Me.tspbMain
                        If k <= .Maximum AndAlso (k Mod 5).Equals(0) Then
                            .Value = k
                            Application.DoEvents()
                        End If
                    End With
                Next dr
NastepnyArkusz:
            Next i
            da = DA_TI("tblINEF", "ID")
            da.Update(dtI)
            dtI.AcceptChanges()
            da = DA_TI("tblCALCO", "ID")
            da.Update(dtC)
            dtC.AcceptChanges()
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message, CO_TITLE, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
        Finally
            ClearStatus()
            dt = Nothing
            dtC = Nothing
            dtI = Nothing
            bsC = Nothing
            bsI = Nothing
            da = Nothing
            m_Ado = Nothing
        End Try
    End Sub

    Private Sub WeryfikacjaDostawcow() Handles tsmWeryfDostawcow.Click

        Dim m_File As String
        Dim m_Path As String = "C:\Tmp\"
        Dim da As OleDbDataAdapter
        Dim dtM As New DataTable
        Dim dtCh As New DataTable
        Dim dt As New DataTable
        Dim i As Integer
        Dim bs As New BindingSource
        Dim m_Txt As String
        Dim m_Ado As New ClADO.ADOGet
        Const CO_TITLE As String = "Weryfikacja dostawców"
        Using New CM
            If Not MessageBox.Show("Weryfikuje dostawców na podstawie pliku ZWMM090.xls" + vbCrLf _
                                + "znajdującego się w katalogu " + m_Path + """." + vbCrLf + vbCrLf _
                                + "Kontynuujesz?", CO_TITLE, MessageBoxButtons.YesNo, MessageBoxIcon.Question, _
                                MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then Exit Sub
        End Using
        Try
            da = DA_TI("tblDostawcy", "ID")
            da.Fill(dtM)
            bs.DataSource = dtM
            m_File = "ZWMM090.XLS"
            If Not My.Computer.FileSystem.FileExists(m_Path + m_File) Then
                Throw New ArgumentException("Plik [" + m_File + "] nie istnieje w lokalizacji """ + m_Path + "")
            End If
            ' otwórz plik
            FillStatusBar(0, m_File)
            With m_Ado
                .Baza = m_File
                .Folder = m_Path
                .HDR = True
                If Not .OpenData(True) Then
                    Throw New ArgumentException("Brak dostępu do pliku [" + m_File + "]")
                End If
                da = New OleDbDataAdapter("SELECT * FROM [Arkusz1$]  ", .CN)
                da.Fill(dt)
            End With
            If dt Is Nothing Then Throw New ArgumentException("Plik [" + m_File + "] nie zawiera danych")
            If Not CP.SourceLayoutControl(dt, "ZWMM090", Me.cns) Then Exit Try
            For Each dr As DataRow In dt.Rows
                m_Txt = (dr("Nazwisko1").ToString.Trim + " " + dr("Nazwa2").ToString.Trim + " " + dr("Nazwa3").ToString.Trim + " " + dr("Nazwa4").ToString.Trim).Trim
                If String.IsNullOrEmpty(m_Txt) Then GoTo Nastepny
                i = bs.Find("ID", dr("Dostawca"))
                If i < 0 Then
                    dtM.Rows.Add(New Object() {dr("Dostawca"), m_Txt, dr("Kraj")})
                Else
                    With dtM.Rows(i)
                        .Item("Nazwa") = m_Txt
                        .Item("Kraj") = dr("Kraj").ToString
                    End With
                End If
Nastepny:
            Next dr
            dtCh = dtM.GetChanges
            If dtCh IsNot Nothing Then
                da = DA_TI("tblDostawcy", "ID")
                da.Update(dtCh)
                dtM.AcceptChanges()
            End If
            Using New CM
                MessageBox.Show("Operacja zakończona pomyślnie", CO_TITLE, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End Using
        Catch ex As Exception
            Using New CM
                If Not ex Is Nothing Then _
                    MessageBox.Show(ex.Message, CO_TITLE, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Using
        Finally
            ClearStatus()
            da = Nothing
            dtM = Nothing
            dt = Nothing
            bs = Nothing
            m_Ado = Nothing
        End Try
    End Sub

    Private Sub AnalizaZrodelDanych() Handles tsmAnalizaZrodel.Click

        Dim fbd As New FolderBrowserDialog
        Dim aLinks As Object
        Dim rgS As XL.Range
        Dim m_Txt, m_Skor, m_For As String
        Dim i, j As Integer
        Dim da As OleDbDataAdapter
        Dim dt As New DataTable
        Dim dtCh As New DataTable
        Dim dtF As New DataTable
        Dim bs As New BindingSource
        Dim drv As DataRowView
        Dim tr As OleDbTransaction = Nothing
        Dim cmd As New OleDbCommand
        Dim dv As New DataView
        Dim XE As New Cl4Excel.Excel(Nothing)
        Static m_Path As String
        Try
            With fbd
                .SelectedPath = m_Path
                .ShowNewFolderButton = False
                If Not .ShowDialog.Equals(Windows.Forms.DialogResult.OK) Then Return
                m_Path = .SelectedPath
            End With
            da = DA_TI("tblFormuly", "ID")
            da.Fill(dtF)
            da.Fill(dtCh)
            da = DA_TI("tblLacza", "ID")
            da.Fill(dt)
            For Each m_File As String In My.Computer.FileSystem.GetFiles(m_Path)
                If Not m_File.ToUpper.Contains(".XL") Then GoTo NastepnyPlik
                Try
                    FillStatusBar(Tekst:=m_File)
                    With XE
                        .oBook = .oApp.Workbooks.Open(Filename:=m_File, UpdateLinks:=False, ReadOnly:=True)
                        aLinks = .oApp.ActiveWorkbook.LinkSources(Type:=XL.XlLinkType.xlLinkTypeExcelLinks)
                    End With
                    If aLinks Is Nothing Then Exit Try
                    dt.Clear()
                    m_Skor = m_File.Substring(m_File.LastIndexOf("\") + 1).Replace(Okres.Substring(0, 7), "rrrr-mm")
                    Using cn As New OleDbConnection(Me.cns.ConnectionString)
                        Try
                            cn.Open()
                            tr = cn.BeginTransaction
                            With cmd
                                .Connection = cn
                                .Transaction = tr
                                .CommandText = "DELETE FROM tblLacza WHERE Skoroszyt = '" + m_Skor + "'"
                                .ExecuteNonQuery()
                                .CommandText = "DELETE FROM tblFormuly WHERE Skoroszyt = '" + m_Skor + "'"
                                .ExecuteNonQuery()
                            End With
                            tr.Commit()
                        Catch ex As Exception
                            tr.Rollback()
                            Using New CM
                                MessageBox.Show(ex.Message + vbCrLf + m_File, "Analiza źródeł zewnętrznych", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            End Using
                            Throw New ArgumentException
                        End Try
                    End Using
                    For Each m_Txt In CType(aLinks, Array)
                        dt.Rows.Add(New Object() {0, m_Skor, m_Txt})
                    Next
                    For Each ws As XL._Worksheet In XE.oApp.ActiveWorkbook.Worksheets
                        dtF.Clear()
                        Try
                            rgS = ws.UsedRange
                            For Each ac As XL.Range In rgS.SpecialCells(XL.XlCellType.xlCellTypeFormulas)
                                Try
                                    m_For = ac.FormulaR1C1.ToString.Replace("'", "").Replace("""", "").Trim
                                    If String.IsNullOrEmpty(m_For) Then Throw New ArgumentException
                                    dtF.Rows.Add(New Object() {0, m_Skor, ws.Name, ac.Address, m_For, ""})
                                Catch ex As Exception
                                End Try
                            Next
                        Catch ex As Exception
                        End Try
                        ' usuwanie nie-łączy
                        bs = New BindingSource
                        With bs
                            .DataSource = Nothing
                            .DataSource = dtF
                        End With
                        For i = 0 To dt.Rows.Count - 1
                            bs.Filter = Nothing
                            With dt(i)("Lacze").ToString
                                bs.Filter = "Formula LIKE '%" + .Substring(0, .LastIndexOf("\") + 1) + "%'"
                            End With
                            For j = 0 To bs.Count - 1
                                With bs
                                    .Position = j
                                    drv = CType(.Current, DataRowView)
                                    drv("DB") = "X"
                                End With
                            Next j
                        Next i
                        dv = (From row In dtF.AsEnumerable() _
                                    Where row!DB = "X" _
                                     Select row).AsDataView
                        dtCh.Clear()
                        For i = 0 To dv.Count - 1
                            With dv(i)
                                dtCh.Rows.Add(New Object() {.Item(0).ToString, .Item(1).ToString, .Item(2).ToString, .Item(3).ToString, .Item(4).ToString})
                            End With
                        Next i
                        da = DA_TI("tblFormuly", "ID")
                        da.Update(dtCh)
                    Next ws
                    da = DA_TI("tblLacza", "ID")
                    da.Update(dt)
                Catch ex As Exception
                    Using New CM
                        MessageBox.Show(ex.Message + vbCrLf + m_File, "Analiza źródeł zewnętrznych", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    End Using
                Finally
                    With XE.oBook
                        .Saved = True
                        .Close()
                    End With
                End Try
NastepnyPlik:
            Next m_File
        Catch ex As Exception
        Finally
            XE.Zwolnij()
            ClearStatus()
            da = Nothing
            dt = Nothing
            dtCh = Nothing
            dtF = Nothing
            dv = Nothing
            bs = Nothing
            cmd = Nothing
            tr = Nothing
        End Try
    End Sub

    ' Odświeżanie sprzedaży z zachowaniem poprzedniego transportu
    Private Sub AdPobierzSprzedaz() Handles tsmAdPobierzSprzedaz.Click

        Dim _Path As String = My.Settings.ToResult
        Dim _Ext As String = ".xl*"
        Dim iRok, iMc As Integer
        Dim strGrMater As String = ""
        Dim strGrKalk As String = ""
        Dim strMaterial As String = ""
        Dim strKlient As String = ""
        Dim strWydzial, strOkres As String
        Dim m_Ado As New ClADO.ADOGet
        Dim da As OleDbDataAdapter
        Dim dt As New DataTable
        Dim dtB As New DataTable
        Dim dtT As New DataTable
        Dim dtCh As New DataTable
        Dim dtMM As New DataTable
        Dim dvK As New DataView
        Dim dv As New DataView
        Dim dvS As New DataView
        Dim dvB As New DataView
        Dim cmd As New OleDbCommand
        Dim i, j, iR, iW As Integer
        Dim m_Waga As Double
        Dim m_Kwota As Double
        Dim m_Rabat As Double
        Dim m_PipSAP As Double
        Dim m_Key As String
        Dim rd As DataRow = Nothing
        Dim tr As OleDbTransaction = Nothing
        Dim blnTylkoSprzedaz As Boolean = False
        Const CO_ACTION As String = "Pobieranie danych sprzedaży"
        If BlokadaAktualizacji("tblSprzedaz") Then Exit Sub
        _Path = CP.GetSourcePath(_Path + "CHO_Result_" + Okres.Substring(0, 7), _Ext, CO_ACTION)
        If String.IsNullOrEmpty(_Path) Then Exit Sub
        With My.Settings
            .ToResult = _Path
            .Save()
        End With
        FillStatusBar(0, Me.tsmAdPobierzSprzedaz.Text.Replace("&", ""))
        With m_Ado
            .Baza = "CHO_Result_" + Okres.Substring(0, 7) + _Ext
            .Folder = _Path
            .HDR = True
            If Not .OpenData(True) Then
                Using New CM
                    MessageBox.Show("Błąd odczytu pliku CHO_Result_" + Okres.Substring(0, 7) + _Ext, CO_ACTION, MessageBoxButtons.OK, MessageBoxIcon.Error)
                End Using
                GoTo Finish
            End If
            Try
                da = New OleDbDataAdapter("SELECT * FROM [Sales$A1:AE50000]  ", m_Ado.CN)
                da.Fill(dt)
                If dt.Rows.Count = 0 Then Throw New ArgumentException("Plik nie spełnia oczekiwań - brak danych w arkuszu [Sales>")
            Catch ex As Exception
                Using New CM
                    MessageBox.Show(ex.Message, CO_ACTION, MessageBoxButtons.OK, MessageBoxIcon.Error)
                End Using
                GoTo Finish
            End Try
        End With
        dt.Columns(0).ColumnName = "Okres"
        dv = dt.DefaultView
        strOkres = Okres.Substring(5, 2) + "." + Okres.Substring(0, 4)
        dv.RowFilter = "Okres = '" + strOkres + "'"
        dv.Sort = "Okres, Materiał, Zleceniodawca"
        iRok = CInt(strOkres.Substring(3, 4))
        iMc = CInt(strOkres.Substring(0, 2))
        da = New OleDbDataAdapter("SELECT * FROM tblsprzedaz WHERE Miesiac = " + iMc.ToString + " ORDER BY Material, IDKlienta", Me.cns)
        da.Fill(dtB)
        For j = 0 To dtB.Rows.Count - 1
            With dtB.Rows(j)
                .Item("Waga") = 0
                .Item("Kwota") = 0
                .Item("Rabat") = 0
                .Item("PIP_SAP") = 0
            End With
        Next j
        dvB = dtB.DefaultView
        dvB.Sort = "Material, IDKlienta"
        da = New OleDbDataAdapter("SELECT ID, Wydzial FROM tblGrupyMat WHERE NOT Wydzial IS NULL ORDER BY ID", Me.cns)
        da.Fill(dtCh)
        Dim aWydz(dtCh.Rows.Count - 1, 1) As String
        For i = 0 To dtCh.Rows.Count - 1
            With dtCh(i)
                aWydz(i, 0) = .Item(0).ToString
                aWydz(i, 1) = .Item(1).ToString
            End With
        Next i
        FillStatusBar(dv.Count, CO_ACTION + "...")
        i = 0
        Using cn As New OleDbConnection(Me.cns.ConnectionString)
            Try
                cn.Open()
                tr = cn.BeginTransaction
                cmd.Connection = cn
                cmd.Transaction = tr
                Do While i < dv.Count
                    With dv(i)
                        strWydzial = "OR"
                        If Not .Item(6).ToString.Contains("JSWZ") Then
                            iR = CP.FindRowOfSortTable(aWydz, 0, .Item(1).ToString)
                            If iR > -1 Then _
                                strWydzial = aWydz(iR, 1)
                        End If
                        ' kontrola 
                        If Not strGrMater.Contains(.Item(1).ToString) Then
                            cmd.CommandText = "SELECT ID FROM tblGrupyMat WHERE ID = '" + .Item(1).ToString + "'"
                            If cmd.ExecuteScalar Is Nothing Then
                                cmd.CommandText = "INSERT INTO tblGrupyMat (ID, Wydzial, GrMaterPL) VALUES ('" + .Item(1).ToString + "', '" _
                                                    + strWydzial + "', '" + .Item(2).ToString + "')"
                                cmd.ExecuteNonQuery()
                            End If
                            strGrMater += "#" + .Item(1).ToString
                        End If
                        ' kontrola 
                        If .Item(5).ToString.Length > 2 AndAlso Not strGrKalk.Contains(.Item(5).ToString) Then
                            cmd.CommandText = "SELECT ID FROM tblGrupyKalk WHERE ID = '" + .Item(5).ToString + "'"
                            If cmd.ExecuteScalar Is Nothing Then
                                cmd.CommandText = "INSERT INTO tblGrupyKalk (ID, Wydzial, GrMater, GrKalkPL) VALUES ('" + .Item(5).ToString + "', '" _
                                                    + .Item("Grupamateriałowa").ToString + "', '" + strWydzial + "', '" + .Item(1).ToString + "', '" + .Item(6).ToString + "')"
                                cmd.ExecuteNonQuery()
                            End If
                            strGrKalk += "#" + .Item(5).ToString
                        End If
                        ' kontrola 
                        If Not strMaterial.Contains(.Item(8).ToString) Then
                            cmd.CommandText = "SELECT ID FROM tblMaterialy WHERE ID = '" + .Item(8).ToString + "'"
                            If cmd.ExecuteScalar Is Nothing Then
                                cmd.CommandText = "INSERT INTO tblMaterialy (ID, MaterialPL, GrMaterID, GrKalkID) VALUES ('" + .Item(8).ToString + "', '" _
                                                + .Item(9).ToString.Replace("'", "") + "', '" + .Item(1).ToString + "', '" + .Item(5).ToString.Replace("#", "") + "')"
                                cmd.ExecuteNonQuery()
                            End If
                            strMaterial += "#" + .Item(8).ToString
                        End If
                        ' kontrola 
                        If Not strKlient.Contains(.Item(12).ToString) Then
                            cmd.CommandText = "SELECT ID FROM tblKlienci WHERE ID = " + .Item(12).ToString
                            If cmd.ExecuteScalar Is Nothing Then
                                cmd.CommandText = "INSERT INTO tblKlienci (ID, Nazwa, GrDekretacji) VALUES (" + .Item(12).ToString + ", '" _
                                                    + .Item(13).ToString.Replace("'", "") + "', '" + CInt(.Item(3)).ToString + "')"
                                cmd.ExecuteNonQuery()
                            End If
                            strKlient += "#" + .Item(12).ToString
                        End If
                        If .Item(8).ToString = "" Then i += 1 : GoTo Nastepny
                    End With
                    ' Zapis sprzedaży
                    m_Waga = 0
                    m_Kwota = 0
                    m_Rabat = 0
                    m_PipSAP = 0
                    rd = dtB.NewRow
                    rd("Miesiac") = iMc.ToString
                    With dv(i)
                        rd("Material") = .Item(8).ToString
                        rd("IDKlienta") = .Item(12).ToString
                        rd("GrMater") = .Item(1).ToString
                        rd("GrKalk") = .Item(5).ToString
                        rd("Wydzial") = strWydzial
                        rd("GrDekretacji") = .Item(3).ToString
                        m_Key = .Item(8).ToString + .Item(12).ToString
                    End With
                    Do While i < dv.Count AndAlso dv.Item(i).Item(8).ToString + dv.Item(i).Item(12).ToString = m_Key
                        With dv(i)
                            Try
                                If Not "WS_R1_R2".Contains(strWydzial) Then
                                    m_Waga += CDbl(.Item(18).ToString)
                                Else
                                    m_Waga += CDbl(.Item(19).ToString)
                                End If
                            Catch ex As Exception
                            End Try
                            Try
                                m_Kwota += CDbl(.Item(22).ToString)
                            Catch ex As Exception
                            End Try
                            Try
                                m_Rabat += CDbl(.Item(23).ToString)
                            Catch ex As Exception
                            End Try
                            Try
                                m_PipSAP += CDbl(.Item(26).ToString)
                            Catch ex As Exception
                            End Try
                            i += 1
                        End With
                    Loop
                    iR = dvB.Find(New Object() {rd("Material"), rd("IDKlienta")})
                    If iR < 0 Then
                        rd("Waga") = (m_Waga / 1000)
                        rd("Kwota") = m_Kwota
                        rd("Rabat") = m_Rabat
                        rd("PIP_SAP") = m_PipSAP
                        dtB.Rows.Add(rd)
                    Else
                        With dvB(iR)
                            .Item("Wydzial") = strWydzial
                            .Item("Waga") = (m_Waga / 1000)
                            .Item("Kwota") = m_Kwota
                            .Item("Rabat") = m_Rabat
                            .Item("PIP_SAP") = m_PipSAP
                        End With
                    End If
Nastepny:
                    With Me.tspbMain
                        If i <= .Maximum AndAlso (i Mod 5).Equals(0) Then _
                            .Value = i : Application.DoEvents()
                    End With
                Loop
                tr.Commit()
            Catch ex As Exception
                tr.Rollback()
                ClearStatus()
                Using New CM
                    MessageBox.Show(ex.Message & vbCrLf & ex.StackTrace, CO_ACTION, MessageBoxButtons.OK, MessageBoxIcon.Error)
                End Using
                GoTo Finish
            End Try
        End Using
NastepnyWydzial:
        Me.tspbMain.Value = iW : Me.Refresh()
        dtCh = New DataTable
        da = DA_TI("tblSprzedaz", "Miesiac DESC, Material, IDKlienta")
        dtCh = dtB.GetChanges
        If Not dtCh Is Nothing Then
            da.Update(dtCh)
            dtB.AcceptChanges()
        End If
        dtCh = New DataTable
        da = DA_TI("tblMaterialy", "ID")
        dtCh = dtMM.GetChanges
        If Not dtCh Is Nothing Then
            da.Update(dtCh)
            dtMM.AcceptChanges()
        End If
        ' 2013-04-12
        Dim m_Kontr As Integer = -1
        RZSprzedazVsZS(iRok.ToString, iMc, m_Kontr)
        If m_Kontr.Equals(-1) Then GoTo Finish
        With Me.txtKomunikat
            If Not .Text.Length.Equals(0) Then
                .Text = CO_ACTION + .Text
                KomunikatShow()
            Else
                Using New CM
                    MessageBox.Show(CO_ACTION + " zakończone pomyślnie", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
                End Using
            End If
        End With
Finish:
        ClearStatus()
        da = Nothing
        dt = Nothing
        dv = Nothing
        cmd = Nothing
        tr = Nothing
        m_Ado = Nothing
        dtB = Nothing
        dtT = Nothing
        dv = Nothing
        dvB = Nothing
        dvS = Nothing
    End Sub

    ' Weryfikuje zapisy sprzedaży DB na podstawie zeszytów sprzedaży
    Private Sub WeryfikacjaSprzedazyWydzialowej()

        Dim strPath As String = My.Settings.ToResult
        Dim m_Ext As String = ".xl*"
        Dim iRok, iMc As Integer
        Dim strGrMater As String = ""
        Dim strGrKalk As String = ""
        Dim strMaterial As String = ""
        Dim strKlient As String = ""
        Dim m_Ado As New ClADO.ADOGet
        Dim da As OleDbDataAdapter
        Dim dtB As New DataTable, dtT As New DataTable
        Dim dvB, dvT As DataView
        Dim i, j, iR As Integer
        Dim m_Key As String
        Dim m_Dekr As String
        Dim rd As DataRow = Nothing
        Dim m_dbl(3, 5) As Double
        Dim dbl, dblW As Double
        Dim m_Txt As String = ""
        Dim blnStare As Boolean
        Const CO_ACTION As String = "Weryfikacja danych sprzedaży"
        If BlokadaAktualizacji("tblSprzedaz") Then Exit Sub
        Try
            With Me
                With .txtKomunikat
                    .Clear()
                    .Visible = False
                End With
                iRok = .dtpOkres.Value.Year
                iMc = .dtpOkres.Value.Month
            End With
            blnStare = iRok.Equals(2008) OrElse iRok.Equals(2009)
            m_Key = "0" + iMc.ToString
            m_Key = "D_" + m_Key.Substring(m_Key.Length - 2)
            FillStatusBar(, "Weryfikacja danych DB zeszytami sprzedaży...")
            da = New OleDbDataAdapter("SELECT * FROM tblSprzedaz WHERE Miesiac = " + iMc.ToString _
                                    + " AND Wydzial IN (" + Me.txtPaWydzialy.Text + ")", Me.cns)
            dtB = New DataTable
            da.Fill(dtB)
            With dtB
                With .Columns
                    .Add("P_Kwota", GetType(Double))
                    .Add("P_PIP", GetType(Double))
                    .Add("P_Transport", GetType(Double))
                    .Add("P_Other", GetType(Double))
                    .Add("P_Overhead", GetType(Double))
                    .Add("ZS_Waga", GetType(Double))
                    .Add("ZS_Kwota", GetType(Double))
                    .Add("ZS_Waga-Waga", GetType(Double))
                End With
                dvB = .DefaultView
            End With
            dvB.Sort = "Wydzial, GrKalk, GrDekretacji"
            For Each m_Item As String In Me.lstPaWybor.CheckedItems
                With m_Ado
                    .Folder = "W:\Costs\CHO_SALES\CHO_" + iRok.ToString + "\"
                    .HDR = True
                    Select Case m_Item.ToString
                        Case "MS"
                            .Baza = "ZkSt"
                        Case "PS"
                            .Baza = "ZkPras"
                        Case "MT"
                            .Baza = "ZkWom"
                        Case "WS"
                            .Baza = "ZwSt"
                        Case "R1"
                            .Baza = "ZwWd"
                        Case "R2"
                            .Baza = "ZwWd2n"
                        Case "OR"
                            .Baza = "ZPoz"
                    End Select
                    .Baza += iRok.ToString + "sprz.xls"
                    If Not .OpenData(True) Then GoTo Nastepny
                    If blnStare Then
                        m_Key = m_Key.Replace("D_", "Mc")
                        da = New OleDbDataAdapter("SELECT * FROM [" + m_Key + "$A4:K305]  ", m_Ado.CN)
                    Else
                        da = New OleDbDataAdapter("SELECT * FROM [" + m_Key + "$A10:G312]  ", m_Ado.CN)
                    End If
                    dtT = New DataTable
                    da.Fill(dtT)
                    With dtT
                        .Columns(1).ColumnName = "GrKalk"
                        .Columns.Add("GD")
                        If blnStare Then
                            For i = 0 To .Rows.Count - 1
                                .Rows(i)("GD") = "01"
                                If i > 154 Then .Rows(i)("GD") = "02"
                            Next i
                        Else
                        End If
                    End With
                    dvT = dtT.DefaultView
                    dvT.RowFilter = "GrKalk <> ''"
                End With
                For Each v As DataRowView In dvT
                    If CDbl(v("Mg").ToString).Equals(0.0) Then GoTo NastepnyZapis
                    If String.IsNullOrEmpty(v(0).ToString) OrElse Not v(0).ToString.StartsWith("J") Then GoTo NastepnyZapis
                    For iR = 0 To m_dbl.GetUpperBound(0)
                        For j = 0 To m_dbl.GetUpperBound(1)
                            m_dbl(iR, j) = 0
                        Next j
                    Next iR
                    If blnStare Then
                        Try
                            m_dbl(0, 0) = CDbl(v(2).ToString)
                            m_dbl(0, 1) = CDbl(v(4).ToString)
                            m_dbl(0, 2) = CDbl(v(6).ToString)
                            m_dbl(0, 3) = CDbl(v(8).ToString)
                            m_dbl(0, 5) = CDbl(v(10).ToString)
                        Catch ex As Exception
                            m_dbl(0, 0) = 0
                        End Try
                    Else
                        Try
                            m_dbl(0, 0) = CDbl(v(1).ToString)
                            m_dbl(0, 1) = CDbl(v(2).ToString)
                            m_dbl(0, 2) = CDbl(v(3).ToString)
                            m_dbl(0, 3) = CDbl(v(4).ToString)
                            m_dbl(0, 4) = CDbl(v(5).ToString)
                            m_dbl(0, 5) = CDbl(v(6).ToString)
                        Catch ex As Exception
                            m_dbl(0, 0) = 0
                        End Try
                    End If
                    If m_dbl(0, 0).Equals(0.0) Then GoTo NastepnyZapis
                    dvB.RowFilter = Nothing
                    If blnStare Then
                    Else
                        m_Dekr = "04"
                        Select Case i
                            Case Is < 76
                                m_Dekr = "01"
                            Case Is < 155
                                m_Dekr = "02"
                            Case Is < 234
                                m_Dekr = "03"
                        End Select
                    End If
                    dvB.RowFilter = "Wydzial = '" + m_Item.ToString _
                                    + "' AND GrKalk = '" + v(0).ToString _
                                    + "' AND GrDekretacji = '" + v("GD").ToString + "'"
                    If dvB.Count.Equals(0.0) Then
                        m_Txt += vbCrLf + "Niedobrany zapis: " + dvB.RowFilter.ToString + "  kwota: " + m_dbl(0, 1).ToString _
                                    + "  waga: " + m_dbl(0, 0).ToString
                        GoTo NastepnyZapis
                    End If
                    dvB(0)("ZS_Waga") = m_dbl(0, 0)
                    dvB(0)("ZS_Kwota") = m_dbl(0, 1)
                    For Each v2 As DataRowView In dvB
                        m_dbl(1, 0) += CDbl(v2("Waga").ToString)
                    Next v2
                    If m_dbl(1, 0).Equals(0.0) Then
                        m_Txt += vbCrLf + "Nierozdzielony zapis: " + dvB.RowFilter.ToString + "  kwota: " + m_dbl(0, 1).ToString _
                                     + "  waga: " + m_dbl(0, 0).ToString
                        GoTo NastepnyZapis
                    End If
                    dvB(0)("ZS_Waga-Waga") = Math.Round(m_dbl(0, 0) - m_dbl(1, 0), 3)
                    For Each v2 As DataRowView In dvB
                        dblW = CDbl(v2("Waga").ToString)
                        If blnStare Then
                            If Not dblW.Equals(0.0) Then
                                Try
                                    dbl = CDbl(v(4).ToString)
                                    m_dbl(2, 1) += dblW * dbl / m_dbl(1, 0)
                                    v2(17) = dblW * dbl / m_dbl(1, 0)
                                Catch ex As Exception
                                End Try
                                Try
                                    dbl = CDbl(v(6).ToString)
                                    m_dbl(2, 2) += dblW * dbl / m_dbl(1, 0)
                                    v2(18) = dblW * dbl / m_dbl(1, 0)
                                Catch ex As Exception
                                End Try
                                Try
                                    dbl = CDbl(v(8).ToString)
                                    m_dbl(2, 3) += dblW * dbl / m_dbl(1, 0)
                                    v2(19) = dblW * dbl / m_dbl(1, 0)
                                Catch ex As Exception
                                End Try
                                Try
                                    dbl = CDbl(v(10).ToString)
                                    m_dbl(2, 5) += dblW * dbl / m_dbl(1, 0)
                                    v2(21) = dblW * dbl / m_dbl(1, 0)
                                Catch ex As Exception
                                End Try
                            End If
                        Else
                            For j = 1 To 6
                                If dblW = 0 Then Exit For
                                Try
                                    dbl = CDbl(v(j).ToString)
                                Catch ex As Exception
                                    dbl = 0
                                End Try
                                If dbl = 0 Then GoTo Stepik
                                m_dbl(2, j - 1) += dblW * dbl / m_dbl(1, 0)
                                v2(15 + j) = dblW * dbl / m_dbl(1, 0)
Stepik:
                            Next j
                        End If
                    Next v2
NastepnyZapis:
                Next v
Nastepny:
            Next m_Item
            dvB.RowFilter = Nothing
            ClearStatus()
            If m_Txt.Length > 0 Then
                Me.txtKomunikat.Text = CO_ACTION + m_Txt.Replace("'", "")
                KomunikatShow()
                Using New CM
                    If MessageBox.Show("Przejść do Excela?", CO_ACTION, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2).Equals(Windows.Forms.DialogResult.No) Then Exit Try
                End Using
            Else
                Using New CM
                    MessageBox.Show("Generowanie danych zakończone pomyślnie", CO_ACTION, MessageBoxButtons.OK, MessageBoxIcon.Information)
                End Using
            End If
            Dim _File As String = "C:\Celsa\DataToDataBase.xls"
            Dim aVal(dvB.Count, dtB.Columns.Count - 1) As Object
            For j = 0 To dtB.Columns.Count - 1
                aVal(0, j) = dtB.Columns(j).ColumnName
            Next j
            For i = 0 To dvB.Count - 1
                For j = 0 To dtB.Columns.Count - 1
                    aVal(i + 1, j) = dvB(i)(j)
                Next j
            Next i
            With New Cl4Excel.Excel(WB:=Nothing, SaveAs:=_File)
                With .oSheet
                    .Name = "Data2DB"
                    .Range("A1").Resize(aVal.GetUpperBound(0) + 1, aVal.GetUpperBound(1) + 1).Value = aVal
                End With
                With .oApp
                    .ActiveWindow.Zoom = 75
                    .ActiveSheet.Range("J2").Select()
                    .ActiveWindow.FreezePanes = True
                End With
                .oBook.Save()
                HideScreen()
                .Show()
            End With
            HideScreen(False)
            aVal = Nothing
            Using New CM
                If MessageBox.Show("Zapisać zmiany w bazie danych?", CO_ACTION, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2).Equals(Windows.Forms.DialogResult.No) Then Exit Try
            End Using
            ' zapis do bazy
            Dim aDane As Array
            With dtB.Columns
                .Remove("P_Kwota")
                .Remove("P_PIP")
                .Remove("P_Transport")
                .Remove("P_Other")
                .Remove("P_Overhead")
                .Remove("ZS_Waga")
                .Remove("ZS_Kwota")
                .Remove("ZS_Waga-Waga")
            End With
            dvB.Sort = "Miesiac, Material, IDKlienta, Wydzial, GrDekretacji"
            With New Cl4Excel.GetFromXls(_File, "Data2DB", "A2:V")
                .Data2Array() : aDane = .Dane : .Zwolnij()
            End With
            If aDane Is Nothing Then Exit Try
            For i = 0 To dtB.Rows.Count - 1
                If Not dtB(i)("Waga").Equals(0.0) Then
                    dtB(i)("Kwota") = 0
                    dtB(i)("Rabat") = 0
                    dtB(i)("PIP") = 0
                    dtB(i)("Transport") = 0
                    dtB(i)("Other") = 0
                    dtB(i)("Overhead") = 0
                End If
            Next i
            i = 1
            Do Until aDane.GetValue(i, 1) Is Nothing
                With aDane
                    iR = dvB.Find(New Object() {.GetValue(i, 1), .GetValue(i, 2), .GetValue(i, 3), .GetValue(i, 6), "0" + .GetValue(i, 7).ToString})
                End With
                If iR < 0 Then
                    Using New CM
                        MessageBox.Show("Procedura nie przewiduje zmiany identyfikatorów (miesiąc, materiał, klient, wydział, grupa dekretacji", CO_ACTION, MessageBoxButtons.OK, MessageBoxIcon.Error)
                    End Using
                    Exit Try
                End If
                Try
                    dvB(iR)("Waga") = aDane.GetValue(i, 8)
                Catch ex As Exception
                End Try
                If dvB(iR)("Waga").Equals(0.0) Then
                    Try
                        dvB(iR)("Kwota") = aDane.GetValue(i, 9)
                    Catch ex As Exception
                    End Try
                    Try
                        dvB(iR)("Rabat") = aDane.GetValue(i, 10)
                    Catch ex As Exception
                    End Try
                    Try
                        dvB(iR)("PIP") = aDane.GetValue(i, 11)
                    Catch ex As Exception
                    End Try
                    Try
                        dvB(iR)("Transport") = aDane.GetValue(i, 12)
                    Catch ex As Exception
                    End Try
                    Try
                        dvB(iR)("Other") = aDane.GetValue(i, 13)
                    Catch ex As Exception
                    End Try
                    Try
                        dvB(iR)("Overhead") = aDane.GetValue(i, 14)
                    Catch ex As Exception
                    End Try
                Else
                    Try
                        dvB(iR)("Kwota") = aDane.GetValue(i, 18)
                    Catch ex As Exception
                    End Try
                    Try
                        dvB(iR)("PIP") = aDane.GetValue(i, 19)
                    Catch ex As Exception
                    End Try
                    Try
                        dvB(iR)("Transport") = aDane.GetValue(i, 20)
                    Catch ex As Exception
                    End Try
                    Try
                        dvB(iR)("Other") = aDane.GetValue(i, 21)
                    Catch ex As Exception
                    End Try
                    Try
                        dvB(iR)("Overhead") = aDane.GetValue(i, 22)
                    Catch ex As Exception
                    End Try
                End If
                i += 1
            Loop
            aDane = Nothing
            da = DA_TI("tblSprzedaz", "Miesiac, Material, IDKlienta, Wydzial, GrDekretacji")
            da.Update(dtB.GetChanges)
            Using New CM
                MessageBox.Show("Dane sprzedaży zostały zmienione", CO_ACTION, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End Using
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message, CO_ACTION, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
        Finally
            da = Nothing
            m_Ado = Nothing
            dtB = Nothing
            dtT = Nothing
            dvB = Nothing
        End Try
    End Sub

    Private Sub AdEBiTDA() Handles tsmAdEBiTDA.Click

        Dim FW As New PobierzWykonanie
        FW.RZ_EBiTDA()
    End Sub

    Private Sub TransportBezSprzedazy() Handles tsmTransportBezSprzedazy.Click

        Dim _Path As String = My.Settings.ToResult
        Dim _Ext As String = ".xl*"
        Dim iRok, iMc As Integer
        Dim strOkres As String
        Dim m_Ado As New ClADO.ADOGet
        Dim da As OleDbDataAdapter
        Dim dt As New DataTable, dtS As New DataTable, dtW As New DataTable
        Const CO_ACTION As String = "Pobieranie danych transportu sprzedaży"
        Try
            If BlokadaAktualizacji("tblTransport") Then Return
            strOkres = Format(Me.dtpOkres.Value, "MM.yyyy")
            iRok = Me.dtpOkres.Value.Year
            iMc = Me.dtpOkres.Value.Month
            _Path = CP.GetSourcePath(_Path + "CHO_Result_" + Okres.Substring(0, 7), _Ext, CO_ACTION)
            If String.IsNullOrEmpty(_Path) Then Return
            With My.Settings
                .ToResult = _Path
                .Save()
            End With
            da = New OleDbDataAdapter("SELECT ID, Wydzial FROM tblGrupyMat WHERE NOT (Wydzial IS NULL OR wydzial = '') ORDER BY ID", Me.cns)
            da.Fill(dtW)
            With m_Ado
                .Baza = "CHO_Result_" + Okres.Substring(0, 7) + _Ext
                .Folder = _Path
                .HDR = True
                If Not .OpenData(True) Then _
                    Throw New ArgumentException("Błąd odczytu pliku CHO_Result_" + Okres.Substring(0, 7) + _Ext)
                da = New OleDbDataAdapter("SELECT * FROM [Transport$A1:AD50000]  ", .CN)
                da.Fill(dt)
                If dt.Rows.Count < 2 Then _
                        Throw New ArgumentException("Plik nie spełnia oczekiwań - brak danych w arkuszu [Transport]")
                da = New OleDbDataAdapter("SELECT * FROM [Sales$A1:W50000]  ", .CN)
                da.Fill(dtS)
            End With
            PobierzTransportSprzedazy(dt, dtW, dtS)
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message, CO_ACTION, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End Using
        Finally
            ClearStatus()
            da = Nothing
            dt = Nothing
            m_Ado = Nothing
            dtW = Nothing
        End Try
    End Sub

#End Region ' Administrator

#Region "Metody pomocnicze"

    Public Sub HideScreen(Optional ByVal Ukryj As Boolean = True)

        If Ukryj Then
            With Me
                .Hide()
                .Cursor = Cursors.WaitCursor
            End With
            Return
        End If
        With Me
            .Cursor = Cursors.Default
            .tsslMain.Text = "Gotowe"
            .tspbMain.Value = 0
            .Visible = True
        End With
    End Sub

    Private Function NotExportFile(ByRef Plik As String) As Boolean

        With New Cl4Excel.CheckFile(My.Settings.Wzorce + "CHO_EXPORT", True, , My.Settings.Wyniki, "m")
            If Not .NOK Then
                Plik = .Plik
                My.Settings.Wzorce = .Wzorce
                My.Settings.Wyniki = .Wyniki
            End If
            NotExportFile = .NOK
        End With
    End Function

#End Region ' Metody pomocnicze

    Private Sub tsm_Ei_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles tsm_Ei.Click

        Dim FM As New ZaOkres
        Dim Rok1, Rok2 As Integer
        Dim dt As New DataTable, dtR As New DataTable
        Dim dv As DataView
        Dim i, j, k, m As Integer
        Dim _Czym As String
        Dim tr As OleDbTransaction = Nothing
        Dim cmd As New OleDbCommand
        Try
            With FM
                With .dtpOd
                    .ShowUpDown = True
                    .CustomFormat = "yyyy"
                End With
                With .dtpDo
                    .ShowUpDown = True
                    .CustomFormat = "yyyy"
                End With
                If .ShowDialog.Equals(Windows.Forms.DialogResult.Cancel) Then Return
                Rok1 = .dtpOd.Value.Year
                Rok2 = .dtpDo.Value.Year
            End With
            FillStatusBar(0, Me.tsm_Ei.Text.Replace("&", ""))
            With dt.Columns
                .Add("Country")
                .Add("Czym")
                .Add("Rok", GetType(Integer))
                .Add("Mg", GetType(Double))
                .Add("PLN", GetType(Double))
            End With
            For i = Rok1 To Rok2
                Dim m_Ado As New ClADO.ADOGet
                Try
                    With m_Ado
                        .Baza = My.Settings.Baza.Replace(Me.dtpOkres.Value.Year.ToString, i.ToString)
                        .Folder = My.Settings.Sciezka
                        .Haslo = My.Settings.Myk
                        If Not .OpenData(True) Then _
                            Throw New ArgumentException("Brak połączenia z plikiem bazy danych [" + .Baza + "]")
                    End With
                    Using cn As New OleDbConnection(m_Ado.CN.ConnectionString)
                        Try
                            cn.Open()
                            tr = cn.BeginTransaction
                            With cmd
                                .Connection = cn
                                .Transaction = tr
                                .CommandText = "SELECT K.Kraj, WarunekWysylki, SUM(Waga) AS [Mg], SUM(Kwota) AS [Kwota] " _
                                            + "FROM tblTransport T LEFT JOIN tblKlienci K ON T.IDKlienta = K.ID " _
                                            + "WHERE Wydzial IN ('WS', 'R1', 'R2') GROUP BY K.Kraj, WarunekWysylki"
                                With dtR
                                    .Clear()
                                    .Load(cmd.ExecuteReader)
                                End With
                            End With
                        Catch ex As Exception
                            Throw New ArgumentException(ex.Message)
                        End Try
                    End Using
                    For Each p In dtR.Rows
                        If p(1).ToString.Equals("W1") Then
                            _Czym = "TRUCK"
                        ElseIf p(1).ToString.Equals("W2") Then
                            _Czym = "RAIL"
                        ElseIf p(1).ToString.Equals("W4") Then
                            _Czym = "VESSEL"
                        Else
                            _Czym = "OTHER"
                        End If
                        dt.Rows.Add(New Object() {p(0), _Czym, i, p(2), p(3)})
                    Next p
                Catch ex As Exception
                    Throw New ArgumentException(ex.Message)
                Finally
                    m_Ado = Nothing
                End Try
            Next i
            Dim query = (From p In dt.AsEnumerable Select p!Country).Distinct().Count
            Dim aDane((query + 1) * 5, (Rok2 - Rok1) * 3 + 4) As Object
            Dim aTotal(4, (Rok2 - Rok1) * 3 + 4) As Object
            For Each p In dt.Rows
                If IsDBNull(p(0)) Then p(0) = "???"
            Next p
            dv = dt.DefaultView
            dv.Sort = "Country, Rok"
            i = 0
            j = 0
            aTotal(0, 0) = "TOTAL"
            aTotal(0, 1) = "TOTAL"
            aTotal(1, 1) = "TRUCK"
            aTotal(2, 1) = "RAIL"
            aTotal(3, 1) = "VESSEL"
            aTotal(4, 1) = "OTHER"
            Do Until i.Equals(dv.Count)
                _Czym = dv(i)(0)
                aDane(j, 0) = _Czym
                aDane(j, 1) = "TOTAL"
                aDane(j + 1, 1) = "TRUCK"
                aDane(j + 2, 1) = "RAIL"
                aDane(j + 3, 1) = "VESSEL"
                aDane(j + 4, 1) = "OTHER"
                Do While dv(i)(0).ToString.Equals(_Czym)
                    k = 2
                    k += 3 * (dv(i)(2) - Rok1)
                    aDane(j, k) += dv(i)(3)
                    aDane(j, k + 1) += dv(i)(4)
                    aDane(j, k + 2) = aDane(j, k + 1) / aDane(j, k)
                    aTotal(0, k) += dv(i)(3)
                    aTotal(0, k + 1) += dv(i)(4)
                    aTotal(0, k + 2) = aTotal(0, k + 1) / aTotal(0, k)
                    Select Case dv(i)(1)
                        Case "TRUCK"
                            aDane(j + 1, k) += dv(i)(3)
                            aDane(j + 1, k + 1) += dv(i)(4)
                            aDane(j + 1, k + 2) = aDane(j + 1, k + 1) / aDane(j + 1, k)
                            aTotal(1, k) += dv(i)(3)
                            aTotal(1, k + 1) += dv(i)(4)
                            aTotal(1, k + 2) = aTotal(1, k + 1) / aTotal(1, k)
                        Case "RAIL"
                            aDane(j + 2, k) += dv(i)(3)
                            aDane(j + 2, k + 1) += dv(i)(4)
                            aDane(j + 2, k + 2) = aDane(j + 2, k + 1) / aDane(j + 2, k)
                            aTotal(2, k) += dv(i)(3)
                            aTotal(2, k + 1) += dv(i)(4)
                            aTotal(2, k + 2) = aTotal(2, k + 1) / aTotal(2, k)
                        Case "VESSEL"
                            aDane(j + 3, k) += dv(i)(3)
                            aDane(j + 3, k + 1) += dv(i)(4)
                            aDane(j + 3, k + 2) = aDane(j + 3, k + 1) / aDane(j + 3, k)
                            aTotal(3, k) += dv(i)(3)
                            aTotal(3, k + 1) += dv(i)(4)
                            aTotal(3, k + 2) = aTotal(3, k + 1) / aTotal(3, k)
                        Case "OTHER"
                            aDane(j + 4, k) += dv(i)(3)
                            aDane(j + 4, k + 1) += dv(i)(4)
                            aDane(j + 4, k + 2) = aDane(j + 4, k + 1) / aDane(j + 4, k)
                            aTotal(4, k) += dv(i)(3)
                            aTotal(4, k + 1) += dv(i)(4)
                            aTotal(4, k + 2) = aTotal(4, k + 1) / aTotal(4, k)
                    End Select
                    i += 1
                    If i.Equals(dv.Count) Then Exit Do
                Loop
                j += 5
            Loop
            Dim _St1 As String = "", _St2 As String = ""
            Dim XE As New Cl4Excel.Excel(Nothing)
            With XE
                With .oSheet
                    .Range("A1").Value = "Country"
                    With .Range("A1:A2")
                        .Merge()
                        .HorizontalAlignment = XE.oApp.XlHAlign.xlHAlignCenter
                        .VerticalAlignment = XE.oApp.XlVAlign.xlVAlignCenter
                    End With
                    _St1 = CP.GetColumnLetter((Rok2 - Rok1) * 3 + 5)
                    .Range("A3:" & _St1 & (aDane.GetUpperBound(0) + 3)).Value2 = aDane
                    m = .Range("A1").CurrentRegion.Rows.Count
                    .Range("A" & (m + 1) & ":" & _St1 & (m + 5)).Value2 = aTotal
                    j = 0
                    For i = Rok1 To Rok2
                        j += 3
                        .Cells(1, j) = "'" + i.ToString
                        _St1 = CP.GetColumnLetter(j)
                        _St2 = CP.GetColumnLetter(j + 2)
                        With .Range(_St1 & "1:" & _St2 & "1")
                            .Merge()
                            .HorizontalAlignment = XE.oApp.XlHAlign.xlHAlignCenter
                        End With
                        With .Range(_St1 & "2:" & _St2 & "2")
                            .Value2 = New String() {"Mg", "PLN", "PLN/Mg"}
                            .HorizontalAlignment = XE.oApp.XlHAlign.xlHAlignCenter
                        End With
                        XE.Obramowanie(.Range(_St1 & "1:" & _St2 & "2"), True)
                    Next i
                    With .Range("A1").CurrentRegion
                        m = .Rows.Count
                        k = .Columns.Count
                    End With
                    XE.Obramowanie(.Range("A3:" & _St2 & m))
                    For i = 3 To m Step 5
                        With .Range("A" & i & ":A" & (i + 4))
                            .Merge()
                            .HorizontalAlignment = XE.oApp.XlHAlign.xlHAlignCenter
                            .VerticalAlignment = XE.oApp.XlVAlign.xlVAlignCenter
                        End With
                        .Range("A" & i & ":" & _St2 & i).Borders(XE.oApp.XlBordersIndex.xlEdgeTop).Weight = XL.XlBorderWeight.xlThin
                    Next i
                    With .Range("A1").CurrentRegion.Offset(2, 2)
                        .NumberFormat = "### ### ### ##0"
                    End With
                    .Range("A:" + _St2).Columns.AutoFit()
                    With .Range("A" + (m - 4).ToString + ":" + _St2 + (m - 4).ToString).Borders(XL.XlBordersIndex.xlEdgeTop)
                        .ColorIndex = XE.oApp.XlColorIndex.xlColorIndexAutomatic
                        .Weight = XE.oApp.XlBorderWeight.xlThin
                    End With
                    For i = 3 To k Step 3
                        _St1 = CP.GetColumnLetter(i)
                        _St2 = CP.GetColumnLetter(i + 2)
                        With .Range(_St1 + "1:" + _St2 + m.ToString).Borders(XE.oApp.XlBordersIndex.xlEdgeRight)
                            .ColorIndex = XE.oApp.XlColorIndex.xlColorIndexAutomatic
                            .Weight = XE.oApp.XlBorderWeight.xlThin
                        End With
                    Next i
                    .Range("C3").Select()
                End With
                With .oApp.ActiveWindow
                    .DisplayGridlines = False
                    .DisplayZeros = False
                    .FreezePanes = True
                    .Zoom = 80
                End With
                ClearStatus()
                .Show()
            End With
        Catch ex As Exception
            ClearStatus()
            Using New CM
                MessageBox.Show(ex.Message, Me.tsm_Ei.Text.Replace("&", ""), MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
        Finally
            dt = Nothing
            dtR = Nothing
            tr = Nothing
            cmd = Nothing
        End Try
    End Sub

#Region "Ad Hoc"

    Private Sub AdHoc() Handles tsmAdHoc.Click

        '' weryfikacja materiałow
        'Dim i As Integer
        'Dim dt As New DataTable, dt1 As New DataTable
        'Dim dv As DataView
        'Dim da As OleDbDataAdapter = New OleDbDataAdapter("SELECT * FROM tblMaterialy1", Me.cns)
        'da.Fill(dt1)
        'da = DA_TI("tblMaterialy", "ID")
        'da.Fill(dt)
        'dv = dt.DefaultView
        'dv.Sort = "ID"
        'For Each r As DataRow In dt1.Rows
        '    If String.IsNullOrEmpty(r("MaterialEN").ToString) Then r("MaterialEN") = r("MaterialPL")
        '    i = dv.Find(r("ID"))
        '    If i.Equals(-1) Then _
        '        dt.Rows.Add(New Object() {r(0), r(1), r(2), r(3), r(4), r(5), r(6)})
        'Next r
        ''dv.RowFilter = "MaterialEN = '' ORELSE MaterialEN IS NOTHING"
        'For Each v As DataRowView In dv
        '    If String.IsNullOrEmpty(v("MaterialEN").ToString) Then _
        '        v("MaterialEN") = v("MaterialPL")
        'Next v
        'dv.RowFilter = Nothing
        'Try
        '    da.Update(dt.GetChanges)
        'Catch ex As Exception
        'End Try
    End Sub

    Private Sub PobierzDaneSprzZBW(ByVal sender As System.Object, ByVal e As System.EventArgs)

        '        Dim strPath As String = "W:\Costs\CHO Wyniki\Wynik " & Me.dtpOkres.Value.Year.ToString + "\Dane\"
        '        Dim m_Ext As String = ".xl*"
        '        Dim iRok, iMc As Integer
        '        Dim strGrMater As String = ""
        '        Dim strGrKalk As String = ""
        '        Dim strMaterial As String = ""
        '        Dim strKlient As String = ""
        '        Dim strWydzial, strOkres As String
        '        Dim m_Ado As New ClADO.ADOget
        '        Dim da As OleDbDataAdapter
        '        Dim dt As New DataTable
        '        Dim dtW As New DataTable
        '        Dim dvW As New DataView
        '        Dim dv As New DataView
        '        Dim v As DataRowView
        '        Dim cmd As New OleDbCommand
        '        Dim i, iR As Integer
        '        Dim m_Waga As Double
        '        Dim m_Kwota As Double
        '        Dim m_Rabat As Double
        '        Dim m_Transport As Double
        '        Dim m_Pip As Double
        '        Dim m_PipSAP As Double
        '        Dim m_Key As String
        '        Dim r As DataRow = Nothing
        '        Dim tr As OleDbTransaction = Nothing
        '        Const CO_ACTION As String = "Pobieranie danych sprzedaży"
        '        If BlokadaAktualizacji("tblSprzedaz") Then Exit Sub
        '        strOkres = Okres.Substring(5, 2) + "." + Okres.Substring(0, 4)
        '        iRok = CInt(strOkres.Substring(3, 4))
        '        iMc = CInt(strOkres.Substring(0, 2))
        '        ' kursy walut
        '        Try
        '            da = DA_TI("tblKursyWalut", "MM_dd")
        '            da.Fill(dt)
        '            dv = dt.DefaultView
        '            dv.RowFilter = "MM_dd = '" + strOkres.Substring(0, 2) + "'"
        '            If dv.Count.Equals(0) Then
        '                Dim aKursy As Array
        '                With New XG("W:\Costs\CELSA\Kursy walut\Kursy_" + iRok.ToString + "_Średnioważone.xls", "średnie M ", "A2:O39")
        '                    .Data2Array()
        '                    aKursy = .Dane
        '                End With
        '                If aKursy Is Nothing Then Exit Try
        '                r = dt.NewRow
        '                r("MM_dd") = strOkres.Substring(0, 2)
        '                For iR = 1 To 4
        '                    For i = 2 To aKursy.GetUpperBound(0)
        '                        If aKursy.GetValue(i, 3).ToString.Contains(dt.Columns(iR).ColumnName) Then
        '                            r(iR) = aKursy.GetValue(i, 3 + iMc)
        '                            Exit For
        '                        End If
        '                    Next i
        '                Next iR
        '                dt.Rows.Add(r)
        '                da.Update(dt.GetChanges)
        '            End If
        '        Catch ex As Exception
        '            Using New CM
        '                MessageBox.Show(ex.Message, "Pobieranie kursów walut", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '            End Using
        '        End Try
        '        If Not SourceFolder(strPath, "S35_Sales_dla RT_" + Okres.Substring(0, 7), m_Ext, CO_ACTION) Then Exit Sub
        '        da = New OleDbDataAdapter("SELECT ID, Wydzial FROM tblGrupyMat WHERE NOT (Wydzial IS NULL OR wydzial = '') ORDER BY ID", Me.cns)
        '        da.Fill(dtW)
        '        dvW = dtW.DefaultView
        '        dvW.Sort = "ID"
        '        With m_Ado
        '            .Baza = "S35_Sales_dla RT_" + Okres.Substring(0, 7) + m_Ext
        '            .Folder = strPath
        '            .HDR = True
        '            If Not .OpenData(True) Then
        '                Using New CM
        '                    MessageBox.Show("Błąd odczytu pliku S35_Sales_dla RT_" + Okres.Substring(0, 7) + m_Ext, CO_ACTION, MessageBoxButtons.OK, MessageBoxIcon.Error)
        '                End Using
        '                Exit Sub
        '            End If
        '            dt = New DataTable
        '            da = New OleDbDataAdapter("SELECT * FROM [Table$F15:AJ50000]  ", m_Ado.CN)
        '            da.Fill(dt)
        '            If dt.Rows.Count.Equals(0) Then Throw New ArgumentException("Plik nie spełnia oczekiwań - brak danych w arkuszu [Table]")
        '        End With
        '        dt.Columns(0).ColumnName = "Okres"
        '        dv = dt.DefaultView
        '        With dv
        '            .RowFilter = "Okres = '" + strOkres + "'"
        '            .Sort = "Okres, Materiał, Zleceniodawca"
        '            FillStatusBar(.Count, CO_ACTION + "...")
        '        End With
        '        i = 0
        '        Using cn As New OleDbConnection(Me.cns.ConnectionString)
        '            Try
        '                cn.Open()
        '                tr = cn.BeginTransaction
        '                With cmd
        '                    .Connection = cn
        '                    .Transaction = tr
        '                    ' usuń poprzednie zapisy
        '                    .CommandText = "DELETE FROM tblSprzedaz WHERE Miesiac = " + iMc.ToString
        '                    .ExecuteNonQuery()
        '                End With
        '                Do While i < dv.Count
        '                    v = dv(i)
        '                    strWydzial = "OR"
        '                    If Not v(6).ToString.Contains("JSWZ") Then
        '                        iR = dvW.Find(v(1).ToString)
        '                        If Not iR.Equals(-1) Then _
        '                            strWydzial = dvW(iR)(1)
        '                    End If
        '                    ' kontrola 
        '                    If Not strGrMater.Contains(v(1).ToString) Then
        '                        Try
        '                            cmd.CommandText = "INSERT INTO tblGrupyMat (ID, Wydzial, GrMaterPL) VALUES ('" + v(1).ToString + "', '" _
        '                                                + strWydzial + "', '" + v(2).ToString + "')"
        '                            cmd.ExecuteNonQuery()
        '                        Catch ex As Exception
        '                        End Try
        '                        strGrMater += "#" + v(1).ToString
        '                    End If
        '                    ' kontrola 
        '                    If v(5).ToString.Length > 2 AndAlso Not strGrKalk.Contains(v(5).ToString) Then
        '                        Try
        '                            With cmd
        '                                .CommandText = "INSERT INTO tblGrupyKalk (ID, Wydzial, GrMater, GrKalkPL) VALUES ('" + v(5).ToString + "', '" _
        '                                                + v("Grupamateriałowa").ToString + "', '" + strWydzial + "', '" + v(1).ToString + "', '" + v(6).ToString + "')"
        '                                .ExecuteNonQuery()
        '                            End With
        '                        Catch ex As Exception
        '                        End Try
        '                        strGrKalk += "#" + v(5).ToString
        '                    End If
        '                    ' kontrola 
        '                    If Not strMaterial.Contains(v(8).ToString) Then
        '                        Try
        '                            With cmd
        '                                .CommandText = "INSERT INTO tblMaterialy (ID, MaterialPL, GrMaterID, GrKalkID) VALUES ('" + v(8).ToString + "', '" _
        '                                                + v(9).ToString.Replace("'", "") + "', '" + v(1).ToString + "', '" + v(5).ToString.Replace("#", "") + "')"
        '                                .ExecuteNonQuery()
        '                            End With
        '                        Catch ex As Exception
        '                        End Try
        '                        strMaterial += "#" + v(8).ToString
        '                    End If
        '                    ' kontrola 
        '                    If Not strKlient.Contains(v(12).ToString) Then
        '                        With cmd
        '                            Try
        '                                .CommandText = "INSERT INTO tblKlienci (ID, Nazwa, GrDekretacji, Kraj) VALUES (" + v(12).ToString + ", '" _
        '                                                + v(13).ToString.Replace("'", "") + "', '" + CInt(v(3)).ToString + "', '" + v(11).ToString + "')"
        '                                .ExecuteNonQuery()
        '                            Catch ex As Exception
        '                                .CommandText = "UPDATE tblKlienci SET Kraj = '" + v(11).ToString + "' WHERE ID = " + v(12).ToString
        '                                .ExecuteNonQuery()
        '                            End Try
        '                        End With
        '                        strKlient += "#" + v(12).ToString
        '                    End If
        '                    If String.IsNullOrEmpty(v(8).ToString) Then
        '                        i += 1 : GoTo Nastepny
        '                    End If
        '                    ' Zapis sprzedaży
        '                    m_Waga = 0
        '                    m_Kwota = 0
        '                    m_Rabat = 0
        '                    m_Transport = 0
        '                    m_Pip = 0
        '                    m_PipSAP = 0
        '                    m_Key = dv(i)(8).ToString + dv(i)(12).ToString
        '                    cmd.CommandText = "INSERT INTO tblSprzedaz (Miesiac, Material, IDKlienta, GrMater, GrKalk, " _
        '                                        + "Wydzial, GrDekretacji, Waga, Kwota, Rabat, PIP_SAP, PIP) " _
        '                                        + "VALUES (" + iMc.ToString + ", '" + dv(i)(8).ToString + "', " + dv(i)(12).ToString + ", '" _
        '                                        + dv(i)(1).ToString + "', '" + dv(i)(5).ToString.Replace("#", "") + "', '" + strWydzial + "', '" _
        '                                        + dv(i)(3).ToString + "', "
        '                    Do While i < dv.Count AndAlso (dv(i)(8).ToString + dv(i)(12).ToString).Equals(m_Key)
        '                        If Not "WS_R1_R2".Contains(strWydzial) Then
        '                            m_Waga += CP.Str2Dbl(dv(i)(18).ToString)
        '                        Else
        '                            m_Waga += CP.Str2Dbl(dv(i)(19).ToString)
        '                        End If
        '                        m_Kwota += CP.Str2Dbl(dv(i)(22).ToString)
        '                        m_Rabat += CP.Str2Dbl(dv(i)(23).ToString)
        '                        m_PipSAP += CP.Str2Dbl(dv(i)(26).ToString)
        '                        i += 1
        '                    Loop
        '                    With cmd
        '                        .CommandText += (m_Waga / 1000).ToString.Replace(",", ".") + ", " + m_Kwota.ToString.Replace(",", ".") _
        '                                        + ", " + m_Rabat.ToString.Replace(",", ".") + ", " + m_PipSAP.ToString.Replace(",", ".") + ", " + m_PipSAP.ToString.Replace(",", ".") + ")"
        '                        .ExecuteNonQuery()
        '                    End With
        'Nastepny:
        '                    With Me.tspbMain
        '                        If i <= .Maximum AndAlso (i Mod 5).Equals(0) Then _
        '                            .Value = i : Application.DoEvents()
        '                    End With
        '                Loop
        '                tr.Commit()
        '            Catch ex As Exception
        '                tr.Rollback()
        '                ClearStatus()
        '                Using New CM
        '                    MessageBox.Show(ex.Message & vbCrLf & ex.StackTrace, CO_ACTION, MessageBoxButtons.OK, MessageBoxIcon.Error)
        '                End Using
        '            End Try
        '        End Using
        '        da = Nothing
        '        dt = Nothing
        '        dv = Nothing
        '        cmd = Nothing
        '        tr = Nothing
        '        m_Ado = Nothing
        '        dtW = Nothing
        '        dvW = Nothing
    End Sub

    '    Private Sub PobierzKosztyArchiwalne(ByVal sender As Object, ByVal e As System.EventArgs)


    '        Dim tsm As ToolStripMenuItem = CType(sender, ToolStripMenuItem)
    '        Dim Tytul As String = tsm.Text.Replace("&", "")
    '        Dim opf As New OpenFileDialog
    '        Dim tr As OleDbTransaction = Nothing
    '        Dim cmd As New OleDbCommand
    '        Dim dt As New DataTable
    '        Dim dv As DataView
    '        Dim _MPK, _MpkP As String
    '        Dim aDane(,), aMpk() As Object
    '        Dim i, j, w As Integer
    '        Dim _Wart As Double
    '        Static _Path As String = "W:\Costs\Celsa"
    '        Try
    '            oApp = New XL.Application
    '            With oApp
    '                .ScreenUpdating = False
    '                .DisplayAlerts = False
    '            End With
    '            With opf
    '                .InitialDirectory = _Path
    '                .Multiselect = True
    '                .Filter = "(*.xls)|*.xls"
    '                If .ShowDialog.Equals(Windows.Forms.DialogResult.Cancel) Then Return
    '                _Path = .FileName(0).ToString.Replace(.SafeFileName(0), "")
    '                Using cn As New OleDbConnection(Me.cns.ConnectionString)
    '                    Try
    '                        cn.Open()
    '                        tr = cn.BeginTransaction
    '                        With cmd
    '                            .Connection = cn
    '                            .Transaction = tr
    '                            .CommandText = "SELECT ID, Typ, Instalacja, Skoroszyt FROM tblMPK"
    '                            dt.Load(.ExecuteReader)
    '                            .CommandText = "SELECT COUNT(*) FROM tblZWPC"
    '                            w = .ExecuteScalar
    '                            If w.Equals(0) Then GoTo PoUsunieciu
    '                            Using New CM
    '                                If MessageBox.Show("Usunąć poprzednie zapisy?", Tytul, MessageBoxButtons.YesNo, _
    '                                                   MessageBoxIcon.Question).Equals(vbNo) Then GoTo PoUsunieciu
    '                            End Using
    '                            .CommandText = "DELETE FROM tblZWPC"
    '                            .ExecuteNonQuery()
    '                        End With
    'PoUsunieciu:
    '                        dv = dt.DefaultView
    '                        dv.Sort = "ID"
    '                        For Each _File As String In .FileNames
    '                            oBook = oApp.Workbooks.Open(_File, False, True)
    '                            For Each oSh As XL._Worksheet In oBook.Worksheets
    '                                If tsm.Name.EndsWith("2") Then GoTo Rok2006
    '                                j = 0
    '                                Try
    'OkreslMpk:
    '                                    If j.Equals(0) Then
    '                                        j = 1
    '                                        _MPK = oSh.Range("A3").Value
    '                                    Else
    '                                        j = 2
    '                                        _MPK = oSh.Range("A1").Value
    '                                    End If
    '                                    aMpk = Split(_MPK, "-")
    '                                    _MPK = aMpk(0).ToString.Trim.ToUpper
    '                                    If String.IsNullOrEmpty(_MPK) AndAlso j.Equals(2) Then GoTo NextSheet
    '                                    i = dv.Find(_MPK)
    '                                    If i.Equals(-1) Then
    '                                        If j.Equals(2) Then GoTo NextSheet
    '                                        GoTo OkreslMpk
    '                                    End If
    '                                    ReDim aDane(1272, 15)
    '                                    aDane = oSh.Range("A6:O1277").Value
    '                                    If dv(i)("Typ").ToString.Equals("P") Then
    '                                        If Not (oSh.Name.Contains("Koszt") OrElse oSh.Name.Contains("Zużycie")) Then GoTo NextSheet
    '                                    End If
    '                                    For w = 1 To aDane.GetLength(0)
    '                                        If aDane(w, 2) Is Nothing Then GoTo NextRow
    '                                        If IsNumeric(aDane(w, 2)) Then GoTo NextRow
    '                                        If Not aDane(w, 2).ToString.Length.Equals(4) Then GoTo NextRow
    '                                        For j = 4 To 15
    '                                            If aDane(w, j) Is Nothing Then GoTo NextMonth
    '                                            If CDbl(aDane(w, j).ToString).Equals(0) Then GoTo NextMonth
    '                                            If dv(i)("Typ").ToString.Equals("P") Then
    '                                                ' produkcyjne
    '                                                _MpkP = dv(i)("ID")
    '                                                If Not aDane(w, 3) Is Nothing Then
    '                                                    If Not IsNumeric(aDane(w, 3)) AndAlso Not String.IsNullOrEmpty(aDane(w, 3).ToString) Then _
    '                                                        _MpkP = aDane(w, 3)
    '                                                End If
    '                                                If oSh.Name.Contains("Koszt") Then
    '                                                    ' wartość
    '                                                    With cmd
    '                                                        .CommandText = "INSERT INTO tblZWPC (Mc, Instalacja, MPK, CalCo, Ilosc, Wartosc, Typ) VALUES (" + (j - 3).ToString + ", '" _
    '                                                                + dv(i)("Instalacja") + "', '" + _MpkP + "', '" + aDane(w, 2) + "', 0, " + aDane(w, j).ToString.Replace(",", ".") + ", '" + dv(i)("Typ") + "')"
    '                                                        Try
    '                                                            .ExecuteNonQuery()
    '                                                        Catch ex As Exception
    '                                                            .CommandText = "SELECT TOP 1 Wartosc FROM tblZWPC WHERE Mc = " + (j - 3).ToString + " AND Instalacja = '" + dv(i)("Instalacja") + "' AND MPK = '" + _MpkP + "' AND CalCo = '" + aDane(w, 2) + "'"
    '                                                            _Wart = .ExecuteScalar
    '                                                            _Wart += aDane(w, j)
    '                                                            .CommandText = "UPDATE tblZWPC SET Wartosc = " + _Wart.ToString.Replace(",", ".") + " WHERE Mc = " + (j - 3).ToString + " AND Instalacja = '" + dv(i)("Instalacja") + "' AND MPK = '" + _MpkP + "' AND CalCo = '" + aDane(w, 2) + "'"
    '                                                            .ExecuteNonQuery()
    '                                                        End Try
    '                                                    End With
    '                                                Else
    '                                                    ' ilość
    '                                                    With cmd
    '                                                        .CommandText = "INSERT INTO tblZWPC (Mc, Instalacja, MPK, CalCo, Ilosc, Wartosc, Typ) VALUES (" + (j - 3).ToString + ", '" _
    '                                                                + dv(i)("Instalacja") + "', '" + _MpkP + "', '" + aDane(w, 2) + "', " + aDane(w, j).ToString.Replace(",", ".") + ", 0, '" + dv(i)("Typ") + "')"
    '                                                        Try
    '                                                            .ExecuteNonQuery()
    '                                                        Catch ex As Exception
    '                                                            .CommandText = "SELECT TOP 1 Ilosc FROM tblZWPC WHERE Mc = " + (j - 3).ToString + " AND Instalacja = '" + dv(i)("Instalacja") + "' AND MPK = '" + _MpkP + "' AND CalCo = '" + aDane(w, 2) + "'"
    '                                                            _Wart = .ExecuteScalar
    '                                                            _Wart += aDane(w, j)
    '                                                            .CommandText = "UPDATE tblZWPC SET Ilosc = " + _Wart.ToString.Replace(",", ".") + " WHERE Mc = " + (j - 3).ToString + " AND Instalacja = '" + dv(i)("Instalacja") + "' AND MPK = '" + _MpkP + "' AND CalCo = '" + aDane(w, 2) + "'"
    '                                                            .ExecuteNonQuery()
    '                                                        End Try
    '                                                    End With
    '                                                End If
    '                                            Else
    '                                                ' pomocnicze
    '                                                With cmd
    '                                                    .CommandText = "INSERT INTO tblZWPC (Mc, Instalacja, MPK, CalCo, Wartosc, Typ) VALUES (" + (j - 3).ToString + ", '" _
    '                                                            + dv(i)("Instalacja") + "', '" + dv(i)("ID") + "', '" + aDane(w, 2) + "', " + aDane(w, j).ToString.Replace(",", ".") + ", '" + dv(i)("Typ") + "')"
    '                                                    Try
    '                                                        .ExecuteNonQuery()
    '                                                    Catch ex As Exception
    '                                                        .CommandText = "SELECT TOP 1 Wartosc FROM tblZWPC WHERE Mc = " + (j - 3).ToString + " AND Instalacja = '" + dv(i)("Instalacja") + "' AND MPK = '" + dv(i)("ID") + "' AND CalCo = '" + aDane(w, 2) + "'"
    '                                                        _Wart = .ExecuteScalar
    '                                                        _Wart += aDane(w, j)
    '                                                        .CommandText = "UPDATE tblZWPC SET Wartosc = " + _Wart.ToString.Replace(",", ".") + " WHERE Mc = " + (j - 3).ToString + " AND Instalacja = '" + dv(i)("Instalacja") + "' AND MPK = '" + dv(i)("ID") + "' AND CalCo = '" + aDane(w, 2) + "'"
    '                                                        .ExecuteNonQuery()
    '                                                    End Try
    '                                                End With
    '                                            End If
    'NextMonth:
    '                                        Next j
    'NextRow:
    '                                    Next w
    '                                Catch ex As Exception
    '                                End Try
    '                                GoTo NextSheet
    'Rok2006:
    '                                If Not oSh.Visible Then GoTo NextSheet
    '                                If oSh.Name.ToUpper.Contains("TOTAL") Then GoTo NextSheet
    '                                If oSh.Range("A2").Value Is Nothing OrElse oSh.Range("A4").Value Is Nothing Then GoTo NextSheet
    '                                If Not oSh.Range("A4").Value.ToString.ToUpper.Equals("MPK") Then GoTo NextSheet
    '                                Dim rng As XL.Range = oSh.Range("A4").CurrentRegion.Offset(1)
    '                                ReDim aDane(rng.Rows.Count, 9)
    '                                aDane = rng.Value
    '                                _MPK = oSh.Range("A2").Value
    '                                aMpk = Split(_MPK, "/")
    '                                _MPK = aMpk(0).ToString.Trim
    '                                If CInt(_MPK) < 1 OrElse CInt(_MPK) > 12 Then Throw New ArgumentException("Skoroszyt [" + _File + "] arkusz [" + oSh.Name + "]: błąd określenia miesiąca")
    '                                For w = 2 To aDane.GetLength(0)
    '                                    If aDane(w, 1) Is Nothing Then GoTo NextRow2
    '                                    If aDane(w, 7) Is Nothing AndAlso aDane(w, 8) Is Nothing Then GoTo NextRow2
    '                                    If aDane(w, 7).Equals(0) AndAlso aDane(w, 8).Equals(0) Then GoTo NextRow2
    '                                    i = dv.Find(aDane(w, 1))
    '                                    If i.Equals(-1) Then Throw New ArgumentException("Dopisz MPK [" + aDane(w, 1).ToString + "]")
    '                                    With cmd
    '                                        .CommandText = "INSERT INTO tblZWPC (Mc, Instalacja, MPK, CalCo, Material, Ilosc, Wartosc, Typ) VALUES (" + _MPK + ", '" _
    '                                                + aDane(w, 9) + "', '" + aDane(w, 1) + "', '" + aDane(w, 5) + "', '" + aDane(w, 3) + "', " + aDane(w, 7).ToString.Replace(",", ".") + ", " + aDane(w, 8).ToString.Replace(",", ".") + ", '" + dv(i)("Typ") + "')"
    '                                        Try
    '                                            .ExecuteNonQuery()
    '                                        Catch ex As Exception
    '                                            Throw New ArgumentException("Skoroszyt [" + _File + "] arkusz [" + oSh.Name + "]: powtórzony zapis [" + aDane(w, 1) + " " + aDane(w, 5) + " " + aDane(w, 3) + "]")
    '                                        End Try
    '                                    End With
    'NextRow2:
    '                                Next w
    'NextSheet:
    '                            Next oSh
    '                            With oBook
    '                                .Saved = True
    '                                .Close()
    '                            End With
    '                        Next _File
    '                        tr.Commit()
    '                    Catch ex As Exception
    '                        tr.Rollback()
    '                        Throw New ArgumentException(ex.Message + vbCrLf + ex.StackTrace)
    '                    End Try
    '                End Using
    '            End With
    '            Using New CM
    '                MessageBox.Show("Pobieranie zakończone pomyślnie", Tytul, MessageBoxButtons.OK, MessageBoxIcon.Information)
    '            End Using
    '        Catch ex As Exception
    '            Using New CM
    '                MessageBox.Show(ex.Message, Tytul, MessageBoxButtons.OK, MessageBoxIcon.Error)
    '            End Using
    '        Finally
    '            tr = Nothing
    '            cmd = Nothing
    '        End Try
    '    End Sub

    Private Sub tsm_PobierzTransport20062012_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Dim tr As OleDbTransaction = Nothing
        Dim cmd As New OleDbCommand
        Dim da As OleDbDataAdapter
        Dim dt As New DataTable, dtK As New DataTable, dtS As New DataTable, dtW As New DataTable
        Dim dv, dvK, dvW As DataView
        Dim i, j, k, m, r As Integer
        Dim _Waga As Double
        Dim _Wydz As String
        Dim _Path As String = "Z:\CHO_Transport\"
        Const CO_ACTION As String = "Pobieranie danych transportu i sprzedaży 2006 - 2012"
        Const CO_CONV_TO_KRAJ As String = "1905446|1902673"
        Try
            Using New CM
                If MessageBox.Show("Are you sure?", CO_ACTION, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2).Equals(Windows.Forms.DialogResult.No) Then Return
            End Using
            FillStatusBar(7, CO_ACTION)
            For i = 2006 To 2012
                Dim _Txt As String = ""
                Dim oFile As Object = My.Computer.FileSystem.GetFiles(_Path, FileIO.SearchOption.SearchAllSubDirectories, "Dane_" + i.ToString + "*.xls")
                Dim m_Ado As New ClADO.ADOGet
                With m_Ado
                    .Baza = My.Settings.Baza.Replace(Me.dtpOkres.Value.Year.ToString, i.ToString)
                    .Folder = My.Settings.Sciezka
                    .Haslo = My.Settings.Myk
                    If Not .OpenData(True) Then _
                        Throw New ArgumentException("Brak połączenia z plikiem bazy danych [" + .Baza + "]")
                End With
                da = New OleDbDataAdapter("SELECT * FROM tblKlienci", m_Ado.CN)
                dtK = New DataTable
                da.Fill(dtK)
                dvK = dtK.DefaultView
                dvK.Sort = "ID"
                da = New OleDbDataAdapter("SELECT ID, Wydzial FROM tblGrupyMat WHERE NOT (Wydzial IS NULL OR wydzial = '') ORDER BY ID", m_Ado.CN)
                dtW = New DataTable
                da.Fill(dtW)
                dvW = dtW.DefaultView
                dvW.Sort = "ID"
                da = New OleDbDataAdapter("SELECT TOP 1 * FROM tblSprzedaz", m_Ado.CN)
                da.Fill(dtS)
                Try
                    With dtS.Columns
                        .Add("GrMatOpis")
                        .Add("GrDekOpis")
                        .Add("MaterOpis")
                        .Add("KK")
                        .Add("Kraj")
                        .Add("Klient")
                    End With
                Catch ex As Exception
                End Try
                da = New OleDbDataAdapter("SELECT TOP 1 * FROM tblTransport", m_Ado.CN)
                da.Fill(dt)
                Try
                    dt.Columns.Add("Dostawca")
                Catch ex As Exception
                End Try
                For Each o In oFile
                    Dim aDane As Object = Nothing
                    Dim aDanS As Object = Nothing
                    With New Cl4Excel.GetFromXls(o, "Table", "F15:AI")
                        .Data2Array() : aDane = .Dane : .Zwolnij()
                    End With
                    If aDane Is Nothing Then GoTo NastepnyPlik
                    Dim ZaOkres As String = DateSerial(i, CInt(aDane(2, 1).ToString.Substring(0, 2)), 1).ToShortDateString.Substring(0, 7)
                    m = CInt(ZaOkres.Substring(5, 2))
                    Try
                        With New Cl4Excel.GetFromXls("Z:\CHO_Transport\Sales_" + ZaOkres + ".XLS", "Table", "F15:AJ")
                            .Data2Array() : aDanS = .Dane : .Zwolnij()
                        End With
                    Catch ex As Exception
                    End Try
                    With dt
                        .AcceptChanges()
                        .Clear()
                    End With
                    dv = dt.DefaultView
                    dv.Sort = "GrMater, IDKlienta, IDDostawcy"
                    dtS.Clear()
                    ' przeniesienie transportu do tabeli
                    j = aDane.GetUpperBound(0)
                    r = 2
                    Do Until r > j
                        _Wydz = "OR"
                        If Not aDane(r, 12).ToString.Contains("JSWZ") Then
                            k = dvW.Find(aDane(r, 6).ToString)
                            If Not k.Equals(-1) Then _Wydz = dvW(k)(1)
                        End If
                        If String.IsNullOrEmpty(aDane(r, 10).ToString) OrElse aDane(r, 10).ToString.Equals("#") Then GoTo Kolejny
                        If String.IsNullOrEmpty(aDane(r, 20).ToString) OrElse aDane(r, 20).ToString.Equals("#") Then GoTo Kolejny
                        If Not aDane(r, 22).ToString.Contains("LF") Then GoTo Kolejny
                        If String.IsNullOrEmpty(aDane(r, 18).ToString) OrElse aDane(r, 18).ToString.Equals("#") Then aDane(r, 18) = 901
                        If String.IsNullOrEmpty(aDane(r, 4).ToString) OrElse aDane(r, 4).ToString.Equals("#") Then
                            k = dvK.Find(aDane(r, 18))
                            If k.Equals(-1) Then
                                aDane(r, 4) = "01"
                            Else
                                aDane(r, 4) = "0" + dvK(k)("GrDekretacji").ToString
                            End If
                        End If
                        aDane(r, 6) = aDane(r, 6).ToString.Replace("#", "")
                        k = dv.Find(New Object() {aDane(r, 6), aDane(r, 18), aDane(r, 20)})
                        If k.Equals(-1) Then
                            dt.Rows.Add(New Object() {m, aDane(r, 6), aDane(r, 18), aDane(r, 20), _Wydz, aDane(r, 4), aDane(r, 24), aDane(r, 28), _
                                                      aDane(r, 29), aDane(r, 21)})
                        Else
                            dv(k)("Waga") = dv(k)("Waga") + aDane(r, 28)
                            dv(k)("Kwota") = dv(k)("Kwota") + aDane(r, 29)
                        End If
Kolejny:
                        r += 1
                    Loop
                    dt.AcceptChanges()
                    ' przeniesienie sprzedazy do tabeli
                    If aDanS Is Nothing Then GoTo Zapis
                    j = aDanS.GetUpperBound(0)
                    r = 2
                    Do Until r > j
                        _Wydz = "OR"
                        If String.IsNullOrEmpty(aDanS(r, 1).ToString) Then Exit Do
                        If Not aDanS(r, 7).ToString.Contains("JSWZ") Then
                            k = dvW.Find(aDanS(r, 2).ToString)
                            If Not k.Equals(-1) Then _Wydz = dvW(k)(1)
                        End If
                        If Not (String.IsNullOrEmpty(aDanS(r, 9).ToString) OrElse String.IsNullOrEmpty(aDanS(r, 13).ToString)) Then
                            If CO_CONV_TO_KRAJ.Contains(aDanS(r, 13).ToString) Then _
                                aDanS(r, 4) = "01"
                            If Not "WS_R1_R2".Contains(_Wydz) Then
                                _Waga = aDanS(r, 19)
                            Else
                                _Waga = aDanS(r, 20)
                            End If
                            If _Waga < 1 Then
                                _Waga = 0
                            Else
                                _Waga = _Waga / 1000
                            End If
                            dtS.Rows.Add(New Object() {m, aDanS(r, 9), aDanS(r, 13), aDanS(r, 2).ToString.Replace("#", ""), aDanS(r, 6).ToString.Replace("#", ""), _Wydz, _
                                                     aDanS(r, 4), _Waga, aDanS(r, 23), aDanS(r, 24), aDanS(r, 27), 0, 0, 0, 0, aDanS(r, 27), 0, _
                                                     aDanS(r, 3), aDanS(r, 5), aDanS(r, 10), aDanS(r, 11), aDanS(r, 12), aDanS(r, 14)})
                        End If
                        r += 1
                    Loop
Zapis:
                    Using cn As New OleDbConnection(m_Ado.CN.ConnectionString)
                        Try
                            cn.Open()
                            tr = cn.BeginTransaction
                            With cmd
                                .Connection = cn
                                .Transaction = tr
                                If dtS.Rows.Count.Equals(0) Then GoTo Akceptacja
                                .CommandText = "DELETE FROM tblSprzedaz WHERE Miesiac = " + m.ToString
                                .ExecuteNonQuery()
                                Dim querS = (From p In dtS.AsEnumerable _
                                             Group By Mater = p("Material"), IDK = p("IDKlienta"), GM = p("GrMater"), GK = p("GrKalk"), _
                                             Wydz = p("Wydzial"), GrD = p("GrDekretacji") _
                                             Into Waga = Sum(p.Field(Of Double)("Waga")), Kwota = Sum(p.Field(Of Double)("Kwota")), Rabat = Sum(p.Field(Of Double)("Rabat")), _
                                             PIP = Sum(p.Field(Of Double)("PIP")), PIPS = Sum(p.Field(Of Double)("PIP_SAP"))).ToArray
                                For Each q In querS
                                    .CommandText = "INSERT INTO tblSprzedaz (Miesiac, Material, IDKlienta, GrMater, GrKalk, Wydzial, GrDekretacji, Waga, Kwota, Rabat, PIP, PIP_SAP) " _
                                            + "VALUES (" + m.ToString + ", '" + q.Mater + "', " + q.IDK.ToString + ", '" + q.GM + "', '" + q.GK + "', '" + q.Wydz + "', '" _
                                            + q.GrD + "', " + q.Waga.ToString.Replace(",", ".") + ", " + q.Kwota.ToString.Replace(",", ".") + ", " + q.Rabat.ToString.Replace(",", ".") _
                                            + ", " + q.PIP.ToString.Replace(",", ".") + ", " + q.PIPS.ToString.Replace(",", ".") + ")"
                                    Try
                                        .ExecuteNonQuery()
                                    Catch ex As Exception
                                        Throw New ArgumentException(ex.Message)
                                    End Try
                                Next q
                                ' uzupełnienie słowników
                                Dim query = (From p In dtS.AsEnumerable _
                                             Where Not String.IsNullOrEmpty(p("GrMater")) _
                                             Select ID = p("GrMater"), Wydz = p("Wydzial"), GrMaterPl = p("GrMatOpis")).Distinct().ToArray()
                                For Each q In query
                                    .CommandText = "INSERT INTO tblGrupyMat (ID, Wydzial, GrMaterPL) VALUES ('" + q.ID + "', '" _
                                                        + q.Wydz + "', '" + q.GrMaterPl + "')"
                                    Try
                                        .ExecuteNonQuery()
                                    Catch ex As Exception
                                    End Try
                                Next q
                                Dim quer2 = (From p In dtS.AsEnumerable _
                                             Where Not String.IsNullOrEmpty(p("GrKalk")) _
                                             Select ID = p("GrKalk"), Wydz = p("Wydzial"), GM = p("GrMater")).Distinct().ToArray()
                                For Each q In quer2
                                    .CommandText = "INSERT INTO tblGrupyKalk (ID, Wydzial, GrMater) VALUES ('" + q.ID + "', '" + q.Wydz + "', '" + q.GM.ToString.Replace("#", "") + "')"
                                    Try
                                        .ExecuteNonQuery()
                                    Catch ex As Exception
                                    End Try
                                Next q
                                Dim quer3 = (From p In dtS.AsEnumerable _
                                             Select ID = p("Material"), Opis = p("MaterOpis"), GM = p("GrMater"), GK = p("GrKalk")).Distinct().ToArray()
                                For Each q In quer3
                                    .CommandText = "INSERT INTO tblMaterialy (ID, MaterialPL, GrMaterID, GrKalkID) VALUES ('" + q.ID + ", '" _
                                                    + q.Opis + "', '" + q.GM.ToString.Replace("#", "") + "', '" + q.GK.ToString.Replace("#", "") + "')"
                                    Try
                                        .ExecuteNonQuery()
                                    Catch ex As Exception
                                    End Try
                                Next q
                                Dim quer4 = (From p In dtS.AsEnumerable _
                                             Select ID = p("IDKlienta"), Opis = p("Klient"), GD = p("GrDekretacji"), Kraj = p("Kraj"), KK = p("KK")).Distinct().ToArray()
                                For Each q In quer4
                                    .CommandText = "INSERT INTO tblKlienci (ID, Nazwa, GrDekretacji, Kraj, KK) VALUES (" + q.ID.ToString + ", '" _
                                                    + q.Opis.Replace("'", "") + "', '" + q.GD + "', '" + q.Kraj + "', '" + q.KK + "')"
                                    Try
                                        .ExecuteNonQuery()
                                    Catch ex As Exception
                                    End Try
                                Next q
                                GoTo WeryfikacjaWagi
Akceptacja:
                                dtS.Clear()
                                .CommandText = "SELECT * FROM tblSprzedaz WHERE Miesiac = " + m.ToString
                                dtS.Load(.ExecuteReader)
WeryfikacjaWagi:
                                .CommandText = "DELETE FROM tblTransport WHERE Miesiac = " + m.ToString
                                .ExecuteNonQuery()
                                Dim querT = (From p In dt.AsEnumerable _
                                             Group By GM = p("GrMater"), IDK = p("IDKlienta"), IDD = p("IDDostawcy"), _
                                             Wydz = p("Wydzial"), GrD = p("GrDekretacji"), WW = p("WarunekWysylki") _
                                             Into Waga = Sum(p.Field(Of Double)("Waga")), Kwota = Sum(p.Field(Of Double)("Kwota")))
                                For Each q In querT
                                    If Not q.Kwota.Equals(0.0) Then
                                        .CommandText = "INSERT INTO tblTransport (Miesiac, GrMater, IDKlienta, IDDostawcy, Wydzial, GrDekretacji, WarunekWysylki, Waga, Kwota) " _
                                                            + "VALUES (" + m.ToString + ", '" + q.GM + "', " + q.IDK.ToString + ", " + q.IDD.ToString + ", '" + q.Wydz + "', '" _
                                                            + q.GrD + "', '" + q.WW + "', " + q.Waga.ToString.Replace(",", ".") + ", " + q.Kwota.ToString.Replace(",", ".") + ")"
                                        .ExecuteNonQuery()
                                    End If
                                Next q
                                .CommandText = "SELECT * FROM tblTransport WHERE Miesiac = " + m.ToString
                                dt.Clear()
                                dt.Load(.ExecuteReader)
                            End With
                            dv = dt.DefaultView
                            dv.Sort = "GrMater, IDKlienta"
                            Dim quer5 = (From p In dtS.AsEnumerable _
                                         Group By GM = p("GrMater"), IDK = p("IDKlienta") _
                                         Into Waga = Sum(p.Field(Of Double)("Waga"))).ToArray
                            For Each q In quer5
                                dv.RowFilter = "GrMater = '" + q.GM + "' AND IDKlienta = " + q.IDK.ToString
                                Dim Mg As Double = 0
                                Dim MgS As Double = q.Waga
                                For Each v In dv
                                    Mg += v("Waga")
                                Next v
                                For Each v In dv
                                    Try
                                        v("Waga") = v("Waga") * MgS / Mg
                                    Catch ex As Exception
                                        Stop
                                    End Try
                                Next v
                                dv.RowFilter = ""
                            Next q
                            For Each v In dv
                                With cmd
                                    .CommandText = "UPDATE tblTransport SET Waga = " + v("Waga").ToString.Replace(",", ".") _
                                                + " WHERE Miesiac = " + m.ToString + " AND GrMater = '" + v("GrMater") _
                                                + "' AND IDKlienta = " + v("IDKlienta").ToString + " AND IDDostawcy = " + v("IDDostawcy").ToString
                                    Try
                                        .ExecuteNonQuery()
                                    Catch ex As Exception
                                    End Try
                                End With
                            Next v
                            tr.Commit()
                        Catch ex As Exception
                            tr.Rollback()
                            Throw New ArgumentException(ex.Message + vbCrLf + ex.StackTrace)
                        End Try
                    End Using
NastepnyPlik:
                Next o
                Me.tspbMain.Value = i - 2005
                Application.DoEvents()
            Next i
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message, CO_ACTION, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End Using
        Finally
            tr = Nothing
            cmd = Nothing
            da = Nothing
            dt = Nothing
            dtK = Nothing
            dtS = Nothing
            dtW = Nothing
            dv = Nothing
            dvK = Nothing
            dvW = Nothing
            ClearStatus()
        End Try
    End Sub

    '    Private Sub KlientOfTransport(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsm_mKT.Click

    '        Dim tr As OleDbTransaction = Nothing
    '        Dim cmd As New OleDbCommand
    '        Dim da As OleDbDataAdapter
    '        Dim dt As New DataTable, dtK As New DataTable
    '        Dim dvK As DataView
    '        Dim j As Integer
    '        Try
    '            da = New OleDbDataAdapter("SELECT * FROM tblTransport", Me.cns)
    '            da.Fill(dt)
    '            da = New OleDbDataAdapter("SELECT * FROM tblKlienci", Me.cns)
    '            dtK = New DataTable
    '            da.Fill(dtK)
    '            dvK = dtK.DefaultView
    '            dvK.Sort = "ID"
    '            Using cn As New OleDbConnection(Me.cns.ConnectionString)
    '                Try
    '                    cn.Open()
    '                    tr = cn.BeginTransaction
    '                    With cmd
    '                        .Connection = cn
    '                        .Transaction = tr
    '                        For Each p In dt.Rows
    '                            j = dvK.Find(p("IDKlienta"))
    '                            If j.Equals(-1) Then
    '                                Try
    '                                    .CommandText = "INSERT INTO tblKlienci (ID) " _
    '                                             + "VALUES (" + p("IDKlienta").ToString + ")"
    '                                    .ExecuteNonQuery()
    '                                Catch ex As Exception
    '                                    'Throw New ArgumentException(ex.Message)
    '                                End Try
    '                            End If
    'Nastepny:
    '                        Next p
    '                    End With
    '                    tr.Commit()
    '                Catch ex As Exception
    '                    tr.Rollback()
    '                    Throw New ArgumentException(ex.Message + vbCrLf + ex.StackTrace)
    '                End Try
    '            End Using
    '        Catch ex As Exception
    '            Using New CM
    '                MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '            End Using
    '        Finally
    '            tr = Nothing
    '            cmd = Nothing
    '            ClearStatus()
    '            da = Nothing
    '            dt = Nothing
    '        End Try
    '    End Sub

    Private Sub XlsTotblTransport_Click() Handles tsmXlsToTblTransport.Click

        Dim dt As New DataTable
        Dim tr As OleDbTransaction = Nothing
        Dim cmd As New OleDbCommand
        Dim _Kwota As Double
        Using cn As New OleDbConnection(Me.cns.ConnectionString)
            Try
                cn.Open()
                tr = cn.BeginTransaction
                With cmd
                    .Connection = cn
                    .Transaction = tr
                    .CommandText = "SELECT TOP 1 * FROM tblTransport"
                    dt.Load(.ExecuteReader)
                    dt.Clear()
                    With New Cl4Excel.GetFromXls("Z:\CHO_Transport\Roznice_" + Me.dtpOkres.Value.Year.ToString + ".xls", "Arkusz1")
                        .TableOfData = dt : .Data2Table() : .Zwolnij()
                    End With
                    For Each r In dt.Rows
                        With cmd
                            .CommandText = "SELECT TOP 1 Kwota FROM tblTransport WHERE Miesiac = " + r("Miesiac").ToString + " AND GrMater = '" + r("GrMater") + "' AND IDKlienta = " + r("IDKlienta").ToString + " AND IDDostawcy = " + r("IDDostawcy").ToString
                            Try
                                _Kwota = .ExecuteScalar
                                .CommandText = "UPDATE tblTransport SET Kwota = " + (_Kwota + r("Kwota")).ToString.Replace(",", ".") + " WHERE Miesiac = " + r("Miesiac").ToString + " AND GrMater = '" + r("GrMater") + "' AND IDKlienta = " + r("IDKlienta").ToString + " AND IDDostawcy = " + r("IDDostawcy").ToString
                                .ExecuteNonQuery()
                            Catch ex As Exception
                                .CommandText = "INSERT INTO tblTransport (Miesiac, GrMater, IDKlienta, IDDostawcy, Wydzial, GrDekretacji, WarunekWysylki, Waga, Kwota) " _
                                                    + "VALUES (" + r("Miesiac").ToString + ", '" + r("GrMater") + "', " + r("IDKlienta").ToString + ", " + r("IDDostawcy").ToString + ", '" + r("Wydzial") + "', '0" _
                                                    + CInt(r("GrDekretacji").ToString).ToString + "', '" + r("WarunekWysylki") + "', 0, " + r("Kwota").ToString.Replace(",", ".") + ")"
                                .ExecuteNonQuery()
                            End Try
                        End With
                    Next r
                End With
                tr.Commit()
            Catch ex As Exception
                tr.Rollback()
                MessageBox.Show(ex.Message + vbCrLf + ex.StackTrace, Me.tsmXlsToTblTransport.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
        End Using
        tr = Nothing
        cmd = Nothing
        dt = Nothing
    End Sub

    'Private Sub ZmianaStruktury() Handles tsmZmianaStruktury.Click
    '    '      Dim dtS As New DataTable
    '    '      Dim dtM As New DataTable
    '    '      Dim bs As New BindingSource
    '    '      Dim da As OleDbDataAdapter
    '    '      Dim dr As DataRow
    '    'Dim i As Integer

    '    'da = DA_ZWPC()
    '    'dtS = New DataTable
    '    'da.Fill(dtS)

    '    'FillStatusBar(dtS.Rows.Count, "Zmiana struktury ZWPC_" + Me.txtRok.Text)

    '    'da = DA_NZWPC()
    '    'dtM = New DataTable
    '    'da.Fill(dtM)

    '    'For i = 0 To dtS.Rows.Count - 1
    '    '	dr = dtM.NewRow
    '    '	With dtS.Rows(i)
    '    '		dr.Item("Mc") = CInt(.Item("Mc").ToString)
    '    '		dr.Item("Instalacja") = .Item("Instalacja").ToString
    '    '		dr.Item("MPK") = .Item("MPK").ToString
    '    '		dr.Item("CalCo") = .Item("CalCo").ToString
    '    '		dr.Item("RK") = .Item("RK").ToString
    '    '		dr.Item("Material") = .Item("Material").ToString
    '    '		dr.Item("GrKoszt") = .Item("GrKoszt").ToString
    '    '		dr.Item("JM") = .Item("JM").ToString
    '    '		dr.Item("Ilosc") = CDbl(.Item("Ilosc").ToString)
    '    '		dr.Item("Wartosc") = CDbl(.Item("Wartosc").ToString)
    '    '		dr.Item("Typ") = .Item("Typ").ToString
    '    '		Try
    '    '			dr.Item("Dostawca") = CInt(.Item("Dostawca").ToString)
    '    '		Catch ex As Exception
    '    '		End Try
    '    '	End With

    '    '	dtM.Rows.Add(dr)

    '    'Next i

    '    'da.Update(dtM)

    '    'FillStatusBar(0, "Gotowe")

    'End Sub

    Private Sub WstawKonta() Handles tsmWstawKonta.Click

        Dim dt As New DataTable
        Dim tr As OleDbTransaction = Nothing
        Dim cmd As New OleDbCommand
        Dim aArk(), _Ark, _CalCo As String
        Dim i, j, k As Integer
        Dim rng As XL.Range
        Try
            Using cn As New OleDbConnection(Me.cns.ConnectionString)
                Try
                    cn.Open()
                    tr = cn.BeginTransaction
                    With cmd
                        .Connection = cn
                        .Transaction = tr
                        .CommandText = "SELECT DISTINCT CalCo, RK, C.Ark, R.Opis FROM (tblZWPC Z INNER JOIN tblCalCo C ON Z.CalCo = C.ID) INNER JOIN tblRK R ON Z.RK = R.ID ORDER BY C.Ark, CalCo, RK"
                        dt.Load(.ExecuteReader)
                    End With
                Catch ex As Exception
                    Throw New ArgumentException(ex.Message)
                End Try
            End Using
            With New Cl4Excel.Excel("W:\Costs\Celsa\Opisy\BazaOpisCalCo.xls")
                Do Until i.Equals(dt.Rows.Count)
                    _Ark = dt.Rows(i)("Ark")
                    aArk = Split(_Ark, ",")
                    Do While dt.Rows(i)("Ark").Equals(_Ark)
                        _CalCo = dt.Rows(i)("CalCo")
                        k = 12
                        .oSheet = CType(.oBook.Worksheets(aArk(0)), XL.Worksheet)
                        rng = .oSheet.Columns(2).find(_CalCo)
                        j = rng.Row
                        Do While dt.Rows(i)("CalCo").Equals(_CalCo)
                            For Each a In aArk
                                With CType(.oBook.Worksheets(a.Trim), XL.Worksheet)
                                    .Cells(j, k) = dt.Rows(i)("RK")
                                    .Cells(j, k + 1) = dt.Rows(i)("Opis")
                                End With
                            Next a
                            k += 2
                            i += 1
                            If i.Equals(dt.Rows.Count) Then Exit Do
                        Loop
                        If i.Equals(dt.Rows.Count) Then Exit Do
                    Loop
                Loop
                HideScreen()
                .Show()
            End With
            HideScreen(False)
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message + vbCrLf + ex.StackTrace, Me.tsmWstawKonta.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
        Finally
            tr = Nothing
            cmd = Nothing
            dt = Nothing
        End Try
    End Sub

#End Region ' Ad Hoc

    Private Sub tsm_Test_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsm_Test.Click

        Dim o As Object
        With New Cl4Excel.GetFromXls("W:\Costs\Celsa\Wykon2015\Plan_2015_oper\War_2015op.xlsx", "m01", "A4:N135")
            .Data2Array()
            o = .Dane
            .Zwolnij()
        End With
    End Sub
End Class
