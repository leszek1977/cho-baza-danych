Attribute VB_Name = "aaaDane"
Option Explicit

' Created  2012-08-09 by KBrzozowski
' Modified 2012-09-03

Public Sub WstawDane(ByVal Miesiac As Integer, ByVal aKoszty As Variant, ByVal aMedia As Variant)
    Dim k As Integer
    Dim i As Integer
    Dim m_Ark As String
    Dim m_Id As String
    Dim fs As Range
    Dim rng As Range
    Dim ws As Worksheet
    Dim intCalculation As Integer
    
' aKoszty
' 0 - T.Skoroszyt
' 1 - M.Skoroszyt
' 2 - Ark
' 3 - CalCo
' 4 - JM
' 5 - Typ
' 6 - Ilosc
' 7 - Wartosc

' aMedia:
' 0 - T.Skoroszyt
' 1 - M.Skoroszyt
' 2 - Ark
' 3 - Media
' 4 - RowZuzycie
' 5 - RowMedia
' 6 - Podzielnik
' 7 - Ilosc
' 8 - Opis

    With Application
        intCalculation = .Calculation
        .DisplayAlerts = False
        .Calculation = xlManual
        .ScreenUpdating = False
    End With
    
    If aKoszty(0, 9) = "" Then GoTo NieProdukcyjne
    
    m_Ark = ""
    
    On Error Resume Next
    
    If aKoszty(0, 8) = "" Then
        Sheets("Koszt").Outline.ShowLevels RowLevels:=2
        Sheets("Zu�ycie").Outline.ShowLevels RowLevels:=2
        Sheets("Us�ugi").Outline.ShowLevels RowLevels:=2
        Sheets("Media").Outline.ShowLevels RowLevels:=2
    Else
        Do
            If InStr(1, m_Ark, aKoszty(i, 8)) = 0 Then
                m_Id = "_" & aKoszty(i, 8)
                m_Ark = m_Ark & m_Id
                Sheets("Koszt" & m_Id).Outline.ShowLevels RowLevels:=2
                Sheets("Zu�ycie" & m_Id).Outline.ShowLevels RowLevels:=2
                Sheets("Us�ugi" & m_Id).Outline.ShowLevels RowLevels:=2
                Sheets("Media" & m_Id).Outline.ShowLevels RowLevels:=2
            End If
            i = i + 1
        Loop Until i > UBound(aKoszty)
    End If
    
    If aKoszty(0, 0) = "JMS03" Then GoTo PoCzyszczeniu
    
    ' czyszczenie poprzednich zapis�w
    
    m_Ark = ""
    
    If aKoszty(0, 8) = "" Then
        With Sheets("Koszt")
            Range(.Cells(5, 3 + Miesiac), .Cells(315, 3 + Miesiac)).SpecialCells(xlCellTypeConstants).ClearContents
        End With
        With Sheets("Zu�ycie")
            Range(.Cells(5, 3 + Miesiac), .Cells(315, 3 + Miesiac)).SpecialCells(xlCellTypeConstants).ClearContents
        End With
        With Sheets("Us�ugi")
            Range(.Cells(5, 3 + Miesiac), .Cells(315, 3 + Miesiac)).SpecialCells(xlCellTypeConstants).ClearContents
        End With
        With Sheets("Media")
            .Range("D4:O69").SpecialCells(xlCellTypeConstants).ClearContents
        End With
    Else
        Do
            If InStr(1, m_Ark, aKoszty(i, 8)) = 0 Then
                m_Id = "_" & aKoszty(i, 8)
                m_Ark = m_Ark & m_Id
                With Sheets("Koszt" & m_Id)
                    Range(.Cells(5, 3 + Miesiac), .Cells(315, 3 + Miesiac)).SpecialCells(xlCellTypeConstants).ClearContents
                End With
                With Sheets("Zu�ycie" & m_Id)
                    Range(.Cells(5, 3 + Miesiac), .Cells(315, 3 + Miesiac)).SpecialCells(xlCellTypeConstants).ClearContents
                End With
                With Sheets("Us�ugi" & m_Id)
                    Range(.Cells(5, 3 + Miesiac), .Cells(315, 3 + Miesiac)).SpecialCells(xlCellTypeConstants).ClearContents
                End With
                With Sheets("Media" & m_Id)
                    .Range("D4:O69").SpecialCells(xlCellTypeConstants).ClearContents
                End With
            End If
            i = i + 1
        Loop Until i > UBound(aKoszty)
    End If
    
    On Error GoTo 0
    
PoCzyszczeniu:

    i = 0
    Do
      
        If aKoszty(i, 8) = "" Then
            m_Id = ""
        Else
            m_Id = "_" & aKoszty(i, 8)
        End If
        
        If aKoszty(i, 5) = "P" Then
            m_Ark = "Koszt" & m_Id
        Else
            m_Ark = "Us�ugi" & m_Id
        End If
        
        With Sheets(m_Ark)
        
            Set fs = .Columns(2).Find(aKoszty(i, 3), LookAt:=xlWhole, LookIn:=xlValues)
        
            If fs Is Nothing Then
                MsgBox "Zeszyt " & ThisWorkbook.Name & ": w arkuszu " & m_Ark & " nie znalaz�em CalCo " & aKoszty(i, 3)
                Exit Sub
            End If
            
            On Error Resume Next
            .Cells(fs.Row, 3 + Miesiac) = .Cells(fs.Row, 3 + Miesiac) + CDbl(aKoszty(i, 7))
            If Err <> 0 Then
                .Cells(fs.Row, 3 + Miesiac) = 0
            End If
            On Error GoTo 0
            
            If aKoszty(i, 5) = "P" Then
            
                With Sheets("Zu�ycie" & m_Id)
                    On Error Resume Next
                    .Cells(fs.Row, 3 + Miesiac) = .Cells(fs.Row, 3 + Miesiac) + CDbl(aKoszty(i, 6))
                    If Err <> 0 Then
                        .Cells(fs.Row, 3 + Miesiac) = 0
                    End If
                    On Error GoTo 0
                End With
            
            End If
        End With
      
        i = i + 1
    
    Loop Until i > UBound(aKoszty)
      
    i = 0
    
    Do
      
        If aMedia(i, 9) = "" Then
            m_Id = ""
        Else
            m_Id = "_" & aMedia(i, 9)
        End If
        
        With Sheets("Media" & m_Id)
        
            On Error Resume Next
            If CInt(aMedia(i, 4)) <> 0 Then
                If Err = 0 Then
                    .Cells(CInt(aMedia(i, 4)), 3 + Miesiac) = .Cells(CInt(aMedia(i, 4)), 3 + Miesiac) + CDbl(aMedia(i, 7)) / CInt(aMedia(i, 6))
                End If
            End If
            On Error GoTo 0
        
        End With
        
        With Sheets("Zu�ycie" & m_Id)
        
            On Error Resume Next
            If CInt(aMedia(i, 3)) <> 0 Then
                If Err <> 0 Then
                    .Cells(CInt(aMedia(i, 3)), 3 + Miesiac) = .Cells(CInt(aMedia(i, 3)), 3 + Miesiac) + CDbl(aMedia(i, 7)) / CInt(aMedia(i, 6))
                End If
            End If
            On Error GoTo 0
        
        End With
        
        i = i + 1
        
    Loop Until i > UBound(aMedia)
    
    m_Ark = ""
    
    If aKoszty(0, 8) = "" Then
        Sheets("Koszt").Outline.ShowLevels RowLevels:=1
        Sheets("Zu�ycie").Outline.ShowLevels RowLevels:=1
        Sheets("Us�ugi").Outline.ShowLevels RowLevels:=1
        Sheets("Media").Outline.ShowLevels RowLevels:=1
    Else
        Do
            If InStr(1, m_Ark, aKoszty(i, 8)) = 0 Then
                m_Id = "_" & aKoszty(i, 8)
                m_Ark = m_Ark & m_Id
                Sheets("Koszt" & m_Id).Outline.ShowLevels RowLevels:=1
                Sheets("Zu�ycie" & m_Id).Outline.ShowLevels RowLevels:=1
                Sheets("Us�ugi" & m_Id).Outline.ShowLevels RowLevels:=1
                Sheets("Media" & m_Id).Outline.ShowLevels RowLevels:=1
            End If
            i = i + 1
        Loop Until i > UBound(aKoszty)
    End If
    
    GoTo Finish
    
NieProdukcyjne:

    ' czyszczenie
    On Error Resume Next
    For Each ws In Worksheets
        If InStr(1, ws.Range("A3"), "INEF") > 0 Then
            ws.Outline.ShowLevels RowLevels:=2
            Range(ws.Cells(5, 3 + Miesiac), ws.Cells(315, 3 + Miesiac)).SpecialCells(xlCellTypeConstants).ClearContents
            Range(ws.Cells(373, 3 + Miesiac), ws.Cells(417, 3 + Miesiac)).SpecialCells(xlCellTypeConstants).ClearContents
        End If
    Next ws
    On Error GoTo 0
    
    Do
        m_Ark = aKoszty(i, 2)
        
        Do While m_Ark = aKoszty(i, 2)
        
            With Sheets(m_Ark)
              
                Set fs = .Columns(2).Find(aKoszty(i, 3), LookAt:=xlWhole, LookIn:=xlValues)
                
                If fs Is Nothing Then
                    MsgBox "Zeszyt " & ThisWorkbook.Name & ": w arkuszu " & m_Ark & " nie znalaz�em CalCo " & aKoszty(i, 3)
                    Exit Sub
                End If
                
                On Error Resume Next
                .Cells(fs.Row, 3 + Miesiac) = CDbl(aKoszty(i, 7))
                If Err <> 0 Then
                    .Cells(fs.Row, 3 + Miesiac) = 0
                End If
                On Error GoTo 0
              
            End With
            
            i = i + 1
            If i > UBound(aKoszty) Then Exit Do
        Loop
      
        ' Media
        For k = 0 To UBound(aMedia)
            If m_Ark = aMedia(k, 2) Then
                With Sheets(m_Ark)
                    Set fs = .Columns(1).Find(aMedia(k, 3), LookAt:=xlWhole, LookIn:=xlValues)
                    If fs Is Nothing Then
                        ' 2012-08-29 zablokowane
'                        MsgBox "Zeszyt " & ThisWorkbook.Name & ": w arkuszu " & m_Ark & vbCrLf & " brak znacznika " & aMedia(k, 3) & " (" & aMedia(k, 8) & ")"
'                        Exit For
                    Else
                        On Error Resume Next
                        If CInt(aMedia(k, 6)) <> 0 Then
                          If Err = 0 Then
                            .Cells(fs.Row, 3 + Miesiac) = CDbl(aMedia(k, 7)) / CInt(aMedia(k, 6))
                          End If
                        End If
                        On Error GoTo 0
                    End If
                End With
            End If
        Next k
      
    Loop Until i > UBound(aKoszty)
    
    For Each ws In Worksheets
        If InStr(1, ws.Range("A3"), "INEF") > 0 Then
            ws.Outline.ShowLevels RowLevels:=1
        End If
    Next ws

Finish:
    With Application
        .DisplayAlerts = True
        .Calculation = intCalculation
        .ScreenUpdating = True
    End With
    Set ws = Nothing
    
    ActiveWorkbook.Save

End Sub

