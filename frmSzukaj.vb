﻿Imports System.Windows.Forms

Public Class frmSzukaj

    Private m_Szukaj As Boolean = False

    Public ReadOnly Property Szukaj() As Boolean
        Get
            Return m_Szukaj
        End Get
    End Property

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        m_Szukaj = True
        Me.Close()
    End Sub

    Private Sub btnAnuluj_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnuluj.Click
        Me.Close()
    End Sub

    Private Sub frmSzukaj_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.txtSzukaj.Focus()
    End Sub

End Class
