﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Wieloletnie
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Wieloletnie))
        Me.tlp = New System.Windows.Forms.TableLayoutPanel
        Me.dgv = New System.Windows.Forms.DataGridView
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.dtpOd = New MyControls.DateTimePickerReadOnly
        Me.dtpDo = New MyControls.DateTimePickerReadOnly
        Me.txtSQL = New System.Windows.Forms.TextBox
        Me.txtID = New System.Windows.Forms.TextBox
        Me.bn = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.ToolStripLabel7 = New System.Windows.Forms.ToolStripLabel
        Me.tsbFirst = New System.Windows.Forms.ToolStripButton
        Me.tsbPrevious = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator42 = New System.Windows.Forms.ToolStripSeparator
        Me.bnPositionItem = New System.Windows.Forms.ToolStripTextBox
        Me.ToolStripSeparator43 = New System.Windows.Forms.ToolStripSeparator
        Me.tsbNext = New System.Windows.Forms.ToolStripButton
        Me.tsbLast = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator44 = New System.Windows.Forms.ToolStripSeparator
        Me.tsbNowy = New System.Windows.Forms.ToolStripButton
        Me.tsbZmien = New System.Windows.Forms.ToolStripButton
        Me.tsbUsun = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator46 = New System.Windows.Forms.ToolStripSeparator
        Me.tsbZapisz = New System.Windows.Forms.ToolStripButton
        Me.tsbWykonaj = New System.Windows.Forms.ToolStripButton
        Me.tlp.SuspendLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bn, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bn.SuspendLayout()
        Me.SuspendLayout()
        '
        'tlp
        '
        Me.tlp.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tlp.ColumnCount = 3
        Me.tlp.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 39.71119!))
        Me.tlp.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60.28881!))
        Me.tlp.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 330.0!))
        Me.tlp.Controls.Add(Me.dgv, 2, 0)
        Me.tlp.Controls.Add(Me.Label1, 0, 0)
        Me.tlp.Controls.Add(Me.Label2, 0, 1)
        Me.tlp.Controls.Add(Me.Label3, 0, 2)
        Me.tlp.Controls.Add(Me.dtpOd, 1, 0)
        Me.tlp.Controls.Add(Me.dtpDo, 1, 1)
        Me.tlp.Controls.Add(Me.txtSQL, 0, 3)
        Me.tlp.Controls.Add(Me.txtID, 1, 2)
        Me.tlp.Location = New System.Drawing.Point(12, 28)
        Me.tlp.Name = "tlp"
        Me.tlp.RowCount = 4
        Me.tlp.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 37.0!))
        Me.tlp.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 37.0!))
        Me.tlp.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 37.0!))
        Me.tlp.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tlp.Size = New System.Drawing.Size(621, 388)
        Me.tlp.TabIndex = 0
        '
        'dgv
        '
        Me.dgv.AllowUserToAddRows = False
        Me.dgv.AllowUserToDeleteRows = False
        Me.dgv.AllowUserToResizeRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.dgv.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgv.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgv.BackgroundColor = System.Drawing.SystemColors.Control
        Me.dgv.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv.ColumnHeadersVisible = False
        Me.dgv.Location = New System.Drawing.Point(293, 3)
        Me.dgv.MultiSelect = False
        Me.dgv.Name = "dgv"
        Me.dgv.ReadOnly = True
        Me.dgv.RowHeadersVisible = False
        Me.tlp.SetRowSpan(Me.dgv, 4)
        Me.dgv.RowTemplate.Height = 24
        Me.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgv.Size = New System.Drawing.Size(325, 382)
        Me.dgv.TabIndex = 46
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(40, 6)
        Me.Label1.Margin = New System.Windows.Forms.Padding(40, 6, 3, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(29, 18)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Od"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(40, 43)
        Me.Label2.Margin = New System.Windows.Forms.Padding(40, 6, 3, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(29, 18)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Do"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(3, 80)
        Me.Label3.Margin = New System.Windows.Forms.Padding(3, 6, 3, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(88, 18)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Zapytanie:"
        '
        'dtpOd
        '
        Me.dtpOd.CustomFormat = "yyyy"
        Me.dtpOd.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpOd.Location = New System.Drawing.Point(118, 3)
        Me.dtpOd.Name = "dtpOd"
        Me.dtpOd.ReadOnly = False
        Me.dtpOd.ShowUpDown = True
        Me.dtpOd.Size = New System.Drawing.Size(77, 26)
        Me.dtpOd.TabIndex = 3
        '
        'dtpDo
        '
        Me.dtpDo.CustomFormat = "yyyy"
        Me.dtpDo.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpDo.Location = New System.Drawing.Point(118, 40)
        Me.dtpDo.Name = "dtpDo"
        Me.dtpDo.ReadOnly = False
        Me.dtpDo.ShowUpDown = True
        Me.dtpDo.Size = New System.Drawing.Size(77, 26)
        Me.dtpDo.TabIndex = 4
        '
        'txtSQL
        '
        Me.txtSQL.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tlp.SetColumnSpan(Me.txtSQL, 2)
        Me.txtSQL.Font = New System.Drawing.Font("Verdana", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.txtSQL.Location = New System.Drawing.Point(3, 114)
        Me.txtSQL.Multiline = True
        Me.txtSQL.Name = "txtSQL"
        Me.txtSQL.Size = New System.Drawing.Size(284, 271)
        Me.txtSQL.TabIndex = 5
        '
        'txtID
        '
        Me.txtID.Location = New System.Drawing.Point(118, 77)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(100, 26)
        Me.txtID.TabIndex = 47
        Me.txtID.Visible = False
        '
        'bn
        '
        Me.bn.AddNewItem = Nothing
        Me.bn.CountItem = Me.ToolStripLabel7
        Me.bn.CountItemFormat = "z {0}"
        Me.bn.DeleteItem = Nothing
        Me.bn.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsbFirst, Me.tsbPrevious, Me.ToolStripSeparator42, Me.bnPositionItem, Me.ToolStripLabel7, Me.ToolStripSeparator43, Me.tsbNext, Me.tsbLast, Me.ToolStripSeparator44, Me.tsbNowy, Me.tsbZmien, Me.tsbUsun, Me.ToolStripSeparator46, Me.tsbZapisz, Me.tsbWykonaj})
        Me.bn.Location = New System.Drawing.Point(0, 0)
        Me.bn.MoveFirstItem = Nothing
        Me.bn.MoveLastItem = Nothing
        Me.bn.MoveNextItem = Nothing
        Me.bn.MovePreviousItem = Nothing
        Me.bn.Name = "bn"
        Me.bn.PositionItem = Me.bnPositionItem
        Me.bn.Size = New System.Drawing.Size(645, 25)
        Me.bn.TabIndex = 26
        Me.bn.Text = "BindingNavigator1"
        '
        'ToolStripLabel7
        '
        Me.ToolStripLabel7.Name = "ToolStripLabel7"
        Me.ToolStripLabel7.Size = New System.Drawing.Size(42, 22)
        Me.ToolStripLabel7.Text = "z {0}"
        Me.ToolStripLabel7.ToolTipText = "Liczba dostępnych pozycji"
        '
        'tsbFirst
        '
        Me.tsbFirst.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbFirst.Image = CType(resources.GetObject("tsbFirst.Image"), System.Drawing.Image)
        Me.tsbFirst.Name = "tsbFirst"
        Me.tsbFirst.RightToLeftAutoMirrorImage = True
        Me.tsbFirst.Size = New System.Drawing.Size(23, 22)
        Me.tsbFirst.Text = "Pierwszy"
        '
        'tsbPrevious
        '
        Me.tsbPrevious.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbPrevious.Image = CType(resources.GetObject("tsbPrevious.Image"), System.Drawing.Image)
        Me.tsbPrevious.Name = "tsbPrevious"
        Me.tsbPrevious.RightToLeftAutoMirrorImage = True
        Me.tsbPrevious.Size = New System.Drawing.Size(23, 22)
        Me.tsbPrevious.Text = "Poprzedni"
        '
        'ToolStripSeparator42
        '
        Me.ToolStripSeparator42.Name = "ToolStripSeparator42"
        Me.ToolStripSeparator42.Size = New System.Drawing.Size(6, 25)
        '
        'bnPositionItem
        '
        Me.bnPositionItem.AccessibleName = "Position"
        Me.bnPositionItem.AutoSize = False
        Me.bnPositionItem.Name = "bnPositionItem"
        Me.bnPositionItem.Size = New System.Drawing.Size(64, 24)
        Me.bnPositionItem.Text = "0"
        Me.bnPositionItem.ToolTipText = "Bieżąca pozycja"
        '
        'ToolStripSeparator43
        '
        Me.ToolStripSeparator43.Name = "ToolStripSeparator43"
        Me.ToolStripSeparator43.Size = New System.Drawing.Size(6, 25)
        '
        'tsbNext
        '
        Me.tsbNext.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbNext.Image = CType(resources.GetObject("tsbNext.Image"), System.Drawing.Image)
        Me.tsbNext.Name = "tsbNext"
        Me.tsbNext.RightToLeftAutoMirrorImage = True
        Me.tsbNext.Size = New System.Drawing.Size(23, 22)
        Me.tsbNext.Text = "Następny"
        '
        'tsbLast
        '
        Me.tsbLast.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbLast.Image = CType(resources.GetObject("tsbLast.Image"), System.Drawing.Image)
        Me.tsbLast.Name = "tsbLast"
        Me.tsbLast.RightToLeftAutoMirrorImage = True
        Me.tsbLast.Size = New System.Drawing.Size(23, 22)
        Me.tsbLast.Text = "Ostatni"
        '
        'ToolStripSeparator44
        '
        Me.ToolStripSeparator44.Name = "ToolStripSeparator44"
        Me.ToolStripSeparator44.Size = New System.Drawing.Size(6, 25)
        '
        'tsbNowy
        '
        Me.tsbNowy.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbNowy.Image = CType(resources.GetObject("tsbNowy.Image"), System.Drawing.Image)
        Me.tsbNowy.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbNowy.Name = "tsbNowy"
        Me.tsbNowy.Size = New System.Drawing.Size(23, 22)
        Me.tsbNowy.Text = "Nowy"
        Me.tsbNowy.ToolTipText = "Nowy Ctrl+N"
        '
        'tsbZmien
        '
        Me.tsbZmien.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbZmien.Image = CType(resources.GetObject("tsbZmien.Image"), System.Drawing.Image)
        Me.tsbZmien.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbZmien.Name = "tsbZmien"
        Me.tsbZmien.Size = New System.Drawing.Size(23, 22)
        Me.tsbZmien.Text = "Zmień"
        Me.tsbZmien.ToolTipText = "Zmień Ctrl+Z"
        '
        'tsbUsun
        '
        Me.tsbUsun.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbUsun.Image = CType(resources.GetObject("tsbUsun.Image"), System.Drawing.Image)
        Me.tsbUsun.Name = "tsbUsun"
        Me.tsbUsun.RightToLeftAutoMirrorImage = True
        Me.tsbUsun.Size = New System.Drawing.Size(23, 22)
        Me.tsbUsun.Text = "Usuń"
        Me.tsbUsun.ToolTipText = "Usuń Ctrl+D"
        '
        'ToolStripSeparator46
        '
        Me.ToolStripSeparator46.Name = "ToolStripSeparator46"
        Me.ToolStripSeparator46.Size = New System.Drawing.Size(6, 25)
        '
        'tsbZapisz
        '
        Me.tsbZapisz.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbZapisz.Image = CType(resources.GetObject("tsbZapisz.Image"), System.Drawing.Image)
        Me.tsbZapisz.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbZapisz.Name = "tsbZapisz"
        Me.tsbZapisz.Size = New System.Drawing.Size(23, 22)
        Me.tsbZapisz.Text = "Zapisz"
        Me.tsbZapisz.ToolTipText = "Zapisz Ctrl+S"
        '
        'tsbWykonaj
        '
        Me.tsbWykonaj.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbWykonaj.Image = CType(resources.GetObject("tsbWykonaj.Image"), System.Drawing.Image)
        Me.tsbWykonaj.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbWykonaj.Name = "tsbWykonaj"
        Me.tsbWykonaj.Size = New System.Drawing.Size(23, 22)
        Me.tsbWykonaj.Text = "Wykonaj"
        '
        'Wieloletnie
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 18.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(645, 428)
        Me.Controls.Add(Me.bn)
        Me.Controls.Add(Me.tlp)
        Me.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Wieloletnie"
        Me.Text = "Generowanie wieloletnich danych"
        Me.tlp.ResumeLayout(False)
        Me.tlp.PerformLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bn, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bn.ResumeLayout(False)
        Me.bn.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tlp As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dtpOd As MyControls.DateTimePickerReadOnly
    Friend WithEvents dtpDo As MyControls.DateTimePickerReadOnly
    Friend WithEvents txtSQL As System.Windows.Forms.TextBox
    Friend WithEvents bn As System.Windows.Forms.BindingNavigator
    Friend WithEvents ToolStripLabel7 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents tsbFirst As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbPrevious As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator42 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents bnPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents ToolStripSeparator43 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsbNext As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbLast As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator44 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsbNowy As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbZmien As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbUsun As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator46 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsbZapisz As System.Windows.Forms.ToolStripButton
    Friend WithEvents dgv As System.Windows.Forms.DataGridView
    Friend WithEvents tsbWykonaj As System.Windows.Forms.ToolStripButton
    Friend WithEvents txtID As System.Windows.Forms.TextBox
End Class
