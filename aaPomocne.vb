﻿
#Region "Opcje i referencje"

Option Explicit On
Option Strict Off
Option Compare Text
Option Infer Off

Imports System
Imports System.Data
Imports System.Data.OleDb
Imports System.Deployment
Imports System.Deployment.Application
Imports System.Drawing
Imports System.IO
Imports System.Linq
Imports System.Reflection
Imports System.Windows.Forms
Imports Microsoft.VisualBasic
Imports XL = Microsoft.Office.Interop.Excel
Imports System.ComponentModel

#End Region ' Opcje i referencje

' Modified 2014-02-20
Module aaPomocne

#Region "Deklaracje"

    Public screenFresh As Boolean
    Private oApp As XL.Application
    Private oBook As XL._Workbook
    Private oSheet As XL._Worksheet
    Private m_Usun As Boolean = False
    Private m_Dodaj As Boolean = False
    Private m_Nazwa As String = ""
    Private m_Row As Integer = 0
    Private m_Col As Integer

#End Region ' Deklaracje

#Region "Konwersja tekstu rozdzielanego |,|| na tabelę i odwrotnie"

    Function ConvertDt2Pos(ByVal Tabela As DataTable) As String

        Dim i As Integer
        Dim m_Ret As String = ""
        Try
            If Tabela.Rows.Count.Equals(0) Then Exit Try
            For Each dr As DataRow In Tabela.Rows
                For i = 0 To dr.ItemArray.GetUpperBound(0)
                    Select Case Tabela.Columns(i).DataType.Name
                        Case "Boolean"
                            If CBool(dr(i)) Then
                                m_Ret += "T|"
                            Else
                                m_Ret += "N|"
                            End If
                        Case "Double"
                            Try
                                m_Ret += dr(i).ToString.Replace(",", ".") + "|"
                            Catch ex As Exception
                                m_Ret += "0|"
                            End Try
                        Case "Int32"
                            Try
                                m_Ret += dr(i).ToString + "|"
                            Catch ex As Exception
                                m_Ret += "0|"
                            End Try
                        Case Else
                            m_Ret += dr(i).ToString + "|"
                    End Select
                Next i
                m_Ret += "|"
            Next dr
            With m_Ret
                If .EndsWith("||") Then m_Ret = .Substring(0, .Length - 2)
            End With
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Konwersja tabeli na tekst", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
        Return m_Ret
    End Function

    Function ConvertDt2Pos(ByVal Tabela As DataTable, _
                                   ByVal IleKolum As Integer, _
                                   ByVal NrKolumny As Integer, _
                                   ByRef Wartosc As Double, _
                                   Optional ByVal Warunek As Integer = -1) As String

        Dim i As Integer
        Dim m_Ret As String = ""
        Dim dbl As Double
        Try
            Wartosc = 0
            If Tabela.Rows.Count.Equals(0) Then Exit Try
            For Each dr As DataRow In Tabela.Rows
                For i = 0 To IleKolum
                    Select Case Tabela.Columns(i).DataType.Name
                        Case "Boolean"
                            If CBool(dr(i)) Then
                                m_Ret += "T|"
                            Else
                                m_Ret += "N|"
                            End If
                        Case "Double"
                            Try
                                m_Ret += dr(i).ToString.Replace(",", ".") + "|"
                            Catch ex As Exception
                                m_Ret += "0|"
                            End Try
                        Case "Int32"
                            Try
                                m_Ret += dr(i).ToString + "|"
                            Catch ex As Exception
                                m_Ret += "0|"
                            End Try
                        Case Else
                            m_Ret += dr(i).ToString + "|"
                    End Select
                Next i
                Try
                    dbl = CDbl(dr(NrKolumny).ToString)
                    If Not Warunek.Equals(-1) Then
                        If CBool(dr(Warunek)) Then Wartosc += dbl
                    Else
                        Wartosc += CDbl(dr(NrKolumny).ToString)
                    End If
                Catch ex As Exception
                    Throw New ArgumentException(ex.Message)
                End Try
                m_Ret += "|"
            Next dr
            With m_Ret
                If .EndsWith("||") Then m_Ret = .Substring(0, .Length - 2)
            End With
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Konwersja tabeli na tekst", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
        Return m_Ret
    End Function

    Sub ConvertPos2Dt(ByVal Tekst As String, _
                      ByRef Tabela As DataTable)

        Dim i, j As Integer
        Dim aPoz() As String
        Dim aItem() As String
        Dim dr As DataRow = Nothing
        Try
            Tabela.Clear()
            aPoz = Split(Tekst.ToString, "||")
            i = aPoz.Length
            If i.Equals(0) OrElse String.IsNullOrEmpty(aPoz(0)) Then Exit Try
            For i = 0 To aPoz.GetUpperBound(0)
                If String.IsNullOrEmpty(aPoz(i)) Then Exit For
                aItem = Split(aPoz(i).ToString, "|")
                dr = Tabela.NewRow
                For j = 0 To aItem.GetUpperBound(0)
                    Select Case Tabela.Columns(j).DataType.Name
                        Case "Boolean"
                            dr(j) = True
                            If aItem(j).ToUpper.Equals("FALSE") _
                                OrElse aItem(j).ToUpper.Equals("FAŁSZ") _
                                OrElse aItem(j).ToUpper.Equals("N") _
                                    Then dr(j) = False
                        Case "Double"
                            Try
                                dr(j) = CDbl(aItem(j).ToString.Replace(".", ","))
                            Catch ex As Exception
                                dr(j) = 0
                            End Try
                        Case "Single"
                            Try
                                dr(j) = CSng(aItem(j).ToString.Replace(".", ","))
                            Catch ex As Exception
                                dr(j) = 0
                            End Try
                        Case "Int32"
                            Try
                                dr(j) = CInt(aItem(j))
                            Catch ex As Exception
                                dr(j) = 0
                            End Try
                        Case Else
                            dr(j) = aItem(j)
                    End Select
                Next j
                Tabela.Rows.Add(dr)
            Next i
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Konwersja tekstu na tabelę", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            aPoz = Nothing
            aItem = Nothing
            dr = Nothing
        End Try
    End Sub

    Sub ConvertPos2Array(ByVal Tekst As String, ByRef aRet(,) As Object)

        Dim i, j As Integer
        Dim _dbl As Double
        Dim _int As Integer
        Dim _dtp As Date
        Dim txt As String
        Dim aPoz() As String
        Dim aItem() As String
        Try
            Tekst = Tekst.Replace("]", "|")
            aPoz = Split(Tekst.ToString, "||")
            i = aPoz.Length
            If i.Equals(0) OrElse String.IsNullOrEmpty(aPoz(0)) Then Exit Try
            aItem = Split(aPoz(0).ToString, "|")
            j = aItem.Length
            ReDim aRet(i, j)
            For i = 0 To aPoz.GetUpperBound(0)
                If String.IsNullOrEmpty(aPoz(i)) Then Exit For
                aItem = Split(aPoz(i).ToString, "|")
                For j = 0 To aItem.GetUpperBound(0)
                    If String.IsNullOrEmpty(aItem(j)) Then GoTo Nastepny
                    With aItem(j)
                        If .ToUpper.Equals("FALSE") _
                            OrElse .ToUpper.Equals("FAŁSZ") _
                            OrElse .ToUpper.Equals("N") Then
                            aRet(i, j) = False
                        ElseIf .ToUpper.Equals("TRUE") _
                            OrElse .ToUpper.Equals("PRAWDA") _
                            OrElse .ToUpper.Equals("T") Then
                            aRet(i, j) = True
                        Else
                            txt = aItem(j)
                            Try
                                _dbl = CDbl(aItem(j))
                            Catch ex As Exception
                                _dbl = 0
                                Try
                                    _int = CInt(aItem(j))
                                Catch
                                    _int = 0
                                    Try
                                        _dtp = CDate(aItem(j))
                                    Catch
                                        _dtp = Nothing
                                    End Try
                                End Try
                            End Try
                            If Not _dbl.Equals(0) Then
                                aRet(i, j) = _dbl
                            ElseIf Not _int.Equals(0) Then
                                aRet(i, j) = _int
                            ElseIf _dtp.ToShortDateString IsNot Nothing Then
                                aRet(i, j) = _dtp
                            Else
                                aRet(i, j) = txt
                            End If
                        End If
                    End With
Nastepny:
                Next j
            Next i
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Konwersja tekstu na tabelę", MessageBoxButtons.OK, MessageBoxIcon.Error)
            aRet = Nothing
        Finally
            aPoz = Nothing
            aItem = Nothing
        End Try
    End Sub

#End Region 'Konwersja tekstu rozdzielanego |,|| na tabelę/tablicę i odwrotnie

#Region "Metody ogólne"

    ' Przechodzenie pomiędzy kontrolkami po naciśnięciu Enter
    Sub EnterKeyPressed(ByVal sender As Object, _
                        ByVal e As System.Windows.Forms.KeyPressEventArgs)

        '   podczas inicjacji wstawić:
        '           AddHandler Ctrl.KeyPress, AddressOf EnterKeyPressed
        Dim Ctrl As Control = CType(sender, Control)
        Dim m_Parent As Control = Ctrl.Parent
        If Not e.KeyChar.Equals(Chr(13)) Then Return
        Try
            If Ctrl.TabIndex.Equals(m_Parent.Controls.Count - 1) Then
                m_Parent.Parent.SelectNextControl(Ctrl, True, True, True, False)
            Else
                m_Parent.SelectNextControl(Ctrl, True, True, True, False)
            End If
        Catch ex As Exception
            Ctrl.Focus()
        End Try
    End Sub

    ' Przypisanie klawisza <Enter> do kontrolek w TableLayoutPanel
    Sub Enter4Tlp(ByVal tlp As TableLayoutPanel)

        For Each Ctrl As Control In tlp.Controls
            If Ctrl.TabStop Then
                With Ctrl.GetType.ToString
                    If .IndexOf("Text") > -1 OrElse .IndexOf("Combo") > -1 _
                        OrElse .IndexOf("Date") > -1 Then _
                        AddHandler Ctrl.KeyPress, AddressOf EnterKeyPressed
                End With
            End If
        Next Ctrl
    End Sub

    Function fArrayLayoutControl(ByVal Dane As Array, ByVal Kontrolny As String, ByVal cns As OleDbConnection) As Boolean

        ' Source (nazwa pliku źródłowego),
        ' NrKol (kolumna pliku źródłowego)
        ' KolPodstawowa (bez spacji i #; nazwy używane przez program)
        ' Wariant1 - alternatywna, kontrolna nazwa
        ' Wariant2 - jw.
        ' Wariant3 - jw.
        Dim i, j, iC As Integer
        Dim dt As New DataTable
        Dim da As OleDbDataAdapter = New OleDbDataAdapter("SELECT * FROM tblSources WHERE Source = '" + Kontrolny + "' ORDER BY NrKol", cns)
        da.Fill(dt)
        Try
            For i = 1 To Dane.GetLength(1)
                Dane(1, i) = Dane.GetValue(1, i).ToString.Replace(" ", "").Replace("#", "").Replace(".", "").ToUpper
            Next i
            ' porównanie kolumn
            For iC = 0 To dt.Rows.Count - 1
                For i = 1 To Dane.GetLength(1)
                    With dt.Rows(iC)
                        If .Item("KolPodstawowa").ToString.ToUpper.Equals(Dane.GetValue(1, i).ToString) Then
                            If Not CInt(.Item("NrKol").ToString).Equals(i) Then
                                Throw New ArgumentException("Zmieniony układ (kolumna [" + .Item("KolPodstawowa").ToString + "])")
                            Else
                                GoTo Dalej
                            End If
                        Else
                            ' przeszukaj pomocnicze kolumny
                            For j = 1 To 3
                                With .Item(j + 2)
                                    If Not String.IsNullOrEmpty(.ToString) AndAlso .ToString.ToUpper.Equals(Dane.GetValue(1, i).ToString) Then
                                        If Not CInt(dt.Rows(iC)("NrKol").ToString).Equals(i) Then
                                            Throw New ArgumentException("Zmieniony układ (kolumna [" + .Item(j + 2).ToString + "])")
                                        Else
                                            GoTo Dalej
                                        End If
                                    End If
                                End With
                            Next j
                        End If
                    End With
                Next i
                Throw New ArgumentException("Niezidentyfikowana kolumna [" + dt.Rows(iC)("KolPodstawowa").ToString + "]")
Dalej:
            Next iC
            Return True
        Catch ex As Exception
            MessageBox.Show(ex.Message, Kontrolny, MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return False
        Finally
            dt = Nothing
            da = Nothing
        End Try
    End Function

    ' Sprawdza poprawność wprowadzonej daty w TextBox
    Function fCheckDate(ByVal sender As TextBox, _
                           Optional ByVal SetFocus As Boolean = False, _
                           Optional ByRef dtmDate As DateTime = Nothing) As Boolean

        Try
            Dim dtm As DateTime = CDate(sender.Text)
            dtmDate = dtm.Date
        Catch ex As Exception
            If SetFocus Then sender.Focus()
            dtmDate = Nothing
            Return False
        End Try
        Return True
    End Function

    ' Sprawdza istnienie folderu i gdy brak tworzy
    Function fCheckFolder(ByVal Nazwa As String) As Boolean

        Dim vSplit() As String
        Dim i As Integer
        Dim m_Path As String
        Dim m_Ret As Boolean = True
        Try
            If My.Computer.FileSystem.DirectoryExists(Nazwa) Then Exit Try
            With Nazwa
                If .EndsWith(Path.DirectorySeparatorChar) Then Nazwa = .Substring(0, .Length - 1)
                vSplit = .Split(Path.DirectorySeparatorChar)
            End With
            m_Path = vSplit(0)
            For i = 1 To UBound(vSplit, 1)
                m_Path += Path.DirectorySeparatorChar + vSplit(i)
                If Not My.Computer.FileSystem.DirectoryExists(m_Path) Then _
                    My.Computer.FileSystem.CreateDirectory(m_Path)
            Next i
        Catch ex As Exception
            m_Ret = False
        End Try
        Return m_Ret
    End Function

    ' Sprawdzenie układu kolumn
    Function fCheckSourceColumns(ByVal Source As DataTable, ByRef Tabela As DataTable) As Boolean

        Dim i, j, k As Integer
        Dim aCol(0 To Source.Rows.Count - 1, 0 To 1) As Integer
        Dim txt As String
        Try
            ' porównanie kolumn
            For i = 0 To Source.Rows.Count - 1
                aCol(i, 0) = CInt(Source.Rows(i)("NrKol").ToString)
                For j = 0 To Tabela.Columns.Count - 1
                    txt = Tabela.Columns(j).ColumnName.ToString.Trim.ToUpper.Replace(".", "").Replace("#", "").Replace(" ", "")
                    If Source.Rows(i)("KolPodstawowa").ToString.ToUpper.Replace(".", "").Replace(" ", "").Equals(txt) Then
                        aCol(i, 1) = j + 1
                        Tabela.Columns(j).ColumnName = Source.Rows(i)("KolPodstawowa").ToString
                        GoTo Nastepny
                    Else
                        ' przeszukaj pomocnicze kolumny
                        For k = 1 To 3
                            If Source.Rows(i)(k + 2).ToString.ToUpper.Replace(".", "").Replace(" ", "").Equals(txt) Then
                                aCol(i, 1) = j + 1
                                Tabela.Columns(j).ColumnName = Source.Rows(i)("KolPodstawowa").ToString
                                GoTo Nastepny
                            End If
                        Next k
                    End If
                Next j
Nastepny:
            Next i
            ' sprawdzenie, czy wszystkie dobrane
            For i = 0 To Source.Rows.Count - 1
                If aCol(i, 1).Equals(0) Then _
                    Throw New ArgumentException("Położenie kolumny [" + Source.Rows(i)("KolPodstawowa").ToString + "] nie zostało ustalone")
            Next i
            Return True
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Kontrola układu kolumn", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return False
        End Try
    End Function

    Function fCheckSourceColumns(ByVal Source As DataTable, ByRef Tablica As Object) As Boolean

        ' dodane usuwanie twardej spacji ASC 160
        Dim i, j, k As Integer
        Dim aCol(0 To Source.Rows.Count - 1, 0 To 1) As Integer
        Dim txt As String
        Try
            ' porównanie kolumn
            For i = 0 To Source.Rows.Count - 1
                aCol(i, 0) = CInt(Source.Rows(i)("NrKol").ToString)
                For j = 1 To Tablica.GetUpperBound(1)
                    txt = Tablica(1, j).Trim.ToUpper.Replace(".", "").Replace("#", "").Replace(" ", "").Replace(Chr(160), "")
                    If Source.Rows(i)("KolPodstawowa").ToString.ToUpper.Replace(".", "").Replace(" ", "").Replace(Chr(160), "").Equals(txt) Then
                        aCol(i, 1) = j
                        Tablica(1, j) = Source.Rows(i)("KolPodstawowa").ToString
                        GoTo Nastepny
                    Else
                        ' przeszukaj pomocnicze kolumny
                        For k = 1 To 3
                            If Source.Rows(i)(k + 2).ToString.ToUpper.Replace(".", "").Replace(" ", "").Replace(Chr(160), "").Equals(txt) Then
                                aCol(i, 1) = j
                                Tablica(1, j) = Source.Rows(i)("KolPodstawowa").ToString
                                GoTo Nastepny
                            End If
                        Next k
                    End If
                Next j
Nastepny:
            Next i
            ' sprawdzenie, czy wszystkie dobrane
            For i = 0 To Source.Rows.Count - 1
                If aCol(i, 1).Equals(0) Then _
                    Throw New ArgumentException("Położenie kolumny [" + Source.Rows(i)("KolPodstawowa").ToString + "] nie zostało ustalone")
            Next i
            Return True
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Kontrola układu kolumn", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return False
        End Try
    End Function

    ' Sprawdzanie i uzupełnianie czasu w TextBox
    Function fCheckTime(ByVal sender As TextBox, _
                           Optional ByVal SetFocus As Boolean = False, _
                           Optional ByRef hhTime As DateTime = Nothing) As Boolean

        Dim strHH As String
        Dim iW As Integer
        Try
            strHH = sender.Text.Trim
            If strHH.Length.Equals(0) Then Throw New ArgumentException("")
            iW = strHH.IndexOf(":")
            Select Case iW
                Case -1
                    Select Case strHH.Length
                        Case 1
                            strHH = "0" + strHH + ":00"
                        Case 2
                            strHH += ":00"
                        Case 3
                            strHH = String.Concat(strHH.Substring(0, 2), ":", strHH.Substring(2), "0")
                        Case 4
                            strHH = String.Concat(strHH.Substring(0, 2), ":", strHH.Substring(2))
                        Case Else
                            Throw New ArgumentException("")
                    End Select
                Case 0
                    strHH = "00" + strHH.Substring(0)
                Case 1
                    strHH = "0" + strHH.Substring(0)
                Case 2
                    If strHH.Length.Equals(3) Then
                        strHH += "00"
                    ElseIf strHH.Length.Equals(4) Then
                        strHH += "0"
                    End If
                Case Else
                    Throw New ArgumentException("")
            End Select
            hhTime = CDate(strHH)
        Catch ex As Exception
            If SetFocus Then sender.Focus()
            hhTime = Nothing
            Return False
        End Try
        Return True
    End Function

    ' "ReadOnly" combobox przykrywa/odkrywa nieudostępniony combobox controlką txt
    Sub fComboToText(ByRef txt As TextBox, _
                     ByRef cmb As ComboBox, _
                     Optional ByVal Reverse As Boolean = False)

        Dim m_Location As Point
        Dim m_Size As Size
        Dim m_Anchor As AnchorStyles
        Dim m_RS, m_CS As Integer
        Dim m_Parent As TableLayoutPanel = Nothing
        Dim m_Rodzic As Control
        Dim m_Cel As TableLayoutPanelCellPosition
        Dim m_Idx As Integer = cmb.SelectedIndex
        If Not cmb.Enabled Then Exit Sub
        If Reverse Then
            If cmb.Visible Then GoTo Finish
            Try
                m_Parent = CType(txt.Parent, TableLayoutPanel)
                m_RS = m_Parent.GetRowSpan(txt)
                m_CS = m_Parent.GetColumnSpan(txt)
                m_Cel = m_Parent.GetCellPosition(txt)
            Catch ex As Exception
            End Try
            With txt
                If .Text.Equals(cmb.Text) Then m_Idx = cmb.SelectedIndex
                .Visible = False
                m_Rodzic = .Parent
                m_Location = .Location
                m_Size = .Size
                m_Anchor = .Anchor
                .Parent = cmb.Parent
                .Anchor = cmb.Anchor
                .Size = cmb.Size
                .Location = cmb.Location
            End With
            If Not m_Parent Is Nothing Then
                m_Parent.SetCellPosition(cmb, m_Cel)
                m_Parent.SetRowSpan(cmb, m_RS)
                m_Parent.SetColumnSpan(cmb, m_CS)
            End If
            With cmb
                .Visible = True
                .Parent = m_Rodzic
                .Location = m_Location
                .Size = m_Size
                .Anchor = m_Anchor
                .SelectionLength = 0
                .SelectedIndex = m_Idx
            End With
            GoTo Finish
        End If
        If Not cmb.Visible Then GoTo Finish
        Try
            m_Parent = CType(cmb.Parent, TableLayoutPanel)
            m_RS = m_Parent.GetRowSpan(cmb)
            m_CS = m_Parent.GetColumnSpan(cmb)
            m_Cel = m_Parent.GetCellPosition(cmb)
        Catch ex As Exception
        End Try
        With cmb
            .Visible = False
            m_Rodzic = .Parent
            m_Location = .Location
            m_Size = .Size
            m_Anchor = .Anchor
            .Parent = txt.Parent
            .Anchor = AnchorStyles.None
            .Size = txt.Size
            .Location = txt.Location
            .SelectedIndex = m_Idx
        End With
        If Not m_Parent Is Nothing Then
            m_Parent.SetCellPosition(txt, m_Cel)
            m_Parent.SetRowSpan(txt, m_RS)
            m_Parent.SetColumnSpan(txt, m_CS)
        End If
        With txt
            .Visible = True
            .Parent = m_Rodzic
            .Location = m_Location
            .Size = m_Size
            .Anchor = m_Anchor
        End With
Finish:
        txt.Text = cmb.Text
        m_Parent = Nothing
        m_Rodzic = Nothing
    End Sub

    ' Porównanie daty modyfikacji pliku z zadaną
    Function fCompareFileDate(ByVal FileSpec As String, _
                              ByVal Dtm2Compare As DateTime, _
                              Optional ByVal AreEqual As Boolean = True, _
                              Optional ByVal BeforeDtm2 As Boolean = False, _
                              Optional ByVal LaterOfDtm2 As Boolean = False, _
                              Optional ByVal OnlyDate As Boolean = False) As Boolean

        Try
            Dim fsi As FileInfo = My.Computer.FileSystem.GetFileInfo(FileSpec)
            Dim dtt As DateTime = fsi.LastWriteTime
            Dim result As Integer
            If OnlyDate Then
                result = Dtm2Compare.Date.CompareTo(dtt.Date)
            Else
                result = Dtm2Compare.CompareTo(dtt)
            End If
            If AreEqual Then
                If BeforeDtm2 Then
                    Return result >= 0
                ElseIf LaterOfDtm2 Then
                    Return result <= 0
                Else
                    Return result = 0
                End If
            ElseIf BeforeDtm2 Then
                If LaterOfDtm2 Then
                    Return result <> 0
                Else
                    Return result > 0
                End If
            ElseIf LaterOfDtm2 Then
                Return result < 0
            End If
            MessageBox.Show("Plik z " + fsi.LastWriteTime.Date + " nie odpowiada warunkom", FileSpec, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Catch ex As Exception
            Return False
        End Try
    End Function

    ' Porównanie dwóch dat (i czasów)
    Function fCompareDates(ByVal Before As Boolean, _
                           ByVal BasicDate As DateTime, _
                           ByVal CompDate As DateTime, _
                           Optional ByVal BasicTime As DateTime = Nothing, _
                           Optional ByVal CompTime As DateTime = Nothing) As Boolean

        Try
            Dim dtm1 As Date = CompDate.Date
            Dim dtm2 As Date = BasicDate.Date
            Dim result_date As Integer = DateTime.Compare(dtm1, dtm2)
            If Before Then
                If result_date < 0 Then Return True
                If result_date.Equals(0) Then
                    Dim result_time As TimeSpan = CompTime - BasicTime
                    Return (result_time.Hours * 60 + result_time.Minutes) < 0
                End If
            Else
                If result_date > 0 Then Return True
                If result_date.Equals(0) Then
                    Dim result_time As TimeSpan = CompTime - BasicTime
                    Return (result_time.Hours * 60 + result_time.Minutes) > 0
                End If
            End If
            Return False
        Catch ex As Exception
            Return False
        End Try
    End Function

    Function fCompareDates(ByVal Before As Boolean, _
                           ByVal BasicDate As TextBox, _
                           ByVal CompDate As TextBox, _
                           Optional ByVal BasicTime As TextBox = Nothing, _
                           Optional ByVal CompTime As TextBox = Nothing) As Boolean

        Try
            Dim dtm1 As Date = CDate(CompDate.Text)
            Dim dtm2 As Date = CDate(BasicDate.Text)
            Dim result_date As Integer = DateTime.Compare(dtm1, dtm2)
            If Before Then
                If result_date < 0 Then Return True
                If result_date.Equals(0) Then
                    Dim result_time As TimeSpan = CDate(CompTime.Text) - CDate(BasicTime.Text)
                    Return (result_time.Hours * 60 + result_time.Minutes) < 0
                End If
            Else
                If result_date > 0 Then Return True
                If result_date.Equals(0) Then
                    Dim result_time As TimeSpan = CDate(CompTime.Text) - CDate(BasicTime.Text)
                    Return (result_time.Hours * 60 + result_time.Minutes) > 0
                End If
            End If
            Return False
        Catch ex As Exception
            Return False
        End Try
    End Function

    ' Konwersja daty z kropką na systemową
    Function fConv2Date(ByVal strDate As String, _
                        Optional ByRef dtmDate As Date = Nothing) As Boolean

        Dim Val As Boolean = False
        Try
            Select Case strDate.IndexOf(".")
                Case 1
                    dtmDate = DateSerial(CInt(strDate.Substring(5, 4)), _
                                         CInt(strDate.Substring(2, 2)), _
                                         CInt(strDate.Substring(0, 1)))
                Case 2
                    dtmDate = DateSerial(CInt(strDate.Substring(6, 4)), _
                                         CInt(strDate.Substring(3, 2)), _
                                         CInt(strDate.Substring(0, 2)))
                Case 4
                    dtmDate = DateSerial(CInt(strDate.Substring(0, 4)), _
                                         CInt(strDate.Substring(3, 2)), _
                                         CInt(strDate.Substring(8, 2)))
                Case Else
                    Throw New ArgumentException
            End Select
            Val = True
        Catch ex As Exception
        End Try
        Return Val
    End Function

    ' Konwersja danych do zapisu w Excel-u
    Function fConv2Str(ByVal Dane As Object) As String

        Try
            Select Case TypeName(Dane)
                Case "String"
                    Return String.Concat("=+", Trim(CType(Dane, String)))
                Case "Date"
                    Return String.Concat("=+", Format(CType(Dane, Date), "yyyy-mm-dd"))
                Case "Integer"
                    Return String.Concat("=+", Trim(CStr(Dane)))
                Case "Long"
                    fConv2Str = String.Concat("=+", Trim(CStr(Dane)))
                Case "Boolean"
                    If CType(Dane, Boolean) Then
                        Return "X"
                    Else
                        Return " "
                    End If
                Case Else
                    Return String.Concat("=+", Trim(CStr(Dane)))
            End Select
        Catch ex As Exception
            Return ""
        End Try
    End Function

    ' Konwersja daty systemowej na datę z kropką
    Function fConvDate2StrDate(ByVal m_Date As String) As String

        Try
            With m_Date
                Return String.Concat(.Substring(8, 2), ".", .Substring(5, 2), ".", .Substring(0, 4))
            End With
        Catch ex As Exception
            Return ""
        End Try
    End Function

    ' Konwersja liczby do zapisu w db (z kropką)
    Function fDbl2Str(ByVal Liczba As Double) As String

        Try
            Return CStr(Liczba).Replace(",", ".")
        Catch ex As Exception
            Return "0"
        End Try
    End Function

    ' Zwraca liczbę dni roboczych w miesiącu
    Function fDniRoboczeWMiesiacu(ByVal Rok As Integer, _
                                  ByVal Miesiac As Integer, _
                                  ByVal Swieta As String, _
                                  Optional ByRef DniRobocze As String = "") As Integer

        Dim iC, iDzien, iDni, iDay As Integer
        Try
            iDzien = DateSerial(Rok, Miesiac + 1, 0).Day
            For iC = 1 To iDzien
                If Swieta.IndexOf(String.Concat(" ", CStr(iC)).Substring(0, 2)).Equals(-1) Then
                    iDay = Weekday(DateSerial(Rok, Miesiac, iC), FirstDayOfWeek.Monday)
                    If iDay >= 1 And iDay <= 5 Then
                        DniRobocze += String.Concat("_", Format(DateSerial(Rok, Miesiac, iC).Day, "dd"))
                        iDni += 1
                    End If
                End If
            Next iC
        Catch ex As Exception
            Return 0
        End Try
        Return iDni
    End Function

    ' Wyszukiwanie elementu posortowanej tablicy
    Function fFindRowOfSortTable(ByVal SourceTable(,) As Object, _
                                 ByVal Column2Search As Integer, _
                                 ByVal ToSearch As Object) As Integer

        Dim iMax As Integer = SourceTable.GetUpperBound(0)
        Dim iR As Integer
        Dim iH As Integer = iMax \ 2
        Dim iMin As Integer = 0
        Dim strSearch As String = ToSearch.ToString
        Dim iFound As Integer = -1
        Try
            For iR = 0 To 4
                Select Case SourceTable(iH, Column2Search).ToString
                    Case strSearch
                        iFound = iH
                        Exit Try
                    Case Is > strSearch
                        iMax = iH
                        iH -= (iH - iMin) \ 2
                    Case Else
                        iMin = iH
                        iH += (iMax - iH) \ 2
                End Select
            Next iR
            If SourceTable(iH, Column2Search).ToString > strSearch Then
                For iH = iH To iMin Step -1
                    If SourceTable(iH, Column2Search).ToString.Equals(strSearch) _
                        Then iFound = iH : Exit Try
                Next iH
            Else
                For iH = iH To iMax
                    If SourceTable(iH, Column2Search).ToString.Equals(strSearch) _
                        Then iFound = iH : Exit Try
                Next iH
            End If
        Catch ex As Exception
        End Try
        Return iFound
    End Function

    ' Zwraca pełny tekst angielski daty
    Function fFullEnglishDate(Optional ByVal dm As DateTime = Nothing, _
                              Optional ByVal BezSufixu As Boolean = False, _
                              Optional ByVal BezDnia As Boolean = False, _
                              Optional ByVal BezRoku As Boolean = False, _
                              Optional ByVal Trzy As Boolean = False) As Object

        Dim strSufix As String = ""
        Dim aMonth() As String = {"January", "February", "March", "April", _
                                  "May", "June", "July", "August", _
                                  "September", "October", "November", "December"}
        Dim _Ret As String = ""
        Try
            Dim txt As String = aMonth(dm.Month - 1)
            If Not BezDnia Then
                _Ret = dm.Day.ToString + " "
                If Not BezSufixu Then
                    Select Case dm.Day
                        Case 1
                            strSufix = "st"
                        Case 2
                            strSufix = "nd"
                        Case 3
                            strSufix = "rd"
                        Case Else
                            strSufix = "th"
                    End Select
                    _Ret += strSufix
                End If
            End If
            If Trzy Then
                _Ret += txt.Substring(0, 3)
            Else
                _Ret += txt
            End If
            If Not BezRoku Then _Ret += " " & dm.Year
        Catch ex As Exception
        End Try
        Return ""
    End Function

    Function fFullPolishDate(ByVal dm As Date, _
                              Optional ByVal BezDnia As Boolean = False, _
                              Optional ByVal BezRoku As Boolean = False, _
                              Optional ByVal Trzy As Boolean = False) As String

        Dim _Ret As String = ""
        Dim txt As String = Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dm.Month)
        Try
            If Not BezDnia Then _Ret = dm.Day.ToString + " "
            If Trzy Then
                _Ret += txt.Substring(0, 3)
            Else
                _Ret += txt
            End If
            If Not BezRoku Then _Ret += " " & dm.Year
        Catch ex As Exception
        End Try
        Return _Ret
    End Function

    ' Zwraca datę ostatniej modyfikacji pliku
    Function fGetFileDateModif(ByVal FileSpec As String) As DateTime

        Dim fsi As FileInfo = My.Computer.FileSystem.GetFileInfo(FileSpec)
        Return fsi.LastWriteTime.Date
    End Function

    ' Zwraca numer nadanego autoincrement ID
    Function fGetLastID(ByVal Polacznie As OleDbConnection, _
                        ByVal Tabela As String, _
                        ByVal Id As String) As Integer

        Dim cmd As OleDbCommand = New OleDbCommand("SELECT TOP 1 * FROM " + Tabela + " ORDER BY " + Id + " DESC", Polacznie)
        Dim value As Integer = -1
        With cmd
            .Connection.Open()
            value = CInt(.ExecuteScalar.ToString)
            .Connection.Close()
        End With
        cmd = Nothing
        Return value
    End Function

    ' Zwraca datę z nazwy pliku
    Function fGetYearMonth(ByVal strFile As String, _
                           Optional ByVal strSeparator As String = " ", _
                           Optional ByVal blnFullDate As Boolean = False, _
                           Optional ByVal intRokInSplit As Integer = 0) As String

        Dim X() As String
        Dim iC As Integer
        Dim dtm As DateTime
        Try
            X = Split(strFile, strSeparator)
            If intRokInSplit > 0 Then
                ' wydzielenie tylko roku
                Return X(intRokInSplit - 1).Substring(0, 4)
            End If
            For iC = 0 To X.GetUpperBound(0)
                If X(iC).IndexOf("-") > -1 Then
                    X = Split(X(iC), "-")
                    If X.GetUpperBound(0) > 1 AndAlso blnFullDate Then
                        dtm = DateSerial(CInt(X(0)), CInt(X(1)), CInt(X(2).Substring(0, 2)))
                        Return dtm.Date.ToString.Substring(0, 10)
                    Else
                        dtm = DateSerial(CInt(X(0)), CInt(X(1).Substring(0, 2)), 1)
                        Return dtm.Date.ToString.Substring(0, 7)
                    End If
                    Exit Try
                End If
            Next iC
        Catch ex As Exception
        End Try
        Return ""
    End Function

    ' Odczytuje dane z pliku HTML, wkleja do Excela i zwraca dane tabelaryczne
    Function fHTMLToObjectViaExcel(ByVal HTMLFile As String, _
                                   ByRef Tabela As Object, _
                                   Optional ByVal ShowMessage As Boolean = False, _
                                   Optional ByVal CompareDate As Boolean = False) As Boolean

        Dim m_Return As Boolean = False
        Dim sr As StreamReader
        Dim m_HTML As String
        Dim oData As New DataObject
        Dim oRng As XL.Range
        Dim i As Integer
        Try
            If Not My.Computer.FileSystem.FileExists(HTMLFile) _
                Then Throw New ArgumentException("Plik [" + HTMLFile + "] nie istnieje")
            If CompareDate AndAlso Not fCompareFileDate(HTMLFile, Today, True, , , True) _
                Then Throw New ArgumentException("Plik [" + HTMLFile + "] utworzony wcześniej")
            sr = New StreamReader(HTMLFile)
            m_HTML = sr.ReadToEnd
            oData.SetData(DataFormats.Text, m_HTML)
            Clipboard.SetDataObject(oData)
            oApp = New XL.Application
            oBook = oApp.Workbooks.Add()
            oSheet = CType(oBook.Sheets(1), XL._Worksheet)
            oSheet.Range("A1").Select()
            oSheet.Paste()
            oBook.Saved = True
            ' ustalenie obszaru
            i = fLastRow(oApp)
            oRng = oSheet.Range("B" + i.ToString).CurrentRegion
            ReDim Tabela(0 To oRng.Rows.Count - 1, oRng.Columns.Count - 1)
            Tabela = oRng.Value2
            m_Return = True
        Catch ex As Exception
            If ShowMessage Then _
                MessageBox.Show(ex.Message, "Błąd odczytu danych HTML", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            oData = Nothing
            sr = Nothing
            oRng = Nothing
            XlsClean()
        End Try
        Return m_Return
    End Function

    ' Sprawdzenie układu kolumn arkusza danych w DataTable
    Function fSourceLayoutControl(ByVal dt As DataTable, ByVal Kontrolny As String, ByVal cns As OleDbConnection) As Boolean
        ' Source (nazwa pliku źródłowego),
        ' NrKol (kolumna pliku źródłowego)
        ' KolPodstawowa (bez spacji i #; nazwy używane przez program)
        ' Wariant1 - alternatywna, kontrolna nazwa
        ' Wariant2 - jw.
        ' Wariant3 - jw.
        Dim i, j, iC As Integer
        Dim dtT As New DataTable
        Dim dvT As New DataView
        Dim da As OleDbDataAdapter = New OleDbDataAdapter("SELECT * FROM tblSources WHERE Source = '" + Kontrolny + "' ORDER BY NrKol", cns)
        da.Fill(dtT)
        dvT = dtT.DefaultView
        Dim aCol(0 To dtT.Rows.Count - 1) As String
        Dim txt As String
        Try
            ' porównanie kolumn
            For iC = 0 To dtT.Rows.Count - 1
                For i = 0 To dt.Columns.Count - 1
                    txt = dt.Columns(i).ColumnName.ToString.Replace("#", "").Replace(" ", "").ToUpper
                    If dtT.Rows(iC)("KolPodstawowa").ToString.ToUpper.Equals(txt) Then
                        If Not (CInt(dtT.Rows(iC)("NrKol").ToString) - 1).Equals(i) Then
                            Throw New ArgumentException("Zmieniony układ kolumn")
                        Else
                            dt.Columns(i).ColumnName = dtT.Rows(iC)("KolPodstawowa").ToString
                            aCol(iC) = txt
                            GoTo Dalej
                        End If
                    Else
                        ' przeszukaj pomocnicze kolumny
                        For j = 1 To 3
                            If dtT.Rows(iC)(j + 2).ToString.ToUpper.Equals(txt) Then
                                If Not (CInt(dtT.Rows(iC)("NrKol").ToString) - 1).Equals(i) Then
                                    Throw New ArgumentException("Zmieniony układ kolumn")
                                Else
                                    dt.Columns(i).ColumnName = dtT.Rows(iC)("KolPodstawowa").ToString
                                    aCol(iC) = txt
                                    GoTo Dalej
                                End If
                            End If
                        Next j
                    End If
                Next i
Dalej:
            Next iC
            For iC = 0 To aCol.GetUpperBound(0)
                If String.IsNullOrEmpty(aCol(iC)) Then
                    Throw New ArgumentException("Układ kolumn niezgodny z założeniami")
                End If
            Next iC
            Return True
        Catch ex As Exception
            MessageBox.Show(ex.Message, Kontrolny, MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return False
        Finally
            dtT = Nothing
            dvT = Nothing
            da = Nothing
        End Try
    End Function

    ' Konwersja str - znak na końcu na double
    Function fStr2Dbl(ByVal strLiczba As String) As Double

        Try
            Dim strDbl As String = strLiczba.Replace(Chr(32), "").Replace(Chr(160), "")
            If strDbl.EndsWith("-") Then _
                strDbl = String.Concat("-", strDbl.Replace("-", ""))
            Return CDbl(strDbl)
        Catch ex As Exception
            Return 0
        End Try
    End Function

    ' Konwersja str - znak na końcu na integer
    Function fStr2Int(ByVal strLiczba As String) As Integer

        Try
            Dim strDbl As String = strLiczba.Replace(" ", "")
            If strDbl.EndsWith("-") Then _
                strDbl = String.Concat("-", strDbl.Replace("-", ""))
            Return CInt(strDbl)
        Catch ex As Exception
            Return 0
        End Try
    End Function

    ' Czy ten sam miesiąc po dodaniu dni
    Function fTenSamMiesiac(ByVal dmDate As DateTime, _
                            ByVal Dni As Integer, _
                            Optional ByRef Dzien As String = "") As Boolean

        Try
            Dim dm As Date = dmDate.AddDays(Dni)
            Dzien = Format(dm.Day, "dd")
            If dmDate.Year.Equals(dm.Year) Then Return dmDate.Month.Equals(dm.Month)
        Catch ex As Exception
        End Try
        Return False
    End Function

    ' Wybór pliku Excela
    Function fWybierzPlik(ByVal Szablon As String) As String

        Dim ofd As New OpenFileDialog
        Dim di As New DirectoryInfo(Szablon)
        Try
            With ofd
                .InitialDirectory = di.FullName
                .Filter = "Pliki Excel (*.xl*)|*.xl*"
                .Multiselect = False
                .ReadOnlyChecked = True
                If .ShowDialog().Equals(Windows.Forms.DialogResult.OK) Then _
                    Return .FileName
            End With
        Catch ex As Exception
        End Try
        Return ""
    End Function

    ' Zastępuje polskie litery
    Function fZastapPolskie(ByVal Tekst As String) As String

        Dim iC, iB As Integer
        Dim newTekst As String = ""
        Const PODMIANA As String = "ACELNOSZZacelnoszz"
        Const ORYGINAL As String = "ĄĆĘŁŃÓŚŹŻąćęłńóśźż"
        Try
            For iC = 0 To Tekst.Length - 1
                iB = ORYGINAL.IndexOf(Tekst.Chars(iC))
                If iB.Equals(-1) Then
                    newTekst += Tekst.Chars(iC)
                Else
                    newTekst += PODMIANA.Chars(iB)
                End If
            Next iC
        Catch ex As Exception
        End Try
        Return newTekst
    End Function

#Region "Aktualizacja wersji aplikacji"

    Sub pCheckVersion(Optional ByVal Publikacja As String = "", _
                      Optional ByVal KopiaZFolderu As String = "")

        Dim m_Assem As Assembly
        Dim m_App, m_Path As String
        Dim m_Date As DateTime
        Try
            m_Assem = Assembly.GetExecutingAssembly()
            m_Path = m_Assem.Location.ToString
            m_Date = My.Computer.FileSystem.GetFileInfo(m_Path).LastWriteTime
            With m_Path
                m_App = .Substring(.LastIndexOf(Path.DirectorySeparatorChar) + 1).Replace(".exe", ".application")
            End With
            If String.IsNullOrEmpty(Publikacja) Then
                Publikacja = ApplicationDeployment.CurrentDeployment.UpdateLocation.ToString
                With Publikacja
                    Publikacja = .Substring(.IndexOf("///") + 3).Replace("/", Path.DirectorySeparatorChar)
                End With
                Publikacja = Publikacja.Replace(Path.DirectorySeparatorChar + m_App, "")
            Else
                With Publikacja
                    If .EndsWith(Path.DirectorySeparatorChar) Then _
                        Publikacja = .Substring(0, .Length - 1)
                End With
            End If
            If Not String.IsNullOrEmpty(KopiaZFolderu) Then
                With KopiaZFolderu
                    If .EndsWith(Path.DirectorySeparatorChar) Then _
                        KopiaZFolderu = .Substring(0, .Length - 1)
                End With
                If fCompareFileDate(KopiaZFolderu + Path.DirectorySeparatorChar + m_App, m_Date, , True) Then Exit Try
                ' kopiowanie
                Dim tsource As Object
                Dim oSource As FileInfo
                Dim i As Integer
                Dim m_Folder, m_Tmp As String
                Try
                    With My.Computer.FileSystem
                        ' usuń wszystkie pliki i podfoldery z folderu docelowego
                        ' za wyjątkiem instalacyjnych
                        tsource = .GetFiles(Publikacja, FileIO.SearchOption.SearchAllSubDirectories)
                        For i = 0 To tsource.Count - 1
                            oSource = .GetFileInfo(tsource(i).ToString)
                            If oSource.FullName.IndexOf("dotnet") < 0 AndAlso oSource.FullName.IndexOf("windowsinstaller") < 0 Then
                                .DeleteFile(oSource.FullName)
                                Try
                                    If .DirectoryExists(oSource.DirectoryName) Then
                                        .DeleteDirectory(oSource.DirectoryName, FileIO.DeleteDirectoryOption.ThrowIfDirectoryNonEmpty)
                                    End If
                                Catch ex As Exception
                                End Try
                            End If
                        Next i
                        tsource = .GetFiles(KopiaZFolderu, FileIO.SearchOption.SearchAllSubDirectories)
                        For i = 0 To tsource.Count - 1
                            oSource = .GetFileInfo(tsource(i).ToString)
                            If oSource.FullName.IndexOf("dotnet") < 0 AndAlso oSource.FullName.IndexOf("windowsinstaller") < 0 Then
                                m_Folder = oSource.DirectoryName.Replace(KopiaZFolderu, Publikacja)
                                If Not .DirectoryExists(m_Folder) Then .CreateDirectory(m_Folder)
                                m_Tmp = m_Folder + Path.DirectorySeparatorChar + oSource.Name
                                If .FileExists(m_Tmp) Then .DeleteFile(m_Tmp)
                                oSource.CopyTo(m_Tmp)
                            End If
                        Next i
                    End With
                Catch ex As Exception
                    Throw New ArgumentException("Kopiowanie plików" + vbCrLf + ex.Message)
                Finally
                    tsource = Nothing
                    oSource = Nothing
                End Try
            Else
                If fCompareFileDate(Publikacja + Path.DirectorySeparatorChar + m_App, m_Date, , True) Then Exit Try
            End If
            InstallUpdateSyncWithInfo(Publikacja)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Kontrola wersji programu", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            m_Assem = Nothing
        End Try
    End Sub

    Private Sub InstallUpdateSyncWithInfo(ByVal SourcePath As String)

        Dim info As UpdateCheckInfo = Nothing
        If (ApplicationDeployment.IsNetworkDeployed) Then
            Dim AD As ApplicationDeployment = ApplicationDeployment.CurrentDeployment
            Try
                info = AD.CheckForDetailedUpdate()
            Catch dde As DeploymentDownloadException
                MessageBox.Show("Nowa wersja aplikacji nie może być teraz pobrana." + vbCrLf + _
                                "Sprawdź dostęp do folderu aktualizacji lub spróbuj później. Błąd: " + dde.Message, _
                                SourcePath, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Return
            Catch ioe As InvalidOperationException
                MessageBox.Show("Aplikacja nie może być aktualizowana. Błąd: " + ioe.Message, SourcePath, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Return
            End Try
            If (info.UpdateAvailable) Then
                Dim doUpdate = True
                If (Not info.IsUpdateRequired) Then
                    Dim dr As DialogResult = MessageBox.Show("Dostępna nowa wersja aplikacji." + vbCrLf _
                                                             + "Aktualizujesz teraz?", SourcePath, MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                    doUpdate = System.Windows.Forms.DialogResult.OK.Equals(dr)
                Else
                    MessageBox.Show("Aplikacja wykryła obowiązkową aktualizację do nowszej wersji." + vbCrLf _
                        + "Nastąpi instalacja nowszej wersji.", SourcePath, MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
                If (doUpdate) Then
                    Try
                        AD.Update()
                        MessageBox.Show("Aktualizacja zakończona pomyślnie. Aplikacja zostanie ponownie uruchomiona." + vbCrLf _
                                        + "Jeśli nie nastąpi to w ciągu kilkunastu sekund - uruchom ręcznie", SourcePath, MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Application.Restart()
                    Catch dde As DeploymentDownloadException
                        MessageBox.Show("Aktualizacja do nowszej wersji nie powiodła się." + vbCrLf + "Sprawdź dostęp lub spróbuj później.", SourcePath, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Return
                    End Try
                End If
            End If
        End If
    End Sub

#End Region ' Aktualizacja wersji aplikacji

#End Region ' Metody ogólne

#Region "Metody Excel- a"

    ' Zwraca literę(y) oznaczające kolumnę
    Function fGetColumnLetter(ByVal Kolumna As Integer) As String

        Dim div As Integer = Kolumna
        Dim _Ret As String = String.Empty
        Dim _Mod As Integer = 0
        While div > 0
            _Mod = (div - 1) Mod 26
            _Ret = Chr(65 + _Mod)
            div = CInt((div - _Mod) \ 26)
        End While
        Return _Ret
    End Function

    'Bezpośredni odczyt danych ze skoroszytu
    Function fGetData2Array(ByVal Plik As String, _
                            ByVal Arkusz As String, _
                            Optional ByVal Obszar As String = "", _
                            Optional ByVal Haslo As String = "", _
                            Optional ByVal NazwaObszaru As String = "") As Array

        ' !!! Array zaczyna się od 1 w obu wymiarach !!!
        ' liczby jako double (0.0)
        Dim aDane As Array = Nothing
        Dim _Obszar As String
        Dim _Row As Integer
        Try
            oApp = New XL.Application()
            oBook = oApp.Workbooks.Open(Filename:=Plik, UpdateLinks:=False, ReadOnly:=True)
            With oBook
                Try
                    oSheet = CType(.Sheets(Arkusz), XL._Worksheet)
                Catch ex As Exception
                    Throw New ArgumentException("Arkusz [" + Arkusz + "] nie istnieje")
                End Try
                With oSheet
                    If Not String.IsNullOrEmpty(Haslo) Then .Unprotect(Haslo)
                    If Not String.IsNullOrEmpty(NazwaObszaru) Then
                        aDane = CType(CType(.Range(NazwaObszaru), XL.Range).Value, Array)
                    ElseIf String.IsNullOrEmpty(Obszar) Then
                        aDane = CType(CType(.Range("A1").CurrentRegion, XL.Range).Value, Array)
                    ElseIf Obszar.IndexOf(":").Equals(-1) Then
                        Obszar += ":" + fLastAddress(oApp, Arkusz)
                        aDane = CType(CType(.Range(Obszar), XL.Range).Value, Array)
                    ElseIf "0123456789".IndexOf(Obszar.Substring(Obszar.Length - 1)) < 0 Then
                        _Obszar = Obszar.Substring(0, Obszar.IndexOf(":"))
                        _Row = CType(.Range(_Obszar), XL.Range).Row
                        _Obszar = Obszar + _Row.ToString
                        Obszar &= CType(.Range(_Obszar).CurrentRegion, XL.Range).Rows.Count + _Row - 2
                        aDane = CType(CType(.Range(Obszar), XL.Range).Value, Array)
                    Else
                        aDane = CType(CType(.Range(Obszar), XL.Range).Value, Array)
                    End If
                    If Not String.IsNullOrEmpty(Haslo) Then .Protect(Haslo)
                End With
            End With
        Catch ex As Exception
            MessageBox.Show(ex.Message + vbCrLf + "Plik: " + Plik + vbCrLf + " Obszar (domyślnie A1 z otoczeniem) [" + Obszar + NazwaObszaru + "]", "Odczyt danych z Excela", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            oBook.Saved = True
            XlsClean()
        End Try
        Return aDane
    End Function

#Region "Kontrola plików wzorcowych i wynikowych"

    ' Wymaga istnienia ustawienia nazw folderów My.Settings.Wzorzec i .Wyniki
    Function fCreateResultFile(ByVal Wzorzec As String, _
                              ByVal Utworz As Boolean, _
                              Optional ByVal Okres As String = "", _
                              Optional ByVal Wynikowy As String = "", _
                              Optional ByVal ExtOfFile As String = "") As String

        Dim m_Ext As String = fCheckWzorzec(Wzorzec, ExtOfFile)
        Dim m_File As String
        Dim m_Path As String = fCheckWyniki()
        Dim m_Data1, m_Data2 As DateTime
        Const CO_NAG As String = "Kontrola pliku wynikowego"
        Try
            If String.IsNullOrEmpty(m_Ext) Then Throw New ArgumentException("Nieokreślona lokalizacja wzorca [" + Wzorzec + "]")
            If Not m_Path.EndsWith(Path.DirectorySeparatorChar) Then m_Path += Path.DirectorySeparatorChar
            If String.IsNullOrEmpty(Okres) Then
                If String.IsNullOrEmpty(Wynikowy) Then
                    m_File = Wzorzec + m_Ext
                Else
                    m_File = Wynikowy + m_Ext
                End If
            Else
                m_File = Wzorzec + "_" + Okres + m_Ext
            End If
            m_Data1 = My.Computer.FileSystem.GetFileInfo(My.Settings.Wzorce + Wzorzec + m_Ext).LastWriteTime
            If My.Computer.FileSystem.FileExists(m_Path + m_File) Then
                ' porównaj datę utworzenia do pliku oryginalnego
                m_Data2 = My.Computer.FileSystem.GetFileInfo(m_Path + m_File).CreationTime
                If DateTime.Compare(m_Data1, m_Data2) <= 0 Then
                    Return m_Path + m_File
                End If
            End If
            If Not Utworz Then Throw New ArgumentException("Plik [" + m_File + "] nie istnieje w lokalizacji " + m_Path)
            My.Computer.FileSystem.CopyFile(My.Settings.Wzorce + Wzorzec + m_Ext, m_Path + m_File, True)
            File.SetCreationTime(m_Path + m_File, m_Data1)
            If Not My.Computer.FileSystem.FileExists(m_Path + m_File) Then _
                Throw New ArgumentException("Błąd utworzenia pliku [" + m_File + "]")
            Return m_Path + m_File
        Catch ex As Exception
            MessageBox.Show(ex.Message, CO_NAG, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return ""
        End Try
    End Function

    Function fCheckWyniki() As String

        Try
            With My.Settings
                If String.IsNullOrEmpty(.Wyniki.Trim) OrElse Not fCheckFolder(.Wyniki) Then
                    Dim fbd As New FolderBrowserDialog
                    With fbd
                        .RootFolder = Environment.SpecialFolder.MyComputer
                        .SelectedPath = My.Settings.Wyniki
                        If .ShowDialog().Equals(Windows.Forms.DialogResult.OK) Then
                            My.Settings.Wyniki = .SelectedPath + Path.DirectorySeparatorChar
                            My.Settings.Save()
                        Else
                            Throw New ArgumentException
                        End If
                    End With
                End If
                Return .Wyniki
            End With
        Catch ex As Exception
            MessageBox.Show("Błąd utworzenia katalogu danych wynikowych", "Zapisy w C:\", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return "C:\"
        End Try
    End Function

    Function fCheckWzorzec(ByVal Nazwa As String, Optional ByVal ExtOfFile As String = "") As String

        Dim m_File As String
        Dim m_Ret As String = ""
        Try
            oApp = New XL.Application()
            If String.IsNullOrEmpty(ExtOfFile) Then
                ExtOfFile = ".xlsm"
                If Val(oApp.Application.Version) < 12 Then ExtOfFile = ".xls"
            End If
            oApp = Nothing
            With My.Settings
                If Not .Wzorce.EndsWith(Path.DirectorySeparatorChar) Then
                    .Wzorce += Path.DirectorySeparatorChar
                End If
                m_File = Nazwa + ExtOfFile
                If String.IsNullOrEmpty(.Wzorce.Trim) OrElse Not My.Computer.FileSystem.FileExists(.Wzorce + m_File) Then
                    If My.Computer.FileSystem.FileExists(.Wzorce + m_File.Replace("xlsm", "xlsx")) Then
                        m_File = m_File.Replace("xlsm", "xlsx")
                        GoTo PoWyborze
                    End If
                    Dim opf As New OpenFileDialog
                    With opf
                        .Multiselect = False
                        .InitialDirectory = My.Settings.Wzorce
                        .FileName = m_File
                        .Filter = "Plik wzorcowy (*.xl*)|*.xl*"
                        If .ShowDialog().Equals(Windows.Forms.DialogResult.OK) Then
                            My.Settings.Wzorce = .FileName.Replace(.SafeFileName, "")
                            My.Settings.Save()
                            m_File = .SafeFileName
                        Else
                            Throw New ArgumentException
                        End If
                    End With
                End If
            End With
PoWyborze:
            m_Ret = m_File.Substring(m_File.LastIndexOf("."))
        Catch ex As Exception
        Finally
            pReleaseObject(oApp)
        End Try
        Return m_Ret
    End Function

#End Region ' Kontrola plików wzorcowych i wynikowych

    Private Sub AktywnyArkusz(ByVal sender As Object, _
                              Optional ByVal NazwaArkusza As String = "", _
                              Optional ByVal Skoroszyt As String = "")

        If String.IsNullOrEmpty(Skoroszyt) Then
            oBook = sender.ActiveWorkbook
        Else
            oBook = sender.Workbooks(Skoroszyt)
        End If
        If String.IsNullOrEmpty(NazwaArkusza) Then
            oSheet = CType(oBook.ActiveSheet, XL._Worksheet)
        Else
            oSheet = CType(oBook.Worksheets(NazwaArkusza), XL._Worksheet)
        End If
    End Sub

    Function fLastRow(ByVal sender As Object, _
                      Optional ByVal Arkusz As String = "", _
                      Optional ByVal StartRow As Integer = 1) As Integer

        Dim i, j As Integer
        oApp = CType(sender, XL._Application)
        AktywnyArkusz(sender, Arkusz)
        With oSheet
            i = .UsedRange.Row - 1 + .UsedRange.Rows.Count
            j = i
            Do Until Not oApp.WorksheetFunction.CountA(.Rows(j)).Equals(0)
                j -= 1
                If j.Equals(0) Then Exit Do
            Loop
            i = j
            If StartRow > 0 Then _
              If i < StartRow Then i = StartRow
        End With
        Return i
    End Function

    Function fLastAddress(ByVal sender As Object, _
                          Optional ByVal Arkusz As String = "", _
                          Optional ByRef Wiersz As Integer = 1, _
                          Optional ByRef Kolumna As Integer = 1, _
                          Optional ByVal StartRow As Integer = 1) As String

        Dim iRow, iR, iCol, iC As Integer
        oApp = CType(sender, XL._Application)
        AktywnyArkusz(sender, Arkusz)
        With oSheet
            Try
                iRow = .UsedRange.Row - 1 + .UsedRange.Rows.Count
                iCol = .UsedRange.Column - 1 + .UsedRange.Columns.Count
                iR = iRow
                Do Until Not oApp.WorksheetFunction.CountA(.Rows(iR)).Equals(0)
                    iR -= 1
                Loop
                Wiersz = iR
                iC = iCol
                Do Until Not oApp.WorksheetFunction.CountA(.Columns(iC)).Equals(0)
                    iC -= 1
                Loop
                Kolumna = iC
                If Not Wiersz.Equals(0) AndAlso Not Kolumna.Equals(0) Then
                    If StartRow > 0 Then _
                      If Wiersz < StartRow Then Wiersz = StartRow
                    Return .Cells(Wiersz, Kolumna).Address
                Else
                    Return ""
                End If
            Catch ex As Exception
                Return ""
            Finally
                oSheet = Nothing
            End Try
        End With
    End Function

    Function fLastColumn(ByVal sender As Object, _
                         Optional ByVal Arkusz As String = "", _
                         Optional ByVal StartCol As Integer = 1) As Integer

        Dim i, j As Integer
        AktywnyArkusz(sender, Arkusz)
        With oSheet
            i = .UsedRange.Column - 1 + .UsedRange.Columns.Count
            j = i
            Do Until Not oApp.WorksheetFunction.CountA(.Columns(j)).Equals(0)
                j -= 1
                If j.Equals(0) Then Exit Do
            Loop
            i = j
            If StartCol > 0 Then _
              If i < StartCol Then i = StartCol
        End With
        oSheet = Nothing
        Return i
    End Function

    Function fNewRow(ByVal sender As Object, _
                     ByVal Start As String, _
                     Optional ByVal Arkusz As String = "") As Integer

        Dim iC As Integer
        AktywnyArkusz(sender, Arkusz)
        With oSheet
            iC = .Range(Start).Column
            fNewRow = .Cells(.Cells.Rows.Count, iC).End(oApp.xlUp).Row + 1
        End With
        oSheet = Nothing
    End Function

    Function fSheetExist(ByVal sender As Object, _
                         ByVal Nazwa As String, _
                         Optional ByVal Usun As Boolean = False, _
                         Optional ByVal Dodaj As Boolean = False) As Boolean

        Try
            oApp = CType(sender, XL._Application)
            oBook = oApp.ActiveWorkbook
            oSheet = oBook.Sheets(Nazwa)
            Try
                m_Usun = Usun
                m_Dodaj = Dodaj
                If m_Usun Then sender.ActiveWindow.SelectedSheets.Delete()
                If m_Dodaj Then
                    With oBook
                        .Sheets.Add(Before:=.Worksheets(1))
                        .ActiveSheet.name = Nazwa
                    End With
                End If
            Catch ex As Exception
                MessageBox.Show(ex.Message, Nazwa, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function

    Sub pAddRow2Range(ByRef Obszar As XL.Range)

        With Obszar
            With .Rows(.Rows.Count)
                .EntireRow.Copy()
                .Insert(Shift:=oApp.xlDown)
            End With
        End With
    End Sub

    ' Usuwanie/wyświetlanie linków
    Private Sub LinksRemove()

        pBreakLinks()
    End Sub

    Private Sub LinksShow()

        pBreakLinks(DoArkusza:="Tmp")
    End Sub

    Sub pBreakLinks(Optional ByVal Pokaz As Boolean = True, _
                    Optional ByVal DoArkusza As String = "", _
                    Optional ByVal Skoroszyt As XL._Workbook = Nothing)

        Dim aLinks As Object
        Dim strLink As Object
        Dim blnJestArkusz As Boolean
        Try
            blnJestArkusz = DoArkusza <> ""
            If Skoroszyt Is Nothing Then _
                  Skoroszyt = oApp.ThisWorkbook
            If blnJestArkusz Then
                oSheet = Skoroszyt.Sheets(DoArkusza)
                oSheet.Cells.ClearContents()
            End If
            aLinks = oApp.ActiveWorkbook.LinkSources(Type:=oApp.xlLinkTypeExcelLinks)
            If aLinks Is Nothing Then Exit Try
            pScreenRefresh(False, "Sprawdzam kolekcję łączy...")
            For Each strLink In aLinks
                If blnJestArkusz Then
                    oSheet.Rows(1).Insert(Shift:=oApp.xlUp)
                    oSheet.Cells(1, 1) = strLink
                Else
                    If Pokaz Then
                        If MessageBox.Show(String.Concat(strLink, vbCrLf, "Usunąć?"), "Usuwanie łączy", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = vbNo _
                          Then GoTo Odpusc
                    End If
                    oApp.ActiveWorkbook.BreakLink(Name:=strLink, _
                            Type:=oApp.xlLinkTypeExcelLinks)
                End If
Odpusc:
            Next strLink
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Edycja łączy", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Sub pClearRange(ByVal Adres As String, _
                    Optional ByVal Arkusz As String = "", _
                    Optional ByVal Zeszyt As String = "")

        Try
            AktywnyArkusz(Arkusz, Zeszyt)
            oSheet.Range(Adres). _
                  SpecialCells(oApp.xlCellTypeConstants).ClearContents()
        Catch ex As Exception
        End Try
    End Sub

    Sub pFilterStore(ByVal Arkusz As String, _
                     ByRef aFilter(,) As Object, _
                     ByRef strFilter As String)

        Dim iC As Integer
        AktywnyArkusz(Arkusz)
        strFilter = ""
        If Not oSheet.AutoFilterMode Then GoTo Koniec
        With oSheet.AutoFilter
            strFilter = .Range.Address
            With .Filters
                ReDim aFilter(0 To .Count - 1, 0 To 2)
                For iC = 1 To .Count
                    With .Item(iC)
                        If .On Then
                            aFilter(iC - 1, 0) = .Criteria1
                            If .Operator Then _
                              aFilter(iC - 1, 1) = .Operator : aFilter(iC - 1, 2) = .Criteria2
                        End If
                    End With
                Next iC
            End With
        End With
        oSheet.AutoFilterMode = False
Koniec:
        oSheet = Nothing
    End Sub

    Sub pFilterRestore(ByVal Arkusz As String, _
                       ByRef aFilter(,) As Object, _
                       ByRef strFilter As String)

        Dim iC As Integer
        If String.IsNullOrEmpty(strFilter) Then Exit Sub
        AktywnyArkusz(Arkusz)
        With oSheet
            .AutoFilterMode = False
            .Range(strFilter).AutoFilter()
            For iC = 0 To aFilter.GetUpperBound(0)
                If Not aFilter(iC, 0) Is Nothing Then
                    If Not aFilter(iC, 2) Is Nothing Then
                        .Range(strFilter).AutoFilter(Field:=iC + 1, _
                                Criteria1:=aFilter(iC, 0), _
                                Operator:=aFilter(iC, 1), _
                                Criteria2:=aFilter(iC, 2))
                    Else
                        .Range(strFilter).AutoFilter(Field:=iC + 1, _
                                Criteria1:=aFilter(iC, 0))
                    End If
                End If
            Next iC
        End With
        oSheet = Nothing
    End Sub

    Sub pFindSettings(Optional ByVal Restore As Boolean = False)

        Dim rng As XL.Range
        Dim rg1 As XL.Range
        Dim fs As XL.Range
        Const strKey As String = "krzyś"
        Static aParam(0 To 3) As Object
        Try
            oBook = oApp.ThisWorkbook
            If Restore Then GoTo Finish
            pScreenRefresh(Odswiez:=False)
            oSheet = oBook.Worksheets.Add()
            rng = oSheet.Range("A1:A2")
            rg1 = rng(1, 1)
            ' Określenie komentarz/formuła/wartość
            rg1.AddComment(strKey)
            fs = rng.Find(What:=strKey)
            If Not fs Is Nothing Then
                aParam(1) = oApp.xlComments
            Else
                With rg1
                    .Comment.Delete()
                    .Value = strKey
                    .EntireRow.Hidden = True
                End With
                fs = rng.Find(What:=strKey)
                If Not fs Is Nothing Then
                    aParam(1) = oApp.xlFormulas
                Else
                    aParam(1) = oApp.xlValues
                End If
            End If
            ' Określenie część/całość
            With rg1
                .Value = strKey
                .EntireRow.Hidden = False
            End With
            fs = rng.Find(What:=Right(strKey, 1), LookIn:=oApp.xlValues)
            If fs Is Nothing Then
                aParam(2) = oApp.xlWhole
            Else
                aParam(2) = oApp.xlPart
            End If
            ' Określenie wiersz/kolumna
            With rng
                rng = .Resize(, 2)
                .ClearContents()
                .Cells(1, 2) = strKey
                .Cells(2, 1) = strKey
            End With
            fs = rng.Find(What:=strKey, LookIn:=oApp.xlValues)
            If fs.Address.Equals(rng(1, 2).Address) Then
                aParam(3) = oApp.xlByRows
            Else
                aParam(3) = oApp.xlByColumns
            End If
            ' Określenie rozróżnia małe/wielkie litery
            fs = rng.Find(What:=UCase(strKey), LookIn:=oApp.xlValues, SearchOrder:=aParam(2))
            aParam(3) = fs Is Nothing
            oSheet.Delete()
            oSheet = Nothing
            rng = Nothing
            rg1 = Nothing
            pScreenRefresh(True)
Finish:
            fs = oBook.ActiveSheet.Cells.Find(What:="", _
                                              LookIn:=aParam(0), _
                                              LookAt:=aParam(1), _
                                              SearchOrder:=aParam(2), _
                                              MatchCase:=aParam(3))
            fs = Nothing
        Catch ex As Exception
        End Try
    End Sub

    ' Rozciąga kolor na sąsiednie komórki w wierszu w zależności od długości tekstu
    Sub pFormatRozwojowe(ByVal Obszar As XL.Range, _
                         Optional ByVal Kolor As Integer = 0)

        Dim iC As Integer, _
            iK As Integer, _
            strFormula As String
        If Kolor.Equals(0) Then Kolor = 15 ' szary 25%
        For iC = 1 To Obszar.Columns.Count
            With Obszar(1, iC)
                .FormatConditions.Delete()
                If iC.Equals(1) Then
                    strFormula = "=DŁ(WK)>0"
                Else
                    strFormula = "=DŁ(PRZESUNIĘCIE(WK;0;-" & iC - 1 & _
                                 "))>KOMÓRKA(""width"";PRZESUNIĘCIE(WK;0;-" & iC - 1 & "))"
                    If iC > 2 Then
                        For iK = iC - 2 To 1 Step -1
                            strFormula = strFormula & "+KOMÓRKA(""width"";PRZESUNIĘCIE(WK;0;-" & iK & "))"
                        Next iK
                    End If
                End If
                .FormatConditions.Add(Type:=oApp.xlExpression, Formula1:=strFormula)
                .FormatConditions(1).Interior.ColorIndex = Kolor
            End With
        Next iC
    End Sub

    ' Wyświetla/usuwa nazwy
    Private Sub NamesShow()

        pHiddenNames(False, "Tmp")
    End Sub

    Private Sub NamesRemove()

        pHiddenNames(True, "Tmp")
    End Sub

    Sub pHiddenNames(Optional ByVal Usun As Boolean = False, _
                     Optional ByVal NazwaArkusza As String = "")

        Dim obName As XL.Name
        Dim fs, rng As XL.Range
        Dim blnJestArkusz As Boolean
        Try
            blnJestArkusz = NazwaArkusza <> ""
            If blnJestArkusz Then
                AktywnyArkusz(NazwaArkusza)
                oBook.Worksheets(NazwaArkusza).Select()
                If Usun Then GoTo UsunZListy
                oBook.ActiveSheet.Cells.ClearContents()
            End If
            pScreenRefresh(False, "Sprawdzam kolekcję nazw...")
            For Each obName In oApp.ActiveWorkbook.Names
                If Not obName.Visible Then _
                  obName.Visible = True
                If blnJestArkusz Then
                    With oBook.ActiveSheet
                        .Rows(1).Insert(Shift:=oApp.xlUp)
                        .Cells(1, 1) = "x"
                        .Cells(1, 2) = obName.Name
                        .Cells(1, 3) = "'" & .RefersToR1C1
                    End With
                Else
                    If Usun Then obName.Delete()
                End If
            Next obName
            Exit Try
UsunZListy:
            If MessageBox.Show("Masz absolutną pewność, że chcesz usunąć zaznaczone nazwy?", oApp.ThisWorkbook.Name, _
                       MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2).Equals(DialogResult.No) Then Exit Try
            pScreenRefresh(False, "Usuwam zaznaczone nazwy...")
            rng = oBook.ActiveSheet.Range("A1").CurrentRegion
            For Each obName In oBook.Names
                If Not obName.Visible Then obName.Visible = True
                fs = oBook.ActiveSheet.Columns(2).Find(obName.Name, LookAt:=oApp.xlWhole)
                If Not fs Is Nothing Then _
                  If oBook.ActiveSheet.Cells(fs.Row, 1).ToString.ToUpper.Equals("X") Then obName.Delete()
            Next obName
            oBook.ActiveSheet.Cells.ClearContents()
            rng = Nothing
            fs = Nothing
        Catch ex As Exception
            MessageBox.Show(ex.Message, oApp.ThisWorkbook.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            obName = Nothing
            pScreenRefresh(True)
        End Try
    End Sub

    Sub pPodkreslBlad(ByVal Obszar As XL.Range)

        With Obszar
            .Interior.ColorIndex = 3
            .Font.ColorIndex = 2
            .Font.Bold = True
        End With
    End Sub

    ' Wyświetla/zmienia formuły z zaznaczonego obszaru aktywnego arkusza/całego skoroszytu
    Private Sub FormulasShow()

        pScreenRefresh(False)
        pPokazFormuly(TargetWS:="Tmp", NotRC:=True, _
                      TargetWB:=oApp.ThisWorkbook.Name, _
                      SourceWB:=oApp.ActiveWorkbook.Name)
        pScreenRefresh(True)
    End Sub

    Private Sub FormulasChange()

        If MessageBox.Show(String.Concat("Polega na wstawieniu w miejsce istniejącej treści z kol. 4;", vbLf, _
                  "jeśli kol. 4 jest pusta, formuła jest usuwana. Kontynuujesz?"), "Automatyczna zmiana formuł", _
                  MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2).Equals(DialogResult.No) Then Exit Sub
        pScreenRefresh(False)
        pPokazFormuly(TargetWS:="Tmp", Zamien:=True, NotRC:=True, _
                      TargetWB:=oApp.ThisWorkbook.Name, SourceWB:=oApp.ActiveWorkbook.Name)
        pScreenRefresh(True)
    End Sub

    Sub pPokazFormuly(ByVal TargetWS As String, _
                      Optional ByVal Zamien As Boolean = False, _
                      Optional ByVal Zakres As String = "", _
                      Optional ByVal NotRC As Boolean = False, _
                      Optional ByVal TargetWB As String = "", _
                      Optional ByVal SourceWB As String = "", _
                      Optional ByVal SourceWS As String = "")

        Dim ac, rng, rgS As XL.Range
        Dim lng As Integer
        Dim ws, wsA As XL._Worksheet
        Dim strAddr As String
        Dim aPoz(,) As Object
        If String.IsNullOrEmpty(SourceWB) Then SourceWB = oApp.ActiveWorkbook.Name
        If String.IsNullOrEmpty(TargetWB) Then TargetWB = oApp.ThisWorkbook.Name
        ws = oApp.Workbooks(TargetWB).Sheets(TargetWS)
        If Not Zamien Then GoTo Pokaz
        rng = ws.Range("A1").CurrentRegion
        aPoz = ws.Range(rng(1, 1), rng(rng.Rows.Count, 4))
        For lng = LBound(aPoz) To UBound(aPoz)
            wsA = oApp.Workbooks(SourceWB).Sheets(aPoz(lng, 0))
            If String.IsNullOrEmpty(aPoz(lng, 3)) Then wsA.Range(aPoz(lng, 1)).Value = "" : GoTo Nastepny
            If NotRC Then
                wsA.Range(rng(lng, 2)).Formula = aPoz(lng, 3)
            Else
                wsA.Range(rng(lng, 2)).FormulaR1C1 = aPoz(lng, 3)
            End If
Nastepny:
        Next lng
        GoTo Finish
Pokaz:
        On Error Resume Next
        ws.Cells.Clear()
        If String.IsNullOrEmpty(SourceWS) Then
            If String.IsNullOrEmpty(Zakres) Then GoTo WszystkieArkusze
            SourceWS = oApp.Workbooks(SourceWB).ActiveSheet.Name
        End If
        If String.IsNullOrEmpty(Zakres) Then
            rgS = oApp.Workbooks(SourceWB).Sheets(SourceWS).UsedRange
        Else
            rgS = oApp.Workbooks(SourceWB).Sheets(SourceWS).Range(Zakres)
        End If
        For Each ac In rgS.SpecialCells(oApp.xlCellTypeFormulas)
            strAddr = ac.Address
            If String.IsNullOrEmpty(strAddr) Then GoTo Nastepny1
            lng += 1
            ws.Cells(lng, 1) = SourceWS
            ws.Cells(lng, 2) = strAddr
            If NotRC Then
                ws.Cells(lng, 3) = "'" & ac.Formula
            Else
                ws.Cells(lng, 3) = "'" & ac.FormulaR1C1
            End If
            strAddr = ""
Nastepny1:
        Next ac
        GoTo Finish
WszystkieArkusze:
        For Each wsA In oApp.Workbooks(SourceWB).Worksheets
            For Each ac In wsA.Cells.SpecialCells(oApp.xlCellTypeFormulas)
                strAddr = ac.Address
                If String.IsNullOrEmpty(strAddr) Then GoTo Nastepny2
                lng += 1
                ws.Cells(lng, 1) = wsA.Name
                ws.Cells(lng, 2) = strAddr
                If NotRC Then
                    ws.Cells(lng, 3) = "'" & ac.Formula
                Else
                    ws.Cells(lng, 3) = "'" & ac.FormulaR1C1
                End If
                strAddr = ""
Nastepny2:
            Next ac
        Next wsA
Finish:
        ac = Nothing
        rng = Nothing
        rgS = Nothing
        ws = Nothing
        wsA = Nothing
        aPoz = Nothing
    End Sub

    Sub pPreviewPage(ByVal sender As Object, _
                     Optional ByVal Pionowa As Boolean = False)

        Dim strAdr, strAc As String
        Dim lng, iC As Integer
        With oApp.ActiveWindow
            .View = oApp.xlPageBreakPreview
            .DisplayZeros = False
        End With
        AktywnyArkusz(oApp)
        strAc = oApp.ActiveCell.Address()
        With oSheet
            If Pionowa Then
                .PageSetup.Orientation = oApp.xlPortrait
            Else
                .PageSetup.Orientation = oApp.xlLandscape
            End If
            strAdr = fLastAddress(sender, Wiersz:=lng, Kolumna:=iC)
            .Columns(iC + 1).ColumnWidth = 3
            .Rows(lng + 1).RowHeight = 10
            strAdr = .Cells(lng + 1, iC + 1).Address
            .Range(.Cells(1, 1), .Cells(lng + 1, iC + 1)).Select()
            .PageSetup.PrintArea = ""
            Try
                If oApp.ReferenceStyle.Equals(oApp.xlR1C1) Then
                    .PageSetup.PrintArea = "R1C1:R" & lng + 1 & "C" & iC + 1
                Else
                    .PageSetup.PrintArea = "A1:" & strAdr
                End If
                .VPageBreaks(1).DragOff(Direction:=oApp.xlToRight, RegionIndex:=1)
            Catch ex As Exception
            End Try
        End With
        oApp.Range(strAc).Select()
    End Sub

    Sub pRangeDelete(ByVal sender As Object, _
                     Optional ByVal Start As String = "", _
                     Optional ByVal Arkusz As String = "")

        Dim lngFirstRow As Integer
        Dim strAddress As String
        Dim rng As XL.Range
        AktywnyArkusz(Arkusz)
        If String.IsNullOrEmpty(Start) Then Start = "A1"
        With oSheet
            Try
                .AutoFilterMode = False
                lngFirstRow = .Range(Start).Row
                strAddress = fLastAddress(sender, Arkusz:=Arkusz, StartRow:=lngFirstRow)
                rng = .Range(Start & ":" & strAddress)
                rng.EntireRow.Delete()
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Błąd czyszczenia obszaru", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Try
        End With
        rng = Nothing
    End Sub

    ' Zwalnianie zasobów
    Sub pReleaseObject(ByVal obj As Object)

        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub

    Sub pScreenRefresh(ByVal Odswiez As Boolean, _
                       Optional ByVal Komunikat As String = "")

        Static intCalculation As Integer
        Try
            With oApp
                If Odswiez Then
                    .DisplayAlerts = True
                    .Calculation = intCalculation
                    .ScreenUpdating = True
                    .StatusBar = False
                    screenFresh = True
                Else
                    intCalculation = .Calculation
                    .DisplayAlerts = False
                    .Calculation = oApp.xlManual
                    .ScreenUpdating = False
                    If String.IsNullOrEmpty(Komunikat) Then
                        .StatusBar = "Przetwarzanie, czekaj proszę..."
                    Else
                        .StatusBar = Komunikat
                    End If
                    screenFresh = False
                End If
            End With
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Błąd odświeżania ekranu", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub XlsClean()

        pReleaseObject(oSheet)
        Try
            oBook.Close()
        Catch ex As Exception
        End Try
        pReleaseObject(oBook)
        Try
            oApp.Quit()
        Catch ex As Exception
        End Try
        pReleaseObject(oApp)
    End Sub

#End Region ' Metody Excel-a

End Module
