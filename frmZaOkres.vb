﻿Imports System.Windows.Forms

Public Class frmZaOkres

    Private OdDnia As DateTime
    Private DoDnia As DateTime

    Public Property Poczatek() As DateTime
        Get
            Return OdDnia
        End Get
        Set(ByVal value As DateTime)
            OdDnia = value
        End Set
    End Property

    Public Property Koniec() As DateTime
        Get
            Return DoDnia
        End Get
        Set(ByVal value As DateTime)
            DoDnia = value
        End Set
    End Property

    Private Sub OK_Button_Click() Handles OK_Button.Click
        With Me
            If .dtpOd.Value > .dtpDo.Value Then Exit Sub
            .Poczatek = .dtpOd.Value
            .Koniec = .dtpDo.Value
        End With
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub Cancel_Button_Click() Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub frmZaOkres_Load() Handles Me.Load

        With Me

            If OdDnia < CDate("1900-01-01") Then
                OdDnia = Now
                DoDnia = Now
            End If
            If OdDnia = DoDnia Then

                .dtpOd.Value = DateSerial(Year(OdDnia), Month(OdDnia), 1)
                .dtpDo.Value = DateSerial(Year(OdDnia), Month(OdDnia) + 1, 0)

            End If

        End With

    End Sub

End Class
