﻿
Imports System.Data.OleDb
Imports CM = ClHelpFul.CenterMsg
Imports XL = Microsoft.Office.Interop.Excel

Public Class Wieloletnie

#Region "Deklaracje"

    Friend MF As frmMain
    'Private oApp As XL.Application
    'Private oBook As XL._Workbook
    'Private oSheet As XL._Worksheet
    Friend WithEvents StatusDgv As New TextBox
    Private m_Nowe As Boolean

#End Region ' Deklaracje

    Private Sub StatusOfDgv() Handles StatusDgv.TextChanged

        Dim m_RO As Boolean = True
        Try
            With Me
                .tsbNowy.Enabled = True
                .tsbZapisz.Enabled = False
                .tsbZmien.Enabled = False
                .tsbUsun.Enabled = False
                .tsbWykonaj.Enabled = False
                Select Case StatusDgv.Text
                    Case CP.Wybrany.ToString
                        .tsbUsun.Enabled = True
                        .tsbZmien.Enabled = True
                        .tsbWykonaj.Enabled = True
                    Case CP.Nowy.ToString
                        .tsbNowy.Enabled = False
                        .tsbZapisz.Enabled = True
                    Case CP.Zmiana.ToString
                        .tsbNowy.Enabled = False
                        .tsbZapisz.Enabled = True
                End Select
            End With
        Catch ex As Exception
            StatusDgv.Text = CP.Niewybrany.ToString
            Me.Cursor = Cursors.Default
        Finally
            With Me
                m_RO = Not .tsbZapisz.Enabled
                With .txtSQL
                    .ReadOnly = m_RO
                    .BackColor = System.Drawing.SystemColors.Window
                End With
            End With
        End Try
    End Sub

    Private Sub Me_Load() Handles Me.Load

        Try
            MF = frmMain
            With Me
                .dtpOd.Value = MF.dtpOkres.Value
                .dtpDo.Value = Now
                .Location = My.Settings.StartOfWieloletnie
                .Size = My.Settings.scrWieloletnie
                .KeyPreview = True
                CP.Enter4Tlp(.tlp)
                Fresh()
            End With
            WindowState = FormWindowState.Normal
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message, "Zapytania wieloletnie", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Using
            Me.Dispose()
        End Try
    End Sub

    Private Sub Koniec() Handles Me.FormClosing

        With My.Settings
            .scrWieloletnie = Me.Size
            .StartOfWieloletnie = Me.Location
            .Save()
        End With
    End Sub

    Private Sub Wykonaj() Handles tsbWykonaj.Click

        Dim da As OleDbDataAdapter
        Dim dt As New DataTable, dtT As New DataTable
        Dim r As DataRow
        Dim m_Txt As String
        Dim m_cns As New OleDbConnection
        Dim m_Con As String = MF.cns.ConnectionString
        Dim i As Integer
        Dim _Col As String
        Dim Pierwszy As Boolean = True
        Try
            If Me.dtpOd.Value.Year > Me.dtpDo.Value.Year Then _
                Throw New ArgumentException("Błędnie ustawiony zakres")
            m_Txt = Me.txtSQL.Text.Trim
            FillStatusBar(0, "Pobieranie danych...")
            m_cns = MF.cns
            For i = Me.dtpOd.Value.Year To Me.dtpDo.Value.Year
                m_cns.ConnectionString = m_Con.Replace("_" & MF.dtpOkres.Value.Year, "_" & i)
                da = New OleDbDataAdapter(m_Txt, m_cns)
                dt = New DataTable
                da.Fill(dt)
                dt.Columns.Add("Year", GetType(Integer))
                For Each r In dt.Rows
                    r("Year") = i
                Next r
                If Pierwszy Then
                    dtT = dt.Clone
                    Pierwszy = False
                End If
                dtT.Merge(dt)
            Next i
            Dim j As Integer
            Dim aVal(dtT.Rows.Count, dtT.Columns.Count - 1) As Object
            With New Cl4Excel.Excel(Nothing)
                If dtT.Rows.Count > 65000 AndAlso .Wersja < 12 Then
                    Using New CM
                        MessageBox.Show(String.Concat("Możesz mieć problem z Excel-em ({0} zapisów)", dtT.Rows.Count.ToString))
                    End Using
                End If
                Try
                    For Each c As DataColumn In dtT.Columns
                        aVal(0, j) = c.ColumnName
                        j += 1
                    Next c
                    i = 0
                    For Each r In dtT.Rows
                        i += 1
                        For j = 0 To dtT.Columns.Count - 1
                            aVal(i, j) = r(j)
                        Next j
                    Next r
                    With .oSheet
                        With .Range("A1")
                            .Resize(dtT.Rows.Count + 1, dtT.Columns.Count).Value = aVal
                            With .CurrentRegion.Resize(1)
                                .Interior.ColorIndex = 15
                                With .Borders(XL.XlBordersIndex.xlInsideVertical)
                                    .Weight = XL.XlBorderWeight.xlThin
                                    .ColorIndex = XL.XlColorIndex.xlColorIndexAutomatic
                                End With
                            End With
                        End With
                        _Col = CP.GetColumnLetter(dtT.Columns.Count)
                        .Range(_Col + ":" + _Col).Cut()
                        With .Range("A:A")
                            .Insert(Shift:=XL.XlInsertShiftDirection.xlShiftToRight)
                            .HorizontalAlignment = XL.XlHAlign.xlHAlignLeft
                        End With
                        .Range("A2").Select()
                    End With
                    .oApp.ActiveWindow.FreezePanes = True
                    frmMain.HideScreen()
                    .Show()
                Catch ex As Exception
                    .Zwolnij()
                    Using New CM
                        MessageBox.Show(ex.Message, "Wklejanie do Excel-a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    End Using
                Finally
                End Try
            End With
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
        Finally
            frmMain.HideScreen(False)
        End Try
    End Sub

    Private Sub Dopisz() Handles tsbNowy.Click

        StatusDgv.Text = CP.Nowy.ToString
        With Me.txtSQL
            .Clear()
            .Focus()
        End With
    End Sub

    Sub Fresh()

        Dim dt As New DataTable
        Dim bs As New BindingSource
        Dim da As OleDbDataAdapter = New OleDbDataAdapter("SELECT ID, Pytanie FROM tblZapytania WHERE Tabela = 'Wieloletnie' ORDER BY ID DESC", MF.cns)
        da.Fill(dt)
        With bs
            .DataSource = dt
            .Sort = "ID"
        End With
        With Me
            With .dgv
                .DataSource = Nothing
                .DataSource = bs
            End With
            With .bn
                .BindingSource = bs
                .Visible = True
            End With
            With .dgv
                .Columns(0).Visible = False
                Try
                    .CurrentCell = .Item(1, 0)
                Catch ex As Exception
                End Try
            End With
        End With
        FreshDetails()
        bs = Nothing
        da = Nothing
        dt = Nothing
    End Sub

    Sub FreshDetails()

        Dim i As Integer
        Try
            Try
                i = CType(dgv.CurrentCell.ColumnIndex, Integer)
            Catch ex As Exception
                i = 1
            End Try
            With Me
                Dim r As DataRow = CType(.bn.BindingSource.Current, DataRowView).Row
                StatusDgv.Text = CP.Wybrany.ToString
                .dgv.CurrentCell = .dgv(i, .bn.BindingSource.Position)
                .tsbPrevious.Enabled = .bn.BindingSource.Position > 0
                .tsbNext.Enabled = .bn.BindingSource.Position < .bn.BindingSource.Count - 1
                .txtID.Text = r("ID").ToString
                .txtSQL.Text = r("Pytanie").ToString.Replace("|", "'")
            End With
        Catch ex As Exception
            StatusDgv.Text = CP.Niewybrany.ToString
            Me.txtSQL.Clear()
        End Try
    End Sub

    Sub Usun() Handles tsbUsun.Click

        Dim tr As OleDbTransaction = Nothing
        Dim cmd As New OleDbCommand
        Dim v As DataRowView = CType(Me.bn.BindingSource.Current, DataRowView)
        Dim _Key As String = v("ID").ToString
        Using New CM
            If MessageBox.Show(String.Format("[{0}]{1}Operacja nieodwracalna.{2}Usunąć?", v("ID").ToString, vbCrLf, vbCrLf), "Usuwanie zapytania", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) _
                .Equals(Windows.Forms.DialogResult.No) Then Return
        End Using
        Using cn As New OleDbConnection(MF.cns.ConnectionString)
            Try
                cn.Open()
                tr = cn.BeginTransaction
                With cmd
                    .Connection = cn
                    .Transaction = tr
                    .CommandText = "DELETE FROM tblZapytania WHERE ID = " + _Key
                    .ExecuteNonQuery()
                End With
                tr.Commit()
                Me.bn.BindingSource.RemoveCurrent()
                FreshDetails()
            Catch ex As Exception
                tr.Rollback()
                Using New CM
                    MessageBox.Show(ex.Message + vbCrLf + ex.StackTrace, "Usuwanie zapytania [" + _Key + "]", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End Using
            End Try
        End Using
        tr = Nothing
        cmd = Nothing
        v = Nothing
    End Sub

    Sub Zapisz() Handles tsbZapisz.Click

        Dim tr As OleDbTransaction = Nothing
        Dim cmd As New OleDbCommand
        Dim m_New As Boolean = StatusDgv.Text.Equals(CP.Nowy.ToString)
        Dim m_ID As String
        Const CO_ACTION As String = "Aktualizacja zapytań"
        Try
            With Me.txtSQL
                If String.IsNullOrEmpty(.Text) OrElse Not .Text.ToUpper.StartsWith("SELECT ") Then _
                        .Focus() : Throw New ArgumentException("Wpisz tekst zapytania")
            End With
            Using cn As New OleDbConnection(MF.cns.ConnectionString)
                Try
                    cn.Open()
                    tr = cn.BeginTransaction
                    With cmd
                        .Connection = cn
                        .Transaction = tr
                        If m_New Then
                            .CommandText = "INSERT INTO tblZapytania (Tabela, Pytanie) " _
                                            + "VALUES ('Wieloletnie', '')"
                            .ExecuteNonQuery()
                            .CommandText = "SELECT TOP 1 ID FROM tblZapytania ORDER BY ID DESC"
                            m_ID = .ExecuteScalar.ToString
                        Else
                            m_ID = Me.txtID.Text
                        End If
                        .CommandText = "UPDATE tblZapytania SET Pytanie = '" + Me.txtSQL.Text.Replace("'", "|") _
                                        + "' WHERE ID = " + m_ID
                        .ExecuteNonQuery()
                    End With
                    tr.Commit()
                    If m_New Then
                        Me.txtID.Text = m_ID
                        Me.bn.BindingSource.AddNew()
                    End If
                    Dim v As DataRowView = CType(Me.bn.BindingSource.Current, DataRowView)
                    v("ID") = CInt(m_ID)
                    v("Pytanie") = Me.txtSQL.Text
                    With Me.bn.BindingSource
                        .EndEdit()
                        If m_New Then .Position = .Find("ID", CInt(Me.txtID.Text))
                    End With
                    StatusDgv.Text = CP.Wybrany.ToString
                    FreshDetails()
                Catch ex As Exception
                    Try
                        tr.Rollback()
                    Catch
                    End Try
                    Throw New ArgumentException(ex.Message)
                End Try
            End Using
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message, CO_ACTION, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
        Finally
            tr = Nothing
            cmd = Nothing
        End Try
    End Sub

    Private Sub Zmien() Handles tsbZmien.Click

        StatusDgv.Text = CP.Zmiana.ToString
    End Sub

    Private Sub BindingNavigator_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) _
                            Handles tsbFirst.Click, _
                                    tsbPrevious.Click, _
                                    tsbNext.Click, _
                                    tsbLast.Click

        Dim tsb As ToolStripButton = CType(sender, ToolStripButton)
        e = Nothing
        Try
            With bn.BindingSource
                .CancelEdit()
                bn.Validate()
                Select Case tsb.Name.Substring(3)
                    Case "First"
                        .MoveFirst()
                    Case "Previous"
                        .MovePrevious()
                    Case "Next"
                        .MoveNext()
                    Case "Last"
                        .MoveLast()
                End Select
            End With
            FreshDetails()
        Catch ex As Exception
        Finally
            tsb = Nothing
        End Try
    End Sub

    Private Sub dgv_Click() Handles dgv.Click

        Try
            Me.bn.BindingSource.Position = CInt(bnPositionItem.Text) - 1
            FreshDetails()
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message, "Odświeżanie danych na podstawie tabeli", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Using
        End Try
    End Sub

    '    Private Sub PokazSkoroszyt()

    '        Try
    '            My.Settings.MainMaximized = MF.WindowState.Equals(FormWindowState.Maximized)
    '            MF.WindowState = FormWindowState.Minimized
    'Poczatek:
    '            With oApp
    '                .Visible = True
    '                .ScreenUpdating = True
    '                While .Visible
    '                End While
    '            End With
    '        Catch ex As Exception
    '            GoTo Poczatek
    '        Finally
    '            XLS_Posprzataj()
    '        End Try
    '    End Sub

    'Private Sub XLS_Posprzataj()

    '    Try
    '        oBook.Close()
    '    Catch ex As Exception
    '    End Try
    '    Try
    '        oApp.Quit()
    '    Catch ex As Exception
    '    End Try
    '    pReleaseObject(oSheet)
    '    pReleaseObject(oBook)
    '    pReleaseObject(oApp)
    '    If My.Settings.MainMaximized Then
    '        MF.WindowState = FormWindowState.Maximized
    '    Else
    '        MF.WindowState = FormWindowState.Normal
    '    End If
    '    Cursor = Cursors.Default
    'End Sub

End Class