﻿
Imports System.Windows.Forms
Imports System.IO
Imports CM = ClHelpFul.CenterMsg
Imports XL = Microsoft.Office.Interop.Excel

Public Class Formuly

    Friend MF As frmMain
    Private m_Target As String
    Private m_Source As String
    Private WybraneArkusze As Boolean

    Private Sub frmFormuly_Load() Handles Me.Load

        MF = frmMain
        With Me
            .chkLacza.Checked = My.Settings.ZF_Lacza
            .txtZmianaZ.Text = My.Settings.ZF_Source
            .txtZmianaNa.Text = My.Settings.ZF_Target
            .txtFile.Text = My.Settings.ZF_Plik
            .btnLista.Visible = False
            .txtZmianaZ.Focus()
        End With
    End Sub

    Private Sub Startuj() Handles btnStart.Click

        With Me
            With .txtZmianaZ
                .Text = .Text.Trim
                If String.IsNullOrEmpty(.Text) Then .Focus() : Exit Sub
                m_Source = .Text
            End With
            With .txtZmianaNa
                .Text = .Text.Trim
                If String.IsNullOrEmpty(.Text) Then .Focus() : Exit Sub
                m_Target = .Text
            End With
            Dim ofd As New OpenFileDialog
            With ofd
                .Title = "Wybierz folder z plikami źródłowymi"
                .Filter = "Pliki Excel (*.xls;*.xls?)|*.xls;*.xls?"
                Try
                    .InitialDirectory = My.Settings.ZF_Plik.Substring(0, My.Settings.ZF_Plik.LastIndexOf("\"))
                Catch ex As Exception
                End Try
                .Multiselect = False
                .ReadOnlyChecked = True
                If Not .ShowDialog().Equals(Windows.Forms.DialogResult.OK) Then _
                    Exit Sub
                Me.txtFile.Text = My.Computer.FileSystem.GetDirectoryInfo(.FileName).FullName
                If Me.chkLacza.Checked Then Me.chkArkusz.Checked = False
                With My.Settings
                    .ZF_Plik = ofd.FileName
                    .ZF_Source = m_Source
                    .ZF_Target = m_Target
                    .ZF_Lacza = Me.chkLacza.Checked
                    .ZF_Arkusz = Me.chkArkusz.Checked
                    .Save()
                End With
            End With
            ofd = Nothing
        End With
        If Me.chkArkusz.Checked Then
            ZmienFormuly(True)
            WybraneArkusze = False
            Me.btnLista.Visible = True
            Exit Sub
        End If
        ZmienFormuly(False)
    End Sub

    Private Sub Lista_Click() Handles btnLista.Click

        WybraneArkusze = True
        Me.btnLista.Visible = False
        ZmienFormuly(True)
    End Sub

    Private Sub Exit_Click() Handles btnExit.Click

        Me.Close()
    End Sub

    Private Sub ZmienFormuly(Optional ByVal CheckSheets As Boolean = False)

        'Dim oBook As XL._Workbook = Nothing
        'Dim oSheets As XL.Sheets
        'Dim oSheet As XL._Worksheet = Nothing
        Dim ac As XL.Range
        Dim strFormula As String = ""
        Dim blnChanged As Boolean = False
        Dim lngLastRow As Integer
        Dim m_Bylo, m_Jest As String
        Const CO_BEZP As String = "Bezpośrednia wymiana łączy"
        Const CO_FORM As String = "Zmiana formuł"
        'oApp = New XL.Application
        With New Cl4Excel.Excel(My.Settings.ZF_Plik)
            Try
                With .oApp
                    .DisplayAlerts = False
                    .Calculation = XL.XlCalculation.xlCalculationManual
                End With
                Try
                    If Not WybraneArkusze AndAlso CheckSheets Then
                        Me.lstArkusze.Items.Clear()
                        For Each oSh As XL.Worksheet In .oBook.Worksheets
                            If oSh.Visible = XL.XlSheetVisibility.xlSheetVisible Then
                                Me.lstArkusze.Items.Add(oSh.Name)
                            End If
                        Next oSh
                        Exit Try
                    End If
                    If Me.chkLacza.Checked Then
                        Dim aLinks As Array
                        Dim strLink As Object
                        Try
                            aLinks = CType(.oApp.ActiveWorkbook.LinkSources(Type:=XL.XlLinkType.xlLinkTypeExcelLinks), Array)
                            If aLinks Is Nothing Then
                                Using New CM
                                    MessageBox.Show("Plik [" + .oBook.Name + "] nie zawiera łączy", CO_BEZP, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                End Using
                                Exit Try
                            End If
                            FillStatusBar(aLinks.GetUpperBound(0), CO_BEZP + "...")
                            Me.txtArkusz.Clear()
                            For Each strLink In aLinks
                                m_Bylo = strLink.ToString
                                If Not m_Bylo.ToUpper.Contains(m_Source.ToUpper) Then GoTo Kolejne
                                m_Jest = m_Bylo.ToUpper.Replace(m_Source.ToUpper, m_Target)
                                If m_Bylo.ToUpper.Equals(m_Jest.ToUpper) Then GoTo Kolejne
                                .oBook.ChangeLink(Name:=m_Bylo, NewName:=m_Jest, _
                                        Type:=XL.XlLinkType.xlLinkTypeExcelLinks)
Kolejne:
                                With MF.tspbMain
                                    .Value += 1
                                    If .Value > .Maximum Then .Value = .Maximum
                                End With
                                Application.DoEvents()
                            Next strLink
                            blnChanged = True
                            Using New CM
                                MessageBox.Show("Wymiana łączy zakończona pomyślnie", CO_BEZP, MessageBoxButtons.OK, MessageBoxIcon.Information)
                            End Using
                        Catch ex As Exception
                            blnChanged = False
                            Throw New ArgumentException(ex.Message)
                        End Try
                        Exit Try
                    End If
                    If Me.chkArkusz.Checked Then
                        If Me.lstArkusze.SelectedIndex.Equals(-1) Then Exit Try
                        Dim m_item As String
                        For Each m_item In Me.lstArkusze.SelectedItems
                            .oSheet = CType(.oBook.Sheets(m_item), XL.Worksheet)
                            lngLastRow = .LastRow
                            FillStatusBar(CO_FORM + "...", lngLastRow)
                            lngLastRow = 1
                            Me.txtArkusz.Text = .oSheet.Name
                            For Each ac In .oSheet.Cells.SpecialCells(XL.XlCellType.xlCellTypeFormulas)
                                If ac.Row > lngLastRow Then
                                    lngLastRow = ac.Row
                                    MF.tspbMain.Value = lngLastRow
                                    Application.DoEvents()
                                End If
                                If String.IsNullOrEmpty(ac.Address) Then GoTo Nastepny2
                                Try
                                    strFormula = ac.Formula.ToString.ToUpper.Replace("\\S216-0005\COMUN", "W:")
                                    If strFormula.ToString.ToUpper.Contains(m_Source.ToUpper) Then
                                        strFormula = strFormula.ToString.ToUpper.Replace(m_Source.ToUpper, m_Target)
                                        ac.Formula = strFormula.ToString
                                        blnChanged = True
                                    End If
                                Catch ex As Exception
                                    Me.txtWymiana.Text += String.Concat(.oSheet.Name, Chr(9), ac.Address, vbCrLf)
                                End Try
Nastepny2:
                            Next ac
                            ClearStatus()
                        Next m_item
                        Using New CM
                            MessageBox.Show("Zmiana formuł wybranych arkuszy zakończona pomyślnie", CO_FORM, MessageBoxButtons.OK, MessageBoxIcon.Information)
                        End Using
                        Exit Try
                    End If
                    For Each oSh As XL.Worksheet In .oBook.Worksheets
                        If Not oSh.Visible = XL.XlSheetVisibility.xlSheetVisible Then GoTo NastepnyArkusz
                        lngLastRow = .LastRow(oSh.Name)
                        FillStatusBar(lngLastRow)
                        lngLastRow = 1
                        Me.txtArkusz.Text = oSh.Name
                        For Each ac In oSh.Cells.SpecialCells(XL.XlCellType.xlCellTypeFormulas)
                            Try
                                If ac.Row > lngLastRow Then
                                    lngLastRow = ac.Row
                                    MF.tspbMain.Value = lngLastRow
                                    Application.DoEvents()
                                End If
                                If String.IsNullOrEmpty(ac.Address) Then GoTo Nastepny
                                strFormula = ac.Formula.ToString.ToUpper.Replace("\\S216-0005\COMUN", "W:")
                                If strFormula.ToUpper.Contains(m_Source.ToUpper) Then
                                    strFormula = strFormula.ToUpper.Replace(m_Source.ToUpper, m_Target)
                                    ac.Formula = strFormula.ToString
                                    blnChanged = True
                                End If
                            Catch ex As Exception
                                Me.txtWymiana.Text += String.Concat(oSh.Name, Chr(9), ac.Address, vbCrLf)
                            End Try
Nastepny:
                        Next ac
                        ClearStatus()
NastepnyArkusz:
                    Next oSh
                    Using New CM
                        MessageBox.Show("Zmiana formuł zakończona pomyślnie", CO_FORM, MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End Using
                Catch ex As Exception
                    Try
                        .oBook.Saved = True
                    Catch
                    End Try
                End Try
            Catch ex As Exception
                ClearStatus()
                Using New CM
                    MessageBox.Show(ex.Message, My.Settings.ZF_Plik, MessageBoxButtons.OK, MessageBoxIcon.Error)
                End Using
                blnChanged = False
            Finally
                ClearStatus()
                Try
                    .oApp.Calculation = XL.XlCalculation.xlCalculationAutomatic
                    If blnChanged Then .oBook.Save()
                Catch
                End Try
                Try
                    .Zwolnij()
                Catch
                End Try
                If Not Me.txtWymiana.Text.Length.Equals(0) Then
                    Dim sw As StreamWriter
                    Dim Plik As String = Me.txtFile.Text.Substring(0, Me.txtFile.Text.LastIndexOf(Path.DirectorySeparatorChar)) & "LogFormula.txt"
                    sw = New StreamWriter(Plik, False)
                    With sw
                        .Write(Me.txtWymiana.Text)
                        .Close()
                    End With
                    sw = Nothing
                    Using New CM
                        MessageBox.Show("Sprawdź zapisy w pliku " + Plik, CO_FORM, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    End Using
                End If
            End Try
        End With
    End Sub

End Class
