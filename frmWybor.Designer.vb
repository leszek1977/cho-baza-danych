﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmWybor
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmWybor))
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.dtpOd = New System.Windows.Forms.DateTimePicker
        Me.dtpDo = New System.Windows.Forms.DateTimePicker
        Me.cmbGrMat = New System.Windows.Forms.ComboBox
        Me.cmbGrKalk = New System.Windows.Forms.ComboBox
        Me.cmbMaterial = New System.Windows.Forms.ComboBox
        Me.cmbKlient = New System.Windows.Forms.ComboBox
        Me.btnOk = New System.Windows.Forms.Button
        Me.txtZlecenie = New System.Windows.Forms.TextBox
        Me.chkDate = New System.Windows.Forms.CheckBox
        Me.chkGM = New System.Windows.Forms.CheckBox
        Me.chkGK = New System.Windows.Forms.CheckBox
        Me.chkMat = New System.Windows.Forms.CheckBox
        Me.chkKl = New System.Windows.Forms.CheckBox
        Me.chkZl = New System.Windows.Forms.CheckBox
        Me.chkMiesieczne = New System.Windows.Forms.CheckBox
        Me.TableLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 5
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 202.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 22.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 34.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.Label1, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Label2, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.Label3, 3, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Label4, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.Label5, 0, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.Label6, 0, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.Label7, 0, 6)
        Me.TableLayoutPanel1.Controls.Add(Me.dtpOd, 2, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.dtpDo, 4, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.cmbGrMat, 2, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.cmbGrKalk, 2, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.cmbMaterial, 2, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.cmbKlient, 2, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.btnOk, 4, 7)
        Me.TableLayoutPanel1.Controls.Add(Me.txtZlecenie, 2, 6)
        Me.TableLayoutPanel1.Controls.Add(Me.chkDate, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.chkGM, 1, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.chkGK, 1, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.chkMat, 1, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.chkKl, 1, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.chkZl, 1, 6)
        Me.TableLayoutPanel1.Controls.Add(Me.chkMiesieczne, 1, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(1, 2)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 8
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(655, 258)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(3, 38)
        Me.Label1.Margin = New System.Windows.Forms.Padding(3, 7, 3, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Padding = New System.Windows.Forms.Padding(0, 3, 0, 0)
        Me.Label1.Size = New System.Drawing.Size(196, 24)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Okres: od"
        '
        'Label2
        '
        Me.Label2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(3, 69)
        Me.Label2.Margin = New System.Windows.Forms.Padding(3, 7, 3, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Padding = New System.Windows.Forms.Padding(0, 3, 0, 0)
        Me.Label2.Size = New System.Drawing.Size(196, 24)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Grupa materiałowa"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(425, 38)
        Me.Label3.Margin = New System.Windows.Forms.Padding(3, 7, 3, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(27, 18)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "do"
        '
        'Label4
        '
        Me.Label4.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(3, 100)
        Me.Label4.Margin = New System.Windows.Forms.Padding(3, 7, 3, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Padding = New System.Windows.Forms.Padding(0, 3, 0, 0)
        Me.Label4.Size = New System.Drawing.Size(196, 24)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Grupa kalkulacyjna"
        '
        'Label5
        '
        Me.Label5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(3, 131)
        Me.Label5.Margin = New System.Windows.Forms.Padding(3, 7, 3, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Padding = New System.Windows.Forms.Padding(0, 3, 0, 0)
        Me.Label5.Size = New System.Drawing.Size(196, 24)
        Me.Label5.TabIndex = 5
        Me.Label5.Text = "Materiał"
        '
        'Label6
        '
        Me.Label6.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(3, 162)
        Me.Label6.Margin = New System.Windows.Forms.Padding(3, 7, 3, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Padding = New System.Windows.Forms.Padding(0, 3, 0, 0)
        Me.Label6.Size = New System.Drawing.Size(196, 24)
        Me.Label6.TabIndex = 6
        Me.Label6.Text = "Klient"
        '
        'Label7
        '
        Me.Label7.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(3, 193)
        Me.Label7.Margin = New System.Windows.Forms.Padding(3, 7, 3, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Padding = New System.Windows.Forms.Padding(0, 3, 0, 0)
        Me.Label7.Size = New System.Drawing.Size(196, 24)
        Me.Label7.TabIndex = 7
        Me.Label7.Text = "Zlecenie"
        '
        'dtpOd
        '
        Me.dtpOd.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dtpOd.CustomFormat = " MMMM"
        Me.dtpOd.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.dtpOd.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpOd.Location = New System.Drawing.Point(227, 34)
        Me.dtpOd.Name = "dtpOd"
        Me.dtpOd.Size = New System.Drawing.Size(192, 26)
        Me.dtpOd.TabIndex = 9
        '
        'dtpDo
        '
        Me.dtpDo.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dtpDo.CustomFormat = " MMMM"
        Me.dtpDo.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.dtpDo.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpDo.Location = New System.Drawing.Point(459, 34)
        Me.dtpDo.Name = "dtpDo"
        Me.dtpDo.Size = New System.Drawing.Size(193, 26)
        Me.dtpDo.TabIndex = 10
        '
        'cmbGrMat
        '
        Me.cmbGrMat.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.SetColumnSpan(Me.cmbGrMat, 3)
        Me.cmbGrMat.FormattingEnabled = True
        Me.cmbGrMat.Location = New System.Drawing.Point(227, 65)
        Me.cmbGrMat.Name = "cmbGrMat"
        Me.cmbGrMat.Size = New System.Drawing.Size(425, 26)
        Me.cmbGrMat.TabIndex = 11
        '
        'cmbGrKalk
        '
        Me.cmbGrKalk.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.SetColumnSpan(Me.cmbGrKalk, 3)
        Me.cmbGrKalk.FormattingEnabled = True
        Me.cmbGrKalk.Location = New System.Drawing.Point(227, 96)
        Me.cmbGrKalk.Name = "cmbGrKalk"
        Me.cmbGrKalk.Size = New System.Drawing.Size(425, 26)
        Me.cmbGrKalk.TabIndex = 12
        '
        'cmbMaterial
        '
        Me.cmbMaterial.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.SetColumnSpan(Me.cmbMaterial, 3)
        Me.cmbMaterial.FormattingEnabled = True
        Me.cmbMaterial.Location = New System.Drawing.Point(227, 127)
        Me.cmbMaterial.Name = "cmbMaterial"
        Me.cmbMaterial.Size = New System.Drawing.Size(425, 26)
        Me.cmbMaterial.TabIndex = 13
        '
        'cmbKlient
        '
        Me.cmbKlient.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.SetColumnSpan(Me.cmbKlient, 3)
        Me.cmbKlient.FormattingEnabled = True
        Me.cmbKlient.Location = New System.Drawing.Point(227, 158)
        Me.cmbKlient.Name = "cmbKlient"
        Me.cmbKlient.Size = New System.Drawing.Size(425, 26)
        Me.cmbKlient.TabIndex = 14
        '
        'btnOk
        '
        Me.btnOk.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.btnOk.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.btnOk.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnOk.Location = New System.Drawing.Point(513, 220)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.Size = New System.Drawing.Size(84, 30)
        Me.btnOk.TabIndex = 0
        Me.btnOk.Text = "O&k"
        Me.btnOk.UseVisualStyleBackColor = True
        '
        'txtZlecenie
        '
        Me.txtZlecenie.Location = New System.Drawing.Point(227, 189)
        Me.txtZlecenie.Name = "txtZlecenie"
        Me.txtZlecenie.Size = New System.Drawing.Size(138, 26)
        Me.txtZlecenie.TabIndex = 15
        '
        'chkDate
        '
        Me.chkDate.AutoSize = True
        Me.chkDate.Location = New System.Drawing.Point(205, 37)
        Me.chkDate.Margin = New System.Windows.Forms.Padding(3, 6, 3, 3)
        Me.chkDate.Name = "chkDate"
        Me.chkDate.Size = New System.Drawing.Size(16, 22)
        Me.chkDate.TabIndex = 16
        Me.chkDate.Text = "CheckBox1"
        Me.chkDate.UseVisualStyleBackColor = True
        '
        'chkGM
        '
        Me.chkGM.AutoSize = True
        Me.chkGM.Location = New System.Drawing.Point(205, 68)
        Me.chkGM.Margin = New System.Windows.Forms.Padding(3, 6, 3, 3)
        Me.chkGM.Name = "chkGM"
        Me.chkGM.Size = New System.Drawing.Size(16, 22)
        Me.chkGM.TabIndex = 17
        Me.chkGM.Text = "CheckBox2"
        Me.chkGM.UseVisualStyleBackColor = True
        '
        'chkGK
        '
        Me.chkGK.AutoSize = True
        Me.chkGK.Location = New System.Drawing.Point(205, 99)
        Me.chkGK.Margin = New System.Windows.Forms.Padding(3, 6, 3, 3)
        Me.chkGK.Name = "chkGK"
        Me.chkGK.Size = New System.Drawing.Size(16, 22)
        Me.chkGK.TabIndex = 18
        Me.chkGK.Text = "CheckBox3"
        Me.chkGK.UseVisualStyleBackColor = True
        '
        'chkMat
        '
        Me.chkMat.AutoSize = True
        Me.chkMat.Location = New System.Drawing.Point(205, 130)
        Me.chkMat.Margin = New System.Windows.Forms.Padding(3, 6, 3, 3)
        Me.chkMat.Name = "chkMat"
        Me.chkMat.Size = New System.Drawing.Size(16, 22)
        Me.chkMat.TabIndex = 19
        Me.chkMat.Text = "CheckBox4"
        Me.chkMat.UseVisualStyleBackColor = True
        '
        'chkKl
        '
        Me.chkKl.AutoSize = True
        Me.chkKl.Location = New System.Drawing.Point(205, 161)
        Me.chkKl.Margin = New System.Windows.Forms.Padding(3, 6, 3, 3)
        Me.chkKl.Name = "chkKl"
        Me.chkKl.Size = New System.Drawing.Size(16, 22)
        Me.chkKl.TabIndex = 20
        Me.chkKl.Text = "CheckBox5"
        Me.chkKl.UseVisualStyleBackColor = True
        '
        'chkZl
        '
        Me.chkZl.AutoSize = True
        Me.chkZl.Location = New System.Drawing.Point(205, 192)
        Me.chkZl.Margin = New System.Windows.Forms.Padding(3, 6, 3, 3)
        Me.chkZl.Name = "chkZl"
        Me.chkZl.Size = New System.Drawing.Size(16, 22)
        Me.chkZl.TabIndex = 21
        Me.chkZl.Text = "CheckBox6"
        Me.chkZl.UseVisualStyleBackColor = True
        '
        'chkMiesieczne
        '
        Me.chkMiesieczne.AutoSize = True
        Me.TableLayoutPanel1.SetColumnSpan(Me.chkMiesieczne, 2)
        Me.chkMiesieczne.Location = New System.Drawing.Point(205, 3)
        Me.chkMiesieczne.Name = "chkMiesieczne"
        Me.chkMiesieczne.Size = New System.Drawing.Size(79, 22)
        Me.chkMiesieczne.TabIndex = 22
        Me.chkMiesieczne.Text = "Ogólne"
        Me.chkMiesieczne.UseVisualStyleBackColor = True
        '
        'frmWybor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 18.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(651, 264)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(555, 291)
        Me.Name = "frmWybor"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Parametry wyboru"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents dtpOd As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpDo As System.Windows.Forms.DateTimePicker
    Friend WithEvents cmbGrMat As System.Windows.Forms.ComboBox
    Friend WithEvents cmbGrKalk As System.Windows.Forms.ComboBox
    Friend WithEvents cmbMaterial As System.Windows.Forms.ComboBox
    Friend WithEvents cmbKlient As System.Windows.Forms.ComboBox
    Friend WithEvents btnOk As System.Windows.Forms.Button
    Friend WithEvents txtZlecenie As System.Windows.Forms.TextBox
    Friend WithEvents chkDate As System.Windows.Forms.CheckBox
    Friend WithEvents chkGM As System.Windows.Forms.CheckBox
    Friend WithEvents chkGK As System.Windows.Forms.CheckBox
    Friend WithEvents chkMat As System.Windows.Forms.CheckBox
    Friend WithEvents chkKl As System.Windows.Forms.CheckBox
    Friend WithEvents chkZl As System.Windows.Forms.CheckBox
    Friend WithEvents chkMiesieczne As System.Windows.Forms.CheckBox
End Class
