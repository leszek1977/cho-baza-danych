﻿
#Region "Opcje i referencje"

Option Explicit On
Option Strict On
Option Infer Off

Imports System.ComponentModel
Imports System.Data
Imports System.Data.OleDb
Imports System.Text
Imports XL = Microsoft.Office.Interop.Excel

#End Region ' Opcje i referencje

Public Class frmSQL

#Region "Deklaracje"

    Private MF As frmMain
    Private DF As frmDane
    Private m_Nowe As Boolean
    Private m_SQL As String

    Public Property Zapytanie() As String
        Get
            Return m_SQL
        End Get
        Set(ByVal value As String)
            m_SQL = value
        End Set
    End Property

#End Region ' Deklaracje

    Private Sub frmSQL_Load() Handles Me.Load

        Try

            MF = frmMain
            DF = frmDane

            SetLista()

        Catch ex As Exception

        End Try

        With Me
            .KeyPreview = True
            .Text += MF.WybranaTabela.Replace("tbl", "")
            .Size = My.Settings.scrZapytania
        End With

    End Sub

    Private Sub SetLista(Optional ByVal ZostawWyniki As Boolean = False)
        Dim da As OleDbDataAdapter
        Dim dt As New DataTable
        Dim i As Integer

        Try

            da = New OleDbDataAdapter("SELECT Pytanie FROM tblZapytania WHERE Tabela = '" + MF.WybranaTabela + "' ORDER BY ID DESC", MF.cns)
            da.Fill(dt)

            For i = 0 To dt.Rows.Count - 1
                dt.Rows(i).Item("Pytanie") = dt.Rows(i).Item("Pytanie").ToString.Replace("|", "'")
            Next i
            With Me
                .dgvLista.DataSource = dt
                If Not ZostawWyniki Then _
                    .dgvWyniki.DataSource = Nothing
            End With

        Catch ex As Exception
        Finally

            With Me
                If Not ZostawWyniki Then
                    .btnWykonaj.Enabled = False
                    .btnXls.Enabled = False
                    .txtSQL.Text = ""
                End If
            End With

            da = Nothing
        End Try


    End Sub

    Private Sub Wykonaj() Handles btnWykonaj.Click
        Dim da As OleDbDataAdapter = New OleDbDataAdapter(Me.txtSQL.Text, MF.cns)
        Dim dtS As New DataTable

        Try
            da.Fill(dtS)

            With Me.dgvWyniki
                .DataSource = Nothing
                .DataSource = dtS
            End With

            Me.btnXls.Enabled = dtS.Rows.Count > 0

            If m_Nowe Then
                m_Nowe = False
                ' zapis zapytania
                Dim cmd As New OleDbCommand
                Dim tr As OleDbTransaction = Nothing

                Using cn As New OleDbConnection(MF.cns.ConnectionString)

                    Try

                        cn.Open()
                        tr = cn.BeginTransaction
                        cmd.Connection = cn
                        cmd.Transaction = tr

                        With cmd
                            .CommandText = "INSERT INTO tblZapytania (Tabela, Pytanie) VALUES ('" + MF.WybranaTabela + "', '" + Me.txtSQL.Text.Replace("'", "|") + "')"
                            .ExecuteNonQuery()
                        End With

                        tr.Commit()

                    Catch ex As Exception
                        tr.Rollback()
                        MessageBox.Show(ex.Message, "Zapis zapytania", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    End Try

                End Using

                SetLista(True)

            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub NoweZapytanie() Handles btnNew.Click
        With Me
            .txtSQL.Text = m_SQL
            .btnWykonaj.Enabled = True
        End With
        m_Nowe = True

    End Sub

    Private Sub dgvLista_Click() Handles dgvLista.Click

        With Me
            .txtSQL.Text = dgvLista.CurrentRow.Cells(0).Value.ToString
            .btnWykonaj.Enabled = .txtSQL.Text.IndexOf("SELECT") > -1
        End With

    End Sub

    Private Sub DoExcela() Handles btnXls.Click
        Dim oApp As New XL.Application
        Dim oBook As XL._Workbook = Nothing
        Dim oSheet As XL._Worksheet = Nothing
        Dim i, j As Integer
        Dim dc As DataGridViewColumn
        Dim dr As DataRow
        Dim bs As New BindingSource
        Dim aVal(Me.dgvWyniki.Rows.Count, Me.dgvWyniki.Columns.Count - 1) As Object
        Me.Cursor = Cursors.WaitCursor

        Try

            bs.DataSource = Me.dgvWyniki.DataSource
            oBook = oApp.Workbooks.Add()
            oSheet = CType(oBook.Sheets(1), XL._Worksheet)
            oApp.ScreenUpdating = False

            For Each dc In Me.dgvWyniki.Columns
                aVal(0, j) = dc.HeaderText
                j += 1
            Next dc
            For i = 1 To bs.Count
                dr = CType(bs.Item(i - 1), DataRowView).Row
                For j = 0 To Me.dgvWyniki.Columns.Count - 1
                    aVal(i, j) = dr.Item(j).ToString
                Next
            Next

            oSheet.Range("A1").Resize(Me.dgvWyniki.Rows.Count + 1, Me.dgvWyniki.Columns.Count).Value = aVal

            With Me
                .WindowState = FormWindowState.Minimized
                .Cursor = Cursors.Default
            End With

            With oApp
                .Visible = True
                .ScreenUpdating = True

                While .Visible
                End While

            End With

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Wklejanie do Excel-a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Try
                oBook.Saved = True
            Catch
            End Try

        Finally

            Try
                oBook.Close()
                oApp.Quit()
                fReleaseObject(oApp)
                fReleaseObject(oBook)
                fReleaseObject(oSheet)

            Catch
            End Try

            With Me
                .WindowState = FormWindowState.Minimized
                .Cursor = Cursors.Default
            End With
            dr = Nothing
            bs = Nothing
        End Try

    End Sub

    Private Sub frmSQL_ResizeEnd() Handles Me.ResizeEnd
        My.Settings.scrZapytania = Me.Size
    End Sub

End Class